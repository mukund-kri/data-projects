use serde::Deserialize;
use std::error::Error;

/// Each company is a data point
#[derive(Debug, Deserialize)]
pub struct Company {
    #[serde(rename = "AUTHORIZED_CAP")]
    pub authorized_cap: f64,

    #[serde(rename = "DATE_OF_REGISTRATION")]
    pub date_of_registration: String,

    #[serde(rename = "PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN")]
    pub pba: String,
}

pub fn load_data(file_name: &str) -> Result<Vec<Company>, Box<dyn Error>> {
    let mut companies = Vec::new();

    // Read the csv in and collect the companies
    let mut rdr = csv::Reader::from_path(file_name)?;
    for result in rdr.deserialize() {
        let company: Company = result?;
        companies.push(company);
    }

    Ok(companies)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_load_data() {
        let data = load_data("../data/mca_westbengal_min.csv").unwrap();
        assert_eq!(data.len(), 100);
    }
}
