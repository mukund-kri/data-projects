mod analyze;
mod data;

pub use analyze::{problem1, problem2, problem3, problem4};
pub use data::{load_data, Company};
