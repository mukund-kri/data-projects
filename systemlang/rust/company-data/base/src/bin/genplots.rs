// Generate plots for the data in the data directory
#![allow(dead_code)]

// imports
use std::error::Error;

use base::problem3;

fn main() -> Result<(), Box<dyn Error>> {
    let data = base::load_data("../data/mca_westbengal_min.csv")?;
    let companies = problem3(&data, 200, 2015);
    println!("{:?}", companies);

    Ok(())
}
