use indexmap::IndexMap;
use std::error::Error;

use crate::data::Company;

// update count
fn update_count(count: &mut IndexMap<String, usize>, key: String) {
    let count = count.entry(key).or_insert(0);
    *count += 1;
}

// extract year from date
fn extract_year(date: &str) -> Result<i32, Box<dyn Error>> {
    let date_parts: Vec<&str> = date.split('-').collect();
    if date_parts.len() != 3 {
        return Err("Invalid date format".into());
    }
    let year = date_parts[2].parse::<i32>()?;
    Ok(year)
}

/// Problem 1
/// Counts of companies by Authorized Capital
///
/// # Arguments
/// * `companies` - A vector of companies
pub fn problem1(data: &Vec<Company>) -> IndexMap<String, usize> {
    // Initialize the authorized capital count so that the order is maintained
    let mut authorized_capital_count = IndexMap::from([
        ("0-1L".to_string(), 0),
        ("1L-10L".to_string(), 0),
        ("10L-1Cr".to_string(), 0),
        ("1Cr-10Cr".to_string(), 0),
        (">10Cr".to_string(), 0),
    ]);

    for company in data {
        match company.authorized_cap {
            x if x <= 1_00_000.0 => update_count(&mut authorized_capital_count, "0-1L".to_string()),
            x if x <= 10_00_000.0 => {
                update_count(&mut authorized_capital_count, "1L-10L".to_string())
            }
            x if x <= 1_00_00_000.0 => {
                update_count(&mut authorized_capital_count, "10L-1Cr".to_string())
            }
            x if x <= 10_00_00_000.0 => {
                update_count(&mut authorized_capital_count, "1Cr-10Cr".to_string())
            }
            _ => update_count(&mut authorized_capital_count, ">10Cr".to_string()),
        }
    }

    authorized_capital_count
}

/// Problem 2
/// Counts of companies by year of incorporation
///
/// # Arguments
/// * `companies` - A vector of companies
/// * `year` - The year to filter by
pub fn problem2(data: &Vec<Company>, start_year: i32, end_year: i32) -> IndexMap<String, usize> {
    let mut year_count = IndexMap::new();

    for company in data {
        let year_of_incorporation = extract_year(&company.date_of_registration);
        match year_of_incorporation {
            Ok(year) => {
                if year >= start_year && year <= end_year {
                    update_count(&mut year_count, year.to_string());
                }
            }
            Err(_) => continue,
        }
    }

    year_count
}

// Problem 3
// Counts of companies by Principal Business Activity
//
// # Arguments
// * `companies` - A vector of companies
// * `start_year` - The start year to filter by
// * `end_year` - The end year to filter by
pub fn problem3(data: &Vec<Company>, start_year: i32, end_year: i32) -> IndexMap<String, usize> {
    let mut pba_count = IndexMap::new();

    for company in data {
        let year_of_incorporation = extract_year(&company.date_of_registration);
        let year_of_incorporation = match year_of_incorporation {
            Ok(year) => year,
            Err(_) => continue,
        };

        if year_of_incorporation >= start_year && year_of_incorporation <= end_year {
            update_count(&mut pba_count, company.pba.clone());
        }
    }

    pba_count
}

// Problem 4
// Counts of companies by Principal Business Activity by year
//
// # Arguments
// * `companies` - A vector of companies
// * `start_year` - The start year to filter by
// * `end_year` - The end year to filter by
pub fn problem4(
    data: &Vec<Company>,
    start_year: i32,
    end_year: i32,
) -> IndexMap<String, IndexMap<String, usize>> {
    let mut pba_year_count = IndexMap::new();

    for company in data {
        let year_of_incorporation = extract_year(&company.date_of_registration);
        let year_of_incorporation = match year_of_incorporation {
            Ok(year) => year,
            Err(_) => continue,
        };
        if year_of_incorporation >= start_year && year_of_incorporation <= end_year {
            let year_count = pba_year_count
                .entry(company.pba.clone())
                .or_insert(IndexMap::new());
            update_count(year_count, year_of_incorporation.to_string());
        }
    }

    pba_year_count
}
