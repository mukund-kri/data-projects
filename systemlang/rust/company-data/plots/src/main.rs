mod gnuplot;
mod highcharts;

use std::error::Error;

use console::Term;
use dialoguer::{theme::ColorfulTheme, Select};

use base::load_data;

use crate::gnuplot::{all_plots, plot_problem1, plot_problem2, plot_problem3, plot_problem4};

fn main() -> Result<(), Box<dyn Error>> {
    // The plotting system to use
    let plotting_system_set = &["Gnuplot", "Highcharts"];

    // Options for command line arguments
    let problem_set = &[
        "All Problems",
        "Problem 1",
        "Problem 2",
        "Problem 3",
        "Problem 4",
    ];

    // Start and End year for the analysis
    let years = (2000..2018)
        .map(|year| year.to_string())
        .collect::<Vec<String>>();

    // Setup term and theme
    let term = Term::buffered_stderr();
    let theme = ColorfulTheme::default();

    let plotting_system_selection = Select::with_theme(&theme)
        .with_prompt("Select a plotting system")
        .items(plotting_system_set)
        .default(0)
        .interact_on(&term)?;

    let problem_selection = Select::with_theme(&theme)
        .with_prompt("Select a problem to solve")
        .items(problem_set)
        .default(0)
        .interact_on(&term)?;

    let start_year_selection = Select::with_theme(&theme)
        .with_prompt("Select the start year")
        .items(&years)
        .default(0)
        .interact_on(&term)?;

    let end_year_selection = Select::with_theme(&theme)
        .with_prompt("Select the end year")
        .items(&years)
        .default(0)
        .interact_on(&term)?;

    let start_year: i32 = start_year_selection as i32 + 2000;
    let end_year: i32 = end_year_selection as i32 + 2000;

    println!(
        "Start year: {} End year: {}",
        start_year_selection, end_year_selection
    );

    // Load only the required data from the CSV file
    let data = load_data("../data/mca_clean.csv")?;

    // Plot the data using gnuplot
    match plotting_system_selection {
        0 => {
            plot_gnuplot(data, problem_selection, start_year, end_year)?;
            println!("Data files written to 'gnuplots/' directory");
        }
        1 => {
            plot_highcharts(data, problem_selection, start_year, end_year)?;
            println!("Data files written to 'highcharts/' directory");
        }
        _ => {
            println!("Invalid selection");
        }
    };

    Ok(())
}

fn plot_gnuplot(
    data: Vec<base::Company>,
    problem_selection: usize,
    start_year: i32,
    end_year: i32,
) -> Result<(), Box<dyn Error>> {
    // Run the analysis based on the selection
    match problem_selection {
        0 => {
            // Run all the problems
            all_plots(&data, start_year, end_year);
        }
        1 => {
            // First problem
            plot_problem1(&data);
        }
        2 => {
            // Second problem
            plot_problem2(&data, start_year, end_year);
        }
        3 => {
            // Third problem
            plot_problem3(&data, start_year, end_year);
        }
        4 => {
            // Fourth problem
            plot_problem4(&data, start_year, end_year);
        }
        _ => {
            println!("Invalid selection");
        }
    };
    Ok(())
}

// Highcharts
fn plot_highcharts(
    data: Vec<base::Company>,
    problem_selection: usize,
    start_year: i32,
    end_year: i32,
) -> Result<(), Box<dyn Error>> {
    match problem_selection {
        0 => {
            // Run all the problems
            highcharts::plot_all(&data, start_year, end_year)
        }
        1 => {
            // First problem
            highcharts::plot_problem1(&data)
        }
        2 => {
            // Second problem
            highcharts::plot_problem2(&data, start_year, end_year)
        }
        3 => {
            // Third problem
            highcharts::plot_problem3(&data, start_year, end_year)
        }
        4 => {
            // Fourth problem
            highcharts::plot_problem4(&data, start_year, end_year)
        }
        _ => {
            println!("Invalid selection");
            Ok(())
        }
    }
}
