use serde::{Deserialize, Serialize};
use std::{error::Error, result::Result};

use base::{problem1, problem2, problem3, problem4, Company};

/// This struct is used to serialize the data into a format that Highcharts can understand
#[derive(Serialize, Deserialize)]
struct TwoDimensionalPlot {
    categories: Vec<String>,
    data: Vec<usize>,
}

/// This struct is used to serialize the data into a format for 2d stacked plots that Highcharts
/// can understand
#[derive(Serialize, Deserialize)]
struct DataElement {
    name: String,
    data: Vec<usize>,
}

#[derive(Serialize, Deserialize)]
struct GroupedTwoDimensionalPlot {
    categories: Vec<String>,
    series: Vec<DataElement>,
}

/// This function generates a Highcharts compatible JSON file for problem 1; distribution of
/// authorized capital.
///
/// # Arguments
///
/// * `raw_data` - The data to plot
///
/// # Returns
///
/// * `Result<(), Box<dyn Error>>` - Mainly to handle Serde / IO errors
pub fn plot_problem1(raw_data: &Vec<Company>) -> Result<(), Box<dyn Error>> {
    let authorized_capital = problem1(raw_data);

    // Pack it into a format that Highcharts can understand
    let plot_data = TwoDimensionalPlot {
        categories: authorized_capital.keys().cloned().collect(),
        data: authorized_capital.values().cloned().collect(),
    };

    // Serialize to JSON
    let json_str = serde_json::to_string(&plot_data)?;

    // Dump JSON to file
    std::fs::write("highcharts/problem1.json", json_str)?;

    Ok(())
}

/// This function generates a Highcharts compatible JSON file for problem 2; distribution of
/// company count by year.
///
/// # Arguments
///
/// * `raw_data` - The data to plot
/// * `start_year` - The start year for the analysis
/// * `end_year` - The end year for the analysis
///
/// # Returns
///
/// * `Result<(), Box<dyn Error>>` - Mainly to handle Serde / IO errors
pub fn plot_problem2(
    raw_data: &Vec<Company>,
    start_year: i32,
    end_year: i32,
) -> Result<(), Box<dyn Error>> {
    // Analyze the data
    let company_registration = problem2(raw_data, start_year, end_year);

    // Pack it into a format that Highcharts can understand
    let plot_data = TwoDimensionalPlot {
        categories: company_registration.keys().cloned().collect(),
        data: company_registration.values().cloned().collect(),
    };

    // Serialize to JSON and dump to file
    let json_str = serde_json::to_string(&plot_data)?;
    std::fs::write("highcharts/problem2.json", json_str)?;

    Ok(())
}

/// This function generates a Highcharts compatible JSON file for problem 3; distribution of
/// company count by Principal Business Activity, over a year range.
///
/// # Arguments
///
/// * `raw_data` - The data to plot
/// * `start_year` - The start year for the analysis
/// * `end_year` - The end year for the analysis
///
/// # Returns
///
/// * `Result<(), Box<dyn Error>>` - Mainly to handle Serde / IO errors
pub fn plot_problem3(
    raw_data: &Vec<Company>,
    start_year: i32,
    end_year: i32,
) -> Result<(), Box<dyn Error>> {
    // Analyze the data
    let company_activity = problem3(raw_data, start_year, end_year);

    // Pack it into a format that Highcharts can understand
    let plot_data = TwoDimensionalPlot {
        categories: company_activity.keys().cloned().collect(),
        data: company_activity.values().cloned().collect(),
    };

    // Serialize to JSON and dump to file
    let json_str = serde_json::to_string(&plot_data)?;
    std::fs::write("highcharts/problem3.json", json_str)?;

    Ok(())
}

/// This function generates a Highcharts compatible JSON file for problem 4; distribution of
/// company count by Principal Business Activity by year, over a year range.
///
/// # Arguments
///
/// * `raw_data` - The data to plot
/// * `start_year` - The start year for the analysis
/// * `end_year` - The end year for the analysis
///
/// # Returns
///
/// * `Result<(), Box<dyn Error>>` - Mainly to handle Serde / IO errors
pub fn plot_problem4(
    raw_data: &Vec<Company>,
    start_year: i32,
    end_year: i32,
) -> Result<(), Box<dyn Error>> {
    // Analyze the data
    let company_activity = problem4(raw_data, start_year, end_year);

    // The categories are the y7ears
    let categories: Vec<String> = (start_year..=end_year).map(|x| x.to_string()).collect();

    // Pack it into a format that Highcharts can understand
    let mut series = Vec::new();
    for (name, data) in company_activity {
        series.push(DataElement {
            name,
            data: data.values().cloned().collect(),
        });
    }

    // Serialize to JSON and dump to file
    let plot_data = GroupedTwoDimensionalPlot { categories, series };

    // Serialize to JSON and dump to file
    let json_str = serde_json::to_string(&plot_data)?;
    std::fs::write("highcharts/problem4.json", json_str)?;

    Ok(())
}

/// Dump all the plots in one go
///
/// # Arguments
///
/// * `raw_data` - The data to plot
/// * `start_year` - The start year for the analysis
/// * `end_year` - The end year for the analysis
///
/// # Returns
///
/// * `Result<(), Box<dyn Error>>` - Mainly to handle Serde / IO errors
pub fn plot_all(
    raw_data: &Vec<Company>,
    start_year: i32,
    end_year: i32,
) -> Result<(), Box<dyn Error>> {
    plot_problem1(raw_data)?;
    plot_problem2(raw_data, start_year, end_year)?;
    plot_problem3(raw_data, start_year, end_year)?;
    plot_problem4(raw_data, start_year, end_year)?;

    Ok(())
}
