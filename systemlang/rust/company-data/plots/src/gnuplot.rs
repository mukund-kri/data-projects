use std::fmt::Display;
use std::fs::File;
use std::io::Write;

use indexmap::IndexMap;

use base::{problem1, problem2, problem3, problem4, Company};

/// This function generates a gnuplot compatible dat file for problem 1; distribution of
/// authorized capital.
///
/// # Arguments
///
/// * `data` - The data to plot
///
pub fn plot_problem1(raw_data: &Vec<Company>) {
    let authorized_capital = problem1(raw_data);
    gen_2d_plot(authorized_capital, "gnuplots/problem1.dat");
}

/// This function generates a gnuplot compatible dat file for problem 2; distribution of
/// company count by year.
///
/// # Arguments
///
/// * `data` - The data to plot
/// * `start_year` - The start year for the analysis
/// * `end_year` - The end year for the analysis
///
pub fn plot_problem2(raw_data: &Vec<Company>, start_year: i32, end_year: i32) {
    let company_registration = problem2(raw_data, start_year, end_year);
    gen_2d_plot(company_registration, "gnuplots/problem2.dat");
}

/// This function generates a gnuplot compatible dat file for problem 3; distribution of
/// company count by Principal Business Activity, over a year range.
///
/// # Arguments
///
/// * `data` - The data to plot
/// * `start_year` - The start year for the analysis
/// * `end_year` - The end year for the analysis
///
pub fn plot_problem3(raw_data: &Vec<Company>, start_year: i32, end_year: i32) {
    let company_activity = problem3(raw_data, start_year, end_year);
    gen_2d_plot(company_activity, "gnuplots/problem3.dat");
}

/// This function generates a gnuplot compatible dat file for problem 4; distribution of
/// company count by Principal Business Activity by year, over a year range.
///
/// # Arguments
///
/// * `data` - The data to plot
/// * `start_year` - The start year for the analysis
/// * `end_year` - The end year for the analysis
///
pub fn plot_problem4(raw_data: &Vec<Company>, start_year: i32, end_year: i32) {
    let company_activity = problem4(raw_data, start_year, end_year);
    gen_grouped_2d_plot(company_activity, "gnuplots/problem4.dat");
}

/// This function generates a 2D bar plot from a dictionary of data and writes it to a
/// file in a from that can be used by gnuplot.
///
/// # Arguments
///
/// * `data` - The data to plot
/// * `dat_file` - Name of the file to write the data to
///
pub fn gen_2d_plot<T: Display, U: Display>(data: IndexMap<T, U>, dat_file: &str) {
    let mut plot_file = File::create(dat_file).expect("Failed to create data file");
    for (year, population) in data {
        let _ = writeln!(plot_file, "\"{year}\" {population}");
    }
}

/// Do all the plots in one go
///
/// # Arguments
///
/// * `raw_data` - The data to plot
/// * `start_year` - The start year for the analysis
/// * `end_year` - The end year for the analysis
///
pub fn all_plots(raw_data: &Vec<Company>, start_year: i32, end_year: i32) {
    plot_problem1(raw_data);
    plot_problem2(raw_data, start_year, end_year);
    plot_problem3(raw_data, start_year, end_year);
    plot_problem4(raw_data, start_year, end_year);
}

/// This function generates a 2D plot for a grouped data
/// The data is a map of country name to a map of year to population
///
/// * `data` - The grouped data
/// * `dat_file` - The file to write the data to
///
pub fn gen_grouped_2d_plot<T: Display, U: Display, V: Display>(
    data: IndexMap<T, IndexMap<U, V>>,
    dat_file: &str,
) {
    let mut plot_file = File::create(dat_file).expect("Failed to create data file");

    for year in 1995..=2015 {
        let _ = write!(plot_file, "{}\t", year);
    }
    let _ = writeln!(plot_file, "");

    for (country, country_data) in data {
        let _ = write!(plot_file, "\"{}\"\t", country);

        for (_, population) in country_data {
            let _ = write!(plot_file, "{population}\t");
        }
        let _ = writeln!(plot_file, "");
    }
}
