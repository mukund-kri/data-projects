# Setup title
set title "Problem 4 :: Registration by Principal Business Activity by Year"
set title font ",20"

# Axis titles
set xlabel "Year"
set ylabel "Number of Companies"

# legend title
set key title "Principal Business Activity"

# xtic
set xtics ()
set xtics rotate by 90 right

# Plot
set style data histogram
set style histogram cluster gap 1

set style fill solid border rgb "black"
set auto x
set yrange [0:*]
plot 'problem4.dat' using 2:xtic(1) title col, \
   '' using 3:xtic(1) title col, \
   '' using 4:xtic(1) title col, \
   '' using 5:xtic(1) title col