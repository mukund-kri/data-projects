let plot4;

$.getJSON('problem4.json', (data) => {
  plot4(data.categories, data.series);
});


plot4 = (categories, series) => {

  return Highcharts.chart('problem4', {
    chart: {
      type: 'bar',
    },
    title: {
      text: 'Companies registered in Maharastra by Year',
    },
    xAxis: {
      categories,
      title: {
        text: null,
      },
    },
    yAxis: {
      min: 0,
      title: {
        text: 'No of Companies',
        align: 'high',
      },
    },
    labels: {
      overflow: 'justify',
    },
    tooltip: {
      valueSuffix: 'companies',
    },
    plotOptions: {
      bar: {
        dataLabels: {
          enabled: true,
        },
      },
    },
    credits: {
      enabled: false,
    },
    series,
  });
};
