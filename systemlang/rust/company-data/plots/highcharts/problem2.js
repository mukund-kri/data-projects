let plot2;

$.getJSON('problem2.json', (data) => {
    console.log(data);
  plot2(data.categories, data.data);  
});

plot2 = (categories, series) => {
  return Highcharts.chart('problem2', {
    chart: {
      type: 'bar',
    },
    title: {
      text: 'Companies registered in Maharastra by Year',
    },
    xAxis: {
      categories,
      title: {
        text: null,
      },
    },
    yAxis: {
      min: 0,
      title: {
        text: 'No of Companies',
        align: 'high',
      },
    },
    labels: {
      overflow: 'justify',
    },
    tooltip: {
      valueSuffix: 'companies',
    },
    plotOptions: {
      bar: {
        dataLabels: {
          enabled: true,
        },
      },
    },
    credits: {
      enabled: false,
    },
    series: [
      {
        name: 'Number of Companies',
        data: series,
      },
    ],
  });
};
