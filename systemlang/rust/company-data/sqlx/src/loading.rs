use std::collections::HashMap;
use std::error::Error;

use sqlx::postgres::PgPoolOptions;
use sqlx::{Postgres, QueryBuilder};

use crate::{dbutil, Company};

pub fn load_from_csv(file_name: &str) -> Result<Vec<Company>, Box<dyn Error>> {
    let mut companies = Vec::new();

    // Read the csv in and collect the companies
    let mut rdr = csv::Reader::from_path(file_name)?;
    for result in rdr.deserialize() {
        let company: Company = result?;
        companies.push(company);
    }

    Ok(companies)
}

pub async fn load_to_db(companies: Vec<Company>) -> Result<(), sqlx::Error> {
    // Connect to postgres, url in .env
    // Connect to the database
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect("postgres://devuser:devpass@localhost:5432/devdb")
        .await?;

    // Start building the insert query with QueryBuilder
    let mut query_builder: QueryBuilder<Postgres> =
        QueryBuilder::new("INSERT INTO companies (authorized_capital, incorporation_year, pba) ");

    // Use push_values to add the companies
    query_builder.push_values(companies, |mut b, company| {
        // Extract out year of incorporation from date
        let year = company
            .date_of_registration
            .split('-')
            .last()
            .unwrap_or_default();
        let year = year.parse::<i32>().unwrap_or_default();

        b.push(company.authorized_cap as i64)
            .push(year)
            .push(format!("'{}'", company.pba));
    });

    // Build and execute the query
    let query = query_builder.build();
    query.execute(&pool).await?;

    // print out the sql
    // println!("{}", query.sql());
    Ok(())
}

/// In the current data set sort  and load the pba's to db.
///
/// Arguments:
///
/// companies: Vec<Company> - the raw data from CSV
pub async fn compute_and_load_top_pbas(companies: Vec<Company>) -> Result<(), Box<dyn Error>> {
    // first accumulate company counts by pba
    let mut counter: HashMap<String, i32> = HashMap::new();

    // Count values
    for company in companies {
        let count = counter.entry(company.pba).or_default();
        *count = *count + 1;
    }

    let mut sorted: Vec<(&String, &i32)> = counter.iter().collect();
    sorted.sort_by(|a, b| b.1.cmp(a.1));

    // Insert into top_pbas table
    // Start the query builder
    let mut query_builder: QueryBuilder<Postgres> =
        QueryBuilder::new("INSERT INTO top_pbas (pba) ");

    // push values into the query
    query_builder.push_values(sorted, |mut b, pba| {
        let pba_name = pba.0;
        b.push(format!("'{}'", pba_name));
    });

    let pool = dbutil::get_db_conn().await?;
    let query = query_builder.build();
    query.execute(&pool).await?;

    // Every thing went right
    println!("Inserted PBA's into db");
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_load_data() {
        let data = load_from_csv("../data/mca_westbengal_min.csv").unwrap();
        assert_eq!(data.len(), 100);
    }
}
