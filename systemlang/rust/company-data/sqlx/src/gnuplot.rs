use std::collections::HashMap;
use std::fs::File;
use std::hash::Hash;
use std::io::Write;

/// Print dat file for gnuplot
pub(super) fn plot1(data: &Vec<(String, i64)>) {
    let mut file =
        File::create("gnuplots/problem1.dat").expect("Could not create file for problem1");
    for (range, count) in data {
        writeln!(file, "{} {}", range, count).unwrap();
    }
}

/// Print dat file for gnuplot
pub(super) fn plot2(data: &Vec<(i32, i64)>) {
    let mut file =
        File::create("gnuplots/problem2.dat").expect("Could not create file for problem2");
    for (year, count) in data {
        writeln!(file, "{} {}", year, count).unwrap();
    }
}

/// Print dat file for gnuplot. Problem 3
pub(super) fn plot3(data: &Vec<(String, i64)>) {
    let mut file =
        File::create("gnuplots/problem3.dat").expect("Could not create file for problem3");
    for (pba, count) in data {
        writeln!(file, "\"{}\" {}", pba, count).unwrap();
    }
}

/// Print dat file for gnuplot. Problem 4
pub(super) fn plot4(data: &HashMap<String, HashMap<i32, i64>>, start_year: i32, end_year: i32) {
    let mut file =
        File::create("gnuplots/problem4.dat").expect("Could not create file for problem4");

    // First get the keys and write to file
    let pbas = data
        .keys()
        .map(|pba| format!("\"{}\"", pba))
        .collect::<Vec<String>>();

    let pbas_line = pbas.join("\t");

    writeln!(file, "{}", pbas_line);

    // Now write the data
    for year in start_year..end_year {
        write!(file, "{}\t", year);
        let mut line = Vec::new();
        for pba in data.keys() {
            let count = data[pba].get(&year).unwrap_or(&0);
            line.push(count.to_string());
        }
        let line = line.join("\t");
        writeln!(file, "{}", line);
    }
}
