use std::error::Error;

use console::Term;
use dialoguer::{theme::ColorfulTheme, Select};

use mca::{generate_gnuplot, PlotSelection};

#[async_std::main]
async fn main() -> Result<(), Box<dyn Error>> {
    // The plotting system to use
    let plotting_system_set = &["Gnuplot", "Highcharts"];

    // Options for command line arguments
    let problem_set = &[
        "All Problems",
        "Problem 1",
        "Problem 2",
        "Problem 3",
        "Problem 4",
    ];

    // Start and End year for the analysis
    let years = (2000..2018)
        .map(|year| year.to_string())
        .collect::<Vec<String>>();

    // Setup term and theme
    let term = Term::buffered_stderr();
    let theme = ColorfulTheme::default();

    let plotting_system_selection = Select::with_theme(&theme)
        .with_prompt("Select a plotting system")
        .items(plotting_system_set)
        .default(0)
        .interact_on(&term)?;

    let problem_selection = Select::with_theme(&theme)
        .with_prompt("Select a problem to solve")
        .items(problem_set)
        .default(0)
        .interact_on(&term)?;

    let start_year_selection = Select::with_theme(&theme)
        .with_prompt("Select the start year")
        .items(&years)
        .default(0)
        .interact_on(&term)?;

    let end_year_selection = Select::with_theme(&theme)
        .with_prompt("Select the end year")
        .items(&years)
        .default(0)
        .interact_on(&term)?;

    let start_year: i32 = start_year_selection as i32 + 2000;
    let end_year: i32 = end_year_selection as i32 + 2000;

    println!("Start year: {} End year: {}", start_year, end_year);

    // Convert problem selection to enum PlotSelection
    let plot_selection = match problem_selection {
        0 => PlotSelection::All,
        1 => PlotSelection::Plot1,
        2 => PlotSelection::Plot2,
        3 => PlotSelection::Plot3,
        4 => PlotSelection::Plot4,
        _ => panic!("Invalid selection"),
    };

    if plotting_system_selection == 0 {
        generate_gnuplot(plot_selection, start_year, end_year).await?;
    } else {
        println!("Highcharts plotting system not implemented yet");
    }

    Ok(())
}
