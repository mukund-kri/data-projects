use std::error::Error;

use console::Term;
use dialoguer::{
    theme::{self, ColorfulTheme},
    Select,
};

use mca::loading::{compute_and_load_top_pbas, load_from_csv, load_to_db};

#[async_std::main]
async fn main() -> Result<(), Box<dyn Error>> {
    // What to load? the filtered data set or Top PBAs
    let selection_labels = &["Load Data", "Load Top PBAs"];

    let term = Term::buffered_stderr();
    let theme = ColorfulTheme::default();

    let action_selection = Select::with_theme(&theme)
        .with_prompt("What do you want to do?")
        .items(selection_labels)
        .default(0)
        .interact_on(&term)?;

    let companies = load_from_csv("../data/mca_clean.csv")?;

    match action_selection {
        0 => {
            println!("Loading from CSV into DB");
            load_to_db(companies).await?;
        }
        1 => {
            println!("Loading top pbas into DB");
            compute_and_load_top_pbas(companies).await?;
        }
        _ => println!("ERROR!, Selection not recognized"),
    }

    Ok(())
}
