use std::{collections::HashMap, error::Error, result};

use sqlx::postgres::PgPool;

/// Query the database and generate data for plot 1. The counts of companies by
/// authorized capital.
pub async fn problem1(conn: &PgPool) -> Result<Vec<(String, i64)>, Box<dyn Error>> {
    let result = sqlx::query!(
        r#"SELECT
        CASE
            WHEN authorized_capital <= 100000 THEN '<=1L'
            WHEN authorized_capital > 100000 AND authorized_capital <= 1000000 THEN '1L-10L'
            WHEN authorized_capital > 1000000 AND authorized_capital <= 10000000 THEN '10L-1Cr'
            WHEN authorized_capital > 10000000 AND authorized_capital <= 100000000 THEN '1Cr-10Cr'
            ELSE '>10Cr'
        END
        AS authorized_capital_range,
        COUNT(authorized_capital) AS counts
    FROM companies
    GROUP BY authorized_capital_range"#
    )
    .fetch_all(conn)
    .await?;

    let mut data = Vec::new();
    for row in result {
        let authorized_cap = row.authorized_capital_range.unwrap();
        let count = row.counts.unwrap();

        data.push((authorized_cap, count));
    }

    Ok(data)
}

/// Query the database and generate data for plot 2. The counts of companies by year of
/// incorporation.
///
/// # Arguments
///
/// * `conn` - A connection to the database
///
/// # Returns
///
/// A vector of tuples where the first element is the year of incorporation and the second
/// element is the count of companies incorporated in that year.
pub async fn problem2(
    conn: &PgPool,
    start_year: i32,
    end_year: i32,
) -> Result<Vec<(i32, i64)>, Box<dyn Error>> {
    let result = sqlx::query!(
        r#"SELECT incorporation_year, COUNT(*)
        FROM companies
        WHERE incorporation_year BETWEEN $1 AND $2
        GROUP BY incorporation_year"#,
        start_year,
        end_year
    )
    .fetch_all(conn)
    .await?;

    let mut data = Vec::new();
    for row in result {
        let year = row.incorporation_year;
        let count = row.count.unwrap();

        data.push((year, count));
    }

    Ok(data)
}

/// Query the database and generate data for plot 3. The counts of companies by pba over
/// a range of years
///
/// # Arguments
///
/// * `conn` - A connection to the database
///
/// # Returns
///
/// A vector of tuples where the first element is the pba and the second
/// element is the count of pba's.
pub async fn problem3(
    conn: &PgPool,
    start_year: i32,
    end_year: i32,
) -> Result<Vec<(String, i64)>, Box<dyn Error>> {
    // group by and count the number of companies by pba
    let result = sqlx::query!(
        r#"SELECT pba, COUNT(*)
        FROM companies
        WHERE incorporation_year BETWEEN $1 AND $2
        AND pba IN (SELECT pba FROM top_pbas LIMIT 5)
        GROUP BY pba"#,
        start_year,
        end_year
    )
    .fetch_all(conn)
    .await?;

    let mut data = Vec::new();
    for row in result {
        let pba = row.pba.unwrap();
        let count = row.count.unwrap();

        data.push((pba, count));
    }
    Ok(data)
}

/// Query the database and generate data for plot 4. The counts of companies by
/// authorized capital and year of incorporation.
///
/// # Arguments
///
/// * `conn` - A connection to the database
/// * `start_year` - The start year for the analysis
/// * `end_year` - The end year for the analysis
///
/// # Returns
///
pub async fn problem4(
    conn: &PgPool,
    start_year: i32,
    end_year: i32,
) -> Result<HashMap<String, HashMap<i32, i64>>, Box<dyn Error>> {
    let result = sqlx::query!(
        r#"SELECT pba, incorporation_year, COUNT(*) FROM companies
        WHERE incorporation_year BETWEEN $1 AND $2
        AND pba IN (SELECT pba FROM top_pbas LIMIT 5)
        GROUP BY pba, incorporation_year"#,
        start_year,
        end_year
    );

    let mut data: HashMap<String, HashMap<i32, i64>> = HashMap::new();
    for row in result.fetch_all(conn).await? {
        let pba = row.pba.unwrap();
        let year = row.incorporation_year;
        let count = row.count.unwrap();

        let entry = data.entry(pba).or_insert(HashMap::new());
        entry.insert(year, count);
    }

    Ok(data)
}
