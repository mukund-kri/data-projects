use std::env;
use std::error::Error;

use sqlx::postgres::PgPoolOptions;
use sqlx::{Pool, Postgres};

pub(crate) async fn get_db_conn() -> Result<Pool<Postgres>, Box<dyn Error>> {
    // Connect to the database
    // Extract the database url from .evn or the environment
    dotenvy::dotenv()?;
    let database_url = env::var("DATABASE_URL")?;

    // Connect to the database
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(&database_url)
        .await?;
    println!("Connected to the database");

    Ok(pool)
}
