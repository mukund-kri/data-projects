pub mod analytics;
pub mod dbutil;
pub mod gnuplot;
pub mod loading;

use std::env;
use std::error::Error;

use serde::Deserialize;
use sqlx::postgres::PgPoolOptions;

use analytics::{problem1, problem2, problem3, problem4};

/// Each company is a data point
#[derive(Debug, Deserialize)]
pub struct Company {
    #[serde(rename = "AUTHORIZED_CAP")]
    pub authorized_cap: f64,

    #[serde(rename = "DATE_OF_REGISTRATION")]
    pub date_of_registration: String,

    #[serde(rename = "PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN")]
    pub pba: String,
}

/// Pass around the information on which plot to generate
#[derive(Debug)]
pub enum PlotSelection {
    All,
    Plot1,
    Plot2,
    Plot3,
    Plot4,
}

/// The single point of entry for gnuplot data generation
///
/// # Arguments
///
/// * `select` :: PlotSelection - The plot to generate
pub async fn generate_gnuplot(
    selection: PlotSelection,
    start_year: i32,
    end_year: i32,
) -> Result<(), Box<dyn Error>> {
    // Connect to the database
    // Extract the database url from .evn or the environment
    dotenvy::dotenv()?;
    let database_url = env::var("DATABASE_URL")?;

    // Connect to the database
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(&database_url)
        .await?;
    println!("Connected to the database");

    // match the plot selection
    match selection {
        PlotSelection::Plot1 => {
            let data = problem1(&pool).await?;
            gnuplot::plot1(&data);
            println!("Generating gnuplot dat file for problem 1");
        }
        PlotSelection::Plot2 => {
            let data = problem2(&pool, start_year, end_year).await?;
            gnuplot::plot2(&data);
            println!("Generated gnuplot dat file for problem 2");
        }
        PlotSelection::Plot3 => {
            let data = problem3(&pool, start_year, end_year).await?;
            gnuplot::plot3(&data);
            println!("Generating gnuplot dat file for problem 3");
        }
        PlotSelection::Plot4 => {
            let data = problem4(&pool, start_year, end_year).await?;
            gnuplot::plot4(&data, start_year, end_year);
            println!("Generating gnuplot dat file for problem 4");
        }
        PlotSelection::All => {
            println!("Generating gnuplot dat file for all problems");
            let data = problem1(&pool).await?;
            gnuplot::plot1(&data);

            let data = problem2(&pool, start_year, end_year).await?;
            gnuplot::plot2(&data);

            let data = problem3(&pool, start_year, end_year).await?;
            gnuplot::plot3(&data);

            let data = problem4(&pool, start_year, end_year).await?;
            gnuplot::plot4(&data, start_year, end_year);
        }
    }
    return Ok(());
}
