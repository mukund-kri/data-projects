-- Create a table top_pbas, where I store the top pbas
CREATE TABLE top_pbas (
  id SERIAL PRIMARY KEY,
  pba TEXT NOT NULL
);
