-- Create a table for companies with 4 columns 
--  id: primary key, auto incrementing integer
--  authorized_capital: integer
--  incorporation_year: integer
--  pba: text
CREATE TABLE companies (
  id SERIAL PRIMARY KEY,
  authorized_capital BIGINT NOT NULL,
  incorporation_year INT NOT NULL,
  pba TEXT
);