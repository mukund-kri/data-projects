# Company Master Data project in Rust


## Description

Implementation of the "Company Mater" data exercise in Rust. Refer the 
[Problem Statement](./PROBLEM-STATEMENT .md) for more details.

## Parts

1. base

The common code that the base analysis on the CSV file. Should be agnostic to the
command line interface and graph rendering engine.

2. gnu-plot

Output the data that can used in gnuplot to generate the graphs.

3. cli

A fancy cli interface for the base code.

