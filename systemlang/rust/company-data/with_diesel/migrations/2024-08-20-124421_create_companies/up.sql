CREATE TABLE companies (
  id SERIAL PRIMARY KEY,
  authorized_capital BIGINT NOT NULL,
  incorporation_year INT NOT NULL,
  pba TEXT NOT NULL
);