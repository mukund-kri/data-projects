pub mod analytics;
pub mod csvops;
pub mod gnuplot;
pub mod models;
pub mod schema;

use std::env;
use std::error::Error;

use diesel::prelude::*;
use diesel::{dsl::insert_into, pg::PgConnection};
use dotenvy::dotenv;

use crate::gnuplot::{plot1, plot2, plot3, plot4};

pub fn establish_connection() -> Result<PgConnection, Box<dyn Error>> {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL")?;

    let connection = PgConnection::establish(&database_url)?;

    Ok(connection)
}

pub fn load_from_csv(path: &str) -> Result<(), Box<dyn Error>> {
    use schema::companies::dsl::*;

    let connection = &mut establish_connection()?;

    let companies_vec = csvops::read_csv(path)?;

    for chunk in companies_vec.chunks(20000) {
        insert_into(companies).values(chunk).execute(connection)?;
    }
    Ok(())
}

pub fn genplots(_plot_type: isize, _plot_id: isize) -> Result<(), Box<dyn Error>> {
    println!("Generating plots...");

    let connection = &mut establish_connection()?;
    plot1(connection)?;

    Ok(())
}
