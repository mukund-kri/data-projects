use diesel::prelude::*;
use serde::Deserialize;

/// The company entity is meant to be selected and queried
#[derive(Queryable, Selectable)]
#[diesel(table_name = crate::schema::companies)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Company {
    pub id: i32,
    pub authorized_capital: i64,
    pub incorporation_year: i32,
    pub pba: String,
}

/// The new company entity is meant to be inserted into the database
#[derive(Insertable, Debug)]
#[diesel(table_name = crate::schema::companies)]
pub struct NewCompany {
    pub authorized_capital: i64,
    pub incorporation_year: i32,
    pub pba: String,
}

/// Deserialized from each row of the company data csv source file.
#[derive(Debug, Deserialize)]
pub struct CsvCompany {
    #[serde(rename = "AUTHORIZED_CAP")]
    pub authorized_capital: f64,

    #[serde(rename = "DATE_OF_REGISTRATION")]
    pub incorporation_date: String,

    #[serde(rename = "PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN")]
    pub pba: String,
}

impl CsvCompany {
    pub fn to(&self) -> Option<NewCompany> {
        if self.pba == "NA" {
            return None;
        }

        if self.incorporation_date == "NA" {
            return None;
        }

        let authorized_capital = self.authorized_capital as i64;
        let incorporation_year = self
            .incorporation_date
            .split('-')
            .last()
            .unwrap()
            .parse()
            .unwrap();
        let pba = self.pba.clone();

        Some(NewCompany {
            authorized_capital,
            incorporation_year,
            pba,
        })
    }
}
