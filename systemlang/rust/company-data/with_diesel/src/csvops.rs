use std::error::Error;

use csv;

use crate::models::{CsvCompany, NewCompany};

pub fn read_csv(path: &str) -> Result<Vec<NewCompany>, Box<dyn Error>> {
    let mut rdr = csv::Reader::from_path(path)?;

    let lst = rdr
        .deserialize::<CsvCompany>()
        .map(|res| res.unwrap().to())
        .filter(|res| res.is_some())
        .map(|res| res.unwrap())
        .collect::<Vec<_>>();

    Ok(lst)
}
