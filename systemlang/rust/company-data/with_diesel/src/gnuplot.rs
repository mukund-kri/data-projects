use std::error::Error;
use std::fs::File;
use std::io::Write;

use diesel::PgConnection;
use indexmap::IndexMap;

use crate::analytics::{problem1, problem2, problem3, problem4};

/// Plot 1: Bar chart of companies by authorized captital
///
/// # Arguments
///
/// * `conn` - A mutable reference to a PgConnection
pub fn plot1(conn: &mut PgConnection) -> Result<(), Box<dyn Error>> {
    // Load the final analysis data from db
    let plot_data = problem1(conn)?;

    // Open and write plot data in dat format to file
    let mut file = File::create("gnuplots/problem1.dat")?;
    for (label, count) in plot_data {
        writeln!(file, "{} {}", label, count)?;
    }
    // close file
    drop(file);

    Ok(())
}

/// Plot 2: Plot the nuber of companies registered in a range of years
///
/// # Arguments
///
/// * `conn` - A mutable reference to a PgConnection
/// * `start_year` - The start year of the range
/// * `end_year` - The end year of the range
pub fn plot2(
    conn: &mut PgConnection,
    start_year: i32,
    end_year: i32,
) -> Result<(), Box<dyn Error>> {
    // Load the final analysis data from db
    let plot_data = problem2(conn, start_year, end_year)?;

    // Open and write plot data in dat format to file
    let mut file = File::create("gnuplots/problem2.dat")?;
    for (year, count) in plot_data {
        writeln!(file, "{} {}", year, count)?;
    }
    // close file
    drop(file);

    Ok(())
}

/// Plot the top N PBAs with the most companies
///
/// # Arguments
///
/// * `conn` - A mutable reference to a PgConnection
/// * `num_pbas` - The number of top PBAs to plot
pub fn plot3(conn: &mut PgConnection, num_pbas: i32) -> Result<(), Box<dyn Error>> {
    // Load the final analysis data from db
    let plot_data = problem3(conn, num_pbas)?;

    // Open and write plot data in dat format to file
    let mut file = File::create("gnuplots/problem3.dat")?;
    for (pba, count) in plot_data {
        writeln!(file, "\"{}\" {}", pba, count)?;
    }
    // close file
    drop(file);

    Ok(())
}

/// Plot the top N PBAs with the most companies over a range of years
///
/// # Arguments
///
/// * `conn` - A mutable reference to a PgConnection
/// * `num_pbas` - The number of top PBAs to plot
/// * `start_year` - The start year of the range
/// * `end_year` - The end year of the range
///
pub fn plot4(
    conn: &mut PgConnection,
    num_pbas: i32,
    start_year: i32,
    end_year: i32,
) -> Result<(), Box<dyn Error>> {
    // Load the final analysis data from db
    let plot_data = problem4(conn, num_pbas, start_year, end_year)?;

    let mut counts: IndexMap<String, IndexMap<i32, i64>> = IndexMap::new();

    // populate the counts map
    for (pba, year, count) in plot_data {
        if !counts.contains_key(&pba) {
            counts.insert(pba.clone(), IndexMap::new());
        }
        counts.get_mut(&pba).unwrap().insert(year, count);
    }

    // Extract out the first 10 PBAs
    let top_pbas = counts.iter().take(5).collect::<Vec<_>>();

    // Open and write plot data in dat format to file
    let mut file = File::create("gnuplots/problem4.dat")?;

    // Write the header which is the pbas
    // Collect the name of the pbas into a vector and join them with a space
    let pbas = top_pbas
        .iter()
        .map(|(pba, _)| format!("\"{}\"", pba))
        .collect::<Vec<_>>()
        .join("\t");

    writeln!(file, "{}", pbas)?;

    // Write the data
    for year in start_year..=end_year {
        let mut line = String::new();
        for (_, data) in top_pbas.iter() {
            let count = data.get(&year).unwrap_or(&0);
            line.push_str(&format!("{}\t", count));
        }
        writeln!(file, "{}\t{}", year, line)?;
    }
    Ok(())
}
