use std::error::Error;

use diesel::pg::PgConnection;
use diesel::sql_types::{Integer, Text};
use diesel::{prelude::*, result};

/// Problem 1: Counts of the number of companies by authorized capital
///
/// # Arguments
///
/// * `connection` - A mutable reference to a PgConnection
pub fn problem1(connection: &mut PgConnection) -> Result<Vec<(String, i64)>, Box<dyn Error>> {
    use crate::schema::companies::columns::*;
    use crate::schema::*;
    use diesel::dsl::case_when;

    let cs = companies::table
        .select(
            case_when::<_, _, Text>(authorized_capital.le(100_000), "0-1L")
                .when(authorized_capital.le(10_00_000), "1L-10L")
                .when(authorized_capital.le(1_00_00_000), "10L-1Cr")
                .when(authorized_capital.le(10_00_00_000), "1Cr-10Cr")
                .otherwise("10Cr+")
                .alias("label"),
        )
        .load::<String>(connection)?;

    println!("Listing ids of companies");
    for c in cs {
        println!("{:?}", c);
    }

    let result = Vec::new();
    Ok(result)
}

/// Problem 2: Histogram of the number of companies registered in a range of years
///
/// # Arguments
///
/// * `connection` - A mutable reference to a PgConnection
/// * `start_year` - The start year of the range
/// * `end_year` - The end year of the range
///
/// # Returns
///
/// A vector of tuples containing the year and the number of companies registered in that year
pub fn problem2(
    connection: &mut PgConnection,
    start_year: i32,
    end_year: i32,
) -> Result<Vec<(i32, i64)>, Box<dyn Error>> {
    use crate::schema::*;

    let cs = companies::table
        .filter(companies::incorporation_year.ge(start_year))
        .filter(companies::incorporation_year.le(end_year))
        .group_by(companies::incorporation_year)
        .select((
            companies::incorporation_year,
            diesel::dsl::count(companies::id),
        ))
        .load::<(i32, i64)>(connection)?;

    Ok(cs)
}

/// Problem 3: Find the top N PBAs with the most companies
///
/// # Arguments
///
/// * `connection` - A mutable reference to a PgConnection
/// * `num_pbas` - The number of top PBAs to return
///
/// # Returns
///
/// A vector of tuples containing the PBA and the number of companies
pub fn problem3(
    connection: &mut PgConnection,
    num_pbas: i32,
) -> Result<Vec<(String, i64)>, Box<dyn Error>> {
    use crate::schema::*;

    let cs = companies::table
        .group_by(companies::pba)
        .order_by(diesel::dsl::count(companies::id).desc())
        .select((companies::pba, diesel::dsl::count(companies::id)))
        .limit(num_pbas as i64)
        .load::<(String, i64)>(connection)?;

    Ok(cs)
}

/// Problem 4: Find the top N PBAs with the most companies in a range of years
///
/// # Arguments
///
/// * `connection` - A mutable reference to a PgConnection
/// * `num_pbas` - The number of top PBAs to return
/// * `start_year` - The start year of the range
/// * `end_year` - The end year of the range
///
/// # Returns
///
/// A vector of tuples containing the PBA and the number of companies
pub fn problem4(
    connection: &mut PgConnection,
    _num_pbas: i32,
    start_year: i32,
    end_year: i32,
) -> Result<Vec<(String, i32, i64)>, Box<dyn Error>> {
    use crate::schema::*;

    let cs = companies::table
        .filter(companies::incorporation_year.ge(start_year))
        .filter(companies::incorporation_year.le(end_year))
        .group_by((companies::pba, companies::incorporation_year))
        .select((
            companies::pba,
            companies::incorporation_year,
            diesel::dsl::count(companies::id),
        ))
        .load::<(String, i32, i64)>(connection)?;

    Ok(cs)
}
