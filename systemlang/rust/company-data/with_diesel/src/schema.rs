// @generated automatically by Diesel CLI.

diesel::table! {
    companies (id) {
        id -> Int4,
        authorized_capital -> Int8,
        incorporation_year -> Int4,
        pba -> Text,
    }
}
