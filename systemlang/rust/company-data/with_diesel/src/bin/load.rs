use std::error::Error;

use with_diesel::load_from_csv;

fn main() -> Result<(), Box<dyn Error>> {
    load_from_csv("../data/mca_clean.csv")?;

    Ok(())
}
