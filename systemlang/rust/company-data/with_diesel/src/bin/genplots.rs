use std::error::Error;

use with_diesel::genplots;

fn main() -> Result<(), Box<dyn Error>> {
    genplots(1, 2)?;
    Ok(())
}
