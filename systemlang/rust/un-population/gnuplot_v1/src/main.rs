use std::error::Error;
use std::fs::File;
use std::io::Write;
use std::process;

use indexmap::IndexMap;
use serde::Deserialize;

#[derive(Debug, Deserialize)]
struct Record {
    #[serde(rename = "Region")]
    country: String,
    #[serde(rename = "Year")]
    year: i32,
    #[serde(rename = "Population")]
    population: f64,
}

// This function reads CSV file and prints its contents, line by line.
fn process(start_year: i32, end_year: i32) -> Result<(), Box<dyn Error>> {
    // List of ASEAN countries
    let asean_countries: Vec<String> = vec![
        "Brunei Darussalam".to_string(),
        "Cambodia".to_string(),
        "Indonesia".to_string(),
        "Lao People's Democratic Republic".to_string(),
        "Malaysia".to_string(),
        "Myanmar".to_string(),
        "Philippines".to_string(),
        "Singapore".to_string(),
        "Thailand".to_string(),
        "Viet Nam".to_string(),
    ];

    // HashMap to collect india data by year
    let mut india_data: IndexMap<i32, f64> = IndexMap::new();

    // HashMap to collect ASEAN data for year 2015
    let mut asean_2015_data: IndexMap<String, f64> = IndexMap::new();

    // ASEAN population over the years
    let mut asean_data: IndexMap<String, f64> = IndexMap::new();

    let file_path = "un-population.csv";

    // Open the csv file and make a CSV reader.
    let rdr = csv::Reader::from_path(file_path);
    for result in rdr?.deserialize() {
        let record: Record = result?;
        if record.country == "India" && record.year >= start_year && record.year <= end_year {
            india_data.insert(record.year, record.population);
        }

        if asean_countries.contains(&record.country) && record.year == 2015 {
            asean_2015_data.insert(record.country, record.population);
        }
    }

    // Next put the map into a gnu plot file
    let mut plot_file = File::create("plots/problem1.dat")?;
    for (year, population) in india_data {
        writeln!(plot_file, "{year} {population}")?;
    }

    let mut plot_file = File::create("plots/problem2.dat")?;
    for (country, population) in asean_2015_data {
        writeln!(plot_file, "{country} {population}")?;
    }

    Ok(())
}

fn main() {
    // All we do here is call the `run` function and handle any errors
    // that might occur.
    if let Err(e) = process(1995, 2015) {
        println!("Application error: {e}");

        process::exit(1);
    }

    println!("Done genrating gnuplot data files");
}
