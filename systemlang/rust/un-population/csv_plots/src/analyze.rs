use indexmap::IndexMap;

use crate::data::PopulationData;

/// Problem 1
/// Track the population growth of India between `start_year` and `end_year`
///
/// # Arguments
///
/// * `data` - The raw population data
/// * `start_year` - Start year of the analysis
/// * `end_year` - End year of the analysis
///
pub fn problem1(data: &Vec<PopulationData>, start_year: i32, end_year: i32) -> IndexMap<i32, f64> {
    let mut india_data: IndexMap<i32, f64> = IndexMap::new();

    for datum in data {
        if datum.country == "India" && datum.year >= start_year && datum.year <= end_year {
            india_data.insert(datum.year, datum.population);
        }
    }

    india_data
}

/// Problem 2
/// Track the population growth of ASEAN countries in a given year
///
/// # Arguments
///
/// * `data` - The raw population data
/// * `year` - The year of the analysis
/// * `asean_countries` - The list of ASEAN countries
///
pub fn problem2(
    data: &Vec<PopulationData>,
    year: i32,
    asean_countries: &Vec<String>,
) -> IndexMap<String, f64> {
    let mut countries_data: IndexMap<String, f64> = IndexMap::new();

    for datum in data {
        if datum.year == year && asean_countries.contains(&datum.country) {
            countries_data.insert(datum.country.clone(), datum.population);
        }
    }

    countries_data
}

/// Problem 3
/// Track the aggregate population growth of SAARC countries between `start_year` and
/// `end_year`
///
/// # Arguments
///
/// * `data` - The raw population data
/// * `start_year` - Start year of the analysis
/// * `end_year` - End year of the analysis
/// * `saarc_countries` - The list of SAARC countries
///
pub fn problem3(
    data: &Vec<PopulationData>,
    start_year: i32,
    end_year: i32,
    saarc_countries: &Vec<String>,
) -> IndexMap<i32, f64> {
    let mut saarc_data: IndexMap<i32, f64> = IndexMap::new();

    for datum in data {
        if saarc_countries.contains(&datum.country)
            && datum.year >= start_year
            && datum.year <= end_year
        {
            *saarc_data.entry(datum.year).or_insert(0.0) += datum.population;
        }
    }

    saarc_data
}

/// Problem 4
/// Track the individual population growth of ASEAN countries between `start_year` and
/// `end_year`
///
/// # Arguments
///
/// * `data` - The raw population data
/// * `start_year` - Start year of the analysis
/// * `end_year` - End year of the analysis
/// * `asean_countries` - The list of ASEAN countries
pub fn problem4(
    data: &Vec<PopulationData>,
    start_year: i32,
    end_year: i32,
    asean_countries: &Vec<String>,
) -> IndexMap<String, IndexMap<i32, f64>> {
    let mut asean_pop: IndexMap<String, IndexMap<i32, f64>> = IndexMap::new();

    for datum in data {
        if asean_countries.contains(&datum.country)
            && datum.year >= start_year
            && datum.year <= end_year
        {
            *asean_pop
                .entry(datum.country.clone())
                .or_insert(IndexMap::new())
                .entry(datum.year)
                .or_insert(0.0) += datum.population;
        }
    }

    asean_pop
}
