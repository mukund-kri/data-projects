use std::error::Error;
use std::fs::File;

use indexmap::IndexMap;
use serde_json::json;

use crate::analyze::{problem1, problem2, problem3, problem4};
use crate::data::PopulationData;

// Analyze and write json
pub fn plot1(
    population_data: &Vec<PopulationData>,
    start_year: i32,
    end_year: i32,
) -> Result<(), Box<dyn Error>> {
    let adata = problem1(population_data, start_year, end_year);
    plot1_json(&adata, start_year, end_year)
}

fn plot1_json(
    index_map: &IndexMap<i32, f64>,
    start_year: i32,
    end_year: i32,
) -> Result<(), Box<dyn Error>> {
    // open json file to dump data
    let file = File::create("highcharts/problem1.json")?;

    // construct a high charts plot-able json
    let json_data = json!({
        "years": index_map.keys().collect::<Vec<&i32>>(),
        "population": index_map.values().collect::<Vec<&f64>>(),
        "start_year": start_year,
        "end_year": end_year
    });

    // write the json data to the file
    serde_json::to_writer_pretty(&file, &json_data)?;

    Ok(())
}

pub fn plot2(
    population_data: &Vec<PopulationData>,
    year: i32,
    asean_countries: &Vec<String>,
) -> Result<(), Box<dyn Error>> {
    let adata = problem2(population_data, year, asean_countries);
    plot2_json(&adata, year)
}

// plot 2: Population distribution of ASEAN countries in a given year
fn plot2_json(index_map: &IndexMap<String, f64>, year: i32) -> Result<(), Box<dyn Error>> {
    // open json file to dump data
    let file = File::create("highcharts/problem2.json")?;

    // construct a high charts plot-able json
    let json_data = json!({
        "selected_year": year,
        "countries": index_map.keys().collect::<Vec<&String>>(),
        "population": index_map.values().collect::<Vec<&f64>>()
    });

    // write the json data to the file
    serde_json::to_writer_pretty(&file, &json_data)?;

    Ok(())
}

// Problem 3: Aggregate population growth of SAARC countries between `start_year` and `end_year`
pub fn plot3(
    population_data: &Vec<PopulationData>,
    start_year: i32,
    end_year: i32,
    saarc_countries: &Vec<String>,
) -> Result<(), Box<dyn Error>> {
    let adata = problem3(population_data, start_year, end_year, saarc_countries);
    plot3_json(&adata, start_year, end_year)
}

fn plot3_json(
    index_map: &IndexMap<i32, f64>,
    start_year: i32,
    end_year: i32,
) -> Result<(), Box<dyn Error>> {
    // open json file to dump data
    let file = File::create("highcharts/problem3.json")?;

    // construct a high charts plot-able json
    let json_data = json!({
        "start_year": start_year,
        "end_year": end_year,
        "years": index_map.keys().collect::<Vec<&i32>>(),
        "population": index_map.values().collect::<Vec<&f64>>()
    });

    // write the json data to the file
    serde_json::to_writer_pretty(&file, &json_data)?;

    Ok(())
}

// Problem 4: Grouped population growth of ASEAN countries between `start_year` and `end_year`
pub fn plot4(
    population_data: &Vec<PopulationData>,
    start_year: i32,
    end_year: i32,
    asean_countries: &Vec<String>,
) -> Result<(), Box<dyn Error>> {
    let adata = problem4(population_data, start_year, end_year, asean_countries);
    plot4_json(&adata, start_year, end_year)
}

fn plot4_json(
    index_map: &IndexMap<String, IndexMap<i32, f64>>,
    start_year: i32,
    end_year: i32,
) -> Result<(), Box<dyn Error>> {
    // open json file to dump data
    let file = File::create("highcharts/problem4.json")?;

    // The categories are the years of the analysis
    let categories: Vec<String> = (start_year..=end_year).map(|x| x.to_string()).collect();

    // The series data is the population growth of each country over the years to json
    let series = index_map
        .iter()
        .map(|(country, data)| {
            json!({
                "name": country,
                "data": data.values().collect::<Vec<&f64>>()
            })
        })
        .collect::<Vec<serde_json::Value>>();

    // construct a high charts plot-able json
    let json_data = json!({
        "start_year": start_year,
        "end_year": end_year,
        "categories": categories,
        "series": series,
    });

    // write the json data to the file
    serde_json::to_writer_pretty(&file, &json_data)?;

    Ok(())
}

// Generate json for all the plots at once
pub fn plot_all(
    population_data: &Vec<PopulationData>,
    asean_countries: &Vec<String>,
    saarc_countries: &Vec<String>,
    selected_year: i32,
    start_year: i32,
    end_year: i32,
) -> Result<(), Box<dyn Error>> {
    plot1(population_data, start_year, end_year)?;
    plot2(population_data, selected_year, asean_countries)?;
    plot3(population_data, start_year, end_year, saarc_countries)?;
    plot4(population_data, start_year, end_year, asean_countries)?;

    Ok(())
}
