pub mod analyze;
pub mod data;
pub mod gnuplot;
pub mod highcharts;

use std::error::Error;

use data::{load_countries, load_data, PopulationData};

pub fn run_plots(
    plotting_system: usize,
    problem_selection: usize,
    selected_year: i32,
    start_year: i32,
    end_year: i32,
) -> Result<(), Box<dyn Error>> {
    let asean_countries = load_countries("../data/asean-countries.txt");
    let saarc_countries: Vec<String> = load_countries("../data/saarc-countries.txt");

    let mut filter_countries: Vec<String> = Vec::new();
    filter_countries.extend(asean_countries.clone());
    filter_countries.extend(saarc_countries.clone());

    let population_data: Vec<PopulationData> =
        load_data("../data/un-population.csv", &filter_countries);

    if plotting_system == 0 {
        println!("\nGenerating Plots for GNUPLOT");
        match problem_selection {
            1 => gnuplot::problem1(&population_data, start_year, end_year)?,
            2 => gnuplot::problem2(&population_data, 2015, &asean_countries)?,
            3 => gnuplot::problem3(&population_data, start_year, end_year, &saarc_countries)?,
            4 => gnuplot::problem4(&population_data, start_year, end_year, &asean_countries)?,
            0 => gnuplot::all_problems(
                &population_data,
                start_year,
                end_year,
                &asean_countries,
                &saarc_countries,
            )?,
            _ => println!("Invalid selection"),
        }
    } else {
        match problem_selection {
            1 => highcharts::plot1(&population_data, start_year, end_year),
            2 => highcharts::plot2(&population_data, 2015, &asean_countries),
            3 => highcharts::plot3(&population_data, start_year, end_year, &saarc_countries),
            4 => highcharts::plot4(&population_data, start_year, end_year, &asean_countries),
            _ => highcharts::plot_all(
                &population_data,
                &asean_countries,
                &saarc_countries,
                selected_year,
                start_year,
                end_year,
            ),
        }?;
    };

    Ok(())
}
