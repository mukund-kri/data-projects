use std::error::Error;
use std::fmt::Display;
use std::fs::File;
use std::io::Write;

use indexmap::IndexMap;

use crate::analyze;
use crate::data::PopulationData;

/// This function generates a 2D bar plot from a dictionary of data and writes it to a
/// file in a from that can be used by gnuplot.
///
/// # Arguments
///
/// * `data` - The data to plot
/// * `dat_file` - Name of the file to write the data to
///
fn gen_2d_plot<T: Display, U: Display>(data: IndexMap<T, U>, dat_file: &str) {
    let mut plot_file = File::create(dat_file).expect("Failed to create data file");
    for (year, population) in data {
        let _ = writeln!(plot_file, "{year} {population}");
    }
}

/// This function generates a 2D plot for a grouped data
/// The data is a map of country name to a map of year to population
///
/// * `data` - The grouped data
/// * `dat_file` - The file to write the data to
///
fn gen_grouped_2d_plot<T: Display, U: Display, V: Display>(
    data: IndexMap<T, IndexMap<U, V>>,
    dat_file: &str,
) {
    let mut plot_file = File::create(dat_file).expect("Failed to create data file");

    for year in 1995..=2015 {
        let _ = write!(plot_file, "{}\t", year);
    }
    let _ = writeln!(plot_file, "");

    for (country, country_data) in data {
        let _ = write!(plot_file, "{}\t", country);

        for (_, population) in country_data {
            let _ = write!(plot_file, "{population}\t");
        }
        let _ = writeln!(plot_file, "");
    }
}

// Problem 1: Population growth of India over the years
pub fn problem1(
    population_data: &Vec<PopulationData>,
    start_year: i32,
    end_year: i32,
) -> Result<(), Box<dyn Error>> {
    let problem1_solution = analyze::problem1(&population_data, start_year, end_year);
    gen_2d_plot(problem1_solution, "gnuplots/problem1.dat");
    Ok(())
}

// Problem 2: Population distribution of ASEAN countries in a given year
pub fn problem2(
    population_data: &Vec<PopulationData>,
    year: i32,
    asean_countries: &Vec<String>,
) -> Result<(), Box<dyn Error>> {
    let problem2_solution = analyze::problem2(&population_data, year, asean_countries);
    gen_2d_plot(problem2_solution, "gnuplots/problem2.dat");
    Ok(())
}

// Problem 3: Aggregate population growth of SAARC countries between `start_year` and `end_year`
pub fn problem3(
    population_data: &Vec<PopulationData>,
    start_year: i32,
    end_year: i32,
    saarc_countries: &Vec<String>,
) -> Result<(), Box<dyn Error>> {
    let problem3_solution =
        analyze::problem3(&population_data, start_year, end_year, saarc_countries);
    gen_2d_plot(problem3_solution, "gnuplots/problem3.dat");
    Ok(())
}

// Problem 4: Grouped population growth of ASEAN countries between `start_year` and `end_year`
pub fn problem4(
    population_data: &Vec<PopulationData>,
    start_year: i32,
    end_year: i32,
    asean_countries: &Vec<String>,
) -> Result<(), Box<dyn Error>> {
    let problem4_solution =
        analyze::problem4(&population_data, start_year, end_year, asean_countries);
    gen_grouped_2d_plot(problem4_solution, "gnuplots/problem4.dat");
    Ok(())
}

// All problems in one go
pub fn all_problems(
    population_data: &Vec<PopulationData>,
    start_year: i32,
    end_year: i32,
    asean_countries: &Vec<String>,
    saarc_countries: &Vec<String>,
) -> Result<(), Box<dyn Error>> {
    let problem1_solution = analyze::problem1(&population_data, start_year, end_year);
    gen_2d_plot(problem1_solution, "gnuplots/problem1.dat");

    let problem2_solution = analyze::problem2(&population_data, 2015, asean_countries);
    gen_2d_plot(problem2_solution, "gnuplots/problem2.dat");

    let problem3_solution =
        analyze::problem3(&population_data, start_year, end_year, saarc_countries);
    gen_2d_plot(problem3_solution, "gnuplots/problem3.dat");

    let problem4_solution =
        analyze::problem4(&population_data, start_year, end_year, asean_countries);
    gen_grouped_2d_plot(problem4_solution, "gnuplots/problem4.dat");

    Ok(())
}
