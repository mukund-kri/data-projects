// Problem 3: Population growth in ASEAN countries from 2000 to 2015.

function plot_problem3() {
    // Get json data for problem 3 and plot
    var xhr = new XMLHttpRequest();

    // on success, just print the data
    xhr.onload = function () {
        var data = JSON.parse(this.responseText);
        let title = `Population growth in ASEAN countries from ${data.start_year} to ${data.end_year}`;
        Highcharts.chart('problem3', {
            chart: {
                type: 'line',
            },
            title: {
                text: title,
                align: 'left',
            },
            xAxis: {
                categories: data.years,
                title: {
                    text: 'Year',
                },
            },
            yAxis: {
                title: {
                    text: 'Population',
                },
            },
            series: [{
                name: 'Population',
                data: data.population,
            }]
        });
    };

    xhr.open("GET", "problem3.json", true);
    xhr.send();
}

// call plot_problem3 when the document is loaded   
document.addEventListener('DOMContentLoaded', function () {
    plot_problem3();
});