// Problem 2: Population distribution in ASEAN countries in 2015.

function plot_problem2() {
    // Get json data for problem 2 and plot
    var xhr = new XMLHttpRequest();

    // on success, just print the data
    xhr.onload = function () {
        var data = JSON.parse(this.responseText);
        let title = `Population distribution in ASEAN countries in ${data.selected_year}`;
        Highcharts.chart('problem2', {
            chart: {
                type: 'bar',
            },
            title: {
                text: title,
                align: 'left',
            },
            xAxis: {
                categories: data.countries,
                title: {
                    text: 'Country',
                },
            },
            yAxis: {
                title: {
                    text: 'Population',
                },
            },
            series: [{
                name: 'Population',
                data: data.population,
            }]
        });
    };

    xhr.open("GET", "problem2.json", true);
    xhr.send();
}

// call plot_problem2 when the document is loaded
document.addEventListener('DOMContentLoaded', function () {
    plot_problem2();
});