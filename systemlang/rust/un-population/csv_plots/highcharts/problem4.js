// Problem 4: Grouped plot of ASEAN contries from 2000 to 2015

function plot_problem4() {
    // Get json data for problem 4 and plot
    var xhr = new XMLHttpRequest();

    // on success, just print the data
    xhr.onload = function () {
        let data = JSON.parse(this.responseText);
        let title = `Grouped plot of ASEAN contries from ${data.start_year} to ${data.end_year}`;
        Highcharts.chart('problem4', {
            chart: {
                type: 'bar',
            },
            title: {
                text: title,
                align: 'left',
            },
            xAxis: {
                categories: data.categories,
                title: {
                    text: 'Year',
                },
            },
            yAxis: {
                title: {
                    text: 'Population',
                },
            },
            series: data.series
        });
    };

    xhr.open("GET", "problem4.json", true);
    xhr.send();
}

document.addEventListener('DOMContentLoaded', function () {
    plot_problem4();
});