function plot_problem1() {
    // Get json data for problem 1 and plot
    var xhr = new XMLHttpRequest();

    // on success, just print the data
    xhr.onload = function () {
        var data = JSON.parse(this.responseText);

        // Title of the chart interpolated with year
        let title = `India Population Growth ${data.start_year} - ${data.end_year}`;
        Highcharts.chart('problem1', {
            chart: {
                type: 'bar',
            },
            title: {
                text: title,
                align: 'left',
            },
            xAxis: {
                categories: data.years,
                title: {
                    text: 'Year',
                },
            },
            series: [{
                name: 'Population',
                data: data.population,
            }]
        });
    };

    xhr.open("GET", "problem1.json", true);
    xhr.send();
}


document.addEventListener('DOMContentLoaded', function () {
    plot_problem1();
});