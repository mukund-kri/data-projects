## Improvements over previous version

1. Use modules to better organize the code
2. Functions commented to the best of my ability
3. Better use of generics, especially in the `gunplot` module
4. More efficient loading of data, by loading the data only once and then passing it 
   to the functions that need it.

## Source data

The code expects the following raw data in the `../data` folder. ie. `data` folder
in the parent director the project folder.

## Running the example

```bash
cargo run
```

