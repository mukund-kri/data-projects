use std::error::Error;

use console::Term;
use dialoguer::{theme::ColorfulTheme, Select};

use with_diesel::{problem1, problem2, problem3, problem4};

fn main() -> Result<(), Box<dyn Error>> {
    let items = &[
        "Problem 1",
        "Problem 2",
        "Problem 3",
        "Problem 4",
        "All Problems",
    ];
    let term = Term::buffered_stderr();
    let theme = ColorfulTheme::default();

    let selection = Select::with_theme(&theme)
        .with_prompt("Select The data problem to run ...")
        .items(items)
        .default(4)
        .interact_on(&term)
        .unwrap();

    let _ = match selection {
        0 => {
            println!("Running Problem 1");
            problem1(1995, 2015)?;
        }
        1 => {
            println!("Running Problem 2");
            problem2(1995)?;
        }
        2 => {
            println!("Running Problem 3");
            problem3(1995, 2015)?;
        }
        3 => {
            println!("Running Problem 4");
            problem4(1995, 2015)?;
        }
        4 => {
            println!("Running All Problems");
            problem1(1995, 2015)?;
            problem2(1995)?;
            problem3(1995, 2015)?;
            problem4(1995, 2015)?;
        }
        _ => {
            println!("Invalid Selection");
        }
    };

    Ok(())
}
