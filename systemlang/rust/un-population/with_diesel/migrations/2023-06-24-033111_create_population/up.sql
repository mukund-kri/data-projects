CREATE TABLE country_stats (
    id SERIAL PRIMARY KEY,
    country VARCHAR(255) NOT NULL,
    year INT NOT NULL,
    population DOUBLE PRECISION NOT NULL
);