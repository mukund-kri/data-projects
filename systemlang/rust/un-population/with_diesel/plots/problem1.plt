# Setup the title
set title "Bar chart of Indian population over the years" font ",18"

# Setup the x-axis
set xlabel "Year" font ",14"
set xtics rotate by -45 scale 0

# Plot the data
plot "problem1.dat" using 2: xtic(1) with histogram title "Population"
