# Setup title and labels
set title "ASEAN population from 2000 to 2015" font ", 18"
set xlabel "Country" font ", 14"
set ylabel "Population (in Hundred Thousand)" font ", 14"

set style data histogram
set style histogram cluster gap 1

set style fill solid border rgb "black"
set xtics rotate by 90 right
set auto x
set yrange [0:*]
plot for [i=2:15] 'problem4.dat' using i:xtic(1) title col
