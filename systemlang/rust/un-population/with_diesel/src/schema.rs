// @generated automatically by Diesel CLI.

diesel::table! {
    country_stats (id) {
        id -> Int4,
        #[max_length = 255]
        country -> Varchar,
        year -> Int4,
        population -> Float8,
    }
}
