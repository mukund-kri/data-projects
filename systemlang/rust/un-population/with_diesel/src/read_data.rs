use std::fs::read_to_string;

use csv::Reader;

use crate::models::*;

/// Loads and returns a vector of countries from a text file
///
/// # Arguments
///
/// * `file_name` - (str) Name of the text file with country names
///
/// # returns
///
/// Vec<String>
pub fn load_countries(file_name: &str) -> Vec<String> {
    let mut countries: Vec<String> = Vec::new();

    for line in read_to_string(file_name).unwrap().lines() {
        countries.push(line.to_string())
    }

    countries
}

/// Load and filters the raw data from the the CSV file.
/// It does the following ...
/// 1. Read the CSV file and load into memory in the form of a collection `Record`s.
/// 2. Filter out the data that is not required for this analysis. In this case
///    we need specific countries
#[allow(dead_code)]
pub fn load_data(file_name: &str, countries: &Vec<String>) -> Vec<CountryStatCsv> {
    let mut result: Vec<CountryStatCsv> = Vec::new();

    let mut rdr = Reader::from_path(file_name).expect("File not found");
    for datum in rdr.deserialize() {
        let record: CountryStatCsv = datum.expect("Failed to deserialize row");

        if countries.contains(&record.country) {
            result.push(record);
        }
    }

    result
}
