// Models for database access
use crate::schema::country_stats;
use diesel::prelude::*;
use serde::Deserialize;

/// Represents a record in the CSV file
#[derive(Debug, Deserialize)]
pub struct CountryStatCsv {
    #[serde(rename = "Region")]
    pub country: String,
    #[serde(rename = "Year")]
    pub year: i32,
    #[serde(rename = "Population")]
    pub population: f64,
}

impl CountryStatCsv {
    /// Convert the CSV record into a NewCountryStat record which can be inserted into
    /// the database
    pub fn to_db_record(&self) -> NewCountryStat {
        NewCountryStat {
            country: &self.country,
            year: self.year,
            population: self.population,
        }
    }
}

/// Data structure to insert a country statistic into the database
#[derive(Insertable)]
#[diesel(table_name = country_stats)]
pub struct NewCountryStat<'a> {
    pub country: &'a str,
    pub year: i32,
    pub population: f64,
}

/// Struct to represent a country statistic in the database
#[derive(Debug, Queryable, AsChangeset)]
#[diesel(table_name = country_stats)]
pub struct CountryStat {
    pub id: i32,
    pub country: String,
    pub year: i32,
    pub population: f64,
}
