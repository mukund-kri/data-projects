use with_diesel::csv_to_db;

/// This binary is just a thin wrapper around the csv_to_db function in the library.
fn main() {
    csv_to_db("data/un-population.csv");
}
