/// The entry point to solving the 4 data problems.
use with_diesel::{problem1, problem2, problem3, problem4};

fn main() {
    let _ = problem1(1995, 2015);
    let _ = problem2(2015);
    let _ = problem3(1995, 2015);
    let _ = problem4(1995, 2015);
}
