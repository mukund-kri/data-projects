// Common db functions go here
use std::env;

use diesel::prelude::*;
use diesel::PgConnection;
use dotenv::dotenv;

/// Get a connection to the Postgres database. It first reads the DATABASE_URL env
/// variable from the .env file and uses it to get a connection to the specified Postgres
/// database.
///
/// # Returns
///
/// * `PgConnection` - Connection to the Postgres database
pub fn connect() -> PgConnection {
    dotenv().ok();
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url).expect(&format!("Error connecting to {}", database_url))
}
