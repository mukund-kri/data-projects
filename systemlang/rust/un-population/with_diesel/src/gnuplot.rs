use std::fmt::Display;
use std::fs::File;
use std::io::Write;

use indexmap::IndexMap;

/// This function generates a 2D bar plot from a dictionary of data and writes it to a
/// file in a from that can be used by gnuplot.
///
/// # Arguments
///
/// * `data` - The data to plot
/// * `dat_file` - Name of the file to write the data to
///
pub fn gen_2d_plot<T: Display, U: Display>(data: IndexMap<T, U>, dat_file: &str) {
    let mut plot_file = File::create(dat_file).expect("Failed to create data file");
    for (year, population) in data {
        let _ = writeln!(plot_file, "{year} {population}");
    }
}

/// This function generates a 2D plot for a grouped data
/// The data is a map of country name to a map of year to population
///
/// * `data` - The grouped data
/// * `dat_file` - The file to write the data to
///
pub fn gen_grouped_2d_plot<T: Display, U: Display, V: Display>(
    data: IndexMap<T, IndexMap<U, V>>,
    dat_file: &str,
) {
    let mut plot_file = File::create(dat_file).expect("Failed to create data file");

    for year in 1995..=2015 {
        let _ = write!(plot_file, "{}\t", year);
    }
    let _ = writeln!(plot_file, "");

    for (country, country_data) in data {
        let _ = write!(plot_file, "{}\t", country);

        for (_, population) in country_data {
            let _ = write!(plot_file, "{population}\t");
        }
        let _ = writeln!(plot_file, "");
    }
}
