mod db;
mod gnuplot;
mod models;
mod read_data;
mod schema;

use std::error::Error;

use diesel::prelude::*;
//use diesel::query_dsl::methods::GroupByDsl;
use indexmap::IndexMap;

use crate::gnuplot::{gen_2d_plot, gen_grouped_2d_plot};
use crate::models::{CountryStat, CountryStatCsv, NewCountryStat};
use crate::read_data::*;

/// Load the contents of the CSV file into the Postgres database
///
/// # Arguments
///
/// * `csv_file` - (str) Name of the CSV file
///
pub fn csv_to_db(csv_file: &str) {
    // Load country names
    let asean_countries = load_countries("data/asean-countries.txt");
    let saarc_countries: Vec<String> = load_countries("data/saarc-countries.txt");

    let mut filter_countries: Vec<String> = Vec::new();
    filter_countries.extend(asean_countries.clone());
    filter_countries.extend(saarc_countries.clone());

    let mut result: Vec<CountryStatCsv> = Vec::new();

    let mut rdr = csv::Reader::from_path(csv_file).expect("File not found");
    for datum in rdr.deserialize() {
        let record: CountryStatCsv = datum.expect("Failed to deserialize row");

        if filter_countries.contains(&record.country) {
            result.push(record);
        }
    }

    // use map method to convert ContryStatCsv to NewCountryStat
    let insertable_stats = result
        .iter()
        .map(|x| x.to_db_record())
        .collect::<Vec<NewCountryStat>>();

    // Now the database part
    let mut conection = db::connect();
    diesel::insert_into(schema::country_stats::table)
        .values(&insertable_stats)
        .execute(&mut conection)
        .expect("Error saving new country stat");
}

/// Get all the records for India between the start and end years.
pub fn problem1(start: i32, end: i32) -> Result<(), Box<dyn Error>> {
    use crate::schema::country_stats::dsl::*;

    let mut connection = db::connect();
    let result: Vec<CountryStat> = country_stats
        .filter(country.eq("India"))
        .filter(year.ge(start))
        .filter(year.le(end))
        .load::<models::CountryStat>(&mut connection)
        .expect("Error loading country stats");

    let solution: IndexMap<i32, f64> = result
        .iter()
        .map(|x| (x.year, x.population))
        .collect::<IndexMap<i32, f64>>();

    gen_2d_plot(solution, "plots/problem1.dat");

    Ok(())
}

/// problem 2: Plot of population of ASEAN countries in given year
///
/// # Arguments
///
/// * `year` - (i32) The year to analyze
///
pub fn problem2(target_year: i32) -> Result<(), Box<dyn Error>> {
    use crate::schema::country_stats::dsl::*;
    let asean_countries = load_countries("data/asean-countries.txt");
    let mut connection = db::connect();
    let result: Vec<CountryStat> = country_stats
        .filter(country.eq_any(&asean_countries))
        .filter(year.eq(target_year))
        .load::<models::CountryStat>(&mut connection)
        .expect("Error loading country stats");

    let solution: IndexMap<String, f64> = result
        .iter()
        .map(|x| (x.country.clone(), x.population))
        .collect::<IndexMap<String, f64>>();

    gen_2d_plot(solution, "plots/problem2.dat");

    Ok(())
}

/// Problem 3: Plot the sum of population of SAARC countries between `start` and
/// `end` years.
///
/// # Arguments
///
/// * `start` - (i32) The start year
/// * `end` - (i32) The end year
///
pub fn problem3(start: i32, end: i32) -> Result<(), Box<dyn Error>> {
    use crate::schema::country_stats::dsl::*;

    // use diesel::sql_types::*;

    let saarc_countries = load_countries("data/saarc-countries.txt");
    let mut connection = db::connect();
    let query = country_stats
        .group_by(year)
        .select((
            year,
            diesel::dsl::sql::<diesel::sql_types::Double>("sum(population)"),
        ))
        .filter(country.eq_any(&saarc_countries))
        .filter(year.ge(start))
        .filter(year.le(end))
        .order(year);
    let result = query.load::<(i32, f64)>(&mut connection)?;

    let solution: IndexMap<i32, f64> = IndexMap::from_iter(result);
    gen_2d_plot(solution, "plots/problem3.dat");

    Ok(())
}

/// Problem 4: Plot the population of ASEAN countries between `start` and `end`
///
/// # Arguments
///
/// * `start` - (i32) The start year
/// * `end` - (i32) The end year
///
pub fn problem4(start: i32, end: i32) -> Result<(), Box<dyn Error>> {
    use crate::schema::country_stats::dsl::*;

    let asean_countries = load_countries("data/asean-countries.txt");
    let mut connection = db::connect();

    let query = country_stats
        .group_by((year, country))
        .select((
            year,
            country,
            diesel::dsl::sql::<diesel::sql_types::Double>("sum(population)"),
        ))
        .filter(country.eq_any(&asean_countries))
        .filter(year.ge(start))
        .filter(year.le(end))
        .order((year, country));

    let result = query.load::<(i32, String, f64)>(&mut connection)?;

    let mut solution: IndexMap<i32, IndexMap<String, f64>> = IndexMap::new();
    for (dyear, dcountry, dpopulation) in result {
        if !solution.contains_key(&dyear) {
            solution.insert(dyear, IndexMap::new());
        }
        solution
            .get_mut(&dyear)
            .unwrap()
            .insert(dcountry, dpopulation);
    }

    gen_grouped_2d_plot(solution, "plots/problem4.dat");

    Ok(())
}

/// Insert a new country into the database
/// This is here just to check DB access and insertion. It has no real use.
pub fn insert_country() {
    // Create and insert a new counter stat
    let new_country_stat = models::NewCountryStat {
        country: "United States",
        year: 2017,
        population: 325700000.0,
    };

    // import the generated dsl
    // use crate::schema::country_stats::dsl::*;

    let mut connection = db::connect();
    diesel::insert_into(schema::country_stats::table)
        .values(&new_country_stat)
        .execute(&mut connection)
        .expect("Error saving new country stat");
}
