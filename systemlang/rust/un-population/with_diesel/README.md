# UN Population project with Diesel ORM

## Setup

First, we need to create a database called `population` and it corresponding owner
`population` in postgres. The script `create_user_db.sql` does this.

Create database and user:

```bash
sudo -u postgres psql << create_user_db.sql
```

### Installing diesel_cli

The diesel_cli is a tool that helps us to manage the database schema and migrations.

```bash
cargo install diesel_cli --no-default-features --features postgres
```

Ref: https://diesel.rs/guides/getting-started

### applying migrations

```bash
diesel migration run
```

## Running

### Loading data from CSV to postgres

```bash
cargo run --bin loader
```

### Generating plots

In this version, gnuplot dat files are generated in the `data` directory. You can then use gnuplot to generate the plots.

To generate the plots, run:

```bash
cargo run --bin main
```

## Viewing the plots

`gnuplot` is required to view the plots. You can install it using:

```bash
sudo apt-get install gnuplot
```

Then, log into the `gnuplot` console and run the following command:

```bash
load 'problem1.plt'
```

to view the plot for problem 1. Just change the filename to view the other plots.


