-- UN population data
CREATE TABLE population_stats (
  id SERIAL PRIMARY KEY,
  country VARCHAR(255) NOT NULL,
  year INTEGER NOT NULL,
  population BIGINT NOT NULL
);
