-- Create the country_grouping table
CREATE TABLE country_grouping (
  id SERIAL PRIMARY KEY,
  country VARCHAR(255) NOT NULL,
  grouping VARCHAR(255) NOT NULL
);