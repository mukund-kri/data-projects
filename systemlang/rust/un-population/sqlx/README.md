# rust/un-population sqlx version

Generates gnuplot dat files and highcharts json files from the UN population data.
The source of data this time is a postgres database instead of a csv file.

## Loaders
This project also provides loaders for the UN population data and the country
groupings.

