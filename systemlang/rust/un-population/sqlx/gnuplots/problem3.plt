# Setup the title
set title "Plot of population of SAARC countries over the years" font ", 18"

# Setup the x-axis label
set xlabel "Year" font ", 14"
set xtics rotate by -45 scale 0

# Plot the data
plot "problem3.dat" using 2: xtic(1) with histogram title "SAARC population"
