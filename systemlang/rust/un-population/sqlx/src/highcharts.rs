use std::error::Error;
use std::io;

use indexmap::IndexMap;
use serde_json::{self, json};
use sqlx::{Pool, Postgres};
use std::fs::File;

use bigdecimal::ToPrimitive;

use crate::analytics::{problem1, problem2, problem3, problem4};

pub async fn genplot1(
    conn: &Pool<Postgres>,
    start_year: i32,
    end_year: i32,
) -> Result<(), Box<dyn Error>> {
    let result = problem1(conn, start_year, end_year).await?;

    write_plot1_data(result, start_year, end_year)?;

    Ok(())
}

fn write_plot1_data(data: Vec<(i32, i64)>, start_year: i32, end_year: i32) -> io::Result<()> {
    let (years, population): (Vec<_>, Vec<_>) = data.iter().cloned().unzip();

    // Generate JSON
    let json_data = json!({
        "years": years,
        "population": population,
        "start_year": start_year,
        "end_year": end_year
    });

    // write json data to file
    let file = File::create("plots/unpopulation/highcharts/problem1.json")?;
    serde_json::to_writer_pretty(&file, &json_data)?;

    Ok(())
}

pub async fn genplot2(conn: &Pool<Postgres>, selected_year: i32) -> Result<(), Box<dyn Error>> {
    let result = problem2(conn, selected_year).await?;

    write_plot2_data(result, selected_year)?;
    Ok(())
}

fn write_plot2_data(data: Vec<(String, i64)>, selected_year: i32) -> io::Result<()> {
    let (countries, population): (Vec<_>, Vec<_>) = data.iter().cloned().unzip();

    // Generate JSON
    let json_data = json!({
        "countries": countries,
        "population": population,
        "selected_year": selected_year
    });

    // write json data to file
    let file = File::create("plots/unpopulation/highcharts/problem2.json")?;
    serde_json::to_writer_pretty(&file, &json_data)?;

    Ok(())
}

pub async fn genplot3(
    conn: &Pool<Postgres>,
    start_year: i32,
    end_year: i32,
) -> Result<(), Box<dyn Error>> {
    let result = problem3(conn, start_year, end_year).await?;

    write_plot3_data(result, start_year, end_year)?;
    Ok(())
}

fn write_plot3_data(
    data: Vec<(i32, sqlx::types::BigDecimal)>,
    start_year: i32,
    end_year: i32,
) -> io::Result<()> {
    let (years, population): (Vec<_>, Vec<_>) = data.iter().cloned().unzip();

    // Convert to i128
    let population: Vec<_> = population.iter().map(|x| x.to_i128().unwrap()).collect();

    // Generate JSON
    let json_data = json!({
        "years": years,
        "population": population,
        "start_year": start_year,
        "end_year": end_year
    });

    // write json data to file
    let file = File::create("plots/unpopulation/highcharts/problem3.json")?;
    serde_json::to_writer_pretty(&file, &json_data)?;

    Ok(())
}

pub async fn genplot4(
    conn: &Pool<Postgres>,
    start_year: i32,
    end_year: i32,
) -> Result<(), Box<dyn Error>> {
    let result = problem4(conn, start_year, end_year).await?;

    write_plot4_data(&result, start_year, end_year)?;
    Ok(())
}

fn write_plot4_data(
    data: &IndexMap<String, IndexMap<i32, i64>>,
    start_year: i32,
    end_year: i32,
) -> io::Result<()> {
    // The categories are the years of the analysis
    let categories: Vec<String> = (start_year..=end_year).map(|x| x.to_string()).collect();

    // The series data is the population growth of each country over the years to json
    let series = data
        .iter()
        .map(|(country, data)| {
            json!({
                "name": country,
                "data": data.values().collect::<Vec<&i64>>()
            })
        })
        .collect::<Vec<serde_json::Value>>();

    // Generate JSON
    let json_data = json!({
        "categories": categories,
        "series": series,
        "start_year": start_year,
        "end_year": end_year
    });

    // write json data to file
    let file = File::create("plots/unpopulation/highcharts/problem4.json")?;
    serde_json::to_writer_pretty(&file, &json_data)?;

    Ok(())
}

pub async fn all_problems(
    conn: &Pool<Postgres>,
    selected_year: i32,
    start_year: i32,
    end_year: i32,
) -> Result<(), Box<dyn Error>> {
    // Call the othere plotting functions
    genplot1(conn, start_year, end_year).await?;
    genplot2(conn, selected_year).await?;
    genplot3(conn, start_year, end_year).await?;
    genplot4(conn, start_year, end_year).await?;
    Ok(())
}
