use std::error::Error;
use std::fs::read_to_string;

use sqlx::{Postgres, QueryBuilder};

use crate::utils::{self, get_connection};
use crate::PopulationData;

/// Read a text file in return the contents as Vec of strings
/// Loads and returns a vector of countries from a text file
///
/// # Arguments
///
/// * `file_name` - (str) Name of the text file with country names
///
/// # returns
///
/// Vec<String>
fn load_countries(file_name: &str) -> Vec<String> {
    let mut countries: Vec<String> = Vec::new();

    for line in read_to_string(file_name).unwrap().lines() {
        countries.push(line.to_string())
    }

    countries
}

// Read the country grouping text files and load into db
pub async fn load_grouping() -> Result<(), Box<dyn Error>> {
    println!("Loading country grouping data");

    let asean: Vec<String> = load_countries("../data/asean-countries.txt");
    let asean: Vec<(&String, &str)> = asean.iter().map(|country| (country, "asean")).collect();

    let saarc = load_countries("../data/saarc-countries.txt");
    let saarc: Vec<(&String, &str)> = saarc.iter().map(|country| (country, "saarc")).collect();

    // Let's connect to the database
    let pool = get_connection().await?;

    // Use sqlx query builder to load all the groupings
    // First do the ASEAN countries
    let mut builder: QueryBuilder<Postgres> =
        QueryBuilder::new("INSERT INTO country_grouping (country, grouping) ");

    builder.push_values(asean, |mut b, country| {
        b.push(format!("'{}'", country.0));
        b.push(format!("'{}'", country.1));
    });
    let query = builder.build();
    query.execute(&pool).await?;

    // Now do the SAARC countries
    let mut builder: QueryBuilder<Postgres> =
        QueryBuilder::new("INSERT INTO country_grouping (country, grouping) ");

    builder.push_values(saarc, |mut b, country| {
        b.push(format!("'{}'", country.0));
        b.push(format!("'{}'", country.1));
    });
    let query = builder.build();
    query.execute(&pool).await?;

    Ok(())
}

/// Read a csv file and return the contents as Vec of PopulationData
///
/// # Arguments
///
/// * `file_name` - (str) Name of the csv file with population data
///
/// # returns
///
/// Vec<PopulationData>
fn load_population_data(file_name: &str) -> Result<Vec<PopulationData>, Box<dyn Error>> {
    let mut population_data: Vec<PopulationData> = Vec::new();

    let mut rdr = csv::Reader::from_path(file_name)?;
    for result in rdr.deserialize() {
        let mut record: PopulationData = result?;
        // correct the country name
        if record.country == "Lao People's Democratic Republic" {
            record.country = "Laos".to_string();
        }
        if record.country == "Côte d'Ivoire" {
            record.country = "Ivory Coast".to_string();
        }
        if record.country == "Dem. People's Republic of Korea" {
            record.country = "North Korea".to_string();
        }
        population_data.push(record);
    }

    Ok(population_data)
}

/// Load the population data given as Vec<PopulationData> into the database
///
/// # Arguments
///&
/// * `population_data` - (Vec<PopulationData>) Population data to load into the database
async fn load_population_data_into_db(
    population_data: Vec<PopulationData>,
) -> Result<(), Box<dyn Error>> {
    // Let's connect to the database
    let pool = utils::get_connection().await?;

    // Use sqlx query builder to load all the population data
    let mut builder: QueryBuilder<Postgres> =
        QueryBuilder::new("INSERT INTO population_stats (country, year, population) ");

    builder.push_values(population_data, |mut b, data| {
        b.push(format!("'{}'", data.country));
        b.push(format!("{}", data.year));
        b.push(format!("{}", data.population));
    });
    let query = builder.build();
    query.execute(&pool).await?;

    Ok(())
}

/// Load the population data from the csv file into the database
/// This function reads the csv file and loads the data into the database
/// The csv file is expected to have the following columns:
/// Region, Year, Population
pub async fn load_population(file_name: &str) -> Result<(), Box<dyn Error>> {
    println!("Loading population data");

    let population_data = load_population_data(file_name)?;
    load_population_data_into_db(population_data).await?;

    Ok(())
}
