pub mod analytics;
pub mod gnuplot;
pub mod highcharts;
pub mod loading;
pub mod utils;

use serde::Deserialize;

/// Represents each line of the csv data. It has only the required fields.
#[derive(Debug, Deserialize)]
pub struct PopulationData {
    #[serde(rename = "Region")]
    pub country: String,
    #[serde(rename = "Year")]
    pub year: i32,
    #[serde(rename = "Population")]
    pub population: f64,
}

/// Entry point for the application. This function takes care of the actual plot data generation
/// Based on the user input.
/// # Arguments
/// * `plotting_system` - (usize) Index of the plotting system to use
/// * `problem` - (usize) Index of the problem to solve
/// * `selected_year` - (i32) Selected year for the analysis
/// * `start_year` - (i32) Start year for the analysis
/// * `end_year` - (i32) End year for the analysis
pub async fn run_plots(
    plotting_system: usize,
    problem: usize,
    selected_year: i32,
    start_year: i32,
    end_year: i32,
) -> Result<(), Box<dyn std::error::Error>> {
    let conn = utils::get_connection().await?;

    match plotting_system {
        0 => match problem {
            0 => gnuplot::all_problems(&conn, selected_year, start_year, end_year).await?,
            1 => gnuplot::genplot1(&conn, start_year, end_year).await?,
            2 => gnuplot::genplot2(&conn, selected_year).await?,
            3 => gnuplot::genplot3(&conn, start_year, end_year).await?,
            4 => gnuplot::genplot4(&conn, start_year, end_year).await?,
            _ => println!("Invalid problem selection"),
        },
        1 => match problem {
            0 => highcharts::all_problems(&conn, selected_year, start_year, end_year).await?,
            1 => highcharts::genplot1(&conn, start_year, end_year).await?,
            2 => highcharts::genplot2(&conn, selected_year).await?,
            3 => highcharts::genplot3(&conn, start_year, end_year).await?,
            4 => highcharts::genplot4(&conn, start_year, end_year).await?,
            _ => println!("Invalid problem selection"),
        },
        _ => println!("Invalid plotting system selection"),
    }

    Ok(())
}
