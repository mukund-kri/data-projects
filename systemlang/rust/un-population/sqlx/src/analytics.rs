use sqlx::{types::BigDecimal, Postgres};

pub async fn problem1(
    conn: &sqlx::Pool<Postgres>,
    start_year: i32,
    end_year: i32,
) -> sqlx::Result<Vec<(i32, i64)>> {
    // Write the SQL query

    // Execute the query
    let result: Vec<(i32, i64)> = sqlx::query_as(
        r#"SELECT year, population FROM population_stats 
            WHERE country='India'
            AND year BETWEEN $1 AND $2"#,
    )
    .bind(start_year)
    .bind(end_year)
    .fetch_all(conn)
    .await?;

    // Return the result
    Ok(result)
}

/// Problem 2 analytics. Produce data for population distribution of ASEAN countries in a given year
/// # Arguments
/// * `conn` - The database connection
/// * `year` - The year for which the distribution is to be plotted
/// # Returns
/// A vector of tuples containing the country name and the population
/// of the country in the given year
pub async fn problem2(conn: &sqlx::Pool<Postgres>, year: i32) -> sqlx::Result<Vec<(String, i64)>> {
    // Write the SQL query

    // Execute the query
    let result: Vec<(String, i64)> = sqlx::query_as(
        r#"SELECT country, population FROM population_stats 
            WHERE year=$1 AND country IN 
            (SELECT country FROM country_grouping WHERE grouping='asean')"#,
    )
    .bind(year)
    .fetch_all(conn)
    .await?;

    // Return the result
    Ok(result)
}

/// Problem 3 analytics. Produce data for total population distribution of ASEAN countries over
/// a range of years.
/// # Arguments
/// * `conn` - The database connection
/// * `start_year` - The start year of the range
/// * `end_year` - The end year of the range
/// # Returns
/// A vector of tuples containing the year and the total population of ASEAN countries
/// over the range of years
pub async fn problem3(
    conn: &sqlx::Pool<Postgres>,
    start_year: i32,
    end_year: i32,
) -> sqlx::Result<Vec<(i32, BigDecimal)>> {
    // Write the SQL query

    // Execute the query
    let result: Vec<(i32, BigDecimal)> = sqlx::query_as(
        r#"SELECT year, SUM(population) FROM population_stats 
            WHERE year BETWEEN $1 AND $2 AND country IN 
            (SELECT country FROM country_grouping WHERE grouping='asean')
            GROUP BY year"#,
    )
    .bind(start_year)
    .bind(end_year)
    .fetch_all(conn)
    .await?;

    // Return the result
    Ok(result)
}

/// Problem 4 analytics. Produce data for the population distribution of ASEAN countries for
/// a range of years.
/// # Arguments
/// * `conn` - The database connection
/// * `start_year` - The year for which the distribution is to be plotted
/// * `end_year` - The year for which the distribution is to be plotted
/// # Returns
/// An IndexMap of String, (IndexMap of i32, i64). The String key is the country name and the
/// inner IndexMap is the year and the population of the country in that year.
pub async fn problem4(
    conn: &sqlx::Pool<Postgres>,
    start_year: i32,
    end_year: i32,
) -> sqlx::Result<indexmap::IndexMap<String, indexmap::IndexMap<i32, i64>>> {
    // Write the SQL query

    // Execute the query
    let result: Vec<(String, i32, i64)> = sqlx::query_as(
        r#"SELECT country, year, population FROM population_stats 
            WHERE year BETWEEN $1 AND $2 AND country IN 
            (SELECT country FROM country_grouping WHERE grouping='asean')"#,
    )
    .bind(start_year)
    .bind(end_year)
    .fetch_all(conn)
    .await?;

    // Create an IndexMap to store the result
    let mut data = indexmap::IndexMap::new();

    // Iterate over the result and populate the IndexMap
    for row in result {
        let country = data.entry(row.0).or_insert_with(indexmap::IndexMap::new);
        country.insert(row.1, row.2);
    }

    // Return the result
    Ok(data)
}

// Tests
#[cfg(test)]
mod tests {

    use sqlx::Pool;

    use super::*;

    // Test the first problem
    #[sqlx::test(fixtures("population_stats"))]
    async fn test_problem1(pool: Pool<Postgres>) -> sqlx::Result<()> {
        let result = problem1(&pool, 2019, 2020).await?;

        assert_eq!(result.len(), 2);
        assert_eq!(result[0].1, 1366417754);

        Ok(())
    }

    // Test the second problem
    #[sqlx::test(fixtures("population_stats", "grouping"))]
    async fn test_problem2(pool: Pool<Postgres>) -> sqlx::Result<()> {
        let result = problem2(&pool, 2015).await?;

        assert_eq!(result.len(), 2);
        assert_eq!(result[0].1, 32365999);

        Ok(())
    }

    // Test the third problem
    #[sqlx::test(fixtures("population_stats", "grouping"))]
    async fn test_problem3(pool: Pool<Postgres>) -> sqlx::Result<()> {
        let result = problem3(&pool, 2015, 2016).await?;

        assert_eq!(result.len(), 2);
        assert_eq!(result[0].1, 38216341.into());

        Ok(())
    }
}
