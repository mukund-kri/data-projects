use std::env::var;
use std::error::Error;

use dotenvy::dotenv;
use sqlx::{PgPool, Pool, Postgres};

pub async fn get_connection() -> Result<Pool<Postgres>, Box<dyn Error>> {
    dotenv().ok();

    // get the database url from the environment
    let database_url = var("DATABASE_URL")?;

    // Create db connection
    let pool = PgPool::connect(&database_url).await?;

    Ok(pool)
}
