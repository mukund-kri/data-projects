use std::error::Error;
use std::fs::File;
use std::io::{Result, Write};

use sqlx::types::BigDecimal;

use crate::analytics;

/// Generate the first plot. India population over a range of years
///
/// # Arguments
/// * `conn` - The database connection
/// * `start_year` - The start year of the range
/// * `end_year` - The end year of the range
pub async fn genplot1(
    conn: &sqlx::Pool<sqlx::Postgres>,
    start_year: i32,
    end_year: i32,
) -> sqlx::Result<(), Box<dyn Error>> {
    // Call analytics::problem1 to get the plot data
    let result = analytics::problem1(&conn, start_year, end_year).await?;

    // Write the plot data to a file
    write_plot1_data(&result, "gnuplots/problem1.dat")?;

    // Return the result
    Ok(())
}

// Write the plot data in gnuplot format to file
fn write_plot1_data(data: &Vec<(i32, i64)>, filename: &str) -> Result<()> {
    let mut file = File::create(filename)?;

    for row in data {
        writeln!(file, "{} {}", row.0, row.1)?;
    }

    Ok(())
}

/// Generate the second plot. Plot the population distribution of ASEAN countries in
/// a given year
///
/// # Arguments
/// * `conn` - The database connection
/// * `year` - The year for which the distribution is to be plotted
pub async fn genplot2(
    conn: &sqlx::Pool<sqlx::Postgres>,
    year: i32,
) -> sqlx::Result<(), Box<dyn Error>> {
    // Call analytics::problem2 to get the plot data
    let result: Vec<(String, i64)> = analytics::problem2(&conn, year).await?;

    // Write the plot data to a file
    write_plot2_data(&result, "gnuplots/problem2.dat")?;

    // Return the result
    Ok(())
}

// Write the plot data in gnuplot format to file
fn write_plot2_data(data: &Vec<(String, i64)>, filename: &str) -> Result<()> {
    let mut file = File::create(filename)?;

    for row in data {
        writeln!(file, "{} {}", row.0, row.1)?;
    }

    Ok(())
}

/// Generate the third plot. Plot the total population distribution of ASEAN countries over a
/// range of years.
/// # Arguments
/// * `conn` - The database connection
/// * `start_year` - The start year of the range
/// * `end_year` - The end year of the range
/// # Returns
/// A vector of tuples containing the year and the total population of ASEAN countries
/// in that year
pub async fn genplot3(
    conn: &sqlx::Pool<sqlx::Postgres>,
    start_year: i32,
    end_year: i32,
) -> sqlx::Result<(), Box<dyn Error>> {
    // Call analytics::problem3 to get the plot data
    let result = analytics::problem3(&conn, start_year, end_year).await?;

    // Write the plot data to a file
    write_plot3_data(&result, "gnuplots/problem3.dat")?;

    // Return the result
    Ok(())
}

// Write the plot data in gnuplot format to file
fn write_plot3_data(data: &Vec<(i32, BigDecimal)>, filename: &str) -> Result<()> {
    let mut file = File::create(filename)?;

    for row in data {
        writeln!(file, "{} {}", row.0, row.1)?;
    }

    Ok(())
}

/// Generate the fourth plot. Plot the population distribution of ASEAN countries over a
/// range of years. This produces a grouped bar chart.
/// # Arguments
/// * `conn` - The database connection
/// * `start_year` - The start year of the range
/// * `end_year` - The end year of the range
pub async fn genplot4(
    conn: &sqlx::Pool<sqlx::Postgres>,
    start_year: i32,
    end_year: i32,
) -> sqlx::Result<(), Box<dyn Error>> {
    // Call analytics::problem4 to get the plot data
    let result = analytics::problem4(&conn, start_year, end_year).await?;

    // Write the plot data to a file
    write_plot4_data(&result, "gnuplots/problem4.dat", start_year, end_year)?;

    // Return the result
    Ok(())
}

// Write the plot data in gnuplot format to file
fn write_plot4_data(
    data: &indexmap::IndexMap<String, indexmap::IndexMap<i32, i64>>,
    filename: &str,
    start_year: i32,
    end_year: i32,
) -> Result<()> {
    let mut file = File::create(filename)?;

    // Write out the countries as the first row
    let years: Vec<String> = (start_year..=end_year)
        .map(|year| year.to_string())
        .collect();
    writeln!(file, "{}", years.join("\t"))?;

    for (country, years) in data {
        let year_pop = years
            .iter()
            .map(|(_, pop)| pop.to_string())
            .collect::<Vec<String>>()
            .join("\t");
        writeln!(file, "\"{}\"\t{}", country, year_pop)?;
    }

    Ok(())
}

/// Plot all problems in on shot
/// # Arguments
/// * `conn` - The database connection
/// * `selected_year` - The selected year for the analysis
/// * `start_year` - The start year of the range
/// * `end_year` - The end year of the range
pub async fn all_problems(
    conn: &sqlx::Pool<sqlx::Postgres>,
    selected_year: i32,
    start_year: i32,
    end_year: i32,
) -> sqlx::Result<(), Box<dyn Error>> {
    genplot1(conn, start_year, end_year).await?;
    genplot2(conn, selected_year).await?;
    genplot3(conn, start_year, end_year).await?;
    genplot4(conn, start_year, end_year).await?;

    // Return the result
    Ok(())
}
