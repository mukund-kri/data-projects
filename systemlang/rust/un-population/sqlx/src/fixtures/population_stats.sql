-- Insert test data into population_stats
INSERT INTO population_stats (country, year, population) VALUES
  ('China', 2020, 1439323776),
  ('India', 2019, 1366417754),
  ('India', 2020, 1380004385),
  ('Malaysia', 2015, 32365999),
  ('Malaysia', 2016, 32776107),
  ('Singapore', 2015, 5850342);