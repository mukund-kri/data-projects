use std::error::Error;

use console::Term;
use dialoguer::{theme::ColorfulTheme, Select};

use unpop_sqlx::loading;

#[async_std::main]
async fn main() -> Result<(), Box<dyn Error>> {
    println!("DB loader");

    let loader_options = &["Population Data", "Grouping"];

    // Setup term and theme
    let term = Term::buffered_stderr();
    let theme = ColorfulTheme::default();

    // Display the loader options select menu
    let loader_select = Select::with_theme(&theme)
        .with_prompt("Select what you want to load")
        .default(0)
        .items(&loader_options[..])
        .interact_on(&term)?;

    match loader_select {
        0 => loading::load_population("../data/un-population.csv").await?,
        1 => loading::load_grouping().await?,
        _ => println!("Invalid selection"),
    }

    Ok(())
}
