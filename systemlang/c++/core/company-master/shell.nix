with import <nixpkgs> {};


stdenv.mkDerivation {

  name = "Datamunge";
  src = ./.;
  
  buildInputs = [
    boost
    gnuplot
  ];
  nativeBuildInputs = [ cmake ];

  shellHook = ''
  alias crun='cmake --build . && ./Datamunge'
  '';

}
