#include <map>
#include <string>
#include <fstream>
#include <iostream>

#include <csv/document.hpp>
#include <boost/algorithm/string.hpp>



void problem2(Csv::Document csv_doc) {

  std::string date;
  std::map<std::string, int> year_hist;

  // Accumulate by year of registration
  for(const auto& row : csv_doc) {
    date = row.get("DATE_OF_REGISTRATION");
      
    std::vector<std::string> date_elements;

    boost::split(date_elements, date, boost::is_any_of("-"));
    if (date_elements.size() == 3) {
      std::string year = date_elements[2];
      year_hist[year]++;
    }
  }

  // Write out the gnuplot data file
  std::ofstream problem2_out ("../plots/problem2.dat");
  for(int i=2000; i<=2018; i++) {
    std::string year = std::to_string(i);
    problem2_out << i - 2000 << "\t"
		 << year << "\t"
		 << year_hist[year] << std::endl;
  }
  problem2_out.close();
  
}
