# Company Master data project in C++

## AIM

To code a data munging and ploting project in plain c++. 
	
## Features

* Company registration data (West Bengal state)
* External lib - csvpp for csv reading
* Generates gnuplot files
* plotting with gnuplot
* CMake for building
* used vector, set, map from standard lib

## Building, Running and Plotting 

- cd into the build directory
- run `cmake ..`
- then run `cmake --build .` to create the executable
- run the executable
	
- the executable creates all the data files needed in `plot`
  directory
- cd into the plot directory
- use `gnuplot` to generate plots
	
## TODO

1. Make the Gnuplots much better. Add labels, text, clean up tics
       etc.
    
