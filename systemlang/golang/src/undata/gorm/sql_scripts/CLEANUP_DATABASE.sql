-- Drop the exercise database
DROP DATABASE IF EXISTS gexercise;

-- Also drop the user that was used the access the database
DROP ROLE IF EXISTS gexercise;
