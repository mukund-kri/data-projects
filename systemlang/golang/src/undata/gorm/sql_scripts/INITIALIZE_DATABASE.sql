-- Create a new database to hold the exercise data
CREATE DATABASE gexercise;

-- Create a new user. This user will be used by GORM to access UN data
CREATE ROLE gexercise PASSWORD 'password';
GRANT ALL ON DATABASE gexercise TO gexercise;
ALTER ROLE gexercise WITH LOGIN;
