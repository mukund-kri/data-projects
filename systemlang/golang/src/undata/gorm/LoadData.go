package main


import (
	"encoding/csv"
	"fmt"
	"strconv"
	"os"
	
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)


const CSV_DATA_FILE = "population-estimates_csv.csv"

// UNData struct represents each line of the UN Population data
type UNData struct {
	gorm.Model
	Region string
	CountryCode int
	Year int
	Population float64
}

func main() {

	// Open and extablish a connection to postgres
	db, err := gorm.Open(
		"postgres",
		"dbname=gexercise user=gexercise password=password host=127.0.0.1 port=5432",
	)
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	// Crate / Update database schema
	db.AutoMigrate(&UNData{})

	// Read data from CSV and load into SQL
	fp, err := os.Open(CSV_DATA_FILE)
	if err != nil {
		panic("Could not load CSV file")
	}

	csvData, _ := csv.NewReader(fp).ReadAll()

	fmt.Println("Loading data please wait ....")
	for _, row := range csvData[1:] {
		country := row[0]
		countryCode, _ := strconv.Atoi(row[1])
		year, _ := strconv.Atoi(row[2])
		population, _ := strconv.ParseFloat(row[3], 64)
		rowObj := &UNData{
			Region: country,
			CountryCode: countryCode,
			Year: year,
			Population: population,
		}
		db.Create(rowObj)
	}

	fmt.Println("Finished Loading data")
}
