import std/[tables]

import common


proc problem1*(data: seq[Company]) =
  ## Solution and Plot for Problem 1
  ## Histogram of 'Authorized Capital' for all companies

  var
    authCapHist: OrderedTable[string, int] = [
      ("<= 1L", 0),
      ("1L to 10L", 0),
      ("10L to 1Cr", 0),
      ("1Cr to 10Cr", 0),
      ("> 10Cr", 0)
    ].toOrderedTable

  for company in data:
    let label = case company.authCap:
      of 0 .. 100000: "<= 1L"
      of 100001 .. 1000000: "1L to 10L"
      of 1000001 .. 10000000: "10L to 1Cr"
      of 10000001 .. 100000000: "1Cr to 10Cr"
      else: "> 10Cr"

    authCapHist[label] += 1

  # Now plot the histogram
  plotTable(
    authCapHist,
    title = "Registration Count by Authorized Capital",
    xlabel = "Authorized Capital",
    ylabel = "Registrations",
  )
