import std/[parseutils, tables, sequtils]

import plotly


type
  ## Relavant information about a company
  Company* = object
    authCap*: float
    registrationYear*: int
    pba*: string

  ## Plot data for pba/registration year
  PbaYearData* = Table[string, Table[string, int]]


## Constructors
func company*(authCap: float, registrationYear: int, pba: string): Company =
  Company(authCap: authCap, registrationYear: registrationYear, pba: pba)


proc extractYear*(date: string, year: var int): bool =
  ## Extracts the year from a date string

  if date.len != 10:
    result = false
  else:
    discard parseInt(date, year, 6)
    result = true


proc plotTable*(
  data: OrderedTable[string, int], title: string, xlabel: string, ylabel: string) =
  ## Make a plotly bar chart from an ordered table

  let d = Trace[int](`type`: PlotType.Bar,
    ys: data.values.toSeq,
    text: data.keys.toSeq,
  )
  let layout = Layout(
    title: title,
    xaxis: Axis(title: xlabel),
    yaxis: Axis(title: ylabel),
  )
  let pp = Plot[int](traces: @[d], layout: layout)
  pp.show()


proc plotTableOfTable*(
  data: PbaYearData, title: string, xlabel: string, ylabel: string) =
  ## Make a plotly bar chart from a table of tables

  var
    traces: seq[Trace[int]]
    d: Trace[int]
  let
    groups = data.keys.toSeq
    layout = Layout(
      width: 1200,
      height: 600,
      title: title,
      xaxis: Axis(title: xlabel),
      yaxis: Axis(title: ylabel),
      barmode: BarMode.Stack,
      autosize: false
    )

  for group in groups:
    d = Trace[int](`type`: PlotType.Bar, name: group, )
    d.ys = data[group].values.toSeq
    d.text = data[group].keys.toSeq
    traces.add(d)

  # Plot
  let p = Plot[int](layout: layout, traces: traces)

  # Plot to browser
  p.show()
