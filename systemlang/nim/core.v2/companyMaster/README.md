# Core V2

This is basically the same as core (plotly version) but in this case I make several
changes for the better it terms of symantics, performance, readablility etc.

## Changes

1. Read CSV only once
   - Read CSV and load relevent data to a sequence
   - Use this sequence to accumulate and plot

2. Use case statement (problem 1)
3. use `let` by default and var when needed.
4. Factor out common plotting code