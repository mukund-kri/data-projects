import std/tables

import common


const pbasToPlot = @[
  "Finance",
  "Manufacturing (Others)",
  "Trading",
  "Transport, storage and Communications"
]

proc initPbaYearCounts(pbas: seq[string], startYear: int, endYear: int): PbaYearData =
  ## Initialize PbaYearData with counts of 0 for each PBA and year

  for pba in pbas:
    result[pba] = initTable[string, int]()

    for year in startYear..endYear:
      result[pba][$year] = 0


proc problem4*(data: seq[Company], startYear: int = 2000, endYear: int = 2017) =
  ## Plot group bar plot of year of registration and "principal business
  ## activity"

  var counts = initPbaYearCounts(pbasToPlot, startYear, endYear)

  for company in data:
    let
      year = company.registrationYear
      pba = company.pba
    if (pba in pbasToPlot) and (year >= startYear and year <= endYear):
      counts[pba][$year] += 1

  plotTableOfTable(counts, "Registation by PBA by Year", "Year", "Registrations")
