import std/[tables]

import plotly

import common


proc problem2*(data: seq[Company], startYear: int = 2000, endYear: int = 2018) =
  ## Solution and plot of problem 2
  ## Bar plot of company registration by year

  var registrationCount: OrderedTable[string, int] = initOrderedTable[string, int]()

  for company in data:
    let year = company.registrationYear
    if year >= startYear and year <= endYear:
      registrationCount[$year] = registrationCount.getOrDefault($year, 0) + 1

  plotTable(
    registrationCount,
    "Registration by year. (2000 - 2018)",
    "Year",
    "Registration Count"
  )
