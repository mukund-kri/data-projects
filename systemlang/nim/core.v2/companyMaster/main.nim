import std/[parsecsv, parseutils]

import common
import problem1, problem2, problem3, problem4


const
  CSV_SOURCE_FILE = "mca.csv"

proc main() =
  ## This is the entry point of the program
  ## First the csv is read and the relevant columns are extracted into a sequence
  ## which is then passed to the 4 procedures that plot the data

  var
    csv: CsvParser
    data: seq[Company]
    year: int
    authCap: float

  # Read csv, parse and store data in a sequence
  csv.open(CSV_SOURCE_FILE)
  csv.readHeaderRow()

  while csv.readRow():
    if extractYear(csv.rowEntry("DATE_OF_REGISTRATION"), year):
      discard parseFloat(csv.rowEntry("AUTHORIZED_CAP"), authCap)
      data.add(company(
        authCap,
        year,
        csv.rowEntry("PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN"),
      ))

  csv.close()

  # Plot the data problems
  problem1(data)
  problem2(data)
  problem3(data)
  problem4(data)

when isMainModule:
  main()
