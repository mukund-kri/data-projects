import std/[tables]

import common


proc problem3*(data: seq[Company]) =
  ## Plot the registration of companies by "Principal Business Activitvity"
  ## for the year 2015

  var counts: OrderedTable[string, int] = initOrderedTable[string, int]()

  for company in data:
    if company.registrationYear == 2015:
      counts[company.pba] = counts.getOrDefault(company.pba, 0) + 1

  plotTable(counts,
    "Companies by Principal Business Activity",
    "Principal Business Activity",
    "Number of Companies")
