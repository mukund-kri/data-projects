import std/tables
import std/parsecsv
import std/parseutils
from std/streams import newFileStream

import plotly


proc main() =
  var 
    p: CsvParser
    authCap: float
    authCapHist = [        
      ("<= 1L", 0),
      ("1L to 10L", 0),
      ("10L to 1Cr", 0),
      ("1Cr to 10Cr", 0),
      ("> 10Cr", 0)
    ].toOrderedTable

  p.open("test.csv")
  p.readHeaderRow()

  while p.readRow():

    discard parseFloat(p.rowEntry("AUTHORIZED_CAP"), authCap)
    if authCap >= 0.0 and authCap < 100000.0:
      authCapHist["<= 1L"] += 1
    elif authCap >= 100000.0 and authCap < 1000000.0:
      authCapHist["1L to 10L"] += 1
    elif authCap >= 1000000.0 and authCap < 10000000.0:
      authCapHist["10L to 1Cr"] += 1
    elif authCap >= 10000000.0 and authCap < 100000000.0:
      authCapHist["1Cr to 10Cr"] += 1
    else:
      authCapHist["> 10Cr"] += 1

  echo authCapHist.
  p.close()

main()