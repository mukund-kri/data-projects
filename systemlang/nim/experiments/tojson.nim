# experiment dumping a table into json

import std/json
import std/tables


proc main() =
  var t = initTable[string, int]()

  t["a"] = 1
  t["b"] = 2
  t["c"] = 3

  echo ( %* t)

if isMainModule:
  main()
