import std/random

import plotly


proc main() =
  let
    d1 = Trace[int](`type`: PlotType.Histogram, opacity: 0.75, name: "2000")
    d2 = Trace[int](`type`: PlotType.Histogram, opacity: 0.75, name: "2010")

  # initalize the sequences that will hold data
  d1.ys = newSeq[int](100)
  d2.ys = newSeq[int](100)

  # Populate random values for the bar plots
  for i, x in d1.ys:
    d1.ys[i] = rand(200)
    d2.ys[i] = rand(300)

  # Create the plot
  let
    layout = Layout(title: "Stacked Histogram",
      width: 1200,
      height: 600,
      yaxis: Axis(title: "Values"),
      xaxis: Axis(title: "Counts"),
      barmode: BarMode.Stack,
      autosize: false
    )
    p = Plot[int](layout: layout, traces: @[d1, d2])

  # plot to browser
  p.show()


main()

