import plotly

var d = Trace[int](mode: PlotMode.LinesMarkers, `type`: PlotType.Scatter)

d.xs = @[1, 2, 3, 4, 5]
d.ys = @[1, 2, 3, 4, 5]

var layout = Layout(title: "Test", xaxis: Axis(title: "X"), yaxis: Axis(title: "Y"))
var p = Plot[int](traces: @[d], layout: layout)
p.show()
