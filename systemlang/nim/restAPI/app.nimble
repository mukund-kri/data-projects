# Package

version       = "0.1.0"
author        = "mukund k"
description   = "Jester API for company master data"
license       = "MIT"
srcDir        = "src"
bin           = @["app"]


# Dependencies

requires "nim >= 1.6.10", "jester", "norm"
