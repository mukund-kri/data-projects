import std/[json, strutils, tables]

import jester

import models


const PORT = 8080.Port
const headers = {
  "Content-Type": "application/json",
  "Access-Control-Allow-Origin": "*"
}


router mcaAPIRouter:
  get "/":
    echo request.params
    resp "Hello World"

  get "/problem1":
    let data = authCapData()
    resp Http200, headers, $( %* data)

  get "/problem2":
    var startYear = parseInt(request.params["startYear"])
    var endYear = parseInt(request.params["endYear"])

    let data = problem2Data(startYear, endYear)
    resp Http200, headers, $( %* data)

  get "/problem3":
    let data = problem3Data()
    resp Http200, headers, $( %* data)

  get "/problem4":
    let data = problem4Data();

    # Transform data into a format that can be used by recharts
    var
      transformedData: Table[string, Table[string, int]] = initTable[string, Table[
        string, int]]()
      pba: string
      year: string

    for datum in data:
      pba = datum.pba
      year = $datum.year

      if pba notin transformedData:
        transformedData[pba] = initTable[string, int]()

      transformedData[pba][year] = datum.registrations

    # Now generate json
    var ans = %* []
    for pba, years in transformedData:
      var
        json: JsonNode = %* {"name": pba}
        year: string
        count: int

      for year, count in years:
        json[year] = %* count
      ans.add(json)

    resp Http200, headers, $ans

proc main() =
  let settings = newSettings(port = PORT)
  var jester = initJester(mcaAPIRouter, settings = settings)
  jester.serve()


when isMainModule:
  main()
