import std/json

import norm/[model, sqlite]


let conn* = open("mca.db", "", "", "")

# ORM models
type
  Company* = ref object of Model
    cin*: string
    authCap*: float
    year*: int
    pba*: string

  AuthCap* = ref object of Model
    label*: string
    count*: int

  CountByYear* = ref object
    year*: int
    registrations*: int

  CountByPBA* = ref object
    pba*: string
    registrations*: int

  CountByYearAndPBA* = ref object
    year*: int
    pba*: string
    registrations*: int

# constructors
func newCompany*(cin: string = "", authCap: float = 0.0, year: int = 0,
    pba: string = ""): Company =
  result = Company(cin: cin, authCap: authCap, year: year, pba: pba)

proc newAuthCap*(label: string = "", count: int = 0): AuthCap =
  result = AuthCap(label: label, count: count)

proc newCountByYear*(year: int = 0, registrations: int = 0): CountByYear =
  result = CountByYear(year: year, registrations: registrations)

proc newCountByPBA*(pba: string = "", registrations: int = 0): CountByPBA =
  result = CountByPBA(pba: pba, registrations: registrations)

proc newCountByYearAndPBA*(year: int = 0, pba: string = "",
    registrations: int = 0): CountByYearAndPBA =
  result = CountByYearAndPBA(year: year, pba: pba, registrations: registrations)

# database
proc createTables*() =
  conn.createTables(newCompany())
  conn.createTables(newAuthCap())


  # # insert new AuthCap
  # var ac1 = newAuthCap(label = "test", count = 1)
  # conn.insert(ac1)

proc authCapData*(): seq[AuthCap] =
  result = @[newAuthCap()]
  conn.selectAll(result)


proc problem2Data*(startYear: int, endYear: int): seq[CountByYear] =
  result = @[newCountByYear()]
  conn.rawSelect("""
  select year, count(*) as registrations from Company 
  where year between ? and ? 
  group by year""", result, startYear, endYear)


proc problem3Data*(): seq[CountByPBA] =
  result = @[newCountByPBA()]
  conn.rawSelect("""
  select pba, count(*) as registrations from Company 
  where year = 2015 
  group by pba""", result)


proc problem4Data*(): seq[CountByYearAndPBA] =
  result = @[newCountByYearAndPBA()]
  conn.rawSelect("""
  select year, pba, count(*) as registrations from Company 
  where year between 2000 and 2017 
  group by year, pba""", result)


if isMainModule:
  var counts = problem4Data()
  echo $( %* counts)
