import std/[tables, parsecsv, parseutils, with]

import norm/[model, sqlite]

import models, utils

const sourceCsv = "mca.csv"

proc loadByAuthCap*() =
  ## Solution and Plot for Problem 1
  ## Histogram of 'Authorized Capital' for all companies

  var
    p: CsvParser
    authCap: float
    ac: AuthCap

    authCapHist: OrderedTable[string, int] = [
      ("<= 1L", 0),
      ("1L to 10L", 0),
      ("10L to 1Cr", 0),
      ("1Cr to 10Cr", 0),
      ("> 10Cr", 0)
    ].toOrderedTable

  p.open(sourceCsv)
  p.readHeaderRow()

  while p.readRow():

    discard parseFloat(p.rowEntry("AUTHORIZED_CAP"), authCap)
    if authCap >= 0.0 and authCap < 100000.0:
      authCapHist["<= 1L"] += 1
    elif authCap >= 100000.0 and authCap < 1000000.0:
      authCapHist["1L to 10L"] += 1
    elif authCap >= 1000000.0 and authCap < 10000000.0:
      authCapHist["10L to 1Cr"] += 1
    elif authCap >= 10000000.0 and authCap < 100000000.0:
      authCapHist["1Cr to 10Cr"] += 1
    else:
      authCapHist["> 10Cr"] += 1


  for label, count in authCapHist:
    ac = newAuthCap(label, count)
    conn.insert(ac)
    echo label, " ", count


proc loadCompanyData() =

  var
    p: CsvParser
    authCap: float
    year: int
    pba: string
    cin: string
    cs: seq[Company] = @[]
    recordCount: int = 0

  p.open(sourceCsv)
  p.readHeaderRow()


  while p.readRow():
    if extractYear(p.rowEntry("DATE_OF_REGISTRATION"), year):
      discard parseFloat(p.rowEntry("AUTHORIZED_CAP"), authCap)
      pba = p.rowEntry("PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN")
      cin = p.rowEntry("CORPORATE_IDENTIFICATION_NUMBER")

      recordCount += 1
      cs.add(newCompany(cin, authCap, year, pba))
      if recordCount mod 1000 == 0:
        conn.insert(cs)
        cs = @[]

if isMainModule:
  createTables()
  loadByAuthCap()
  loadCompanyData()
