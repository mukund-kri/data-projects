import std/parseutils


proc extractYear*(date: string, year: var int): bool =
  ## Extracts the year from a date string

  if date.len != 10:
    result = false
  else:
    discard parseInt(date, year, 6)
    result = true
