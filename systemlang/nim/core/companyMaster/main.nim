from problem1 import problem1
from problem2 import problem2
from problem3 import problem3
from problem4 import problem4


const SOURCE_CSV = "mca.csv"


proc main() =
  problem1(SOURCE_CSV)
  problem2(SOURCE_CSV)
  problem3(SOURCE_CSV)
  problem4(SOURCE_CSV, 2000, 2018)

when isMainModule:
  main()
