# Data munging with core nim :: With `company master dataset`

In this project ...

1. I solve the company master problems set with nim.
2. The code is written mostly in an imperative way. Similar to what I would do with 
   python.
3. Plots are produced with nim-plotly, a nim port of plotly.

## Prerequisites

1. Working 1.x nim compiler
2. The following packages, installed with nimble ...
   * plotly

## Data Set
TODO:

## Running the code

Just running `main.nim` should do the trick ...

```shell
> nim run main.nim
```

## Todo

 - [X] Clean up plot 1 by adding better Headings/Labels/Ticks
 - [X] Clean up plot 2 by adding better Headings/Labels/Ticks
 - [X] Clean up plot 3 by adding better Headings/Labels/Ticks
 - [X] Finish the plots for problem 4
