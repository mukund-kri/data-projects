import std/tables
import std/sequtils
import std/parsecsv

import plotly

import utils


proc problem2*(sourceCsv: string, startYear: int = 2000, endYear: int = 2018) =
  ## Solution and plot of problem 2
  ## Bar plot of company registration by year

  var
    csv: CsvParser
    year: int
    registrationCount: OrderedTable[int, int] = initOrderedTable[int, int]()

  # Initialize the table with registration counts set to zero for all years
  for i in countup(startYear, endYear):
    registrationCount[i] = 0

  # Read csv file and count registrations by year
  csv.open(sourceCsv)
  csv.readHeaderRow()
  while csv.readRow():
    if extractYear(csv.rowEntry("DATE_OF_REGISTRATION"), year):
      if year >= startYear and year <= endYear:
        registrationCount[year] += 1

  csv.close()

  # Now plot the histogram
  var d = Trace[int](`type`: PlotType.Bar,
    xs: toSeq(registrationCount.keys),
    ys: toSeq(registrationCount.values)
  )
  var layout = Layout(
    title: "Registration by year. (2000 - 2018)",
    xaxis: Axis(title: "Year"),
    yaxis: Axis(title: "Registration Count"),

  )
  var pp = Plot[int](traces: @[d], layout: layout)
  pp.show()

