import std/tables
import std/parsecsv
import std/sequtils

import plotly

import utils


const pbasToPlot = [
  "Finance",
  "Manufacturing (Others)",
  "Trading",
  "Transport, storage and Communications"
]

proc problem4*(sourceCsv: string, startYear: int, endYear: int) =
  ## Plot group bar plot of year of registration and "principal business
  ## activity"

  var
    csv: CsvParser
    year: int
    pba: string
    counts: Table[string, Table[int, int]] = initTable[string,
        Table[int, int]]()

  # Read csv and count registrations per year and pba
  csv.open(sourceCsv)
  csv.readHeaderRow()

  while csv.readRow():
    if extractYear(csv.rowEntry("DATE_OF_REGISTRATION"), year):
      if year >= startYear and year <= endYear:
        pba = csv.rowEntry("PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN")
        if counts.hasKey(pba):
          if counts[pba].hasKey(year):
            counts[pba][year] += 1
          else:
            counts[pba][year] = 1
        else:
          counts[pba] = initTable[int, int]()
          counts[pba][year] = 1

  csv.close()

  # Reorganize data for plotting
  var
    traces: seq[Trace[int]]
    d: Trace[int]

  for pba in pbasToPlot:

    d = Trace[int](`type`: PlotType.Bar, name: pba)
    d.ys = toSeq(counts[pba].values)
    d.xs = toSeq(counts[pba].keys)
    echo()
    traces.add(d)

  # Plot
  let
    layout = Layout(
      width: 1200,
      height: 600,
      title: "Registrations by PBA by Year",
      xaxis: Axis(title: "Year"),
      yaxis: Axis(title: "Registrations"),
      barmode: BarMode.Stack,
      autosize: false
    )
    p = Plot[int](layout: layout, traces: traces)

  # Plot to browser
  p.show()


