import std/tables
import std/sequtils
import std/parsecsv
import std/parseutils

import plotly


proc problem1*(sourceCsv: string) =
  ## Solution and Plot for Problem 1
  ## Histogram of 'Authorized Capital' for all companies

  var
    p: CsvParser
    authCap: float
    authCapCounts: seq[int]
    authCapNames: seq[string]

    authCapHist: OrderedTable[string, int] = [
      ("<= 1L", 0),
      ("1L to 10L", 0),
      ("10L to 1Cr", 0),
      ("1Cr to 10Cr", 0),
      ("> 10Cr", 0)
    ].toOrderedTable

  p.open(sourceCsv)
  p.readHeaderRow()

  while p.readRow():

    discard parseFloat(p.rowEntry("AUTHORIZED_CAP"), authCap)
    if authCap >= 0.0 and authCap < 100000.0:
      authCapHist["<= 1L"] += 1
    elif authCap >= 100000.0 and authCap < 1000000.0:
      authCapHist["1L to 10L"] += 1
    elif authCap >= 1000000.0 and authCap < 10000000.0:
      authCapHist["10L to 1Cr"] += 1
    elif authCap >= 10000000.0 and authCap < 100000000.0:
      authCapHist["1Cr to 10Cr"] += 1
    else:
      authCapHist["> 10Cr"] += 1

  authCapNames = toSeq(authCapHist.keys)
  authCapCounts = toSeq(authCapHist.values)
  p.close()

  # Now plot the histogram
  var d = Trace[int](`type`: PlotType.Bar,
    ys: authCapCounts,
    text: authCapNames
  )

  var layout = Layout(
    title: "Registration Count by Authorized Capital",
    xaxis: Axis(title: "Authorized Capital"),
    yaxis: Axis(title: "Restistraions")
  )
  var pp = Plot[int](traces: @[d], layout: layout, )
  pp.show()
