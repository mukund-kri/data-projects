import std/json
import std/tables
import std/sequtils
import std/parsecsv


import utils


proc problem2*(sourceCsv: string, startYear: int = 2000, endYear: int = 2018) =
  ## Solution and plot of problem 2
  ## Bar plot of company registration by year

  var
    csv: CsvParser
    year: int
    registrationCount: OrderedTable[int, int] = initOrderedTable[int, int]()

  # Initialize the table with registration counts set to zero for all years
  for i in countup(startYear, endYear):
    registrationCount[i] = 0

  # Read csv file and count registrations by year
  csv.open(sourceCsv)
  csv.readHeaderRow()
  while csv.readRow():
    if extractYear(csv.rowEntry("DATE_OF_REGISTRATION"), year):
      if year >= startYear and year <= endYear:
        registrationCount[year] += 1

  csv.close()

  # Generate plot data and dump to json
  let
    jsonString = $( %* {"categories": registrationCount.keys.toSeq,
        "data": registrationCount.values.toSeq})
  writeFile("web/problem2.json", jsonString)
