# Data munging with core nim :: Highcharts version

In this project ...

1. I solve the company master problems set with nim.
2. The raw data in CSV format is read in and the answers are computed.
3. The computed plot data is dumped into a json file.
4. These json are used to plot using the Highcharts JavaScript library
   on the browser.

## Prerequisites

1. Working 1.x nim compiler

## Data Set


You can download the data set here ...
TODO: describe how to get csv

## Running the code

### Step 1: Generate JSON

Run the following command to generate JSON from the source csv

```shell
> nim run main.nim
```

### Step 2: Plot on browser

The plot is rendered on the browser. You will need a http server to
serve the files to the browser.

first go into the plots folder.

```shell
> cd plots
```

Then run any web server on this folder, I use the built in server that comes
with python.

```shell
> python -m http.server 9000
```

The plots can seen on this URL, if everything went as expected.

http://localhost:9000/


## Todo

- [] Finish Problems 2, 3, 4 JSON generation
- [] Port the JS plotting code from plain JS to nim js target.
