import std/json
import std/tables
import std/sequtils
import std/parsecsv

import utils


proc problem3*(sourceCsv: string) =
  ## Plot the registration of companies by "Principal Business Activitvity"
  ## for the year 2015

  var
    csv: CsvParser
    year: int
    pba: string
    counts: OrderedTable[string, int] = initOrderedTable[string, int]()

  csv.open(sourceCsv)
  csv.readHeaderRow()

  # Read csv and count registrations
  while csv.readRow():
    if extractYear(csv.rowEntry("DATE_OF_REGISTRATION"), year):
      if year == 2015:
        pba = csv.rowEntry("PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN")
        if counts.hasKey(pba):
          counts[pba] += 1
        else:
          counts[pba] = 1

  csv.close()

  # Generate plot data and dump to file
  let
    jsonString = $( %* {
      "categories": counts.keys.toSeq,
      "data": counts.values.toSeq
    })
  writeFile("web/problem3.json", jsonString)
