
import std/[json, tables, parsecsv, sequtils]

import utils


proc problem4*(sourceCsv: string, startYear: int, endYear: int) =
  ## Plot group bar plot of year of registration and "principal business
  ## activity"

  var
    csv: CsvParser
    year: int
    pba: string
    counts: Table[string, OrderedTable[int, int]] = initTable[string,
        OrderedTable[int, int]]()
    plotData: OrderedTable[string, seq[int]] = initOrderedTable[string,
        seq[int]]()

  # Read csv and count registrations per year and pba
  csv.open(sourceCsv)
  csv.readHeaderRow()

  while csv.readRow():
    if extractYear(csv.rowEntry("DATE_OF_REGISTRATION"), year):
      if year >= startYear and year <= endYear:
        pba = csv.rowEntry("PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN")
        if counts.hasKey(pba):
          if counts[pba].hasKey(year):
            counts[pba][year] += 1
          else:
            counts[pba][year] = 1
        else:
          counts[pba] = initOrderedTable[int, int]()
          counts[pba][year] = 1
  csv.close()

  var
    pbas = counts.keys.toSeq
    size = pbas.len

  # Sort initialize the plot data sequences
  for year in countup(startYear, endYear):
    plotData[$year] = newSeq[int](size)

    for index, pba in pbas:

      if counts[pba].hasKey(year):
        plotData[$year][index] = counts[pba][year]
      else:
        plotData[$year][index] = 0

  # Generate the plot and dump to file
  let
    jsonString = $( %* {
      "series": plotData,
      "categories": pbas,
    })

  writeFile("web/problem4.json", jsonString)
