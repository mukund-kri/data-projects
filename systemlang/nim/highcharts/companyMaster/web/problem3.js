let plot3;

$.getJSON('problem3.json', (data) => {
  plot3(data.categories, data.data);
});

plot3 = (categories, series) => {
  return Highcharts.chart('problem3', {
    chart: {
      type: 'bar',
    },
    title: {
      text: 'Companies registered in Maharastra by Year',
    },
    xAxis: {
      categories,
      title: {
        text: null,
      },
    },
    yAxis: {
      min: 0,
      title: {
        text: 'No of Companies',
        align: 'high',
      },
    },
    labels: {
      overflow: 'justify',
    },
    tooltip: {
      valueSuffix: 'companies',
    },
    plotOptions: {
      bar: {
        dataLabels: {
          enabled: true,
        },
      },
    },
    credits: {
      enabled: false,
    },
    series: [
      {
        name: 'Number of Companies',
        data: series,
      },
    ],
  });
};
