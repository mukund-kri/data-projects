let plot4;

$.getJSON('problem4.json', (data) => {
  plot4(data.categories, data.series);
});

const process = (series) => {
  const s = [];
  Object.entries(series).forEach(([year, data]) => {
    s.push({ name: year.toString(), data });
  });
  console.log(s);
  return s;
};

plot4 = (categories, series) => {
  const s = process(series);
  return Highcharts.chart('problem4', {
    chart: {
      type: 'bar',
    },
    title: {
      text: 'Companies registered in Maharastra by Year',
    },
    xAxis: {
      categories,
      title: {
        text: null,
      },
    },
    yAxis: {
      min: 0,
      title: {
        text: 'No of Companies',
        align: 'high',
      },
    },
    labels: {
      overflow: 'justify',
    },
    tooltip: {
      valueSuffix: 'companies',
    },
    plotOptions: {
      bar: {
        dataLabels: {
          enabled: true,
        },
      },
    },
    credits: {
      enabled: false,
    },
    series: s,
  });
};
