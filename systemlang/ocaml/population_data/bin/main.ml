module Populations = Map.Make(String);;


(*
let saarc_countries = [
    "Afghanistan";
    "Bangladesh";
    "Bhutan";
    "India";
    "Maldives";
    "Nepal";
    "Pakistan";
    "Sri Lanka";
] *)


(* problem 1 *)
(* let () =
    (Csv.load "population.csv") 
    |> List.filter (fun row -> (List.hd row) = "India")
    |> List.filter (fun row -> (List.nth row 2) > "2000")
    |> List.map (fun row -> (List.nth row 2) :: (List.nth row 3) :: [])
    |> Csv.save ~separator:' '  "india_population.csv" *)

let update_population populations row =
    let year = (List.nth row 2) in
    let population = Float.of_string (List.nth row 3) in 
    let update = function
    | Some pop -> Some (pop +. population)
    | None -> Some population in
    Populations.update year update populations 



(* problem 2 *)
let () =
    let saarc_pop: float Populations.t =  Populations.empty in
    (Csv.load "population.csv") 
    |> List.filter (fun row -> (List.hd row) = "India") 
    |> List.fold_left update_population saarc_pop
    |> Populations.iter (fun year pop -> Printf.printf "%s: %f\n" year pop)

