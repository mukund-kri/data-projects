// swift-tools-version: 5.10
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "app",
    products: [
        // Products define the executables and libraries a package produces, making them visible to other packages.
        .executable(
            name: "app",
            targets: ["app"]),
    ],
    dependencies: [
        .package(url: "https://github.com/swiftcsv/SwiftCSV.git", from: "0.10.0"),
        .package(url: "https://github.com/apple/swift-argument-parser", from: "1.4.0"),

    ],
    targets: [
        // Targets are the basic building blocks of a package, defining a module or a test suite.
        // Targets can depend on other targets in this package and products from dependencies.
        .executableTarget(
            name: "app", 
            dependencies: ["SwiftCSV", .product(name: "ArgumentParser", package: "swift-argument-parser")]),
        .testTarget(
            name: "appTests",
            dependencies: ["app"]),
    ]
)
