# Company Master data project in SWIFT :: Gnuplot version

As with projects in other languages, I will make a few meaningful, analytical plots
for company registration data provided by the Government of India open data initiative.

This project will be coded in the SWIFT programming language. This will act as a
comparision to the other languages of the same class such as golang, rust, etc.


## Features

This will be an overkill in terms of design. It so that I can learn the language
better.

1. Swift Package Manager to manage the lifecycle of the project.
1. External libraries like SwiftCSV etc. 
2. S    eparate modules for data processing and plotting.
3. Unit tests for the modules.
4. Documentation for the modules.
5. Command line interface for the project with swift-argument-parser.
6. Excellent Readme.
7. Command line interface for the project with swift-argument-parser.


## How to run

### Prerequisites
1. Swift 5.3 or higher
2. Swift Package Manager


### Steps

1. At the root of the project, run the following command to build the project.
```bash
swift run app all
```
should generate the plots in the `Plots` directory.

2. To run individual problem sets, run the following command.
```bash
swift run app <problem_number>
```
where `<problem_number>` is one of the following:
    i. problem1
    ii. problem2
    iii. problem3
    iv. problem4

## View the plots

You will need gnuplot to render the plots. We have provided the gnuplot script
in the `Plots` directory. The named  `problem<problem_number>.plt` where
`<problem_number>` is one of 1, 2, 3, 4.

So to view the plot for problem 1, first cd into the `Plots` directory and fire up
gnuplot.

```bash
cd Plots
gnuplot
```

Then in the gnuplot prompt, run the following command.
```bash
load 'problem1.plt'
```

This should render a nice plot on the screen.

You plot problems 2, 3, 4 in a similar way.

## License

Creative Commons Zero v1.0 Universal
