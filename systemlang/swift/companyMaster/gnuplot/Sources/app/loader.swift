import Foundation

import SwiftCSV


// Strut to store each row of the csv file
struct Company {
    var authorizedCapital: Int
    var registrationYear: Int
    var pba: String
}

let formatter = NumberFormatter()

func loadRawDataFromCsv(csvFileName : String) -> [Company] {
    
    
    var companies: [Company] = []

    do {
        let csvFile: CSV = try CSV<Named>(url: URL(fileURLWithPath: csvFileName))

        for row in csvFile.rows {
            
            // Simple validation remove NA values
            if row["AUTHORIZED_CAP"] == "NA" || row["DATE_OF_REGISTRATION"] == "NA" || row["PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN"] == "NA" {
                continue
            }
        
            // First convert the authorized capital to double from String
            let f = formatter.number(from: row["AUTHORIZED_CAP"]!)!
            let authorizedCapital = Int(f)

            let registrationDate = row["DATE_OF_REGISTRATION"]!

            // Extract year from the date
            let registrationYear = Int(registrationDate.suffix(2))!

            let pba = row["PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN"]!

            let company = Company(
                authorizedCapital: authorizedCapital, 
                registrationYear: registrationYear + 2000, 
                pba: pba
            )
            companies.append(company)
        }
    } catch {
        print("Error reading csv file")
    }       

    // return the companies
    return companies
}