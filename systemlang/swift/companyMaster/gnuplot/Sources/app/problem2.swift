// Problem 2: Company counts by registration year over a given year range
import Foundation

import SwiftCSV


class Problem2 {

    // The final analysed data
    var counts: [Int: Int] 
    var startYear: Int
    var endYear: Int

    // constructor, store and and analyze data
    init(data: [Company], startYear: Int, endYear: Int) {

        self.startYear = startYear
        self.endYear = endYear
        // Initialize the counts dictionary to 0. So as no year is missed
        self.counts = Dictionary(uniqueKeysWithValues: (startYear...endYear).map { ($0, 0) })

        print("Analyzing data for problem 2 ...")
        for company in data {
            let year = company.registrationYear
            if year >= startYear && year <= endYear {
                self.counts[year]! += 1
            }
        }
    }

    // write out data in gunplot .dat format
    func writeDataToGnuplotDatFile() {
        print("Writing plotting data for problem 2 ...")
        do {
            let datFile = "Plots/problem2.dat"
            // Make sure a fresh file is created
            if FileManager.default.fileExists(atPath: datFile) {
                try FileManager.default.removeItem(atPath: datFile) 
            }
            FileManager.default.createFile(atPath: datFile, contents: nil, attributes: nil)

            // write the contents of the counter dictionary to the file
            if let fileHandle = FileHandle(forWritingAtPath: datFile) {
                for year in (self.startYear...self.endYear) {
                    let count = self.counts[year]!
                    fileHandle.write("\(year)\t\(count)\n".data(using: .utf8)!)
                }
                fileHandle.closeFile()
            } else {
                print("Error opening file for writing")
            }
        } catch {
            print("Error writing to file")
        }
    }
}