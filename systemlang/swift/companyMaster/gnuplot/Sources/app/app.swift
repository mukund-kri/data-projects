// The Swift Programming Language
// https://docs.swift.org/swift-book

import Foundation

import ArgumentParser


@main
struct App : ParsableCommand {
        // let data = loadRawDataFromCsv(csvFileName: "Data/Delhi.clean.csv")
        
        // let problem1 = Problem4(data: data, startYear: 2000, endYear: 2020)
        // problem1.writeDataToGnuplotDatFile()

    @Argument(help: "The problem to run. Can be either problem1, problem2, problem3, problem4 or all")
    var problem: String

    mutating func run() throws {
        switch problem {
        case "problem1":
            let data = loadRawDataFromCsv(csvFileName: "Data/Delhi.clean.csv")
            let problem1 = Problem1(data: data)
            problem1.writeDataToGnuplotDatFile()
        case "problem2":
            let data = loadRawDataFromCsv(csvFileName: "Data/Delhi.clean.csv")
            let problem2 = Problem2(data: data, startYear: 2000, endYear: 2020)
            problem2.writeDataToGnuplotDatFile()
        case "problem3":
            let data = loadRawDataFromCsv(csvFileName: "Data/Delhi.clean.csv")
            let problem3 = Problem3(data: data, startYear: 2000, endYear: 2020)
            problem3.writeDataToGnuplotDatFile()
        case "problem4":
            let data = loadRawDataFromCsv(csvFileName: "Data/Delhi.clean.csv")
            let problem4 = Problem4(data: data, startYear: 2000, endYear: 2020)
            problem4.writeDataToGnuplotDatFile()
        case "all":
            let data = loadRawDataFromCsv(csvFileName: "Data/Delhi.clean.csv")
            let problem1 = Problem1(data: data)
            problem1.writeDataToGnuplotDatFile()
            let problem2 = Problem2(data: data, startYear: 2000, endYear: 2020)
            problem2.writeDataToGnuplotDatFile()
            let problem3 = Problem3(data: data, startYear: 2000, endYear: 2020)
            problem3.writeDataToGnuplotDatFile()
            let problem4 = Problem4(data: data, startYear: 2000, endYear: 2020)
            problem4.writeDataToGnuplotDatFile()
        default:
            print("Invalid problem")
        }
    }
    
}