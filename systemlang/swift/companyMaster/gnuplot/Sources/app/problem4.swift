// Problem 4: Stacked bar chart of company counts by "Principal Business Activity" over 
// a given year range.

import Foundation

class Problem4 {

    // The final analysed data
    var counts: [Int: [String: Int]] = [:]
    var startYear: Int
    var endYear: Int

    // constructor, store and and analyze data
    init(data: [Company], startYear: Int, endYear: Int) {

        self.startYear = startYear
        self.endYear = endYear
        
        // Initialize the counts dictionary to 0. So no PBA is missed
        for year in (startYear...endYear) {
            self.counts[year] = [:]
            for pba in top_pbas {
                self.counts[year]![pba] = 0
            }
        }

        print("Analyzing data for problem 4 ...")
        for company in data {
            let year = company.registrationYear
            let pba = company.pba

            // Ignore companies that are not in the top pbas
            if !top_pbas.contains(pba) {
                continue
            }
            
            if year >= startYear && year <= endYear {
                self.counts[year]![pba]! += 1
            }
        }
    }

    // write out data in gunplot .dat format
    func writeDataToGnuplotDatFile() {
        print("Writing plotting data for problem 4 ...")
        do {
            let datFile = "Plots/problem4.dat"
            // Make sure a fresh file is created
            if FileManager.default.fileExists(atPath: datFile) {
                try FileManager.default.removeItem(atPath: datFile) 
            }
            FileManager.default.createFile(atPath: datFile, contents: nil, attributes: nil)

            // write the contents of the counter dictionary to the file
            if let fileHandle = FileHandle(forWritingAtPath: datFile) {

                // First write the header row, the list of pbas
                var header = ""
                for pba in top_pbas {
                    header += "\"\(pba)\"\t"
                }
                fileHandle.write("\(header)\n".data(using: .utf8)!)

                // Then the full yearly data
                for year in (self.startYear...self.endYear) {
                    var line = "\(year)"
                    for pba in top_pbas {
                        line += "\t\(self.counts[year]![pba]!)"
                    }
                    fileHandle.write("\(line)\n".data(using: .utf8)!)
                }
                fileHandle.closeFile()
            } else {
                print("Error opening file for writing")
            }
        } catch {
            print("Error writing to file")
        }
    }
}