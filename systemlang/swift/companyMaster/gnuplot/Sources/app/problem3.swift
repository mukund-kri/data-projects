// Problem 3: Company counts by "Principal Business Activity" over a given year range
import Foundation


class Problem3 {

    // The final analysed data
    var counts: [String: Int] = [:]
    var startYear: Int
    var endYear: Int

    // constructor, store and and analyze data
    init(data: [Company], startYear: Int, endYear: Int) {

        self.startYear = startYear
        self.endYear = endYear
        
        // Initialize the counts dictionary to 0. So no PBA is missed
        for pba in top_pbas {
            self.counts[pba] = 0
        }

        print("Analyzing data for problem 3 ...")
        for company in data {
            let year = company.registrationYear

            // Ignore companies that are not in the top pbas
            if !top_pbas.contains(company.pba) {
                continue
            }

            if year >= startYear && year <= endYear {
                self.counts[company.pba]! += 1
            }
        }
    }

    // write out data in gunplot .dat format
    func writeDataToGnuplotDatFile() {
        print("Writing plotting data for problem 3 ...")
        do {
            let datFile = "Plots/problem3.dat"
            // Make sure a fresh file is created
            if FileManager.default.fileExists(atPath: datFile) {
                try FileManager.default.removeItem(atPath: datFile) 
            }
            FileManager.default.createFile(atPath: datFile, contents: nil, attributes: nil)

            // write the contents of the counter dictionary to the file
            if let fileHandle = FileHandle(forWritingAtPath: datFile) {
                for (pba, count) in self.counts {
                    fileHandle.write("\"\(pba)\"\t\(count))\n".data(using: .utf8)!)
                }
                fileHandle.closeFile()
            } else {
                print("Error opening file for writing")
            }
        } catch {
            print("Error writing to file")
        }
    }       
}