import Foundation

class Problem1 {

    // The final analysed data
    var counts: [String: Int] =  [
        "0-1L": 0,
        "1L-10L": 0,
        "10L-1Cr": 0,
        "1Cr-10Cr": 0,
        ">10Cr": 0,
    ]

    // Labels to get correct order in gnuplot
    let labels = ["0-1L", "1L-10L", "10L-1Cr", "1Cr-10Cr", ">10Cr"]

    // constructor, store and and analyze data
    init(data: [Company]) {
        print("Analyzing data for problem 1 ...")
        for company in data {
            let ac = company.authorizedCapital
            if ac <= 100000 {
                counts["0-1L"]! += 1
            } else if ac <= 10000000 {
                counts["1L-10L"]! += 1
            } else if ac <= 100000000 {
                counts["10L-1Cr"]! += 1
            } else if ac <= 1000000000 {
                counts["1Cr-10Cr"]! += 1
            } else {
                counts[">10Cr"]! += 1
            }
        }
    }

    // write out data in gunplot .dat format
    func writeDataToGnuplotDatFile() {
        print("Writing plotting data for problem 1 ...")
        do {
            let datFile = "Plots/problem1.dat"
            // Make sure a fresh file is created
            if FileManager.default.fileExists(atPath: datFile) {
                try FileManager.default.removeItem(atPath: datFile) 
            }
            FileManager.default.createFile(atPath: datFile, contents: nil, attributes: nil)

            // write the contents of the counter dictionary to the file
            if let fileHandle = FileHandle(forWritingAtPath: datFile) {
                for label in self.labels {
                    let value = self.counts[label]!
                    fileHandle.write("\"\(label)\"\t\(value)\n".data(using: .utf8)!)
                }
                fileHandle.closeFile()
            } else {
                print("Error opening file for writing")
            }
        } catch {
            print("Error writing to file")
        }

    }
}