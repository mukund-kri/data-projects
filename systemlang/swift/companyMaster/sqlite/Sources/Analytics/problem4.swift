import Foundation

import SQLite


class Problem4 {

    let dbFile : String
    let startYear : Int
    let endYear : Int

    let queryPt1 = """
    SELECT pba, registration_year, COUNT(*) FROM company
    WHERE registration_year BETWEEN ? AND ?
    AND pba IN (
    """

    let queryPt2 = ") GROUP BY pba, registration_year"

    // Constructor
    init(dbFile: String, startYear: Int, endYear: Int) {
        self.dbFile = dbFile
        self.startYear = startYear
        self.endYear = endYear
    }

    // Query to get the count of companies for each year over a range of years
    func generateGnuplotDataFile() {
        do {
            // Turn the top pbs into SQL for in statement
            let topPbasString = topPbas.map { "'\($0)'" }.joined(separator: ", ")
            
            let query = queryPt1 + topPbasString + queryPt2

            // Query the db
            let db = try Connection(dbFile)
            let stmt = try db.prepare(query, startYear, endYear)
            print("Running query: ")

            // Load the counts into a dictionary, makes it easy to generate dat file
            var counts = [Int64: [String: Int64]]()
            for row in stmt {
                let pba = row[0] as! String
                let year = row[1] as! Int64
                let count = row[2] as! Int64

                if counts[year] == nil {
                    counts[year] = [String: Int64]()
                }
                counts[year]![pba] = count
            }

            // The header is the list of top pba
            let header = topPbas.map { "\"\($0)\"" }.joined(separator: "\t")

            let datFile = "Plots/problem4.dat"
            // Make sure a fresh file is created
            if FileManager.default.fileExists(atPath: datFile) {
                try FileManager.default.removeItem(atPath: datFile) 
            }
            FileManager.default.createFile(atPath: datFile, contents: nil, attributes: nil)

            // write the contents of the counter dictionary to the file
            if let fileHandle = FileHandle(forWritingAtPath: datFile) {

                // First write the header row, the list of pbas
                fileHandle.write("\(header)\n".data(using: .utf8)!)

                // Then the full yearly data
                for year in (self.startYear...self.endYear) {
                    var line = "\(year)"
                    for pba in topPbas {
                        line += "\t\(counts[Int64(year)]![pba]!)"
                    }
                    fileHandle.write("\(line)\n".data(using: .utf8)!)
                }
                fileHandle.closeFile()
            } else {
                print("Error opening file for writing")
            }
        } catch {
            print("Error writing to file")
        }
    }        
}