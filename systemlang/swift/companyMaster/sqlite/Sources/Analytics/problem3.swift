import Foundation

import SQLite


class Problem3 {

    let dbFile : String
    let startYear : Int
    let endYear : Int

    let queryPt1 = """
    SELECT pba, COUNT(*) AS counts
    FROM company
    WHERE registration_year BETWEEN ? AND ?
    AND pba IN (
    """

    let queryPt2 = ") GROUP BY pba"

    // Constructor
    init(dbFile: String, startYear: Int, endYear: Int) {
        self.dbFile = dbFile
        self.startYear = startYear
        self.endYear = endYear
    }

    // Query to get the count of companies for each year over a range of years
    func generateGnuplotDataFile() {
        do {
            // Turn the top pbs into SQL for in statement
            let topPbasString = topPbas.map { "'\($0)'" }.joined(separator: ", ")
            
            let query = queryPt1 + topPbasString + queryPt2

            // Query the db
            let db = try Connection(dbFile)
            let stmt = try db.prepare(query, startYear, endYear)

            print("Running query: ")
            // Format the db result to gnuplot format
            var data = stmt.map { row in
                "\"\(row[0] as! String)\"\t\(row[1] as! Int64)"
            }.joined(separator: "\n")
            data = data + "\n"


            // Write the data to a file
            try data.write(toFile: "Plots/problem3.dat", atomically: true, encoding: .utf8)
            print("Generated Plots/problem3.dat")

        } 
        catch {
            print(error)
        }
    }

}