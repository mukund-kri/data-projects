import Foundation

import SQLite

class Problem2 {

    let dbFile : String

    // Query to get the count of companies for each year over a range of years
    let query = """
    SELECT registration_year, COUNT(registration_year) FROM company
    WHERE registration_year BETWEEN 2000 AND 2018
    GROUP BY registration_year
    """

    init(dbFile: String) {
        self.dbFile = dbFile
    }

    // Generate gnuplot .dat file from the query result
    func generateGnuplotDataFile() {
        do {
            // Query the db
            let db = try Connection(dbFile)
            let stmt = try db.prepare(query)
            
            // Format the db result to gnuplot format
            var data = stmt.map { row in
                "\(row[0] as! Int64)\t\(row[1] as! Int64)"
            }.joined(separator: "\n")
            data = data + "\n"

            // Write the data to a file
            let file = "Plots/problem2.dat"
            try data.write(toFile: file, atomically: true, encoding: .utf8)
            print("Generated \(file)")
        } catch {
            print(error)
        }
    }

}