import Foundation

import SQLite


class Problem1 {

    let dbFile : String

    // To get labeling order correct in the plot
    let labels = ["<=1L", "1L-10L", "10L-1Cr", "1Cr-10Cr", ">10Cr"]

    // Query to get the count of companies in different authorized capital ranges
    let query = """
    SELECT
            CASE
                WHEN authorized_capital <= 100000 THEN '<=1L'
                WHEN authorized_capital > 100000 AND authorized_capital <= 1000000 THEN '1L-10L'
                WHEN authorized_capital > 1000000 AND authorized_capital <= 10000000 THEN '10L-1Cr'
                WHEN authorized_capital > 10000000 AND authorized_capital <= 100000000 THEN '1Cr-10Cr'
                ELSE '>10Cr'
            END
            AS authorized_capital_range,
            COUNT(authorized_capital) AS counts
        FROM company
        GROUP BY authorized_capital_range
    """

    init(dbFile: String) {
        self.dbFile = dbFile
    }

    // Generate gnuplot .dat file from the query result
    func generateGnuplotDataFile() {
        do {
            // Query the db
            let db = try Connection(dbFile)
            let stmt = try db.prepare(query)
            
            // Convert into a dictionary
            var counts = [String: Int64]()
            for row in stmt {
                let label = row[0] as! String 
                let count = row[1] as! Int64
                counts[label] = count
            }

            // Format the db result to gnuplot format
            let data = self.labels.map { label in
                "\(label) \(counts[label] ?? 0)"
            }.joined(separator: "\n")

            // Write the data to a file
            let file = "Plots/problem1.dat"
            try data.write(toFile: file, atomically: true, encoding: .utf8)
            print("Generated \(file)")
        } catch {
            print(error)
        }
    }
}