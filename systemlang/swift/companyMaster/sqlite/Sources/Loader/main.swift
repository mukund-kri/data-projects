import Foundation

import SQLite
import SwiftCSV


// DB connection
let dbFile = "Delhi.db"

// CSV Source
let csvFile = "Data/Delhi.clean.csv"

// Number formatter to convert string to double
let formatter = NumberFormatter()


var insertQuery = "INSERT INTO company (registration_year, authorized_capital, pba) VALUES "
// Read the CSV file and construct query
do {
    let csv = try CSV<Named>(url: URL(fileURLWithPath: csvFile))
    for row in csv.rows {

        // Simple validation remove NA values
        if row["AUTHORIZED_CAP"] == "NA" || row["DATE_OF_REGISTRATION"] == "NA" || row["PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN"] == "NA" {
            continue
        }

        // First convert the authorized capital to double from String
        let f = formatter.number(from: row["AUTHORIZED_CAP"]!)!
        let authorizedCapital = Int(f)

        let registrationDate = row["DATE_OF_REGISTRATION"]!

        // Extract year from the date
        let registrationYear = Int(registrationDate.suffix(2))!

        let pba = row["PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN"]!

        if registrationYear < 20 {
            insertQuery += "(\(registrationYear + 2000), \(authorizedCapital), '\(pba)'),"
        } else {
            insertQuery += "(\(registrationYear + 1900), \(authorizedCapital), '\(pba)'),"
        }   
    }
    insertQuery.removeLast()

} catch {
    print(error)
}


// Now lets insert data into the DB
do {
    let db = try Connection(dbFile)
    let output = try db.execute(insertQuery)
    print(output)
    print("Inserted data into the DB")
} catch {
    print(error)
}
