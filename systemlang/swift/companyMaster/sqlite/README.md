# Company Master :: Swift :: SQlite version

This project contains code for the "Company Master" data project. In this case implemented in 
Swift using SQLite as the data store.

It has 2 parts:

1. A Loader, that reads the CSV file and loads the data into a SQLite database.
2. Analytics that queries the SQLite database to answer the questions.


## NOTE!

Though `SQLite.swift` provides a nice `swift-language` interface to SQLite, it does not 
fully work on linux. So, i'm going to use only the *raw sql interface* in this version.
In future when I get access to a Mac, I will implement the swift-language interface.

## How to run

1. Make sure no previous database exists. If it does, delete it. We can do that by 
checking for a file called `Delhi.db` and deleting it.

2. Run the loader:

```bash
swift run loader
```

3. Run the analytics:

```bash
swift run analytics
```

This should generate all plots.
