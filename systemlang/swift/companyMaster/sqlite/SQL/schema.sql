CREATE TABLE company (
    id INT PRIMARY KEY,
    corporate_id varchar(255),
    authorized_capital INT,
    registration_year INT,
    pba varchar(512)
);