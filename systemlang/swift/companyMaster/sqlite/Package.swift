// swift-tools-version: 5.10
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "sqlite",
    products: [
        .executable(name: "analytics", targets: ["Analytics"]),
        .executable(name: "loader", targets: ["Loader"])
    ],
    dependencies: [
        .package(url: "https://github.com/stephencelis/SQLite.swift.git", from: "0.15.3"),
        .package(url: "https://github.com/swiftcsv/SwiftCSV.git", from: "0.8.0")

    ],
    targets: [
        // Targets are the basic building blocks of a package, defining a module or a test suite.
        // Targets can depend on other targets in this package and products from dependencies.
        .executableTarget(
            name: "Analytics", dependencies: [
                .product(name: "SQLite", package: "SQLite.swift")
            ]),
        .executableTarget(
            name: "Loader", dependencies: [
                .product(name: "SQLite", package: "SQLite.swift"),
                "SwiftCSV"
            ]),
    ]
)
