require "csv"
require "cryplot"

CSV_FILE   = "../data/Delhi.clean.csv"
START_YEAR =  1
END_YEAR   = 20

# # the collector data structure
companies_by_year = Hash(Int32, Int32).new

# Initialize the values of companies_by_year to zero
index = START_YEAR
while index <= END_YEAR
  companies_by_year[index] = 0_i32
  index += 1
end

# # read CSV
File.open CSV_FILE do |file|
  csv = CSV.new file, headers: true

  while csv.next
    date = csv.row["DATE_OF_REGISTRATION"].to_s

    if date == "NA"
      next
    end
    year = date.split("-")[2].to_i32

    if year < START_YEAR || year > END_YEAR
      next
    end

    companies_by_year[year] += 1
  end
end

x = companies_by_year.keys.map { |year| year + 2000 }
y = companies_by_year.values

# # plot
Cryplot.plot {
  legend.at_top_left

  ylabel "Number of Companies"
  xlabel "Year"

  draw_boxes(x, y)
    .fill_solid
    .fill_color("pink")
    .fill_intensity(0.5)
    .border_show
    .label_none

  box_width_relative(0.75)

  show
}
