require "csv"

require "cryplot"

CSV_FILE = "../data/Delhi.clean.csv"
BINS     = ["0-1L", "1L-10L", "10L-1Cr", "1Cr-10Cr", "10Cr+"]

# # The collector data structure
counts = Hash(String, Int64).new

# # Read CSV
File.open(CSV_FILE) do |file|
  csv = CSV.new(file, headers: true)

  while csv.next
    auth_cap = csv.row["AUTHORIZED_CAP"].to_f64.to_i64

    case auth_cap
    when 1..1_00_000
      counts["0-1L"] = counts.fetch("0-1L", 0_i64) + 1
    when 1_00_001..10_00_000
      counts["1L-10L"] = counts.fetch("1L-10L", 0_i64) + 1
    when 10_00_001..1_00_00_000
      counts["10L-1Cr"] = counts.fetch("10L-1Cr", 0_i64) + 1
    when 1_00_00_001..10_00_00_000
      counts["1Cr-10Cr"] = counts.fetch("1Cr-10Cr", 0_i64) + 1
    else
      counts["10Cr+"] = counts.fetch("10Cr+", 0_i64) + 1
    end
  end
end

# Collect the counts as the y data
y = BINS.map { |bin| counts.fetch(bin, 0_i64) }

# # Plot
Cryplot.plot {
  legend.at_top_left

  ylabel "Number of Companies"
  xlabel "Authorized Capital"

  draw_boxes(BINS, y)
    .fill_solid
    .fill_color("pink")
    .fill_intensity(0.5)
    .border_show
    .label_none

  box_width_relative(0.75)

  show
}
