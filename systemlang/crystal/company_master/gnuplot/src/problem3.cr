require "csv"
require "cryplot"

CSV_FILE   = "../data/Delhi.clean.csv"
START_YEAR =  1
END_YEAR   = 20

# # the collector data structure
pba_by_year = Hash(String, Int32).new

# # read CSV
File.open CSV_FILE do |file|
  csv = CSV.new file, headers: true

  while csv.next
    pba = csv.row["PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN"]
    if pba == "NA"
      next
    end
    pba_by_year[pba] = pba_by_year.fetch(pba, 0_i32) + 1
  end
end

# Write to dat file instead of plotting
top_pbas = pba_by_year.to_a.sort_by { |x| x[1] }.reverse[0..9]

File.open("plots/problem3.dat", "w") do |file|
  top_pbas.each do |pba, count|
    file.puts "\"#{pba}\" #{count}"
  end
end
