require "csv"
require "cryplot"

CSV_FILE   = "../data/Delhi.clean.csv"
START_YEAR =  1
END_YEAR   = 20

# # the collector data structure
counts = Hash(String, Hash(Int32, Int32)).new

# Read file and load the pba line by line into an array
top_pbas = File.read_lines "../data/top_pba.txt"

# Initialize the counts to zero
top_pbas.each do |pba|
  counts[pba] = Hash(Int32, Int32).new
  index = START_YEAR
  while index <= END_YEAR
    counts[pba][index] = 0_i32
    index += 1
  end
end

# # read CSV
File.open CSV_FILE do |file|
  csv = CSV.new file, headers: true

  while csv.next
    pba = csv.row["PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN"]

    # Ignore lines in which pba is NA
    if pba == "NA"
      next
    end

    # if the pba is not in the top_pbas, skip
    if !top_pbas.index(pba)
      next
    end

    date = csv.row["DATE_OF_REGISTRATION"]
    if date == "NA"
      next
    end

    year = date.split("-")[2].to_i32

    if year < START_YEAR || year > END_YEAR
      next
    end

    counts[pba][year] += 1
  end
end

# # Plot
# Generate dat file for the plot

File.open("plots/problem4.dat", "w") do |file|
  # The top line is a tab seperated list of top pbas
  file.puts top_pbas.join("\t")

  # Then count up the years
  index = START_YEAR
  while index <= END_YEAR
    line = top_pbas.map { |pba| counts[pba][index] }
    file.puts "#{index}\t#{line.join("\t")}"
    index += 1
  end
end
