require "db"
require "sqlite3"
require "cryplot"

module Problem
  class Plot1
    QUERY = %q(SELECT
        CASE
            WHEN authorized_capital <= 100000 THEN '<=1L'
            WHEN authorized_capital > 100000 AND authorized_capital <= 1000000 THEN '1L-10L'
            WHEN authorized_capital > 1000000 AND authorized_capital <= 10000000 THEN '10L-1Cr'
            WHEN authorized_capital > 10000000 AND authorized_capital <= 100000000 THEN '1Cr-10Cr'
            ELSE '>10Cr'
        END
        AS authorized_capital_range,
        COUNT(authorized_capital) AS counts
    FROM company
    GROUP BY authorized_capital_range)

    # Company count accumulator
    property counts : Hash(String, Int32)

    # Constructor
    def initialize(db_url : String)
      # Initialize the counts hash
      @counts = {
        "<=1L"     => 0,
        "1L-10L"   => 0,
        "10L-1Cr"  => 0,
        "1Cr-10Cr" => 0,
        ">10Cr"    => 0,
      }

      # Open connection and query
      DB.open db_url do |db|
        db.query QUERY do |results|
          results.each do
            range = results.read(String)
            count = results.read(Int32)

            # Add to the counts hash
            @counts[range] = count
          end
        end
      end
    end # end of initialize

    # # Generate gnuplot compatable .dat file
    def generate
      File.open("plots/problem1.dat", "w") do |file|
        @counts.each do |k, v|
          file.puts "#{k} #{v}"
        end
      end
    end

    # Plot the graph
    def plot
      puts "Plotting the graph..."
      # Plot
      Cryplot.plot {
        title "Company registrations in Delhi"
        xlabel "Authorized Capital Range"
        ylabel "Number of companies"
        draw_boxes(@counts.keys, @counts.values)
          .fill_solid
          .fill_color("blue")
          .fill_intensity(0.5)
          .border_show
          .label_none

        box_width_relative(0.75)
        show
      }
    end # end of plot

  end
end
