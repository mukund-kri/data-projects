require "csv"
require "db"
require "sqlite3"

module Loader
  extend self

  def load(csv_file : String, db_url : String)
    # Open connection
    db = DB.open db_url

    insert_stmt = ""

    File.open(csv_file) do |file|
      csv = CSV.new(file, headers: true)

      insert_stmt = String.build do |buffer|
        buffer << "INSERT INTO company (corporate_id, registration_year, authorized_capital, pba) VALUES "

        csv.each do |row|
          corporate_id = row["CORPORATE_IDENTIFICATION_NUMBER"]
          registration_date = row["DATE_OF_REGISTRATION"]
          auth_cap = row["AUTHORIZED_CAP"]
          pba = row["PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN"]

          # Clean up all NA values
          if registration_date == "NA" || auth_cap == "NA" || pba == "NA"
            next
          end

          # Extract year from the date
          registration_year = registration_date.split("-")[2].to_i

          # Add to the insert statement
          buffer << "(\"#{corporate_id}\", \"#{registration_year}\", #{auth_cap}, \"#{pba}\"),"
        end
      end
    end

    # Remove the trailing comma
    insert_stmt = insert_stmt.rchop

    # Execute the insert statement
    db.exec insert_stmt

    # Clean up db connection
    db.close
  end
end
