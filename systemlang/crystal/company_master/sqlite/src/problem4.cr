require "db"
require "sqlite3"

module Problem
  class Plot4
    # Query is in two parts as insertion into IN statement is not supported
    QUERY1 = %Q(SELECT pba, registration_year, COUNT(*) FROM company
WHERE registration_year BETWEEN ? AND ?
AND pba IN)
    QUERY2 = " GROUP BY pba, registration_year"

    # Data accumulator
    property counts : Hash(String, Hash(Int32, Int32))

    # pre-declared pbas
    property top_pbas : Array(String)

    # Constructor
    def initialize(db_url : String, @start_year : Int32, @end_year : Int32)
      # Initialize the counts hash
      @counts = Hash(String, Hash(Int32, Int32)).new
      @top_pbas = [] of String

      # Load pbas from file
      File.each_line("../data/top_pba.txt") do |line|
        @top_pbas << line
      end

      # top pbas to query
      pbas = "(\"#{top_pbas.join("\", \"")}\")"

      # Zero out all the values in the counts hash
      @top_pbas.each do |pba|
        @counts[pba] = Hash(Int32, Int32).new
        (start_year..end_year).each do |year|
          @counts[pba][year] = 0
        end
      end

      # Construct the query
      query = QUERY1 + pbas + QUERY2

      # Open connection and query
      DB.open db_url do |db|
        db.query query, @start_year, @end_year do |results|
          results.each do
            pba = results.read(String)
            year = results.read(Int32)
            count = results.read(Int32)

            @counts[pba][year] = count
          end
        end
      end
    end # end of initialize

    # Plot the graph
    # # TODO: Study cryplot to make this plot not suck. Now it's just a placeholder.
    def plot
    end

    # Generate gnuplot compatable .dat file
    def generate
      File.open("plots/problem4.dat", "w") do |file|
        # The top line is a tab seperated list of top pbas
        file.puts @top_pbas.map { |pba| "\"#{pba}\"" }.join("\t")

        # Then count up the years
        index = @start_year
        while index <= @end_year
          year = index + 2000
          line = @top_pbas.map { |pba| @counts[pba][index] }
          file.puts "#{year}\t#{line.join("\t")}"
          index += 1
        end
      end
    end # end of generate

  end # end of Plot4
end   # end of Problem
