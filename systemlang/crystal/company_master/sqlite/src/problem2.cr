require "db"
require "sqlite3"
require "cryplot"

module Problem
  class Plot2
    QUERY = %q(SELECT registration_year, COUNT(registration_year) FROM company
    WHERE registration_year BETWEEN 1 AND 18
    GROUP BY registration_year)

    property y : Array(Int64)
    property years : Array(Int64)

    def initialize(db_url : String)
      # The x and y axis array
      x = [] of Int64
      @y = [] of Int64

      # Open connection and query
      DB.open DB_CONNECTION do |db|
        db.query QUERY do |results|
          results.each do
            year = results.read(Int)
            counts = results.read(Int)

            x << year
            y << counts
          end
        end
      end

      # Properly format the years
      @years = x.map { |year| year + 2000 }
    end

    # # Generate data file for gnuplot
    def generate
      File.open("plots/problem2.dat", "w") do |file|
        @years.zip(@y).each do |k, v|
          file.puts "#{k} #{v}"
        end
      end
    end

    def plot
      puts "Plotting the graph..."
      # Plot
      Cryplot.plot {
        title "Company registrations in Delhi"
        xlabel "Year"
        ylabel "Number of companies"
        draw_boxes(@years, @y)
          .fill_solid
          .fill_color("blue")
          .fill_intensity(0.5)
          .border_show
          .label_none

        box_width_relative(0.75)
        show
      }
    end
  end
end
