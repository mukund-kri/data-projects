# # Plot of count of company registration by principal business activity
# #  - We can spicify the range of years to plot
##

require "db"
require "sqlite3"
require "cryplot"

module Problem
  class Plot3
    QUERY1 = %q(SELECT pba, COUNT(*) AS counts
    FROM company
    WHERE registration_year BETWEEN ? AND ?
    AND pba IN )

    QUERY2 = " GROUP BY pba"

    # Data accumulator
    property counts : Hash(String, Int32)

    # pre-declared pbas
    property top_pbas : Array(String)

    # Constructor
    def initialize(db_url : String, start_year : Int32, end_year : Int32)
      # Initialize the counts hash
      @counts = Hash(String, Int32).new
      @top_pbas = [] of String

      # Load pbas from file
      File.each_line("../data/top_pba.txt") do |line|
        @top_pbas << line
      end

      # top pbas to query
      pbas = "(\"#{top_pbas.join("\", \"")}\")"

      # Construct the query
      query = QUERY1 + pbas + QUERY2

      # Open connection and query
      DB.open db_url do |db|
        db.query query, start_year, end_year do |results|
          results.each do
            pba = results.read(String)
            count = results.read(Int32)

            # Add to the counts hash
            @counts[pba] = count
          end
        end
      end
    end # end of initialize

    # Plot the graph
    def plot
    end

    # Write data file for gnuplot
    def generate
      # Dump out to dat file
      File.open("plots/problem3.dat", "w") do |file|
        @counts.each do |k, v|
          file.puts "\"#{k}\" #{v}"
        end
      end
    end
  end # end of class
end   # end of module
