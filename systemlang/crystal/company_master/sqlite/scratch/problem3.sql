SELECT pba, COUNT(*) AS counts
FROM company
WHERE registration_year BETWEEN 0 AND 10
AND pba IN ("Construction", "Real estate activities")

GROUP BY pba
