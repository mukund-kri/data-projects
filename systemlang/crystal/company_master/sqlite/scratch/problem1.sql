SELECT 
    CASE 
        WHEN authorized_capital <= 100000 THEN '<=1L'
        WHEN authorized_capital > 100000 AND authorized_capital <= 1000000 THEN '1L-10L'
        WHEN authorized_capital > 1000000 AND authorized_capital <= 10000000 THEN '10L-1Cr'
        WHEN authorized_capital > 10000000 AND authorized_capital <= 100000000 THEN '1Cr-10Cr'
        ELSE '>10Cr'
    END
    AS authorized_capital_range,
    COUNT(authorized_capital) AS counts
FROM company
GROUP BY authorized_capital_range;