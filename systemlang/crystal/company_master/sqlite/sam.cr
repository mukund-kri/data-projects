#!/bin/crystal

require "sam"

require "./src/loader.cr"
require "./src/problem1.cr"
require "./src/problem2.cr"
require "./src/problem3.cr"
require "./src/problem4.cr"

SOURCE_CSV    = "../data/Delhi.clean.csv"
DB_CONNECTION = "sqlite3://Delhi.db"

task "db-load" do
  Loader.load(SOURCE_CSV, DB_CONNECTION)
end

task "db-clean" do
  # Delete the db file
  File.delete("Delhi.db")
end

task "db-init" do
  # run the process command sqlite3 Delhi.db < src/schema.sql
  Process.run("sqlite3", args: ["Delhi.db", "<", "sql/schema.sql"]) do |process|
    output = process.output
    error = process.error

    output.each_line do |line|
      puts line
    end

    error.each_line do |line|
      puts line
    end
  end
end

# Plot the first problem
task "problem1" do
  puts "Generating plot for problem 1..."
  plot = Problem::Plot1.new(DB_CONNECTION)
  plot.generate
  puts "Done!"
end

# Plot the second problem
task "problem2" do
  puts "Generating plot for problem 2..."
  plot = Problem::Plot2.new(DB_CONNECTION)
  plot.generate
  puts "Done!"
end

# Plot the third problem
task "problem3" do
  puts "Generating plot for problem 3..."
  plot = Problem::Plot3.new(DB_CONNECTION, 1, 20)
  plot.generate
  puts "Done!"
end

# Plot the fourth problem
task "problem4" do
  puts "Generating plot for problem 4..."
  plot = Problem::Plot4.new(DB_CONNECTION, 1, 20)
  plot.generate
  puts "Done!"
end

# Plot all the problems
task "all" do
  puts "(Re)generating all plots..."
  plot = Problem::Plot1.new(DB_CONNECTION)
  plot.generate

  plot = Problem::Plot2.new(DB_CONNECTION)
  plot.generate

  plot = Problem::Plot3.new(DB_CONNECTION, 1, 20)
  plot.generate

  plot = Problem::Plot4.new(DB_CONNECTION, 1, 20)
  plot.generate
end
