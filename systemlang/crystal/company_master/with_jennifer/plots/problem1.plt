# Setup title
set title "Problem 1 :: Registration by Authorized Capital"
set title font ",20"

# xtic
set xtics ()
set xtics rotate by 0

# Plot
plot "problem1.dat" using 2: xtic(1) with histogram title "Registration by Authorized Capital" 
