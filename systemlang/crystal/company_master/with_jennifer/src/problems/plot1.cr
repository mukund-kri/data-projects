# # Generate plot for problem 1

module Problems
  class Plot1
    # Isolate only the CASE clause for authorized capital
    CASE_CLAUSE = %q(
    CASE
    WHEN authorized_capital <= 100000 THEN '0-1L'
    WHEN authorized_capital <= 1000000 THEN '1L-10L'
    WHEN authorized_capital <= 10000000 THEN '10L-1Cr'
    WHEN authorized_capital <= 100000000 THEN '1Cr-10Cr'
    ELSE '10Cr+'
    END
    AS category
    )

    # Counts accumulator for each category.
    # Note: Sqlite3 int is 64 bit so we use Int64. Not that the number is that big.
    property counts : Hash(String, Int64)

    def initialize
      # Initialize counts
      # Zero out the counts. Need to do this to ensure all categories are present
      # and the order is maintained.
      @counts = {
        "0-1L"     => 0_i64,
        "1L-10L"   => 0_i64,
        "10L-1Cr"  => 0_i64,
        "1Cr-10Cr" => 0_i64,
        "10Cr+"    => 0_i64,
      }

      # Histogram of companies by authorized capital
      companies = Company
        .select(CASE_CLAUSE + ", COUNT(*) as count")
        .group("category")
        .pluck(:category, :count)

      # Organize the results into a hash
      companies.each do |row|
        category = row[0].as(String)
        count = row[1].as(Int64)
        @counts[category] = count
      end
    end # initialize

    def generate
      File.open("plots/problem1.dat", "w") do |file|
        @counts.each do |k, v|
          file.puts "\"#{k}\"\t#{v}"
        end
      end # File.open
    end   # generate
  end     # Plot1
end       # Problems
