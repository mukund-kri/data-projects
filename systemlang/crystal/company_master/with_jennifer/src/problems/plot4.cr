module Problems
  class Plot4
    # Counts accumulator for each PBA per year
    property counts : Hash(String, Hash(Int32, Int32))

    # Top 10 PBAs under consideration
    property top_pbas : Array(String)

    def initialize(@start_year : Int32, @end_year : Int32)
      # Load top PBAs
      @top_pbas = load_top_pbas

      # Initialize counts to zero
      @counts = Hash(String, Hash(Int32, Int32)).new
      @top_pbas.each do |pba|
        @counts[pba] = Hash(Int32, Int32).new
        (start_year..end_year).each do |year|
          @counts[pba][year] = 0
        end
      end

      # Update counts with DB results
      result.each do |row|
        pba = row[0].as(String)
        year = row[1].as(Int64).to_i
        count = row[2].as(Int64).to_i
        @counts[pba][year] = count
      end
    end # initialize

    # # Load the top 10 PBAs from file top_pba.txt
    def load_top_pbas : Array(String)
      top_pbas = Array(String).new
      File.each_line("../data/top_pba.txt") do |line|
        top_pbas << line
      end
      top_pbas
    end # load_top_pbas

    # Generate a gnuplot comptible data file for problem 4
    def generate
      File.open("plots/problem4.dat", "w") do |file|
        # The top line is a tab seperated list of top pbas
        file.puts @top_pbas.map { |pba| "\"#{pba}\"" }.join("\t")

        # Then count up the years
        (@start_year..@end_year).each do |index|
          year = index + 2000
          line = @top_pbas.map { |pba| @counts[pba][index] }
          file.puts "#{year}\t#{line.join("\t")}"
          index += 1
        end
      end
    end # generate

  end # Plot4

end # Problems
