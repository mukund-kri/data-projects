module Problems
  class Plot3
    property counts : Hash(String, Int32)
    property top_pbas : Array(String)

    def initialize(start_year : Int32, end_year : Int32)
      # Initialize counts
      @counts = Hash(String, Int32).new

      @top_pbas = load_top_pbas

      # Load all the companies
      result = Company
        .select("pba, COUNT(*) as count")
        .group("pba")
        .where { _registration_year.between(1, 20) & _pba.in(@top_pbas) }
        .pluck(:pba, :count)

      # Update counts with DB rusults
      result.each do |row|
        pba = row[0].as(String)
        count = row[1].as(Int64).to_i
        @counts[pba] = count
      end
    end # initialize

    # Load the top 10 PBAs from file top_pba.txt
    def load_top_pbas : Array(String)
      top_pbas = Array(String).new
      File.each_line("../data/top_pba.txt") do |line|
        top_pbas << line
      end
      top_pbas
    end # load_top_pbas

    def generate
      # Write counts to dat file
      File.open "plots/problem3.dat", "w" do |file|
        @counts.each do |pba, count|
          file.puts "\"#{pba}\"\t#{count}"
        end
      end # File.open
    end   # generate
  end     # Plot2
end       # Problems
