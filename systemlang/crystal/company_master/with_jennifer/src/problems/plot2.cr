# # Generate plot for problem 2

module Problems
  class Plot2
    property counts : Hash(Int32, Int32)

    def initialize(start_year : Int32, end_year : Int32)
      # Initialize counts
      @counts = Hash(Int32, Int32).new
      (start_year..end_year).each { |year| @counts[year] = 0 }

      # Load all the companies
      result = Company
        .select("registration_year, COUNT(*) as count")
        .group("registration_year")
        .where { _registration_year.between(1, 20) }
        .pluck(:registration_year, :count)

      # Update counts with DB rusults
      result.each do |row|
        year = row[0].as(Int64).to_i
        count = row[1].as(Int64).to_i
        @counts[year] = count
      end
    end # initialize

    def generate
      # Write counts to dat file
      File.open "plots/problem2.dat", "w" do |file|
        @counts.each do |year, count|
          file.puts "#{year}\t#{count}"
        end
      end # File.open
    end   # generate
  end
end
