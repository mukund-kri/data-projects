require "jennifer"

class Company < Jennifer::Model::Base
  with_timestamps

  mapping(
    id: Primary64,
    authorized_capital: Int64,
    registration_year: Int32,
    pba: String,
    created_at: Time?,
    updated_at: Time?,
  )
end
