# # Some common db maintainance tasks

module DBOps
  extend self

  def clean_company_table
    Company.delete
  end
end # DBOps
