#!/bin/crystal

# # We mostly run our migration with SAM

require "./config/*"
require "sam"

require "./db/migrations/*"
require "./src/load"
require "./src/dbops"

# # Require all the problem solutions
require "./src/problems/*"

load_dependencies "jennifer"

CSV_SOURCE = "../data/Delhi.clean.csv"

task "load" do
  Loader.load CSV_SOURCE
end

task "load-raw" do
  Loader.load_raw CSV_SOURCE, "sqlite3://db/Delhi.mca.db"
end

task "problem1" do
  plot = Problems::Plot1.new

  plot.generate
end

task "problem2" do
  plot = Problems::Plot2.new 1, 20
  plot.generate
end

task "problem3" do
  plot = Problems::Plot3.new 1, 20
  plot.generate
end

task "problem4" do
  plot = Problems::Plot4.new 1, 20
  plot.generate
end

# Clean up the database. This eases testing
task "clean-db" do
  DBOps.clean_company_table
end
