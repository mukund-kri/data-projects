class CreateCompanies < Jennifer::Migration::Base
  def up
    create_table :companies do |t|
      t.integer :authorized_capital, {:null => false}
      t.integer :registration_year, {:null => false}
      t.string :pba, {:null => false}

      t.timestamps
    end
  end

  def down
    drop_table :companies if table_exists? :companies
  end
end
