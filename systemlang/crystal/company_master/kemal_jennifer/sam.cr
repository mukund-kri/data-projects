#!/bin/crystal

# # Sam takes care of migrations for us
require "./config/*"
require "sam"

require "./db/migrations/*"

require "./scripts/*"

# # Load the jennifer scripts
load_dependencies "jennifer"

CSV_SOURCE = "../data/Delhi.clean.csv"

task "load" do
  Loader.load CSV_SOURCE
end

task "load-raw" do
  Loader.load_raw CSV_SOURCE, "sqlite3://db/Delhi.mca.db"
end
