CREATE TABLE migration_versions (version text NOT NULL);
CREATE TABLE companies (id integer PRIMARY KEY AUTOINCREMENT, authorized_capital integer NOT NULL, registration_year integer NOT NULL, pba text NOT NULL, created_at text NOT NULL, updated_at text NOT NULL);
