# # This will eventually load data from CSV. For now let'd do crud

require "csv"

require "../src/models/*"

module Loader
  extend self

  # NOTE: Takes too long recomend not use this. Use load_raw instead
  def load(csv_file : String)
    File.open csv_file do |file|
      csv = CSV.new file, headers: true

      csv.each do |row|
        registration_date = row["DATE_OF_REGISTRATION"]
        auth_cap = row["AUTHORIZED_CAP"]
        pba = row["PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN"]

        # Clan up all the NA values
        if registration_date == "NA" || auth_cap == "NA" || pba == "NA"
          next
        end

        # Extract registration year from date
        registration_year = registration_date.split("-")[2].to_i

        # update year to 4 digit
        if registration_year < 20
          registration_year += 2000
        else
          registration_year += 1900
        end

        # Insert row
        Company.create(
          authorized_capital: auth_cap.to_f.to_i64,
          registration_year: registration_year,
          pba: pba
        )
      end
    end # File.open
  end   # load

  def load_raw(csv_file : String, db_url : String)
    # Open connection
    db = DB.open db_url

    insert_stmt = ""

    File.open(csv_file) do |file|
      csv = CSV.new(file, headers: true)

      insert_stmt = String.build do |buffer|
        buffer << "INSERT INTO companies (registration_year, authorized_capital, pba, created_at, updated_at) VALUES "

        csv.each do |row|
          registration_date = row["DATE_OF_REGISTRATION"]
          auth_cap = row["AUTHORIZED_CAP"]
          pba = row["PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN"]

          # Clean up all NA values
          if registration_date == "NA" || auth_cap == "NA" || pba == "NA"
            next
          end

          # Extract year from the date
          registration_year = registration_date.split("-")[2].to_i

          # update year to 4 digit
          if registration_year < 20
            registration_year += 2000
          else
            registration_year += 1900
          end

          # Add to the insert statement
          buffer << "(\"#{registration_year}\", #{auth_cap}, \"#{pba}\", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),"
        end
      end
    end

    # Remove the trailing comma
    insert_stmt = insert_stmt.rchop

    # Execute the insert statement
    db.exec insert_stmt

    # Clean up db connection
    db.close
  end # load_raw
end   # Loader end
