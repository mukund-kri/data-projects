require "kemal"

require "../config/*"

before_all do |env|
  env.response.headers["Access-Control-Allow-Origin"] = "*"
  env.response.content_type = "application/json"
end

require "./problems/*"

# Run the server on port 8080
Kemal.run 8080
