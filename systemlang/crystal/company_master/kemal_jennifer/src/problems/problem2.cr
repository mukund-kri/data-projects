require "kemal"

require "../models/*"

module Problem2
  get "/problem2" do |env|
    # Fromat required by front end
    output = Array(Hash(String, Int64)).new

    # Get the start and end year from the query string
    start_year = env.params.query["startYear"].to_i
    end_year = env.params.query["endYear"].to_i

    # Load all the companies
    result = Company
      .select("registration_year, COUNT(*) as count")
      .group("registration_year")
      .where { _registration_year.between(start_year, end_year) }
      .pluck(:registration_year, :count)

    # Organize the results as required by the front end
    result.each do |row|
      puts row
      year = row[0].as(Int64)
      count = row[1].as(Int64)
      output << {"year" => year, "registrations" => count}
    end

    # Send json responseq
    output.to_json
  end # get "/problem2"

end # module Problem2
