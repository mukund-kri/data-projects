require "kemal"

require "../models/*"
require "./top_pbas"

module Problem4
  get "/problem4" do |env|
    start_year = 2010
    end_year = 2014

    # Query
    result = Company
      .select("pba, registration_year, COUNT(*) as count")
      .group("pba, registration_year")
      .where { _registration_year.between(start_year, end_year) & _pba.in(Data::TOP_PBAS) }
      .pluck(:pba, :registration_year, :count)

    # Aggregate by pba by year. And zero out the counts.
    counts = Hash(String, Hash(Int64, Int64)).new
    Data::TOP_PBAS.each do |pba|
      counts[pba] = Hash(Int64, Int64).new
      (start_year..end_year).each do |year|
        counts[pba][year] = 0_i64
      end
    end

    # Organize the results into a hash
    result.each do |row|
      pba = row[0].as(String)
      year = row[1].as(Int64)
      count = row[2].as(Int64)
      counts[pba][year] = count
    end

    # Organize the results as required by the front end
    output = Array(Hash(String, String | Int64)).new

    counts.each do |pba, years|
      row_hash = Hash(String, String | Int64).new
      row_hash["name"] = pba
      years.each do |year, count|
        row_hash[year.to_s] = count
      end
      output << row_hash
    end

    # Send json response
    output.to_json
  end
end # module Problem4
