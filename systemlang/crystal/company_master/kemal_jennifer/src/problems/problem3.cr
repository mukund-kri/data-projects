require "kemal"

require "../models/*"
require "./top_pbas"

module Problem3
  get "/problem3" do |env|
    output = Array(Hash(String, String | Int64)).new

    # Load all the companies
    result = Company
      .select("pba, COUNT(*) as count")
      .group("pba")
      .where { _registration_year.between(2000, 2020) & _pba.in(Data::TOP_PBAS) }
      .pluck(:pba, :count)

    # Organize the results as required by the front end
    result.each do |row|
      pba = row[0].as(String)
      count = row[1].as(Int64)
      output << {"pba" => pba, "registrations" => count}
    end

    # Send json response
    output.to_json
  end # get "/problem2"

end # module Problem2
