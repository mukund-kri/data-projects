require "kemal"

require "../models/*"

module Problem1
  # Isolate only the CASE clause for authorized capital
  CASE_CLAUSE = %q(
    CASE
    WHEN authorized_capital <= 100000 THEN '0-1L'
    WHEN authorized_capital <= 1000000 THEN '1L-10L'
    WHEN authorized_capital <= 10000000 THEN '10L-1Cr'
    WHEN authorized_capital <= 100000000 THEN '1Cr-10Cr'
    ELSE '10Cr+'
    END
    AS category)

  get "/problem1" do |env|
    # Initialize counts
    # Zero out the counts. Need to do this to ensure all categories are present
    # and the order is maintained.
    counts = {
      "0-1L"     => 0_i64,
      "1L-10L"   => 0_i64,
      "10L-1Cr"  => 0_i64,
      "1Cr-10Cr" => 0_i64,
      "10Cr+"    => 0_i64,
    }

    # Histogram of companies by authorized capital
    companies = Company
      .select(CASE_CLAUSE + ", COUNT(*) as count")
      .group("category")
      .pluck(:category, :count)

    # Organize the results into a hash
    companies.each do |row|
      category = row[0].as(String)
      count = row[1].as(Int64)
      counts[category] = count
    end

    output = Array(Hash(String, Int64 | String)).new

    # Transform
    counts.each do |category, count|
      output << {"label" => category, "count" => count}
    end
    # Send json response
    output.to_json
  end
end # module Problem1
