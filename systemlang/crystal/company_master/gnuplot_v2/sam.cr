#!/bin/crystal

require "sam"

# Require my plotting code
require "./src/problems/plot1"
require "./src/problems/plot2"
require "./src/problems/plot3"
require "./src/problems/plot4"
require "./src/data"

CSV_FILE = "../data/Delhi.clean.csv"

companies = Data.load_from_csv(CSV_FILE)

# Generate plot for problem 1
task "problem1" do
  puts "Problem 1: Plot for problem 1"
  plot = Problems::Plot1.new(companies)
  plot.generate
end

# Generate plot for problem 2
task "problem2" do
  puts "Problem 2: Plot for problem 2"
  plot = Problems::Plot2.new(companies, 0, 20)
  plot.generate
end

# Generate plot for problem 3
task "problem3" do
  puts "Problem 3: Plot for problem 3"
  plot = Problems::Plot3.new(companies, 0, 20)
  plot.generate
end

# Generate plot for problem 4
task "problem4" do
  puts "Problem 4: Plot for problem 4"
  plot = Problems::Plot4.new(companies, 0, 20)
  plot.generate
end

# Generate plot for all problems
task "all" do
  puts "Plots for all problems"

  plot = Problems::Plot1.new(companies)
  plot.generate

  plot = Problems::Plot2.new(companies, 0, 20)
  plot.generate

  plot = Problems::Plot3.new(companies, 0, 20)
  plot.generate

  plot = Problems::Plot4.new(companies, 0, 20)
  plot.generate
end
