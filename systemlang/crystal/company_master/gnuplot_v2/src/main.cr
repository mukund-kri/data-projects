require "./problems/plot1"
require "./problems/plot2"
require "./problems/plot3"
require "./problems/plot4"
require "./data"

CSV_FILE = "../data/Delhi.clean.csv"

companies = Data.load_from_csv(CSV_FILE)

plot = Problems::Plot2.new(companies, 0, 20)
plot.generate
