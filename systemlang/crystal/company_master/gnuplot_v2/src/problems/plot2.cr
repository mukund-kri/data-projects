## Problem 2: Company counts by registration year over a given year range
require "cryplot"

require "../data"

module Problems
  class Plot2
    property x : Array(Int32)
    property y : Array(Int32)

    def initialize(companies : Array(Data::Company), start_year : Int32, end_year : Int32)
      companies_by_year = Hash(Int32, Int32).new

      # initialize counts to zero
      (start_year..end_year).each do |year|
        companies_by_year[year] = 0
      end

      # Count by year of registration
      companies.each do |company|
        if company.regiration_year < start_year || company.regiration_year > end_year
          next
        end
        companies_by_year[company.regiration_year] += 1
      end

      # x access
      @x = companies_by_year.keys.map { |k| k + 2000 }
      @y = companies_by_year.values
    end

    # # Generate the .dat file for gnuplot
    def generate
      File.open("plots/problem2.dat", "w") do |file|
        @x.zip(@y).each do |k, v|
          file.puts "#{k} #{v}"
        end
      end
    end

    def plot_to_screen
      Cryplot.plot do
        legend.at_top_left

        ylabel "Number of Companies"
        xlabel "Year"

        draw_boxes(@x, @y)
          .fill_solid
          .fill_color("pink")
          .fill_intensity(0.5)
          .border_show
          .label_none

        box_width_relative(0.75)

        show
      end
    end
  end
end
