require "cryplot"

require "../data"

module Problems
  class Plot1
    property counts : Hash(String, Int64)
    property x : Array(String)
    property y : Array(Int64)

    def initialize(companies : Array(Data::Company))
      @counts = Hash(String, Int64).new
      @x = ["0-1L", "1L-10L", "10L-1Cr", "1Cr-10Cr", "10Cr+"]
      @y = Array(Int64).new

      # Count by year of registration
      companies.each do |company|
        case company.authorized_cap
        when 1..1_00_000
          @counts["0-1L"] = @counts.fetch("0-1L", 0_i64) + 1
        when 1_00_001..10_00_000
          @counts["1L-10L"] = @counts.fetch("1L-10L", 0_i64) + 1
        when 10_00_001..1_00_00_000
          @counts["10L-1Cr"] = @counts.fetch("10L-1Cr", 0_i64) + 1
        when 1_00_00_001..10_00_00_000
          @counts["1Cr-10Cr"] = @counts.fetch("1Cr-10Cr", 0_i64) + 1
        else
          @counts["10Cr+"] = @counts.fetch("10Cr+", 0_i64) + 1
        end
      end

      # y access
      @y = @x.map { |k| @counts.fetch(k, 0_i64) }
    end

    def generate
      File.open("plots/problem1.dat", "w") do |file|
        @x.zip(@y).each do |k, v|
          file.puts "#{k} #{v}"
        end
      end
    end

    def plot_to_screen
      Cryplot.plot do
        legend.at_top_left

        ylabel "Number of Companies"
        xlabel "Authorized Capital"

        draw_boxes(@x, @y)
          .fill_solid
          .fill_color("pink")
          .fill_intensity(0.5)
          .border_show
          .label_none

        box_width_relative(0.75)

        show
      end
    end
  end
end
