# Problem 3: Company counts by "Principal Business Activity" over a given year range
require "cryplot"

require "../data"

module Problems
  class Plot3
    property counts : Hash(String, Int64)

    def initialize(companies : Array(Data::Company), start_year : Int32, end_year : Int32)
      top_pbas = Array(String).new
      @counts = Hash(String, Int64).new

      # Load up the top pbas
      File.each_line("../data/top_pba.txt") do |line|
        top_pbas << line
      end

      # Count the pbas
      companies.each do |company|
        if company.regiration_year < start_year || company.regiration_year > end_year
          next
        end
        if !top_pbas.includes?(company.pba)
          next
        end

        # update pba counts
        @counts[company.pba] = @counts.fetch(company.pba, 0_i64) + 1
      end
    end

    def generate
      # Dump out to dat file
      File.open("plots/problem3.dat", "w") do |file|
        @counts.each do |k, v|
          file.puts "\"#{k}\" #{v}"
        end
      end
    end

    def plot_to_screen
    end
  end
end
