## Problem 4: Stacked bar chart of company counts by "Principal Business Activity" over 
## a given year range.

require "cryplot"

require "../data"

module Problems
  class Plot4
    @top_pbas : Array(String)
    property counts : Hash(String, Hash(Int32, Int32))

    def initialize(companies : Array(Data::Company), @start_year : Int32, @end_year : Int32)
      @top_pbas = Array(String).new
      @counts = Hash(String, Hash(Int32, Int32)).new

      # Load up the top pbas
      File.each_line("../data/top_pba.txt") do |line|
        @top_pbas << line
      end

      # Initialize counts to zero
      @top_pbas.each do |pba|
        @counts[pba] = Hash(Int32, Int32).new
        # Now initialize the years to zero
        (@start_year..@end_year).each do |year|
          @counts[pba][year] = 0
        end
      end

      # Count the pbas
      companies.each do |company|
        if company.regiration_year < start_year || company.regiration_year > end_year
          next
        end
        if !@top_pbas.includes?(company.pba)
          next
        end

        # update pba counts
        @counts[company.pba][company.regiration_year] += 1
      end
    end

    def generate
      File.open("plots/problem4.dat", "w") do |file|
        # The top line is a tab seperated list of top pbas
        file.puts @top_pbas.map { |pba| "\"#{pba}\"" }.join("\t")

        # Then count up the years
        index = @start_year
        while index <= @end_year
          year = index + 2000
          line = @top_pbas.map { |pba| @counts[pba][index] }
          file.puts "#{year}\t#{line.join("\t")}"
          index += 1
        end
      end
    end

    def plot_to_screen
    end
  end
end
