require "spec"

require "../src/problems/plot3"
require "../src/data"

extend Data

test_data_company = [
  Company.new(3, 1_07_500, "Construction"),
  Company.new(3, 1_000, "Real estate activities"),
]

top_pbas = [
  "Other business activities",
  "Computer and related activities",
  "Wholesale trade and commission trade, except of motor vehicles and motorcycles",
  "Construction",
  "Real estate activities",
  "Financial intermediation",
  "Transport, storage and communications",
  "Other community, social and personal service activities",
  "Retail trade, except of motor vehicles and motorcycles; repair of personal and household goods",
  "Manufacture of chemicals and chemical products",
]

describe Problems::Plot3 do
  it "should compute plot data" do
    plot = Problems::Plot3.new(test_data_company, 1, 5)

    plot.counts.keys.should eq ["Construction", "Real estate activities"]
    plot.counts.values.should eq [1, 1]
  end

  it "should generate plot problem2.dat" do
    plot = Problems::Plot3.new(test_data_company, 1, 5)

    plot.generate

    File.read_lines("plots/problem3.dat").to_a.should eq([
      "\"Construction\" 1",
      "\"Real estate activities\" 1",
    ])
  end
end
