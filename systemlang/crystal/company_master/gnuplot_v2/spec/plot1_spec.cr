require "spec"

require "../src/problems/plot1"
require "../src/data"

extend Data

test_data_company = [
  Company.new(5, 1_07_500, "Unclassified"),
  Company.new(5, 1_000, "Computers"),
]

describe Problems::Plot1 do
  it "should compute plot data" do
    plot = Problems::Plot1.new(test_data_company)

    counts = plot.counts

    counts["0-1L"].should eq(1)
    counts["1L-10L"].should eq(1)

    plot.x.should eq(["0-1L", "1L-10L", "10L-1Cr", "1Cr-10Cr", "10Cr+"])
    plot.y.should eq([1, 1, 0, 0, 0])
  end

  it "should generate plot data" do
    plot = Problems::Plot1.new(test_data_company)

    plot.generate

    File.read_lines("plots/problem1.dat").to_a.should eq([
      "0-1L 1",
      "1L-10L 1",
      "10L-1Cr 0",
      "1Cr-10Cr 0",
      "10Cr+ 0",
    ])
  end
end
