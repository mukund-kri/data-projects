require "spec"

require "../src/problems/plot4"
require "../src/data"

extend Data

test_data_company = [
  Company.new(3, 1_07_500, "Construction"),
  Company.new(3, 1_000, "Real estate activities"),
]

top_pbas = [
  "Other business activities",
  "Computer and related activities",
  "Wholesale trade and commission trade, except of motor vehicles and motorcycles",
  "Construction",
  "Real estate activities",
  "Financial intermediation",
  "Transport, storage and communications",
  "Other community, social and personal service activities",
  "Retail trade, except of motor vehicles and motorcycles; repair of personal and household goods",
  "Manufacture of chemicals and chemical products",
]

describe Problems::Plot4 do
  it "should compute plot data" do
    plot = Problems::Plot4.new(test_data_company, 1, 5)

    counts = plot.counts

    counts["Construction"][1].should eq(0)
    counts["Construction"][3].should eq(1)

    counts["Real estate activities"][2].should eq(0)
    counts["Real estate activities"][3].should eq(1)
  end
end
