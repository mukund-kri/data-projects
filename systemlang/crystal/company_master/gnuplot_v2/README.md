# gnuplot_v2

## Aim

To make a better version of the gnuplot charts of the company master data set.

## What is new?

1. Use crystal modules to better organize code.
2. Use `sam` to build/run tasks.
   - `sam problem1` to generate the first chart
   - `sam problem2` to generate the second chart
   - `sam problem3` to generate the third chart
   - `sam problem4` to generate the fourth chart
3. Tests for the crystal modules.
4. Read CSV once per run, not one per chart.
5. Structure for reading CSV into memory. Structs for each row.
6. Class based plots
7. OPTIONAL: plot to screen. otherwise, only saving to .dat file is OK.
8. Well written blog quality `README.md` with sections for initializing DB,
loading data, and running.

## Generating Charts

## Viewing Charts (Optional)