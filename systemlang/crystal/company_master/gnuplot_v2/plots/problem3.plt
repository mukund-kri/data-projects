# Setup title
set title "Problem 3 :: Registration by Principal Business Activity"
set title font ",20"

# xtic
set xtics ()
set xtics rotate by 90 right

# Plot
plot "problem3.dat" using 2: xtic(1) with histogram title "Registration by Principal Business Activity"
