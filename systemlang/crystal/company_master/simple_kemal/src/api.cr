require "kemal"

# Dummy data
companyData = [
  {"label" => "1L", "count" => 123},
  {"label" => "2L", "count" => 456},
  {"label" => "3L", "count" => 789},
]

before_all do |env|
  env.response.headers["Access-Control-Allow-Origin"] = "*"
  env.response.content_type = "application/json"
end

options "/*" do |env|
end

get "/problem1" do |env|
  companyData.to_json
end

# Run the server on port 8080
Kemal.run 8080
