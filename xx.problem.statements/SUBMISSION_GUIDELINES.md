# Submission Guidelines

## Checklist

Please make sure you have done all these things before you submit your code
for evaluation.

1. Code for node.js work and browser code should be separate folders.
1. The README.md should detail how to run the given code.
1. Please name the files with care so that the evaluator can understand the 
purpose of each file with ease.

