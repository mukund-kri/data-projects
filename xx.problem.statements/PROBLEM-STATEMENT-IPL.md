# IPL data set analytics

## Aim

To convert raw open data (run by run records in this case) into charts that 
tell some kind of story.

## Preparation

### raw data

The data for this exercise is sourced from https://www.kaggle.com/datasets/patrickb1912/ipl-complete-dataset-20082020?resource=download.

*NOTE* you might have to find data sources on your own. For example the country of origin
for the Umpires


## Problems

### 1. Total runs scored by team

Plot a chart of the total runs scored by each teams over the history of IPL.
Hint: use the total_runs field.

### 2. Top batsman for Royal Challengers Bangalore

Consider only games played by Royal Challengers Bangalore. Now plot the total
runs scored by every batsman playing for Royal Challengers Bangalore over the
history of IPL.

### 3. Foreign umpire analysis

Obtain a source for country of origin of umpires. 
Plot a chart of number of umpires by in IPL by country. Indian umpires should
be ignored as this would dominate the graph.

### 4. Stacked chart of matches won by team by season

Plot a stacked bar chart of ...

- number of games played
- by team 
- by season