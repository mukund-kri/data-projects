(defpackage company-master-gnuplot
  (:use :cl :cl-csv :iterate :parse-float))

(in-package company-master-gnuplot)


(defvar *labels* '("<1L" "1L - 10L" "10L - 1Cr" "1Cr - 10Cr" ">= 10Cr"))

;; (ql:quickload "cl-csv")
;; (ql:quickload "parse-float")

(defun get-paidup-cap (csv-file-name)
  "Extract the paidup capital from csv"
  (iter (for row in-csv csv-file-name skipping-header t)
    (collect (parse-float (nth 8 row)))))


(parse-float "2500000")
;; (get-paidup-cap #P"../data/mca.min.csv")
(defun label-cap (cap)
  (cond
    ((< cap 100000) "<1L")
    ((< cap 1000000) "1L - 10L")
    ((< cap 10000000) "10L - 1Cr")
    ((< cap 100000000) "1Cr - 10Cr")
    (t ">= 10Cr")))

(defun accumulate (lst)
  (let ((table (make-hash-table)))
    (loop for e in lst
	  do  (incf (gethash e table 0)))
    table))

(defun prob1 (csv-file-name)
  (accumulate
   (map
    'list
    #'label-cap
    (get-paidup-cap csv-file-name))))

;; (get-paidup-cap #P"../data/mca.clean.csv")

(defun print-ans (counts)
  (loop for label in *labels*
	do (format t "\"~a\" ~a~%" label (gethash label counts 0))))

;; (print-ans (prob1 #P"../data/mca.clean.csv"))

(defun main ()
  (print-ans (prob1 #P"./data/mca.clean.csv")))
