set style data histogram
set style histogram cluster gap 1

set style fill solid border rgb "black"
set xtics rotate by 90 right
set auto x
set yrange [0:*]
plot 'problem4.dat' using 2:xtic(1) title col, \
   '' using 3:xtic(1) title col, \
   '' using 4:xtic(1) title col, \
   '' using 5:xtic(1) title col