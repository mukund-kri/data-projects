(defsystem "company-master-gnuplot"
  :depends-on ("cl-csv" "parse-float")
  :components ((:module "src"
		:components
		((:file "main"))))
  :build-operation "asdf:program-op"
  :build-pathname "company-master-gnuplot"
  :entry-point "company-master-gnuplot::main")
