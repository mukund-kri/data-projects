# Data projects for introduction to programming

This is the `DATA-MUNGING`  problems that I use in my bootcamps. 


### List of data-sets

1. Company Master
1. IPL
1. UN Population Data

### Data sets in the pipeline

1. Chicago Crime (very large dataset)


### Languages covered matrix

|                             | IPL | Company Master | UN DATA |
|-----------------------------|-----|----------------|---------|
| ***Scripting Languages***   |     |                |         |
| Python                      | X   | X              | X       |
| Ruby                        | X   |                |         |
| ***JavaScript Based***      |     |                |         |
| ES6                         |     | X              |         |
| coffeescript                | X   |                |         |
| es6                         |     | X              |         |
| typescript                  |     | X              |         |
| ***Performance Languages*** |     |                |         |
| golang                      |     | X              | X       |
| rust                        |     |                |         |
| c++                         |     | X              |         |
| ocaml and rescript          |     |                | X       | 
| nim                         |     | X              |         |
| ***JVM***                   |     |                |         |
| scala                       |     | X              |         |
| clojure                     |     | X              |         |
| java                        |     |                |         |
| kotlin                      |     |                |         |
| groovy                      |     |                |         |




