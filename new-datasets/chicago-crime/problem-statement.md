# Chicago Crime data

Note: This is a very large data set.

## Aim
To convert raw open data into charts that tell some kind of story

## Prepairation

This data is sourced from 
https://data.cityofchicago.org/Public-Safety/Crimes-2001-to-present/ijzp-q8t2

## Instructions

1. Download all the data needed. Consult your mentor if you have any problems
accessiong the raw data.
2. Initialize a node type script project. All your code should be in typescrpt


### Part 1 :: CSV -> JSON
**Important** This is where all your logic will be. Code a node program which
will load the raw csv and convert into a format that can be used to create plots
in part 2.

### Part 2 :: Plot with high charts 
High charts is an open source lib to create plots on the browser. Use the 
json generated previously to plot. Use ajax to load the data onto the 
browser.