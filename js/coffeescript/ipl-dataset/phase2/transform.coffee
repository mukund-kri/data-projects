import { createReadStream, writeFile } from 'fs'
import csv from 'csv-parser'


scorecards = {}


createReadStream 'matches.csv'
  .pipe csv()
  .on 'data', (data) ->

    year = data.season
    team1 = data.team1
    team2 = data.team2
    date = data.date
    venue = data.venue

    scorecards[year] = {} unless scorecards[year]?
    scorecards[year][team1] = {} unless scorecards[year][team1]
    scorecards[year][team2] = {} unless scorecards[year][team2]

    scorecards[year][team1][data.id] = {team1, team2, date, venue}
    scorecards[year][team2][data.id] = {team1, team2, date, venue}


  .on 'end', () ->

    writeFile './public/data.json', (JSON.stringify scorecards, null, 2), (err) ->
      console.log err
