var mkMatchTable, mkTeamMenuDiv, mkYearDiv, mkYearDivTitle;

mkMatchTable = function(matches) {
  var match, matchId, table;
  table = $('<table>', {
    'class': 'match-data'
  });
  for (matchId in matches) {
    match = matches[matchId];
    table.append("<tr>\n  <td>" + match.date + "</td>\n  <td>" + match.venue + "</td>\n  <td>" + match.team1 + "</td>\n  <td>" + match.team2 + "</td>  \n</tr>");
  }
  return table;
};

mkYearDivTitle = function(year, div) {
  var ele;
  ele = $("<div>\n  <span class='expander'></span>\n  <span>" + year + "</span>\n</div>");
  ele.click(function() {
    var i, itm, len, ref;
    ref = $('.active');
    for (i = 0, len = ref.length; i < len; i++) {
      itm = ref[i];
      if (!($(itm)).is(div)) {
        ($(itm)).toggleClass("collapsed active");
      }
    }
    return div.toggleClass("collapsed active");
  });
  return ele;
};

mkTeamMenuDiv = function(teamName, matches) {
  var ele;
  ele = $("<div class='menu-team'>\n  <span class='spacer'></span>\n  <span>" + teamName + "</span>\n</div>");
  return ele.click(function() {
    return ($('#content')).html(mkMatchTable(matches));
  });
};

mkYearDiv = function(year, team) {
  var div, k, v;
  div = $('<div>', {
    'class': 'years collapsed'
  });
  div.append(mkYearDivTitle(year, div));
  ((function() {
    var results;
    results = [];
    for (k in team) {
      v = team[k];
      results.push([k, v]);
    }
    return results;
  })()).map(function(arg) {
    var matches, teamName;
    teamName = arg[0], matches = arg[1];
    return mkTeamMenuDiv(teamName, matches);
  }).forEach(function(k) {
    return div.append(k);
  });
  return div;
};

$.getJSON('data.json', function(data) {
  var menu, teams, year;
  menu = $('#menu');
  return ((function() {
    var results;
    results = [];
    for (year in data) {
      teams = data[year];
      results.push([year, teams]);
    }
    return results;
  })()).map(function(arg) {
    var team, year;
    year = arg[0], team = arg[1];
    return mkYearDiv(year, team);
  }).forEach(function($year) {
    return menu.append($year);
  });
});
