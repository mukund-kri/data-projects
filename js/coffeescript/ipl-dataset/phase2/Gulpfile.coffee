gulp = require 'gulp'
coffee = require 'gulp-coffee'
webserver = require 'gulp-webserver'


gulp.task 'watch', () ->
  gulp.watch 'main*.coffee', ['compile']

gulp.task 'compile', () ->
  gulp.src 'main*.coffee'
    .pipe (coffee bare: true)
    .pipe (gulp.dest './public/')

gulp.task 'server', () ->
  gulp.src './public/'
    .pipe webserver
      livereload: true
      directoryListing: true
      open: true
      
gulp.task 'default', ['compile', 'watch']
