$.getJSON 'problem4.json', (data) ->
  plot4 data.categories, data.series
  

plot4 = (categories, series) ->

  Highcharts.chart 'problem4', 
    chart: 
      type: 'bar'
    title: 
      text: 'Top bowlers (by economy) of 2015'

    xAxis: 
      categories: categories
      title: 
        text: null
  
    yAxis: 
      min: 0,
      title: 
        text: 'Runs per Over',
        align: 'high'
    labels: 
      overflow: 'justify'

    tooltip: 
      valueSuffix: ' runs/over'

    plotOptions: 
      bar: 
        dataLabels: 
          enabled: false
      
    credits: 
      enabled: false

    series: [{
      name: 'Runs per over',
      data: series
    }]
