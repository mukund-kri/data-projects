import express from 'express'

import Match from './match-model.coffee'


router = express.Router()

# Add a sample route
router.get '/', (req, res) ->    
  res.render 'index'

router.get '/years', (req, res) ->
  Match.collection.distinct 'season', (err, data) ->
    console.log err, data
    res.json data

router.get '/teams/:year', (req, res) ->
  year = req.params['year']
  Match.collection.distinct 'team1', {season: Number(year)}, (err, data) ->
    res.json data
  
router.get '/schedule/:year/:team', (req, res) ->
  year = req.params['year']
  team = req.params['team']
  
  Match.find {season: 2008, '$or': [{team1: team}, {team2: team}]}, (err, data) ->
    res.json data
  

export default router
