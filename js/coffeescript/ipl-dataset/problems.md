# Problems

## Simple Histograms

### Total runs scored by team

Plot a chart of the total runs scored by each teams over the history of IPL.
Hint: use the total_runs field.


### Total runs by a batsman

Plot the total runs scored by each batsman over the history of IPL.
Note: There are too many batsman to fit into one graph so only plot the top 10
run scorers. 
Hint: use the batsman_runs field.


### Total extras allowed by a bowling team 

Compute and plot the total number of extras allowed by each team over the 
history of IPL

Hint: use the extra_runs field

### Top "Man of the Match" 

Compute an plot how many times each player has been awarded "Man of the Match".

Note: There may be too many values to plot on a single graph, so choose an 
cut off number above which the data is plotted.

## Filter + Histogram 

### Top batsman for Royal Challengers Bangalore

Consider only games played by Royal Challengers Bangalore. Now plot the total
runs scored by every batsman playing for Royal Challengers Bangalore over the
history of IPL.

### Runs scored by Royal Challengers Bangalore by Season 

Note: for this exercise you will have to use both deliveries.csv and matches.csv
data sources.

Compute and plot total number of runs scored by Royal Challengers Bangalore by
season.


## External Source

### Foreign umpire analysis

Obtain a source for country of origin of umpires. 
Plot a chart of number of umpires by in IPL by country. Indian umpires should
be ignored as this would dominate the graph.

### Foreign players who played for Royal challengers Bangalore

Obtain a source for country of origin of players.
Plot a chart of the number of foreign players that played for Royal challengers
Bangalore.

Note: Foreign Players == Players of nationality not Indian.


## Stacked bar chart 

### Stacked chart of matches played by team by season

Plot a stacked bar chart of ...

  * number of games played
  * by team 
  * by season
  

### Stacked chart of matches played by stadium by season

Plot a stacked bar chart of ...

  * number of games played
  * by stadium
  * by season


### Stacked chart of runs scored by RCB against other teams by season

Plot a stacked bar chart of ...

  * runs scored by Royal Challengers Bangalore
  * against every other team 
  * by season



  
