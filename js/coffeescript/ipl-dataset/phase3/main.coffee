mkMatchTable = (teamName, year) ->
  $.getJSON "/schedule/#{year}/#{teamName}", (data) ->
    table = $('<table>', {'class': 'match-data'})
    for match in data
      table.append """
<tr>
  <td>#{match.date}</td>
  <td>#{match.venue}</td>
  <td>#{match.team1}</td>
  <td>#{match.team2}</td>  
</tr>"""
      ($ "#content").html table


mkTeamMenuDiv = (teamName, year) ->
  ele = $ """
<div class='menu-team'>
  <span class='spacer'></span>
  <span>#{teamName}</span>
</div>
"""
  ele.click () ->
    mkMatchTable teamName, year



ajaxPopulateMenu = (parent) ->
  parent.data().filled = true
  year = parent.data('year')

  $.getJSON "/teams/#{year}", (data) ->
    data.map (team) -> mkTeamMenuDiv team, year
    .forEach ($team) -> parent.append $team


mkYearDivTitle = (year, div) ->
  ele = $ """
  <div>
    <span class='expander'></span>
    <span>#{year}</span>
  </div>
  """
  
  ele.click () ->
    # the for loop is to close all other active menu items
    for itm in ($ '.active')
      if  !($ itm).is(div)
        ($ itm).toggleClass "collapsed active"
    if !(div.data 'filled')
      ajaxPopulateMenu div
    div.toggleClass "collapsed active"
  ele


mkYearDiv = (year) ->
  div = $ '<div>', 'class': 'years collapsed'
  div.data filled: false, year: year
  div.append mkYearDivTitle year, div
  div
  

$.getJSON '/years', (data) ->
  menu = ($ '#menu')
  
  data
  .map (year) -> mkYearDiv year
  .forEach ($year) -> menu.append $year
