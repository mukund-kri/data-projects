import * as csv from 'csv-parser';
import { createReadStream, ReadStream, writeFile } from 'fs';
import * as _ from 'lodash';


const increment = (map: Map<string, number>, key: string): void => {
    const value = map.get(key);
    if (value) {
        map.set(key, Number(value) + 1);
    } else {
        map.set(key, 1);
    }
};

const dumpJSON = (name: string, categories: string[], data: number[]): void => {
    writeFile(
        `./public/${name}.json`,
        JSON.stringify({ categories, data }),
        'utf8',
        () => undefined,
    );
};


// Read in a process the input csv file
const stream: ReadStream = createReadStream('./raw_data/mca_maharashtra_21042018.csv');
const csvStream = stream.pipe(csv());


// Problem 1: ------------------------------------------------------------------
const accumulator: Map<string, number> = new Map([
    ['<1L', 0],
    ['Between 1L and 10L', 0],
    ['Between 10L and 1Cr', 0],
    ['Between 1Cr and 10Cr', 0],
    ['Greater 10Cr', 0],
]);

csvStream.on('data', (line: any) => {
    const ac: number = line.AUTHORIZED_CAP;
    if (ac <= 100000) {
        increment(accumulator, '<1L');
    } else if (ac > 100000 && ac <= 1000000) {
        increment(accumulator, 'Between 1L and 10L');
    } else if (ac > 1000000 && ac <= 10000000) {
        increment(accumulator, 'Between 10L and 1Cr');
    } else if (ac > 10000000 && ac <= 100000000) {
        increment(accumulator, 'Between 1Cr and 10Cr');
    } else {
        increment(accumulator, 'Greater 10Cr');
    }
});

csvStream.on('end', () => {
    const categories: string[] = [];
    const data: number[] = [];

    for (const [key, value] of accumulator) {
        categories.push(key);
        data.push(value);
    }

    dumpJSON('problem1', categories, data);
});


// PROBLEM 2 -------------------------------------------------------------------
const registrationDateAccm = new Map<string, number>();

csvStream.on('data', (line: any) => {
    increment(
        registrationDateAccm,
        line.DATE_OF_REGISTRATION.split('-')[2],
    );
});

csvStream.on('end', () => {
    const categories: string[] = [];
    const data: number[] = [];

    for (let i = 2000; i <= 2018; i += 1) {
        const year: string = i.toString();
        categories.push(year);
        data.push(registrationDateAccm.get(year));
    }

    dumpJSON('problem2', categories, data);
});


// PROBLEM 3 -------------------------------------------------------------------
const registration2015: Map<string, number> = new Map();

csvStream.on('data', (line: any) => {
    const year: string = line.DATE_OF_REGISTRATION.split('-')[2];
    const activity: string = line.PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN;

    if (year === '2015') {
        increment(registration2015, activity);
    }
});

csvStream.on('end', () => {
    const categories: string[] = [];
    const data: number[] = [];

    for (const [category, count] of registration2015) {
        categories.push(category);
        data.push(count);
    }
    dumpJSON('problem3', categories, data);
});

// PROBLEM 4 -------------------------------------------------------------------
const activityYearAccum: Map<string, Map<string, number>> = new Map();

csvStream.on('data', (line: any) => {
    const year: string = line.DATE_OF_REGISTRATION.split('-')[2];
    const activity: string = line.PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN;

    if (!activityYearAccum.has(activity)) {
        activityYearAccum.set(activity, new Map());
    }

    increment(
        activityYearAccum.get(activity),
        year,
    );
});

csvStream.on('end', () => {
    const categories: string[] = [...activityYearAccum.keys()];
    const years = _.range(2000, 2018);
    const series: any = {};

    years.forEach((year) => {
        if (!(year in series)) { series[year] = []; }

        categories.forEach((category) => {
            const val = activityYearAccum.get(category).get(year.toString());
            if (val) {
                series[year].push(val);
            } else {
                series[year].push(0);
            }
        });
    });

    writeFile(
        './public/problem4.json',
        JSON.stringify({ series, categories }),
        'utf8',
        () => undefined,
    );

});

