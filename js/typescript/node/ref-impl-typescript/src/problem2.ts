import * as Highcharts from 'highcharts';

export const plot2 = (categories: Array<string>, data: Array<number>): void => {

    Highcharts.chart('plot2', {
        chart: {
            type: 'bar',
        },
        title: {
            text: 'Registered companies (Maharastra) by Year'
        },
        xAxis: {
            categories: categories,
            title: {
                text: null,
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of companies',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor:  '#FFFFFF',
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Registered Companies',
            data: data
        }],
    });
};
