import * as Highcharts from 'highcharts';

export const plot4 = (categories: Array<string>, data: any): void => {
    console.log(data);
    Highcharts.chart('plot4', {
        chart: {
            type: 'bar',
        },
        title: {
            text: 'Registered companies (Maharastra) by Registration Year by Business Activity'
        },
        xAxis: {
            categories: categories,
            title: {
                text: null,
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of companies',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor:  '#FFFFFF',
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: data
    });
};
