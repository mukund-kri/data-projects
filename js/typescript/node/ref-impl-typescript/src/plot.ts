import { plot1 } from './problem1';
import { plot2 } from './problem2';
import { plot3 } from './problem3';
import { plot4 } from './problem4';


const plotAll = () => {

    // First problem
    fetch('problem1.json')
    .then((response) => response.json())
    .then((json) => plot1(json.categories, json.data));

    // Second Problem
    fetch('problem2.json')
    .then((response) => response.json())
    .then((json) => plot2(json.categories, json.data));

    // Third Problem
    fetch('problem3.json')
    .then((response) => response.json())
    .then((json) => plot3(json.categories, json.data));

    // Fourth Problem
    fetch('problem4.json')
    .then((response) => response.json())

    .then((json) => {

        let hSeries = Object.entries(json.series)
            .map(([year, values] : [string, any]) => {
                return {name: year, data: values}
            });
        console.log(hSeries, json.series);
        plot4(json.categories, hSeries);
    });
}

document.addEventListener("DOMContentLoaded", function(event) { 
    plotAll();
});
