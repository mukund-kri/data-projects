const path = require('path');

module.exports = {
    entry: './src/plot.ts',
    module: {
        rules: [
          {
            test: /\.tsx?$/,
            use: 'ts-loader',
            exclude: /node_modules/
          }
        ]
      },
      resolve: {
        extensions: [ '.tsx', '.ts', '.js' ]
      },
      output: {
        filename: 'plot.js',
        path: path.resolve(__dirname, 'public')
      },
      mode: 'development',
      devServer: {
          contentBase: './public'
      },
}