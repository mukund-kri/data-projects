# data.gov.in :: Company Master :: Maharashtra


## Aim
To convert raw open data into charts that tell some kind of story.

## Preparation

#### raw data 

| Name                               | source                                          |
|------------------------------------|-------------------------------------------------|
| Company master data of Maharashtra | https://data.gov.in/catalog/company-master-data |


#### Ancilary data :: Industrial Class

http://mospi.nic.in/classification/national-industrial-classification/alphabetic-index-5digit

**Todo:** upload csv version of this data.


## Instructions

1. Download all the data needed. Consult your mentor if you have any problems
accessiong the raw data.
2. Initialize a node type script project. All your code should be in typescrpt


### Part 1 :: CSV -> JSON
**Important** This is where all your logic will be. Code a node program which
will load the raw csv and convert into a format that can be used to create plots
in part 2.

### Part 2 :: Plot with high charts 
High charts is an open source lib to create plots on the browser. Use the 
json generated previously to plot. Use ajax to load the data onto the 
browser.

## Problems

### 1. Histogram of Authorized Cap

Plot a histogram on AUTHORIZE_CAP with the following intervals
  
  1. < 1L
  2. 1L to 10L
  3. 10L to 1Cr
  4. 10Cr to 100Cr
  5. More than 100Cr
  
Note: You will have to adjust the intervals if you have a un-balanced plot.

### 2. Histogram of company registration by year

From the field DATE_OF_REGISTRATION parse out the registration year. Using
this data, plot a histogram of the number of company registration by year.

### 3. Top registrations by "Principal Business Activity" for the year 2015

In this exercise ... 

  1. only consider registrations for the year 2015
  2. classify and count registrations by PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN
  3. Pick on the top 10 of these values
  4. Plot
  
  
### 4. Top registrations by "Industrial Class"

In this exercise ...
  1. Obtain the industrial class names csv.
  2. count the registrations by INDUSTRIAL_CLASS
  3. Plot top 10 values. The plot should have "Industrial Class" names and not codes
  
### 5. Stacked Bar Chart.

Plot a stacked bar chart by aggregating registrations count over ...
  1. Year of registration
  2. Principal Business Activity
  
  
## Problems :: Alternatives

### 1a. Histogram by Principal Business Activity

Plot a histogram of all the company registrations on the field PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN

### 1b. Histogram by Company Status

Use the field COMPANY_STATUS to compute and plot no. of companies by Company Status

Note: If the plot is un-balanced, ie. one or two values dominate, split the 
plot into two. The first plot with the higher values, and the second with the
lower values.


## Problems :: Advanced

### 1. Table: by email domain.

Most companies registered do not seem to have an online presence. A probable
indication is EMAIL_ADDR is popular email hosting service like gmail, reddif etc.

1. Find the top domains by splitting EMAIL_ADDR and determine which are the 
are "non custom" domains. Produce a table with domain vs. Registration count.
2. Using the list  form 1. list the custom domains. Filter by only those that
have filed financial statements last year (2017). Sort by AUTHORIZED_CAP.

#### Alternatives
3. Of the "no custom" domains, group by "Principal Business Activity" and 
sort by AUTHORIZED_CAP


### 2. Name recognition

Most emails have the name of a person in it. 
1. Search and download a list Indian names (first and last)
2. If possible determine the first name and last name of the person form his/her
email.
3. Make a list of Company :: Contact person

### 3. Scrape company "Industrial Class" name

In the data set, only the INDUSTRIAL_CLASS code is used. To make it readable,
we need the code to name mapping. This is avalible on this page. 

http://mospi.nic.in/classification/national-industrial-classification/alphabetic-index-5digit

Scrape this page and convert into csv.


