/*
 * Transform the input csv into json sutible to plot with highcharts.
 */

const fs = require('fs');
const csv = require('csv-parser');
const _ = require('lodash');

// Problem 1 accumulator. Map of counts by AUTHORIZED_CAP
const authorizeCapAccum = {
  range1: 0,
  range2: 0,
  range3: 0,
  range4: 0,
  range5: 0,
};


const csvStream = fs.createReadStream('./raw_data/mca_maharashtra_21042018.csv')
  .pipe(csv());


// PROBLEM 1 -----------------------------------------------------------------
csvStream.on('data', (line) => {
  // On each line accumulate to the right bucket
  if (line.AUTHORIZED_CAP <= 100000) {
    authorizeCapAccum.range1 += 1;
  } else if (line.AUTHORIZED_CAP > 100000 && line.AUTHORIZED_CAP <= 1000000) {
    authorizeCapAccum.range2 += 1;
  } else if (line.AUTHORIZED_CAP > 1000000 && line.AUTHORIZED_CAP <= 10000000) {
    authorizeCapAccum.range3 += 1;
  } else if (line.AUTHORIZED_CAP > 10000000 && line.AUTHORIZED_CAP <= 100000000) {
    authorizeCapAccum.range4 += 1;
  } else {
    authorizeCapAccum.range5 += 1;
  }
});

csvStream.on('end', () => {
  const categories = ['< 1L', '1L to 10L', '10L to 1Cr', '1Cr to 10Cr', 'More than 10Cr'];
  const data = [
    authorizeCapAccum.range1,
    authorizeCapAccum.range2,
    authorizeCapAccum.range3,
    authorizeCapAccum.range4,
    authorizeCapAccum.range5,
  ];

  fs.writeFile(
    './public/problem1.json',
    JSON.stringify({ categories, data }),
    'utf8',
    () => {},
  );
});


// PROBLEM 2 -----------------------------------------------------------------
const registrationDateAccum = {};

csvStream.on('data', (line) => {
  const year = line.DATE_OF_REGISTRATION.split('-')[2];
  if (!(year in registrationDateAccum)) registrationDateAccum[year] = 0;
  registrationDateAccum[year] += 1;
});

csvStream.on('end', () => {
  const categories = [];
  const data = [];
  for (let i = 2000; i <= 2018; i += 1) {
    const year = i.toString();
    categories.push(year);
    data.push(registrationDateAccum[year]);
  }

  fs.writeFile(
    './public/problem2.json',
    JSON.stringify({ data, categories }),
    'utf8',
    () => {},
  );
});

// PROBLEM 3 -----------------------------------------------------------------
const problem3 = {};

csvStream.on('data', (line) => {
  const year = line.DATE_OF_REGISTRATION.split('-')[2];
  // console.log(year);
  if (year === '2015') {
    if (!(line.PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN in problem3)) {
      problem3[line.PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN] = 0;
    }
    problem3[line.PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN] += 1;
  }
});

csvStream.on('end', () => {
  const categories = [];
  const data = [];

  const entries = Object.entries(problem3);
  entries.forEach(([key, value]) => {
    categories.push(key);
    data.push(value);
  });

  fs.writeFile(
    './public/problem3.json',
    JSON.stringify({ data, categories }),
    'utf8',
    () => {},
  );
});


// PROBLEM 4 -----------------------------------------------------------------
const problem4 = {};

csvStream.on('data', (line) => {
  const year = line.DATE_OF_REGISTRATION.split('-')[2];
  const activity = line.PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN;

  if (!(activity in problem4)) problem4[activity] = {};
  if (!(year in problem4[activity])) problem4[activity][year] = 0;

  problem4[activity][year] += 1;
});

csvStream.on('end', () => {
  const categories = Object.keys(problem4);
  const years = _.range(2000, 2018);

  const series = {};

  years.forEach((year) => {
    if (!(year in series)) series[year] = [];
    categories.forEach((category) => {
      const val = problem4[category][year];
      if (val) series[year].push(val);
      else series[year].push(0);
    });
  });

  fs.writeFile(
    './public/problem4.json',
    JSON.stringify({ series, categories }),
    'utf8',
    () => {},
  );
});
