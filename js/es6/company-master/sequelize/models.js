const { Sequelize, DataTypes } = require('sequelize');

// initialize connection to postgres
const sequelize = new Sequelize('postgres://testuser:1234@localhost:5432/company_master')


const Company = sequelize.define('mh_company', {
  cin: DataTypes.TEXT,
  registration_year: DataTypes.TEXT,
  auth_cap: DataTypes.BIGINT,
  pba: DataTypes.TEXT
});


module.exports = {
  sequelize,
  Company
}
