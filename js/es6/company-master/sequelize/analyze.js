const fs = require('fs');

const { Op, fn, col } = require('sequelize');

const { sequelize, Company } = require('./models.js');



async function problem2() {
  const companies = await Company.findAll({
    attributes: [
      'registration_year',
      [fn('count', col('id')), 'count']
    ], 
    where: {
      registration_year: {
	[Op.between]: ['2000', '2018']
      }
    },
    group: 'registration_year',
  });

  const ans = companies
    .map(_ => _.dataValues)
    .reduce((accumulator, currentValue) => {
      accumulator[currentValue.registration_year] = currentValue.count;
      return accumulator;
    }, {});
  
  fs.writeFileSync(
    'problem2.json',
    JSON.stringify(ans),
  );
}


async function problem3() {
  const companies = await Company.findAll({
    attributes:[
      'pba',
      [fn('count', col('id')), 'count']
    ],
    where: {
     registration_year: '2015'
    },
    group: 'pba',
  });

  const ans = companies
    .map(_ => _.dataValues)
    .reduce((accumulator, currentValue) => {
      accumulator[currentValue.pba] = Number(currentValue.count);
      return accumulator;
    }, {});
  
  fs.writeFileSync(
    'problem3.json',
    JSON.stringify(ans),
  );
}


async function problem4() {
  const companies = await Company.findAll({
    attributes: [
      'pba',
      'registration_year',
      [fn('count', col('id')), 'count']
    ],
    group: ['pba', 'registration_year'],
  });

  const ans = companies
	.map(_ => _.dataValues)
	.reduce((accumulator, currentValue) => {
	  accumulator[currentValue.pba] = (accumulator[currentValue.pba] || {});
	  accumulator[currentValue.pba][currentValue.registration_year] = Number(currentValue.count);
	  return accumulator;
	}, {});

  fs.writeFileSync(
    'problem4.json',
    JSON.stringify(ans),
  );

}


problem4();
