const fs = require('fs');

const csv = require('csv-parser');

const { sequelize, Company } = require('./models.js');


// Delete the old table and create a new one
async function createTable() {
  await sequelize.sync({force: true})
}
createTable();

// read in the CSV and prep the data for insertion to DB
const csvStream = fs.createReadStream('./data/mca.csv')
      .pipe(csv());

let companies = [];

csvStream.on('data', (line) => {

  let year = line.DATE_OF_REGISTRATION.split('-')[2];
  let authCap = Number(line.AUTHORIZED_CAP);

  companies.push({
    cin: line.CORPORATE_IDENTIFICATION_NUMBER,
    registration_year: year,
    auth_cap: authCap,
    pba: line.PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN,
  });
});


// At the end of file read insert data into DB
csvStream.on('end', () => {
  bulkLoad(companies);
});


async function bulkLoad(rows) {
  await Company.bulkCreate(rows); 
}

    
