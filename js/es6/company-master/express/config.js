module.export = {
  HOST: 'localhost',
  USER: 'testuser',
  PASSWORD: '1234',
  DB: 'company_master',
  dialect: 'postgres',

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};
