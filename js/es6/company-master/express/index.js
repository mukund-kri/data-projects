import 'dotenv/config';
import express from 'express';

const app = express();



app.get('/', (req, res) => {
  return res.send('Company Master APP');
});


app.get('/registrationByYear/:year', (req, res) => {
  
  res.json({ message: `hi there ${req.params.year}` });
});

app.listen(3000, () => console.log(`Starting server on port 3000`));
