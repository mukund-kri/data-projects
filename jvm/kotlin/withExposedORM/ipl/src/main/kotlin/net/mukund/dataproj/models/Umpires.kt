package net.mukund.dataproj.models

import org.jetbrains.exposed.dao.id.IntIdTable


object Umpires : IntIdTable() {
    val name = varchar("name", 256)
    val country = varchar("country", 4)
}