package net.mukund.dataproj.models

import org.jetbrains.exposed.dao.id.IntIdTable

object Matches : IntIdTable() {
    val winner = varchar("winner", 512)
    val season = varchar("season", 12)
}