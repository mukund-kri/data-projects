package net.mukund.dataproj

import net.mukund.dataproj.loading.Loader
import net.mukund.dataproj.plots.Gnuplot
import net.mukund.dataproj.plots.Highcharts
import org.jetbrains.exposed.sql.Database


fun main(args: Array<String>) {

    // Initialize connection to database
    Database.connect(
        url = "jdbc:postgresql://127.0.0.1:5432/ipl?reWriteBatchedInserts=true",
        user = "devuser",
        password = "devpass"
    )

    if (args.isEmpty())
        println("Argument not given")
    else
        when (args[0]) {
            // Load data from csv to db
            "load" -> Loader.load()
            "gnuplot" -> Gnuplot(2000, 2010, 2010).allPlots()
            "highcharts" -> Highcharts(2000, 2010, 2010, "../plots/ipl")
                .allPlots()
            else -> println("\nInvalid Argument")
        }
}
