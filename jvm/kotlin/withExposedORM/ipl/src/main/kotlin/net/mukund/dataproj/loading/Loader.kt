package net.mukund.dataproj.loading

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import net.mukund.dataproj.models.Deliveries
import net.mukund.dataproj.models.Matches
import net.mukund.dataproj.models.Umpires
import org.jetbrains.exposed.sql.batchInsert
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File

object Loader {


    private const val DATA_DIR = "/home/mukund/workarea/teaching/data-projects/raw_data/ipl"

    /**
     * Load all the data for this analysis. Including ...
     *
     * 1. The population data from un-population.csv
     * 2. The country groupings for asean and saarc
     */
    fun load() {

        // loadDeliveryData()
        // loadMatchData()
        loadUmpireData()
    }


    // Load delivery data into database
    private fun loadDeliveryData() {

        val csvFileName = "$DATA_DIR/corrected_deliveries.csv"
        // Read in csv
        val rows = csvReader()
            .readAllWithHeader(File(csvFileName))

        transaction {
            Deliveries.batchInsert(rows) {
                this[Deliveries.batter] = it["batter"]!!
                this[Deliveries.battingTeam] = it["batting_team"]!!
                this[Deliveries.batsmanRuns] = it["batsman_runs"]?.toInt() ?: 0
                this[Deliveries.totalRuns] = it["total_runs"]?.toInt() ?: 0
            }
        }
    }


    // Load match data into database
    private  fun loadMatchData() {
        val csvFileName = "$DATA_DIR/corrected_matches.csv"
        val matches = csvReader()
            .readAllWithHeader(File(csvFileName))

        transaction {
            Matches.batchInsert(matches) {
                this[Matches.season] = it["season"]!!
                this[Matches.winner] = it["winner"]!!
            }
        }
    }

    // Load umpire data into database
    private  fun loadUmpireData() {
        val csvFileName = "$DATA_DIR/ipl-umpire.csv"
        val matches = csvReader()
            .readAllWithHeader(File(csvFileName))

        transaction {
            Umpires.batchInsert(matches) {
                this[Umpires.name] = it["name"]!!
                this[Umpires.country] = it["country"]!!
            }
        }
    }
}