package net.mukund.dataproj.models

import kotlinx.serialization.Serializable


@Serializable
class Problem1(val teams: List<String>, val runs: List<Long>)

@Serializable
class Problem2(val batsman: List<String>, val runs: List<Long>)

@Serializable
class  Problem3(val countries: List<String>, val count: List<Long>)

@Serializable
class Series(val name: String, val data: List<Int>)

@Serializable
class Problem4(val teams: List<String>, val series: List<Series>)