package net.mukund.dataproj.plots

import net.mukund.dataproj.analysis.Analysis
import java.io.File

class Gnuplot(private val startYear: Int, private val endYear: Int, private val selectedYear: Int) {

    private val gnuplotsBasePath = "../plots/ipl/gnuplot"
    private val analysis = Analysis(startYear, endYear, selectedYear)


    private fun plot1() {
        val plotData = analysis.problem1()
        File("$gnuplotsBasePath/problem1.dat").printWriter().use { out ->
            plotData.forEach { out.println("\"${it.first}\"\t${it.second    }") }
        }
    }

    // Plot 2:
    private fun plot2() {
        val plotData = analysis.problem2()
        File("$gnuplotsBasePath/problem2.dat").printWriter().use { out ->
            plotData.forEach { out.println("\"${it.first}\"\t${it.second}") }
        }
    }

    // Plot 3:
    private fun plot3() {
        val plotData = analysis.problem3()
        File("$gnuplotsBasePath/problem3.dat").printWriter().use { out ->
            plotData.forEach { out.println("\"${it.first}\"\t${it.second}") }
        }
    }

    // Plot 4:
    private fun plot4() {
        val (data, teams, seasons) = analysis.problem4()
        File("$gnuplotsBasePath/problem4.dat").printWriter().use { out ->

            // Write the team names as header
            out.println(teams.joinToString(separator = "\t") { "\"$it\"" })
            seasons.forEach { season ->
                out.print("\"${season}\"")
                teams.forEach { team -> out.print("\t${data[team]?.get(season) ?: 0}") }
                out.println()
            }
        }
    }

    /**
     * Analyze and write out gnuplot data (.dat) files for all 4 problems of the UN population data set.
     */
    fun allPlots() {
        println("Generating all plots")
        plot1()
        plot2()
        plot3()
        plot4()
    }
}