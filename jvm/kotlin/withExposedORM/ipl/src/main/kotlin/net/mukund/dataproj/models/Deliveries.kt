package net.mukund.dataproj.models

import org.jetbrains.exposed.dao.id.IntIdTable

object Deliveries : IntIdTable("deliveries") {
    val battingTeam = varchar("batting_team", 512)
    val totalRuns = integer("total_runs")
    val batter = varchar("batter", 512)
    val batsmanRuns = integer("batsman_runs")
}