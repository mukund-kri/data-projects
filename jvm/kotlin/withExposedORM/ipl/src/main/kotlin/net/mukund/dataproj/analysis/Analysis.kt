package net.mukund.dataproj.analysis

import net.mukund.dataproj.models.Deliveries
import net.mukund.dataproj.models.Matches
import net.mukund.dataproj.models.Umpires
import org.jetbrains.exposed.sql.SortOrder
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.SqlExpressionBuilder.neq
import org.jetbrains.exposed.sql.count
import org.jetbrains.exposed.sql.transactions.transaction


typealias TeamSeasonRuns = Map<String, Map<String, Int>>

class Analysis(private val startYear: Int, private val endYear: Int, private val selectedYear: Int) {

    /**
     * Problem 1: Total runs scored by team in all of IPL
     *
     * @return returns a List of team name to runs scored pairs
     */
    fun problem1(): List<Pair<String, Long>> {
        val runCounts = Deliveries.totalRuns.count()
        return transaction {
            Deliveries
                .select(Deliveries.battingTeam, runCounts)
                .groupBy(Deliveries.battingTeam)
                .map { it[Deliveries.battingTeam] to it[runCounts] }
        }
    }

    /**
     * Problem 2: Top batsman for RCB
     *
     * @returns A List of top batsmen and the total runs they have scored.
     */
    fun problem2(numResults: Int = 10): List<Pair<String, Long>> {
        val batsmenTotals = Deliveries.batsmanRuns.count()
        return transaction {
            Deliveries
                .select(Deliveries.batter, batsmenTotals)
                .where(Deliveries.battingTeam eq  "Royal Challengers Bangalore")
                .groupBy(Deliveries.batter)
                .orderBy(batsmenTotals to SortOrder.DESC)
                .limit(numResults)
                .map { it[Deliveries.batter] to it[batsmenTotals] }
        }

    }

    /**
     * Problem 3: Number of foreign umpires by country of origin
     *
     * @returns A List of umpires to the country of origin
     */
    fun problem3(): List<Pair<String, Long>> {
        val countryCount = Umpires.country.count()
        return transaction {
            Umpires
                .select(Umpires.country, countryCount)
                .where(Umpires.country neq "IND")
                .groupBy(Umpires.country)
                .map { it[Umpires.country] to it[countryCount] }
        }

    }

    /**
     * Problem 4: Matches won by ipl team by season
     *
     * @return map of map, for team by season
     */
    fun problem4(): Triple<TeamSeasonRuns, Set<String>, Set<String>> {
        val matchesCount = Matches.winner.count()
        val counts = mutableMapOf<String, MutableMap<String, Int>>()
        val teams = mutableSetOf<String>()
        val seasons = mutableSetOf<String>()

        transaction {
            val result = Matches
                .select(Matches.winner, Matches.season, matchesCount)
                .groupBy(Matches.winner, Matches.season)

            result.forEach {
                val team = it[Matches.winner]
                val season = it[Matches.season]
                val runs = it[matchesCount]

                if (counts[team] == null) counts[team] = mutableMapOf<String, Int>()
                counts[team]?.set(season, runs.toInt())

                teams.add(team)
                seasons.add(season)
            }
        }
        return Triple(counts, teams, seasons)
    }
}