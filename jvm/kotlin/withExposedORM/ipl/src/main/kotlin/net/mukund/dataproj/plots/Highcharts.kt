package net.mukund.dataproj.plots

import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

import net.mukund.dataproj.analysis.Analysis
import net.mukund.dataproj.models.*
import java.io.File

class Highcharts(
    private val startYear: Int,
    private val endYear: Int,
    private val selectedYear: Int,
    private val plotBasePath: String) {

    private val analysis = Analysis(startYear, endYear, selectedYear)
    private val highchartsPlotPath = "$plotBasePath/highcharts"

    private fun plot1() {
        val (teams, runs) = analysis.problem1().unzip()

        val problem1Ans = Problem1(
            teams,
            runs,
        )

        // Dump out json to file
        File("$highchartsPlotPath/problem1.json").printWriter().use { out ->
            out.println(Json.encodeToString(problem1Ans))
        }
    }

    // Plot problem 2. Top batsmen for Royal challengers bangalore
    private fun plot2() {
        // Do the analysis on the raw data
        val (batsmen, runs) = analysis.problem2().unzip()
        // Construct the outgoing json data
        val p2 = Problem2(
            batsmen,
            runs
        )
        // Dump out json to file
        File("$highchartsPlotPath/problem2.json").printWriter().use { out ->
            out.println(Json.encodeToString(p2))
        }
    }

    // Plot problem 3. Count of Non-Indian umpires by country
    private fun plot3() {
        // Do the analysis on raw data
        val (country, count) = analysis.problem3().unzip()
        // Construct the outgoing json data
        val p3 = Problem3 (
            country,
            count
        )
        // Dump out json to file
        File("$highchartsPlotPath/problem3.json").printWriter().use { out ->
            out.println(Json.encodeToString(p3))
        }
    }

    // Plot problem 4. Matches won by team by season
    private fun plot4() {
        // Do the analysis on the raw data
        val (data, teamsSet, seriesSet) = analysis.problem4()
        val series = seriesSet.toList()
        val teams = teamsSet.toList()

        // Construct the outgoing json
        val seriesOut = mutableListOf<Series>()
        series.forEach { s ->
            val runsList = mutableListOf<Int>()
            teams.forEach { team ->
                runsList.add(data[team]?.get(s) ?: 0)
            }
            seriesOut.add(Series(s, runsList))
        }
        val p4 = Problem4 (teams, seriesOut)
        // Dump out json to file
        File("$highchartsPlotPath/problem4.json").printWriter().use { out ->
            out.println(Json.encodeToString(p4))
        }
    }

    // Plot problem 4. Matches won by team by year
    fun allPlots() {
        plot1()
        plot2()
        plot3()
        plot4()
    }
}