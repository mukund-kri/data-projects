CREATE TABLE matches (
    id SERIAL PRIMARY KEY,
    season VARCHAR(12),
    winner VARCHAR(512)
)