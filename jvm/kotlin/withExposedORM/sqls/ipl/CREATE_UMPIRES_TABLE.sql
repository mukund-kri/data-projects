CREATE TABLE umpires (
    id SERIAL PRIMARY KEY,
    name VARCHAR(256),
    country VARCHAR(4)
)