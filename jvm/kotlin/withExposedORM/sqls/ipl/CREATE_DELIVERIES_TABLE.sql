CREATE TABLE deliveries (
    id SERIAL PRIMARY KEY,
    batting_team VARCHAR(512),
    total_runs INT,
    batter VARCHAR(512),
    batsman_runs INT
)