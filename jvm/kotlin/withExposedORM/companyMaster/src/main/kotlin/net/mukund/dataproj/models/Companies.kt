package net.mukund.dataproj.models

import org.jetbrains.exposed.dao.id.IntIdTable

object Companies : IntIdTable("companies") {
    val authorizedCapital = long("authorized_capital")
    val registrationYear = integer("registration_year")
    val pba = text("pba")
}