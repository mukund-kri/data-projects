package net.mukund.dataproj.plots

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import net.mukund.dataproj.analysis.Analysis
import java.io.File

// Class for serialization to JSON
@Serializable
class Problem1(
    val categories: List<String>,
    val data: List<Int>,
)

@Serializable
class Problem2(
    val categories: List<Int>,
    val data: List<Long>,
    @SerialName("start_year") val startYear: Int,
    @SerialName("end_year") val endYear: Int
)

@Serializable
class Problem3 (
    val categories: List<String>,
    val data: List<Int>,
    @SerialName("selected_year") val selectedYear: Int,
)
@Serializable
class Series(val name: String, val data: List<Int>)

@Serializable
class Problem4(val categories: List<Int>, val series: List<Series>)


class Highcharts(private val startYear: Int, private val endYear: Int, private val selectedYear: Int) {

    private val plotsBasePath = "../plots/company-master/highcharts"

    // Plot 1:
    private fun plot1() {

        val plotData = Analysis.problem1().toMap()
        val categories = listOf("< 1L", "1L - 10L", "10L - 1Cr", "1Cr - 10Cr", "> 10Cr")
        val data = categories.map { plotData[it] ?: 0 }
        val p1 = Problem1(
            categories,
            data
        )
        File("$plotsBasePath/problem1.json").printWriter().use { out ->
            out.println(Json.encodeToString(p1))
        }
    }

    // Plot 2:
    private fun plot2() {
        val plotData = Analysis.problem2(startYear, endYear)
        val (years, counts) = plotData.unzip()
        val p2 = Problem2(years, counts, startYear, endYear)
        File("$plotsBasePath/problem2.json").printWriter().use { out ->
            out.println(Json.encodeToString(p2))
        }
    }

    // Plot 3:
    private fun plot3() {
        val plotData = Analysis.problem3(selectedYear)
        val (pbas, counts) = plotData.unzip()
        val p3 = Problem3 (
            pbas,
            counts,
            selectedYear,
        )
        File("$plotsBasePath/problem3.json").printWriter().use { out ->
            out.println(Json.encodeToString(p3))
        }
    }

    // Plot 4:
    private fun plot4() {

        val pbas = listOf("Trading", "Business Services", "Real Estate and Renting", "Construction",)
        val plotData = Analysis.problem4(startYear, endYear)

        val years = (startYear..endYear).toList()
        val series = mutableListOf<Series>()

        pbas.forEach { pba ->
            val pbaData = mutableListOf<Int>()
            years.forEach { year ->
                pbaData.add(plotData[year]?.get(pba) ?: 0)
            }

            series.add(Series(pba, pbaData))
        }

        val p4 = Problem4(years, series)
        File("$plotsBasePath/problem4.json").printWriter().use { out ->
            out.println(Json.encodeToString(p4))
        }

    }

    /**
     * Analyze and write out gnuplot data (.dat) files for all 4 problems of the UN population data set.
     */
    fun allPlots() {
        plot1()
        plot2()
        plot3()
        plot4()
    }

}