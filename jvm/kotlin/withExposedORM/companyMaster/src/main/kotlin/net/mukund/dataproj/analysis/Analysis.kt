package net.mukund.dataproj.analysis

import net.mukund.dataproj.models.Companies
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.lessEq
import org.jetbrains.exposed.sql.transactions.transaction


object Analysis {

    fun problem1(): List<Pair<String, Int>> {

        println("Problem 1")
        // Alias the case statement
        val caseStatement = Case()
            .When(Op.build { Companies.authorizedCapital lessEq 1_00_000 }, stringLiteral("< 1L"))
            .When(Op.build { Companies.authorizedCapital lessEq 10_00_000 }, stringLiteral("1L - 10L"))
            .When(Op.build { Companies.authorizedCapital lessEq 1_00_00_000 }, stringLiteral("10L - 1Cr"))
            .When(Op.build { Companies.authorizedCapital lessEq 10_00_00_000 }, stringLiteral("1Cr - 10Cr"))
            .Else(stringLiteral("> 10Cr"))

        // val counts = caseStatement.count()
        val companies = transaction {
            Companies
                .select(caseStatement)
                .map { it[caseStatement] }
        }

        // Doing it in kotlin as groupBy does not seem to work with case statement in jdbc
        return  companies
            .groupBy { it }
            .map { it.key to it.value.count() }
    }

    fun problem2(startYear: Int, endYear: Int): List<Pair<Int, Long>> {
        // Alias the count
        val yearCounts = Companies.registrationYear.count()

        return transaction {
            Companies
                .select(Companies.registrationYear, yearCounts)
                .where { Companies.registrationYear.between(startYear, endYear) }
                .groupBy(Companies.registrationYear)
                .map { it[Companies.registrationYear] to it[yearCounts] }
        }
    }

    fun problem3(selectedYear: Int, top: Int = 5): List<Pair<String, Int>> {
        val pbaCounts = Companies.pba.count()
        return transaction {
            Companies
                .select(Companies.pba, pbaCounts)
                .where { Companies.registrationYear eq selectedYear }
                .groupBy(Companies.pba)
                .orderBy(pbaCounts to SortOrder.DESC)
                .limit(top)
                .map { it[Companies.pba] to it[pbaCounts].toInt() }
        }
    }

    fun problem4(startYear: Int, endYear: Int): Map<Int, Map<String, Int>> {
        val counts = mutableMapOf<Int, MutableMap<String, Int>>()

        val countsExpr = Companies.pba.count()
        transaction {
            Companies
                .select(Companies.pba, Companies.registrationYear, countsExpr)
                .where { (Companies.registrationYear greaterEq startYear) and (Companies.registrationYear lessEq endYear) }
                .groupBy(Companies.pba, Companies.registrationYear)
                .forEach {
                    val pba = it[Companies.pba]
                    val year = it[Companies.registrationYear]
                    val count = it[countsExpr]
                    if (counts[year] == null) {
                        counts[year] = mutableMapOf<String, Int>()
                    }
                    counts[year]?.put(pba ?: "", count.toInt())
                }
        }
        return counts
    }
}