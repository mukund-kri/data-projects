package net.mukund.dataproj.loading

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import net.mukund.dataproj.models.Companies
import org.jetbrains.exposed.sql.batchInsert
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File
import kotlin.concurrent.thread

object Loader {

    private const val DATA_DIR = "/home/mukund/workarea/teaching/data-projects/raw_data/company-master"

    /**
     * Load all the data for this analysis. Including ...
     *
     * Company data from CSV
     */
    fun load() {
        loadCompanyData()
    }

    private fun loadCompanyData() {
        val csvFileName = "$DATA_DIR/maharashtra_company_master.csv"
        // Read in csv
        val rows = csvReader()
            .readAllWithHeader(File(csvFileName))
            .map { Triple(it["AUTHORIZED_CAP"], it["DATE_OF_REGISTRATION"], it["PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN"]) }
            .mapNotNull {
                val (authCap, dateOfRegistration, pba) = it
                val authCapInt = authCap?.toFloat()?.toLong()
                if (authCapInt != null)
                    Triple(authCapInt, dateOfRegistration, pba)
                else
                    null
            }
            .mapNotNull {
                val (authCap, dateOfRegistration, pba) = it
                when (val registrationYear = dateOfRegistration?.takeLast(4)) {
                    "NA" -> null
                    null -> null
                    else -> Triple(authCap, registrationYear.toInt(), pba)
                }
            }

        transaction {
            Companies.batchInsert(rows) {
                this[Companies.authorizedCapital] = it.first
                this[Companies.registrationYear] = it.second
                this[Companies.pba] = it.third!!
            }
        }
    }
}