package net.mukund.dataproj.plots

import net.mukund.dataproj.analysis.Analysis
import java.io.File

class Gnuplot(private val startYear: Int, private val endYear: Int, private val selectedYear: Int) {

    private val plotsBasePath = "../plots/company-master/gnuplot"

    // Plot 1:
    private fun plot1() {

        val plotData = Analysis.problem1().toMap()
        val categories = listOf("< 1L", "1L - 10L", "10L - 1Cr", "1Cr - 10Cr", "> 10Cr")

        File("${plotsBasePath}/problem1.dat").printWriter().use { out ->
            categories.forEach { out.println("\"${it}\" ${plotData[it]}") }
        }
    }

    // Plot 2:
    private fun plot2() {
        val plotData = Analysis.problem2(startYear, endYear)
        File("$plotsBasePath/problem2.dat").printWriter().use { out ->
            plotData.forEach { out.println("${it.first}\t${it.second}") }
        }
    }

    // Plot 3:
    private fun plot3() {
        val plotData = Analysis.problem3(selectedYear)
        File("$plotsBasePath/problem3.dat").printWriter().use { out ->
            plotData
                .forEach { out.println("\"${it.first.take(20)}\" ${it.second}") }
        }
    }

    // Plot 4:
    private fun plot4() {

        val pbas = listOf("Trading", "Business Services", "Real Estate and Renting", "Construction",)
        val plotData = Analysis.problem4(startYear, endYear)

        File("$plotsBasePath/problem4.dat").printWriter().use { out ->
            out.println(pbas.joinToString(separator = "\"\t\"", prefix = "\"", postfix = "\"" ))

            (startYear .. endYear).forEach {
                out.print("\"$it\"")
                val yearData = plotData[it]
                pbas.forEach { pba ->
                    val pbaCount = yearData?.get(pba)
                    out.print("\t$pbaCount")
                }
                out.println()
            }
        }
    }

    /**
     * Analyze and write out gnuplot data (.dat) files for all 4 problems of the UN population data set.
     */
    fun allPlots() {
        plot1()
        plot2()
        plot3()
        plot4()
    }
}