package net.mukund.dataproj

import net.mukund.dataproj.loading.Loader
import net.mukund.dataproj.plots.Gnuplot
import net.mukund.dataproj.plots.HighCharts
import org.jetbrains.exposed.sql.Database


fun main(args: Array<String>) {

    // Initialize connection to database
    Database.connect(
        url = "jdbc:postgresql://127.0.0.1:5432/un_population?reWriteBatchedInserts=true",
        user = "devuser",
        password = "devpass"
    )

    // Do task according to the argument
    if (args.isEmpty())
        println("Argument not given")
    else
        when (args[0]) {
            // Load data from csv to db
            "load" -> Loader.load()
            "gnuplot" -> Gnuplot(2000, 2010, 2010).allPlots()
            "highcharts" -> HighCharts.allPlots(2000, 2010, 2010)
            else -> println("\nInvalid Argument")
        }
}
