package net.mukund.dataproj.plots

import net.mukund.dataproj.analysis.Analysis
import java.io.File

private const val PLOTS_BASE_PATH = "../plots/unpopulation/gnuplot"


class Gnuplot(val startYear: Int, val endYear: Int, val selectedYear: Int) {

    // Plot 1:  India population over a selected range of years.
    private fun plot1() {

        val plotData = Analysis.problem1(startYear, endYear)
        File("$PLOTS_BASE_PATH/problem1.dat").printWriter().use { out ->
            plotData.forEach { (year, population) ->
                out.println("$year $population")
            }
        }
    }

    // Plot 2: For a given year; the population of the ASEAN block countries
    private fun plot2() {

        val plotData = Analysis.problem2(selectedYear)
        File("$PLOTS_BASE_PATH/problem2.dat").printWriter().use { out ->
            plotData.forEach { (country, population) ->
                out.println("\"$country\"\t $population")
            }
        }
    }

    // Plot 3: The growth of the "total population" of SAARC countries over a range of years.
    private fun plot3() {
        val plotData = Analysis.problem3(startYear, endYear)
        File("$PLOTS_BASE_PATH/problem3.dat").printWriter().use { out ->
            plotData.forEach { (year, population) ->
                out.println("\"$year\"\t $population")
            }
        }
    }

    // Plot 4:Data for grouped Bar Chart of population of ASEAN block countries over a range
    // of years.
    private fun plot4() {
        val plotData = Analysis.problem4(startYear, endYear)

        val years = (startYear..endYear).toList()
        File("$PLOTS_BASE_PATH/problem4.dat").printWriter().use { out ->
            out.println(years.joinToString(separator = "\t"))

            Analysis.aseanCountries().forEach { country ->
                out.print("\"$country\"")
                years.forEach { year ->
                    out.print("\t${plotData[year]?.get(country)}")
                }
                out.println()
            }

        }
    }

    /**
     * Analyze and write out gnuplot data (.dat) files for all 4 problems of the UN population data set.
     */
    fun allPlots() {
        plot1()
        plot2()
        plot3()
        plot4()
    }
}