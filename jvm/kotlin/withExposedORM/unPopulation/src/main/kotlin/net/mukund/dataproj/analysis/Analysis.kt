package net.mukund.dataproj.analysis

import net.mukund.dataproj.models.Grouping
import net.mukund.dataproj.models.PopulationData
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.between
import org.jetbrains.exposed.sql.SqlExpressionBuilder.inSubQuery

import org.jetbrains.exposed.sql.transactions.transaction

object Analysis {

    /**
     * Problem 1 analysis: India population over a selected range of years.
     *
     * @param startYear The start of the range of years
     * @param endYear   The end of the range of years
     *
     * @return list of pairs of year vs. population of that year
     */
    fun problem1(startYear: Int, endYear: Int): List<Pair<Int, Double>> {
        return transaction {
            PopulationData
                .selectAll()
                .where { (PopulationData.country eq "India") and (PopulationData.year.between(startYear, endYear)) }
                .map { it[PopulationData.year]  to it[PopulationData.population] }
        }
    }

    /**
     * Problem 2 Analysis: For a given year; the population of the ASEAN block countries
     *
     * @param selectedYear The year for which the population is to be reported.
     *
     * @return List of pairs of country vs. population
     */
    fun problem2(selectedYear: Int): List<Pair<String, Double>> {
       val aseanCountries = Grouping.select(Grouping.country).where { Grouping.grouping eq "asean"}
       return transaction {
            PopulationData
                .select(PopulationData.country, PopulationData.population)
                .where {
                    (PopulationData.year eq selectedYear) and
                            (PopulationData.country inSubQuery aseanCountries)
                }
                .map { it[PopulationData.country] to it[PopulationData.population] }
        }
    }

    /**
     * Problem 3 analysis: The growth of the "total population" of SAARC countries over a range of years.
     *
     * @param startYear The start of the range of years
     * @param endYear   The end of the range of years
     *
     * @return list of pairs of years vs total population
     */
    fun problem3(startYear: Int, endYear: Int): List<Pair<Int, Double>> {
        val saarcCountries = Grouping.select(Grouping.country).where { Grouping.grouping eq "saarc"}
        val popSum = PopulationData.population.sum()

        return transaction {
            PopulationData
                .select(PopulationData.year, popSum)
                .where {
                    (PopulationData.year.between(startYear, endYear)) and
                            (PopulationData.country inSubQuery saarcCountries)
                }
                .groupBy(PopulationData.year)
                .map { it[PopulationData.year] to (it[popSum] ?: 0.0) }
        }

    }

    /**
     * Problem 4 analysis: Data for grouped Bar Chart of population of ASEAN block countries over a range
     * of years.
     *
     * @param startYear The start of the range of years
     * @param endYear   The end of the range of years
     */
    fun problem4(startYear: Int, endYear: Int): Map<Int, Map<String, Double>>  {
        val counts = mutableMapOf<Int, MutableMap<String, Double>>()

        val popSum = PopulationData.population.sum()
        val aseanCountries = Grouping.select(Grouping.country).where { Grouping.grouping eq "asean"}

        transaction {
            PopulationData
                .select(PopulationData.year, PopulationData.country, popSum)
                .where(
                    (PopulationData.year.between(startYear, endYear))
                            and (PopulationData.country inSubQuery aseanCountries)
                )
                .groupBy(PopulationData.year, PopulationData.country)
                .forEach {
                    val year = it[PopulationData.year]
                    val country = it[PopulationData.country]
                    val population = it[popSum] ?: 0.0

                    if (counts[year] == null) counts[year] = mutableMapOf(country to population)
                    else counts[year]?.put(country, population)
                }
        }

        return counts
    }

    /**
     * List of ASEAN countries in the DB
     *
     * @return List<String> the strings being the countries
     */
    fun aseanCountries(): List<String> {
        return transaction {
            Grouping
                .select(Grouping.country)
                .where { Grouping.grouping eq "asean"}
                .map { it[Grouping.country] }
        }
    }

}