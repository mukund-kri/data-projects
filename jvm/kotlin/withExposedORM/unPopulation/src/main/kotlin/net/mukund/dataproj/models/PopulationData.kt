package net.mukund.dataproj.models

import org.jetbrains.exposed.dao.id.IntIdTable

object PopulationData : IntIdTable("population_data") {
    val country = varchar("country", 256)
    val year = integer("year")
    val population = double("population")
}
