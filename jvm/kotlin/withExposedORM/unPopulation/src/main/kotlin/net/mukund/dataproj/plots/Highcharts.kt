package net.mukund.dataproj.plots

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import net.mukund.dataproj.analysis.Analysis
import java.io.File


@Serializable
class Problem1(
    val years: List<Int>,
    val population: List<Double>,
    @SerialName("start_year") val startYear: Int,
    @SerialName("end_year") val endYear: Int
)

@Serializable
class Problem2(
    val countries: List<String>,
    val population: List<Double>,
    @SerialName("selected_year") val selectedYear: Int
)

@Serializable
class Series(val name: String, val data: List<Double>)

@Serializable
class Problem4(val categories: List<Int>, val series: List<Series>)

object HighCharts {

    private const val BASE_DIR = "../plots/unpopulation/highcharts"

    private fun plot1(startYear: Int, endYear: Int) {
        val plotData = Analysis.problem1(startYear, endYear)

        val (years, population) = plotData.unzip()
        val p1 = Problem1(
            years,
            population,
            startYear,
            endYear
        )

        File("$BASE_DIR/problem1.json").printWriter().use { out ->
            out.println(Json.encodeToString(p1))
        }
    }

    private fun plot2(selectedYear: Int) {
        val plotData = Analysis.problem2(selectedYear)
        val (countries, population) = plotData.unzip()

        val p1 = Problem2(
            countries,
            population,
            selectedYear
        )

        File("$BASE_DIR/problem2.json").printWriter().use { out ->
            out.println(Json.encodeToString(p1))
        }
    }

    private fun plot3(startYear: Int, endYear: Int) {
        val plotData = Analysis.problem3(startYear, endYear)
        val (countries, population) = plotData.unzip()
        val p1 = Problem1(
            countries,
            population,
            startYear,
            endYear
        )

        File("$BASE_DIR/problem3.json").printWriter().use { out ->
            out.println(Json.encodeToString(p1))
        }
    }

    /**
     * Generate highcharts json file for problem 4
     */
    fun plot4(startYear: Int, endYear: Int) {
        val plotData = Analysis.problem4(startYear, endYear)

        val years = (startYear..endYear).toList()
        val series = mutableListOf<Series>()

        Analysis.aseanCountries().forEach { coutry ->
            val pbaData = mutableListOf<Double>()
            years.forEach { year ->
                pbaData.add(plotData[year]?.get(coutry) ?: 0.0)
            }

            series.add(Series(coutry, pbaData))
        }

        val p4 = Problem4(years, series)
        File("$BASE_DIR/problem4.json").printWriter().use { out ->
            out.println(Json.encodeToString(p4))
        }
    }

    fun allPlots(startYear: Int, endYear: Int, selectedYear: Int) {
        plot1(startYear, endYear)
        plot2(selectedYear)
        plot3(startYear, endYear)
        plot4(startYear, endYear)
    }


}