package net.mukund.dataproj.loading

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import net.mukund.dataproj.models.Grouping
import net.mukund.dataproj.models.PopulationData
import org.jetbrains.exposed.sql.batchInsert
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File

object Loader {


    private const val DATA_DIR = "/home/mukund/workarea/teaching/data-projects/raw_data/un-population"

    /**
     * Load all the data for this analysis. Including ...
     *
     * 1. The population data from un-population.csv
     * 2. The country groupings for asean and saarc
     */
    fun load() {

        loadPopulationData()
        loadGrouping("asean")
        loadGrouping("saarc")
    }


    // Load population data from csv file to DB
    private fun loadPopulationData() {

        val csvFileName = "$DATA_DIR/population-estimates_csv.csv"
        // Read in csv
        val rows = csvReader()
            .readAllWithHeader(File(csvFileName))

        transaction {
            PopulationData.batchInsert(rows) {
                this[PopulationData.country] = it["Region"]!!
                this[PopulationData.year] = it["Year"]?.toInt()!!
                this[PopulationData.population] = it["Population"]?.toDouble()!!
            }
        }
    }

    // Load country grouping from text file to DB
    private  fun loadGrouping(groupName: String) {
        val groupingFileName = "$DATA_DIR/$groupName-countries.txt"
        val countries = File(groupingFileName).readLines()

        transaction {
            Grouping.batchInsert(countries) {
                this[Grouping.country] = it
                this[Grouping.grouping] = groupName
            }
        }
    }
}