package net.mukund.dataproj.models

import org.jetbrains.exposed.dao.id.IntIdTable

object Grouping : IntIdTable("grouping") {
    val country = varchar("country", 256)
    val grouping = varchar("grouping", 10)
}