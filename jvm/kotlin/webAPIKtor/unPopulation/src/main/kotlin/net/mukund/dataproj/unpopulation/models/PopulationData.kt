package net.mukund.dataproj.unpopulation.models

import org.ktorm.schema.*

object PopulationData : Table<Nothing>("population_data") {
    val id = int("id").primaryKey()
    val country = varchar("country")
    val year = int("year")
    val population = double("population")
}