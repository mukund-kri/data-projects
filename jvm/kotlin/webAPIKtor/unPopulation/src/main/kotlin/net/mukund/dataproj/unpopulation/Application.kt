package net.mukund.dataproj.unpopulation

import io.ktor.server.application.*
import net.mukund.dataproj.unpopulation.plugins.configureHTTP
import net.mukund.dataproj.unpopulation.plugins.configureRouting
import net.mukund.dataproj.unpopulation.plugins.configureSerialization

fun main(args: Array<String>) {
    io.ktor.server.netty.EngineMain.main(args)
}

fun Application.module() {
    configureSerialization()
    configureHTTP()
    configureRouting()
}
