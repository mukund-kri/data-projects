package net.mukund.dataproj.unpopulation.plugins

import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import net.mukund.dataproj.unpopulation.analysis.Analyzer
import net.mukund.dataproj.unpopulation.models.*
import org.ktorm.database.Database

private val database = Database.connect(
    "jdbc:postgresql://127.0.0.1:5432/un_population",
    user = "devuser",
    password = "devpass")
private val analysis = Analyzer(database)

fun Application.configureRouting() {
    analysis.initialize()
    routing {

        get("/problem1") {
            // Get the start year and end year from the query path
            val startYear = call.request.queryParameters["startYear"]?.toIntOrNull() ?: 2000
            val endYear = call.request.queryParameters["endYear"]?.toIntOrNull() ?: 2010

            // Get and analyze data from DB
            val plotData = analysis.problem1(startYear, endYear)
            // Transform data into a serializable from
            val p1 = plotData.map { P1DataRow(it.first, it.second) }
            call.respond(p1)
        }

        get("/problem2") {
            // Get the selected year from query path
            val selectedYear = call.request.queryParameters["selectedYear"]?.toIntOrNull() ?: 2000

            // Get and analyze data from DB
            val plotData = analysis.problem2(selectedYear)
            // Transform data into a serializable form
            val p2 = plotData.map { P2DataRow(it.first, it.second) }
            call.respond(p2)
        }

        get("/problem3") {
            // Get the start year and end year from the query path
            val startYear = call.request.queryParameters["startYear"]?.toIntOrNull() ?: 2000
            val endYear = call.request.queryParameters["endYear"]?.toIntOrNull() ?: 2010

            // Get and analyze data from DB
            val plotData = analysis.problem3(startYear, endYear)
            // Transform data into a serializable from
            val p3 = plotData.map { P3DataRow(it.first, it.second) }
            call.respond(p3)
        }

        get("/problem4") {
            // Get the start year and end year from the query path
            val startYear = call.request.queryParameters["startYear"]?.toIntOrNull() ?: 2000
            val endYear = call.request.queryParameters["endYear"]?.toIntOrNull() ?: 2010

            // Get and analyze data from DB
            val plotData = analysis.problem4(startYear, endYear)
            call.respond(plotData)

        }
    }
}
