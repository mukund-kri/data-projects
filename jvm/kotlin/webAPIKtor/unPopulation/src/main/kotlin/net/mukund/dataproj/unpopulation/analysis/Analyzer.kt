package net.mukund.dataproj.unpopulation.analysis

import org.ktorm.database.Database
import org.ktorm.dsl.*
import net.mukund.dataproj.unpopulation.models.*

class Analyzer(private val database: Database) {
    private lateinit var aseanCountries: List<String>
    private lateinit var saarcCountries: List<String>

    fun initialize() {
        aseanCountries = database
            .from(Groupings)
            .select(Groupings.country)
            .where(Groupings.grouping eq "asean")
            .mapNotNull { it.getString(1) }

        saarcCountries = database
            .from(Groupings)
            .select(Groupings.country)
            .where(Groupings.grouping eq "saarc")
            .mapNotNull { it.getString(1) }

    }
    fun problem1(startYear: Int, endYear: Int): List<Pair<Int, Double>> {

        return database
            .from(PopulationData)
            .select(PopulationData.year, PopulationData.population)
            .where {
                (PopulationData.year between startYear..endYear) and
                        (PopulationData.country eq "India")
            }
            .map { it.getInt(1) to it.getDouble(2)}
            .toList()
    }

    fun problem2(selectedYear: Int): List<Pair<String, Double>> {

        return database
            .from(PopulationData)
            .select(PopulationData.country, PopulationData.population)
            .where((PopulationData.year eq selectedYear) and
                (PopulationData.country inList aseanCountries))
            .map { (it.getString(1) ?: "") to it.getDouble(2) }
            .toList()
    }

    fun problem3(startYear: Int, endYear: Int): List<Pair<Int, Double>> {

        return database
            .from(PopulationData)
            .select(PopulationData.year, sum(PopulationData.population))
            .where(
                (PopulationData.country inList saarcCountries)
                        and (PopulationData.year between startYear..endYear))
            .groupBy(PopulationData.year)
            .map { it.getInt(1) to it.getDouble(2) }
            .toList()
    }

    fun problem4(startYear: Int, endYear: Int): List<Map<String, Double>> {

        val counts = mutableMapOf<Int, MutableMap<String, Double>>()

        val result = database
            .from(PopulationData)
            .select(PopulationData.year, PopulationData.country, sum(PopulationData.population))
            .where((PopulationData.year between startYear .. endYear)
                    and (PopulationData.country inList aseanCountries))
            .groupBy(PopulationData.year, PopulationData.country)
            .forEach {
                val year = it.getInt(1)
                val country = it.getString(2).toString()
                val population = it.getDouble((3))

                if (counts[year] == null) counts[year] = mutableMapOf(country to population)
                else counts[year]?.put(country, population)
            }

        return counts.map { (year, counts) ->
            counts["year"] = year.toDouble()
            counts
        }
    }
}