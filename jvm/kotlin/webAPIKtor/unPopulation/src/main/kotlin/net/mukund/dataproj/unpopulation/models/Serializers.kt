package net.mukund.dataproj.unpopulation.models

import kotlinx.serialization.Serializable


@Serializable
class P1DataRow(val year: Int, val population: Double)

@Serializable
class P2DataRow(val country: String, val population: Double)

@Serializable
class P3DataRow(val year: Int, val population: Double)