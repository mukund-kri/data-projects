package net.mukund.dataproj.unpopulation.models

import org.ktorm.schema.*

object Groupings : Table<Nothing>("grouping") {
    val id = int("id").primaryKey()
    val country = varchar("country")
    val grouping = varchar("grouping")
}