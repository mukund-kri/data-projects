CREATE TABLE companies (
  id SERIAL PRIMARY KEY,
  authorized_capital BIGINT NOT NULL,
  registration_year INT NOT NULL,
  pba TEXT
);
