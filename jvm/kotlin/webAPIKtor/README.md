# Data problems :: Web API with ktor

## What is ktor?

Ktor is a framework for building asynchronous servers and clients in connected systems 
using the powerful Kotlin programming language. It is a framework that is easy to use 
and has a lot of features that make it a great choice for building web applications.

## ORM

This set of projects uses the KtORM library to interact with the database. KtORM is a
Kotlin ORM library that is easy to use and has a lot of features that make it a great
choice for building web applications.

## Database

The database used in this project is PostgreSQL. PostgreSQL is a powerful, open-source
object-relational database system that is easy to use and has a lot of features that
make it a great choice for building web applications.

## What software do you need?

1. Java 21
2. Gradle v8+
3. Docker / Docker Compose

## How to run the project

### 1. Run postgres database

I have created a docker-compose file that will start a PostgreSQL database. This
covers all our DB needs

```shell
docker-compose up -d
```

### 2. Create DB and schema
    
This is project specific. The SQL for DB and schema creation is in the `sqls` folder.

So to create the DB and schema, for say the `ipl` run all the SQL files in the `sqls/ipl` folder.

```shell
psql -h localhost -U postgres -d postgres -a -f sqls/ipl/<sql script name>
```

### 3. cd into the project you want to run

Say for the IPL project ...

```shell
cd data-problems
```

#### 3.1. Load Data:: 
TODO

#### 3.2 Run the project

```shell
gradle run
```
