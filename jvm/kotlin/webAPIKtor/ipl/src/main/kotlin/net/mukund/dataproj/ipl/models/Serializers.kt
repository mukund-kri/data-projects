package net.mukund.dataproj.ipl.models

import kotlinx.serialization.Serializable


@Serializable
class P1DataRow(val label: String, val count: Int)

@Serializable
class P2DataRow(val batsman: String, val runs: Int)

@Serializable
class P3DataRow(val country: String, val count: Int)