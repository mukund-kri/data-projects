package net.mukund.dataproj.ipl.plugins

import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import net.mukund.dataproj.ipl.analytics.Analyzer
import net.mukund.dataproj.ipl.models.P1DataRow
import net.mukund.dataproj.ipl.models.P2DataRow
import net.mukund.dataproj.ipl.models.P3DataRow
import org.ktorm.database.Database

private val database = Database.connect(
    "jdbc:postgresql://127.0.0.1:5432/ipl",
    user = "devuser",
    password = "devpass")
private val analysis = Analyzer(database)

fun Application.configureRouting() {
    routing {

        get("/problem1") {
            val plotData = analysis.problem1()

            val p1 = plotData.map { P1DataRow(it.key, it.value) }
            call.respond(p1)
        }

        get("/problem2") {
            val plotData = analysis.problem2()
            val p2 = plotData.map { P2DataRow(it.key, it.value)}
            call.respond(p2)
        }

        get("/problem3") {
            val plotData = analysis.problem3()
            val p3 = plotData.map { P3DataRow(it.key, it.value) }
            call.respond(p3)
        }

        get("/problem4") {
            val plotData = analysis.problem4()
            call.respond(plotData)
        }
    }
}
