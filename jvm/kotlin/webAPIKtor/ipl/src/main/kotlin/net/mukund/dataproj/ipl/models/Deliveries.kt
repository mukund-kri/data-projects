package net.mukund.dataproj.ipl.models

import org.ktorm.schema.Table
import org.ktorm.schema.int
import org.ktorm.schema.varchar


object Deliveries : Table<Nothing>("deliveries") {
    val id = int("id").primaryKey()
    val battingTeam = varchar("batting_team")
    val totalRuns = int("total_runs")
    val batter = varchar("batter")
    val batsmanRuns = int("batsman_runs")
}
