package net.mukund.dataproj.ipl.models

import org.ktorm.schema.Table
import org.ktorm.schema.int
import org.ktorm.schema.varchar


object Umpires : Table<Nothing>("umpires") {
    val id = int("id").primaryKey()
    val name = varchar("name")
    val country = varchar("country")
}