package net.mukund.dataproj.ipl.analytics

import net.mukund.dataproj.ipl.models.*
import org.ktorm.database.Database
import org.ktorm.dsl.*

typealias TeamSeasonRuns = Map<String, Map<String, Int>>

class Analyzer(private  val database: Database) {
    /**
     * Problem 1: Total runs scored by team in all of IPL
     *
     * @return returns a map of team name to runs scored
     */
    fun problem1(): Map<String, Int> {

        return database.from(Deliveries)
            .select(Deliveries.battingTeam, sum(Deliveries.totalRuns))
            .groupBy(Deliveries.battingTeam)
            .associate { it.getString(1)!! to it.getInt(2) }
    }

    /**
     * Problem 2: Top batsman for RCB
     *
     * @returns A map of top batsmen and the total runs they have scored.
     */
    fun problem2(): Map<String, Int> {

        val totalBatsmanRun = sum(Deliveries.batsmanRuns)
        return database.from(Deliveries)
            .select(Deliveries.batter, totalBatsmanRun)
            .groupBy(Deliveries.batter)
            .orderBy(totalBatsmanRun.desc())
            .limit(10)
            .associate { it.getString(1)!! to it.getInt(2) }
    }

    /**
     * Problem 3: Number of foreign umpires by country of origin
     *
     * @returns A map of umpires to the country of origin
     */
    fun problem3(): Map<String, Int> {

        val countryCount = count(Umpires.country)
        return database.from(Umpires)
            .select(Umpires.country, countryCount)
            .where(Umpires.country neq "IND")
            .groupBy(Umpires.country)
            .associate { it.getString(1)!! to it.getInt(2) }
    }

    /**
     * Problem 4: Matches won by ipl team by season
     *
     * @return map of map, for team by season
     */
    fun problem4(): List<Map<String, String>>  {
        val matchesCount = count(Matches.winner)
        val counts = mutableMapOf<String, MutableMap<String, Int>>()

        database.from(Matches)
            .select(Matches.winner, Matches.season, matchesCount)
            .groupBy(Matches.winner, Matches.season)
            .forEach {
                val team = it.getString(1)!!
                val season = it.getString(2)!!
                val runs = it.getInt(3)

                if (counts[team] == null) counts[team] = mutableMapOf()
                counts[team]?.set(season, runs)
            }
        return counts
            .map {
                val rowMap = it.value.map { ele -> ele.key to ele.value.toString() }.toMap().toMutableMap()
                rowMap["team"] = it.key
                rowMap
            }
    }
}