package net.mukund.dataproj.ipl

import io.ktor.server.application.*
import net.mukund.dataproj.ipl.plugins.configureHTTP
import net.mukund.dataproj.ipl.plugins.configureRouting
import net.mukund.dataproj.ipl.plugins.configureSerialization

fun main(args: Array<String>) {
    io.ktor.server.netty.EngineMain.main(args)
}

fun Application.module() {
    configureSerialization()
    configureHTTP()
    configureRouting()
}
