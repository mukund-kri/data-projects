package net.mukund.dataproj.ipl.models

import org.ktorm.schema.Table
import org.ktorm.schema.int
import org.ktorm.schema.varchar


object Matches : Table<Nothing>("matches") {
    val id = int("id").primaryKey()
    val winner = varchar("winner")
    val season = varchar("season")
}
