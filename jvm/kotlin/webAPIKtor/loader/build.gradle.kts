plugins {
    // Apply the org.jetbrains.kotlin.jvm Plugin to add support for Kotlin.
    alias(libs.plugins.kotlin.jvm)

    // Apply the application plugin to add support for building a CLI application in Java.
    application
}

repositories {
    // Use Maven Central for resolving dependencies.
    mavenCentral()
}

dependencies {

    // This dependency is used by the application.
    implementation(libs.guava)

    // Database related
    implementation(libs.ktorm.core)
    implementation(libs.ktorm.postgres)
    implementation(libs.postgresql.driver)

    // CSV processing
    implementation(libs.kotlin.csv)

    // For data classes of company master
    implementation(project(":companyMaster"))

}

// Apply a specific Java toolchain to ease working on different environments.
java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(21)
    }
}

testing {
    suites {
        // Configure the built-in test suite
        val test by getting(JvmTestSuite::class) {
            // Use "Kotlin Test" test framework
            useKotlinTest("2.0.0")
        }
    }
}

application {
    // Define the main class for the application.
    mainClass = "net.mukund.dataproj.loader.MainKt"
}

tasks.named<Test>("test") {
    // Use JUnit Platform for unit tests.
    useJUnitPlatform()
}

