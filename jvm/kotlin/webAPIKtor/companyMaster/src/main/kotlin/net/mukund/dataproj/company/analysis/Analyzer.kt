package net.mukund.dataproj.company.analysis

import net.mukund.dataproj.company.models.Companies
import net.mukund.dataproj.company.models.*
import org.ktorm.database.Database
import org.ktorm.database.asIterable
import org.ktorm.dsl.*

class Analyzer(private val database: Database) {

    /**
     * Compute Histogram of registered companies by Authorized Capital
     */
    fun problem1(): Map<String, Int> {
        val sql = """SELECT
            CASE
                WHEN authorized_capital <= 100000 THEN '< 1L'
                WHEN authorized_capital > 100000 AND authorized_capital <= 1000000 THEN '1L - 10L'
                WHEN authorized_capital > 1000000 AND authorized_capital <= 10000000 THEN '10L - 1Cr'
                WHEN authorized_capital > 10000000 AND authorized_capital <= 100000000 THEN '1Cr - 10Cr'
                ELSE '> 10Cr'
            END
            AS authorized_capital_range,
            COUNT(authorized_capital) AS counts
        FROM companies
        GROUP BY authorized_capital_range
        """.trimIndent()

        return database.useConnection { conn ->
            conn.prepareStatement(sql).use { statement ->
                statement.executeQuery().asIterable().map { it.getString(1) to it.getInt(2) }
            }
        }.toMap()
    }

    /**
     * Compute counts of companies by Registration Year, between year range
     */
    fun problem2(startYear: Int, endYear: Int): List<P2DataRow> {

        // get all companies between startYear And endYear
        return database
            .from(Companies)
            .select(Companies.registrationYear, count(Companies.registrationYear))
            .where { (Companies.registrationYear greaterEq startYear) and (Companies.registrationYear lessEq endYear)}
            .groupBy(Companies.registrationYear)
            .map { P2DataRow(it.getInt(1), it.getInt(2))}
    }

    /**
     * Compute counts of pba
     */
    fun problem3(selectedYear: Int, selectedTop: Int): List<P3DataRow> {
        val counts = count(Companies.pba).aliased("counts")

        return  database.from(Companies)
            .select(Companies.pba, counts)
            .where { Companies.registrationYear eq selectedYear }
            .groupBy(Companies.pba)
            .orderBy(counts.desc())
            .map { P3DataRow((it.getString(1) ?: ""), it.getInt(2)) }
            .take(selectedTop)
    }

    fun problem4(startYear: Int, endYear: Int): List<Map<String, String>> {

        val counts = mutableMapOf<String, MutableMap<String, Int>>()

        database.from(Companies)
            .select(Companies.pba, Companies.registrationYear, count())
            .where { (Companies.registrationYear greaterEq startYear) and (Companies.registrationYear lessEq endYear) }
            .groupBy(Companies.pba, Companies.registrationYear)
            .forEach {
                val pba = it.getString(1) ?: ""
                val year = it.getString(2) ?: ""
                val count = it.getInt(3)
                if (counts[pba] == null) {
                    counts[pba] = mutableMapOf()
                }
                counts[pba]?.put(year, count)
            }

        return counts
            .map {
                val rowMap = it.value.map { ele -> ele.key to ele.value.toString() }.toMap().toMutableMap()
                rowMap["pba"] = it.key
                rowMap
            }

    }
}