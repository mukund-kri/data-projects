package net.mukund.dataproj.company.plugins

import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import net.mukund.dataproj.company.analysis.Analyzer
import net.mukund.dataproj.company.models.*
import org.ktorm.database.Database

private val database = Database.connect(
    "jdbc:postgresql://127.0.0.1:5432/company_master",
    user = "devuser",
    password = "devpass")
private val analysis = Analyzer(database)
private val problem1Categories = listOf("< 1L", "1L - 10L", "10L - 1Cr", "1Cr - 10Cr", "> 10Cr")

fun Application.configureRouting() {
    routing {

        get("/problem1") {
            val plotData = analysis.problem1()

            val p1 = problem1Categories.map { P1DataRow(it, plotData[it] ?: 0) }.toList()
            call.respond(p1)
        }

        get("/problem2") {
            val startYear = call.request.queryParameters["startYear"]?.toIntOrNull() ?: 2000
            val endYear = call.request.queryParameters["endYear"]?.toIntOrNull() ?: 2010

            val plotData = analysis.problem2(startYear, endYear)
            call.respond(plotData)
        }

        get("/problem3") {
            val selectedYear = call.request.queryParameters["selectedYear"]?.toIntOrNull() ?: 2000
            val top = call.request.queryParameters["numOfPbas"]?.toIntOrNull() ?: 5

            val plotData = analysis.problem3(selectedYear, top)
            call.respond(plotData)
        }

        get("/problem4") {
            val startYear = call.request.queryParameters["startYear"]?.toIntOrNull() ?: 2000
            val endYear = call.request.queryParameters["endYear"]?.toIntOrNull() ?: 2010

            val plotData = analysis.problem4(startYear, endYear)
            call.respond(plotData)
        }
    }
}
