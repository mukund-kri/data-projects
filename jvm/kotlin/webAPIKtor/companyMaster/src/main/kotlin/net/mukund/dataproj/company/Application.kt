package net.mukund.dataproj.company

import io.ktor.server.application.*
import net.mukund.dataproj.company.plugins.configureHTTP
import net.mukund.dataproj.company.plugins.configureRouting
import net.mukund.dataproj.company.plugins.configureSerialization

fun main(args: Array<String>) {
    io.ktor.server.netty.EngineMain.main(args)
}

fun Application.module() {
    configureSerialization()
    configureHTTP()
    configureRouting()
}
