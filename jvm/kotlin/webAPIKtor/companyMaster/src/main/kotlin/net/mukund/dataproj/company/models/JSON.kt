package net.mukund.dataproj.company.models

import kotlinx.serialization.Serializable

@Serializable
class P1DataRow(val label: String, val count: Int)

@Serializable
class P2DataRow(val year: Int, val registrations: Int)

@Serializable
class P3DataRow(val pba: String, val registrations: Int)