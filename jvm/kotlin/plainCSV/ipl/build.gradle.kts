
plugins {
    // Apply the org.jetbrains.kotlin.jvm Plugin to add support for Kotlin.
    alias(libs.plugins.kotlin.jvm)


    // For json serialization
    kotlin("plugin.serialization") version "2.0.21"

    // Apply the application plugin to add support for building a CLI application in Java.
    application
}

repositories {
    // Use Maven Central for resolving dependencies.
    mavenCentral()
}

dependencies {

    // This dependency is used by the application.
    implementation(libs.guava)

    implementation("com.jsoizo:kotlin-csv-jvm:1.10.0")

    // JSON dumping
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.7.3")

}

// Apply a specific Java toolchain to ease working on different environments.
java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(21)
    }
}

testing {
    suites {
        // Configure the built-in test suite
        val test by getting(JvmTestSuite::class) {
            // Use "Kotlin Test" test framework
            useKotlinTest("2.0.21")
        }
    }
}

application {
    // Define the main class for the application.
    mainClass = "net.mukund.dataprob.MainKt"
}

tasks.named<Test>("test") {
    // Use JUnit Platform for unit tests.
    useJUnitPlatform()
}
