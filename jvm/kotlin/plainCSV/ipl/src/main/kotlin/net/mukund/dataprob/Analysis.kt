package net.mukund.dataprob

const val rcbName = "Royal Challengers Bangalore"

typealias Problem4PlotData = Map<String, Map<String, Int>>
typealias SetOfString = Set<String>


object Analysis {

    fun problem1(csvData: List<Delivery>): Map<String, Int> {

        var counts = mutableMapOf<String, Int>()

        csvData
            .forEach {
                val runs = counts.getOrDefault(it.battingTeam, 0)
                counts[it.battingTeam] = runs + it.totalRuns
            }

        return counts
    }

    // Problem 2: Runs scored by batsmen of RCB
    fun problem2(csvData: List<Delivery>, top: Int = 10): Map<String, Int> {

        return csvData
            .filter { it.battingTeam == rcbName }
            .groupBy( {it.batter}, { 1 })
            .map { it.key to it.value.size }
            .sortedBy { it.second }
            .reversed()
            .take(top)
            // .forEach { println(it) }
            .toMap()
    }

    // Problem 3: Count of Non-Indian umpires by origin
    fun problem3(umpireList: List<Umpire>): Map<String, Int> {
        return umpireList
            .filter { it.country != "IND" }
            .groupBy { it.country }
            .map { it.key to it.value.size }
            .toMap()
    }

    // Problem 4: Grouped chart of matches won by team by season
    fun problem4(matchData: List<Match>): Triple<Problem4PlotData, SetOfString, SetOfString>  {

        val teams: MutableSet<String> = mutableSetOf()
        val seasons: MutableSet<String> = mutableSetOf()
        val counts: MutableMap<String, MutableMap<String, Int>> = mutableMapOf()

        matchData
            .forEach { match ->
                teams.add(match.winner)
                seasons.add(match.season)
            }
        teams.remove("NA")

        // Now populate wins
        matchData.forEach {
            val seasonMap = counts.getOrDefault(it.winner, mutableMapOf())
            val count = seasonMap.getOrDefault(it.season, 0)
            seasonMap[it.season] = count + 1
            counts[it.winner] = seasonMap
        }

        return Triple(counts, teams, seasons)
    }
}