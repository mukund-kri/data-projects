package net.mukund.dataprob

import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File

@Serializable
class Problem1(val teams: List<String>, val runs: List<Int>)

@Serializable
class Problem2(val batsman: List<String>, val runs: List<Int>)

@Serializable
class  Problem3(val countries: List<String>, val count: List<Int>)

@Serializable
class Series(val name: String, val data: List<Int>)

@Serializable
class Problem4(val teams: List<String>, val series: List<Series>)


class Highcharts(val plotBasePath: String) {

    private fun plot1() {
        val data = Analysis.problem1(CsvOps.csvData)

        val problem1Ans = Problem1(
            data.keys.toList(),
            data.values.toList()
        )

        // Dump out json to file
        File("$plotBasePath/problem1.json").printWriter().use { out ->
            out.println(Json.encodeToString(problem1Ans))
        }
    }

    // Plot problem 2. Top batsmen for Royal challengers bangalore
    private fun plot2() {
        // Do the analysis on the raw data
        val data = Analysis.problem2(CsvOps.csvData)
        // Construct the outgoing json data
        val p2 = Problem2(
            data.keys.toList(),
            data.values.toList()
        )
        // Dump out json to file
        File("$plotBasePath/problem2.json").printWriter().use { out ->
            out.println(Json.encodeToString(p2))
        }
    }

    // Plot problem 3. Count of Non-Indian umpires by country
    private fun plot3() {
        // Do the analysis on raw data
        val data = Analysis.problem3(CsvOps.umpireData)
        // Construct the outgoing json data
        val p3 = Problem3 (
            data.keys.toList(),
            data.values.toList()
        )
        // Dump out json to file
        File("$plotBasePath/problem3.json").printWriter().use { out ->
            out.println(Json.encodeToString(p3))
        }
    }

    // Plot problem 4. Matches won by team by season
    private fun plot4() {
        // Do the analysis on the raw data
        val (data, teamsSet, seriesSet) = Analysis.problem4(CsvOps.matchData)
        val series = seriesSet.toList()
        val teams = teamsSet.toList()

        // Construct the outgoing json
        val seriesOut = mutableListOf<Series>()
        series.forEach { s ->
            val runsList = mutableListOf<Int>()
            teams.forEach { team ->
                runsList.add(data[team]?.get(s) ?: 0)
            }
            seriesOut.add(Series(s, runsList))
        }
        val p4 = Problem4 (teams, seriesOut)
        // Dump out json to file
        File("$plotBasePath/problem4.json").printWriter().use { out ->
            out.println(Json.encodeToString(p4))
        }
    }

    // Plot problem 4. Matches won by team by year
    fun plotAll() {
        // plot1()
        // plot2()
        // plot3()
        plot4()
    }
}