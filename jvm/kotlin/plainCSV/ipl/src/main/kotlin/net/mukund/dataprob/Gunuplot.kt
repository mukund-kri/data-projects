package net.mukund.dataprob

import java.io.File

class Gnuplot(val plotBasePath: String) {

    private fun plot1() {

        val data = Analysis.problem1(CsvOps.csvData)
        File("$plotBasePath/problem1.dat").printWriter().use { out ->
            data.forEach { (key, value) -> out.println("\"$key\"\t$value") }
        }
    }

    private fun plot2() {

        val data = Analysis.problem2(CsvOps.csvData)
        File("$plotBasePath/problem2.dat").printWriter().use { out ->
            data.forEach { (key, value) -> out.println("\"$key\"\t$value") }
        }
    }

    private fun plot3() {
        val data = Analysis.problem3(CsvOps.umpireData)
        File("$plotBasePath/problem3.dat").printWriter().use { out ->
            data.forEach { (key, value) -> out.println("\"$key\"\t$value") }
        }
    }

    private fun plot4() {
        val (data, teams, seasons) = Analysis.problem4(CsvOps.matchData)

        File("$plotBasePath/problem4.dat").printWriter().use { out ->

            // Write the team names as header
            out.println(teams.joinToString(separator = "\t") { "\"$it\"" })
            seasons.forEach { season ->
                out.print("\"${season}\"")
                teams.forEach { team -> out.print("\t${data[team]?.get(season) ?: 0}") }
                out.println()
            }

        }

    }

    fun plotAll() {
        // plot1()
        // plot2()
        // plot3()
        plot4()
    }


}