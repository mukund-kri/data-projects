package net.mukund.dataprob

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import java.io.File

const val dataPath =  "../../../../raw_data/ipl"

data class Delivery(val battingTeam: String, val totalRuns: Int, val batter: String, val batsmanRuns: Int)

data class Umpire(val name: String, val country: String)

data class Match(val season: String, val winner: String)

object CsvOps {

    var csvData: List<Delivery> = CsvOps.loadFromCsv("$dataPath/corrected_deliveries.csv")
    var umpireData = CsvOps.loadUmpiresFromCsv("$dataPath/ipl-umpire.csv")
    var matchData = CsvOps.loadMatchesFromCsv("$dataPath/corrected_matches.csv")

    fun loadFromCsv(csvFileName: String): List<Delivery> {
        return csvReader()
            .readAllWithHeader(File(csvFileName))
            .mapNotNull(::rowToCsv)
    }

    fun rowToCsv(row: Map<String, String>): Delivery? {

        // Required data
        val battingTeam = row["batting_team"]
        val totalRuns = row["total_runs"]?.toIntOrNull()
        val batter = row["batter"]
        val batsmanRuns = row["batsman_runs"]?.toInt()

        if (battingTeam == null || totalRuns == null ) return null
        else return Delivery(battingTeam, totalRuns, batter!!, batsmanRuns!!)
    }

    fun loadUmpiresFromCsv(csvFileName: String): List<Umpire> {
        return csvReader()
            .readAllWithHeader(File(csvFileName))
            .map { row ->
                val name: String? = row["name"]
                val country: String? = row["country"]

                Umpire(name!!, country!!)
            }
    }

    fun loadMatchesFromCsv(csvFileName: String): List<Match> {
        return csvReader()
            .readAllWithHeader(File(csvFileName))
            .map { row ->
                val season = row["season"]
                val winner= row["winner"]

                Match(season!!, winner!!)
            }
    }
}