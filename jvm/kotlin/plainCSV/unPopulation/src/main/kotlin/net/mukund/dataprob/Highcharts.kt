package net.mukund.dataprob

import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import net.mukund.dataprob.models.*
import java.io.File


class Highcharts(private val baseDataPath: String, private val basePlotsPath: String) {

    private val csvOps = CsvOps(baseDataPath)

    // Plot 1: India population by year
    private fun plot1(startYear: Int, endYear: Int) {
        // Do the analysis
        val plotData = Analysis.problem1(csvData = csvOps.populationData, startYear, endYear)
        // Construct the outgoing json data
        val p1 = Problem1(
            plotData.keys.toList(),
            plotData.values.toList(),
            startYear,
            endYear
        )
        // Dump out json to file
        File("$basePlotsPath/problem1.json").printWriter().use { out ->
            out.println(Json.encodeToString(p1))
        }
    }

    // Plot 2: ASEAN population in selected year
    private fun plot2(selectedYear: Int) {
        // do the analysis
        val plotData = Analysis.problem2(csvOps.populationData, csvOps.aseanCountries, selectedYear)
        // Construct the outgoing JSON
        val p2 = Problem2 (
            plotData.keys.toList(),
            plotData.values.toList(),
            selectedYear
        )
        // Dump out json to file
        File("$basePlotsPath/problem2.json").printWriter().use { out ->
            out.println(Json.encodeToString(p2))
        }
    }

    // Plot 3: Total SAARC population by year over a year range
    private fun plot3(startYear: Int, endYear: Int) {
        // do analysis
        val plotData = Analysis.problem3(csvOps.populationData, csvOps.saarcCountries, startYear, endYear)
        // Construct the outgoing JSON
        val p3 = Problem3 (
            plotData.keys.toList(),
            plotData.values.toList(),
            startYear,
            endYear
        )
        // Dump out json to file
        File("$basePlotsPath/problem3.json").printWriter().use { out ->
            out.println(Json.encodeToString(p3))
        }
    }

    // Plot 4: ASEAN population by country by year
    private fun plot4(startYear: Int, endYear: Int) {
        val plotData = Analysis.problem4(csvOps.populationData, csvOps.aseanCountries, startYear, endYear)
        val years = startYear..endYear
        val series = mutableListOf<Series>()

        csvOps.aseanCountries.forEach { country ->
            val countsData = mutableListOf<Float>()
            years.forEach { year ->
                countsData.add(plotData[year]?.get(country) ?: 0.0f)
            }

            series.add(Series(country, countsData))
        }

        val p4 = Problem4(years.toList(), series, startYear, endYear)
        File("$basePlotsPath/problem4.json").printWriter().use { out ->
            out.println(Json.encodeToString(p4))
        }
    }

    /**
     * Create oll the JSON data files for UN Population data set to be rendered in HighCharts.
     *
     * @param startYear The start year in plots with date ranges
     * @param endYear The end year in plots with date ranges
     * @param selectedYear For problems that based on a specific year
     */
    fun plotAll(startYear: Int, endYear: Int, selectedYear: Int) {

        println("Data Input Dir :: $baseDataPath")
        println("Data Output Dir :: $basePlotsPath")

//        plot1(startYear, endYear)
//        plot2(selectedYear)
//        plot3(startYear, endYear)
        plot4(startYear, endYear)
    }
}