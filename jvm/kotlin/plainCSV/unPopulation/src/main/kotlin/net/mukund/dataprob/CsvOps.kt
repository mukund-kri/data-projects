package net.mukund.dataprob

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import net.mukund.dataprob.models.PopulationData
import java.io.File


class CsvOps(private val dataPath: String) {

    val populationData = loadFromCsv("$dataPath/population-estimates_csv.csv")
    val saarcCountries = loadGrouping("saarc")
    val aseanCountries = loadGrouping("asean")

    // Load the raw source CSV
    private fun loadFromCsv(dataFile: String): List<PopulationData> {
        return csvReader()
            .readAllWithHeader(File(dataFile))
            .map { PopulationData(
                it["Region"]!!,
                it["Population"]?.toFloat() ?: 0.0f,
                it["Year"]?.toInt() ?: 0,
                ) }
    }

    // For loading ASEAN and SAARC countries
    private fun loadGrouping(groupName: String): List<String> {
        val groupingFileName = "$dataPath/$groupName-countries.txt"
        return File(groupingFileName).readLines().map { it.trim() }
    }
}