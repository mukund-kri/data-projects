package net.mukund.dataprob

import net.mukund.dataprob.models.PopulationData

typealias YearCountyData = Map<Int, Map<String, Float>>


object Analysis {

    /**
     * Problem 1: Analyze the population growth in india over the given range of years.
     *
     * @param csvData the raw population data extracted from source csv
     * @param startYear start of the year range
     * @param endYear end of year range
     *
     * @return the result of the analysis; a map of years vs population
     */
    fun problem1(csvData: List<PopulationData>, startYear: Int, endYear: Int): Map<Int, Float> {
        return csvData
            .filter { it.country == "India" }
            .filter { it.year in startYear..endYear }
            .associate { it.year to it.population }
    }

    /**
     * Problem 2: Filter population of ASEAN countries in the selected year.
     *
     * @param csvData the raw population data extracted from source csv
     * @param aseanCountries List of countries in the ASEAN block
     * @param selectedYear selected year to get the population data for ASEAN countries
     *
     * @return the result of analysis; a map of country vs. population
     */
    fun problem2(csvData: List<PopulationData>, aseanCountries: List<String>, selectedYear: Int):
            Map<String, Float> {
        return csvData
            .filter { it.country in aseanCountries }
            .filter { it.year == selectedYear }
            .associate { it.country to it.population }
    }

    /**
     * Problem 3: Population growth of total SAARC population over the range of years.
     *
     * @param csvData the raw population data extracted from source csv
     * @param saarcCountries List of countries in the SAARC block
     * @param startYear start of the year range
     * @param endYear end of year range
     *
     * @return the result of the analysis: a map of year vs. population
     */
    fun problem3(csvData: List<PopulationData>, saarcCountries: List<String>, startYear: Int, endYear: Int):
            Map<Int, Float> {

        return csvData
            .filter { it.year in startYear..endYear }
            .filter { it.country in saarcCountries }
            .groupBy { it.year }
            .map { (year, population) -> year to population.fold(0.0f) { acc, pop -> acc + pop.population} }
            .toMap()

    }

    /**
     * Problem 4: Population of individual ASEAN countries over a range of years.
     *
     * @param csvData the raw population data extracted from source csv
     * @param aseanCountries List of countries in the ASEAN block
     * @param startYear start of the year range
     * @param endYear end of year range
     *
     * @return the result of the analysis; a "map of map" of population of ASEAN countries over the
     *  range of years
     */
    fun problem4(csvData: List<PopulationData>, aseanCountries: List<String>, startYear: Int, endYear: Int):
            YearCountyData {

        val counts = mutableMapOf<Int, MutableMap<String, Float>>()

        csvData
            .filter { it.year in startYear..endYear }
            .filter { it.country in aseanCountries }
            .forEach {
                if (!counts.containsKey(it.year)) counts[it.year] = mutableMapOf<String, Float>()

                counts[it.year]?.compute(it.country) { _, oldValue ->
                    (oldValue ?: 0.0f) + it.population
                }
            }

        return counts
    }
}
