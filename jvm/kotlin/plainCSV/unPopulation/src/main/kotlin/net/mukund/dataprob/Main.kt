package net.mukund.dataprob


fun main() {
    println("Plotting the UN Population data set .................")

    if (args.isEmpty())
        println("Argument not given")
    else
        when (args[0]) {
            // Load data from csv to db
            "gnuplot" -> Gnuplot(
                    "/home/mukund/workarea/teaching/data-projects/raw_data/un-population",
                    "../plots/unpopulation/gnuplot"
                ).plotAll(2000, 2010, 2005)
            "highcharts" -> HighCharts(
                    "/home/mukund/workarea/teaching/data-projects/raw_data/un-population",
                    "../plots/unpopulation/gnuplot"
                ).plotAll(2000, 2010, 2005)
            else -> println("\nInvalid Argument")
        }

}