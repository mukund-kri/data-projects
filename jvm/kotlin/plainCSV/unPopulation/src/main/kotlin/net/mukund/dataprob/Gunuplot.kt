package net.mukund.dataprob

import kotlinx.serialization.encodeToString
import net.mukund.dataprob.models.*
import java.io.File


class Gnuplot(private val baseDataPath: String, private val basePlotsPath: String) {

    private val csvOps = CsvOps(baseDataPath)

    // Plot 1: India population by year
    private fun plot1(startYear: Int, endYear: Int) {
        // Do the analysis
        val plotData = Analysis.problem1(csvData = csvOps.populationData, startYear, endYear)
        // Dump out dat file
        File("$basePlotsPath/problem1.dat").printWriter().use { out ->
            plotData.forEach { out.println("${it.key}\t${it.value}") }
        }
    }

    // Plot 2: ASEAN population in selected year
    private fun plot2(selectedYear: Int) {
        // do the analysis
        val plotData = Analysis.problem2(csvOps.populationData, csvOps.aseanCountries, selectedYear)
        // Dump out dat file
        File("$basePlotsPath/problem2.dat").printWriter().use { out ->
            plotData.forEach { out.println("${it.key}\t${it.value}") }
        }
    }

    // Plot 3: Total SAARC population by year over a year range
    private fun plot3(startYear: Int, endYear: Int) {
        // do analysis
        val plotData = Analysis.problem3(csvOps.populationData, csvOps.saarcCountries, startYear, endYear)

        // Dump out dat file
        File("$basePlotsPath/problem3.dat").printWriter().use { out ->
            plotData.forEach { out.println("${it.key}\t${it.value}") }
        }
    }

    // Plot 4: ASEAN population by country by year
    private fun plot4(startYear: Int, endYear: Int) {
        val plotData = Analysis.problem4(csvOps.populationData, csvOps.aseanCountries, startYear, endYear)
        val years = startYear..endYear

        // Dump out dat file
        File("$basePlotsPath/problem4.dat").printWriter().use { out ->
            out.println(years.joinToString(separator = "\t"))

            csvOps.aseanCountries.forEach { country ->
                out.print("\"$country\"")
                years.forEach { year ->
                    out.print("\t${plotData[year]?.get(country)}")
                }
                out.println()
            }
        }
    }

    /**
     * Create oll the JSON data files for UN Population data set to be rendered in HighCharts.
     *
     * @param startYear The start year in plots with date ranges
     * @param endYear The end year in plots with date ranges
     * @param selectedYear For problems that based on a specific year
     */
    fun plotAll(startYear: Int, endYear: Int, selectedYear: Int) {

        println("Data Input Dir :: $baseDataPath")
        println("Data Output Dir :: $basePlotsPath")

        plot1(startYear, endYear)
        plot2(selectedYear)
        plot3(startYear, endYear)
        plot4(startYear, endYear)
    }
}