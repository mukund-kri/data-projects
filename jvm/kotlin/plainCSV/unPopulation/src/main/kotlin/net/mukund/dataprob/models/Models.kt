package net.mukund.dataprob.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


// Data classes for loading CSVs
data class PopulationData (
    val country: String,
    val population: Float,
    val year: Int,
)

// Models for JSON generation
@Serializable
class Problem1(
    val years: List<Int>,
    val population: List<Float>,
    @SerialName("start_year") val startYear: Int,
    @SerialName("end_year") val endYear: Int
)

@Serializable
class Problem2 (
    val countries: List<String>,
    val population: List<Float>,
    @SerialName("selected_year") val selectedYear: Int
)

@Serializable
class Problem3 (
    val years: List<Int>,
    val population: List<Float>,
    @SerialName("start_year") val startYear: Int,
    @SerialName("end_year") val endYear: Int
)

@Serializable
// Represent each series of bars in barchart
class Series(val name: String, val data: List<Float>)

@Serializable
class Problem4 (
    val categories: List<Int>,
    val series: List<Series>,
    @SerialName("start_year") val startYear: Int,
    @SerialName("end_year") val endYear: Int
)