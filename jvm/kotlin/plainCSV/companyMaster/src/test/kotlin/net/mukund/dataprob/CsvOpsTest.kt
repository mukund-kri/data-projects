package net.mukund.dataprob

import kotlin.test.Test
import kotlin.test.assertNull

class CsvOpsTest {

    @Test
    fun loadFromCsvTest() {

        // Simulate row with NA data
        val row = HashMap<String, String>()
        row["AUTHORIZED_CAP"] = "0"
        row["DATE_OF_REGISTRATION"] = "NA"
        row["PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN"] = "pba"

        val company = CsvOps.rowToCsv(row)
        assertNull(company)
    }
}