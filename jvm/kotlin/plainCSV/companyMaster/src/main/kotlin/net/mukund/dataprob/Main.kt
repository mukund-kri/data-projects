package net.mukund.dataprob

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import java.io.File


fun main() {
    println("Plotting Project ..................")
    val companies = CsvOps
        .loadFromCsv("../../../../raw_data/company-master/maharashtra_company_master.csv")

    Gnuplot.plotAll(companies, 2000, 2010, 2010)
}
