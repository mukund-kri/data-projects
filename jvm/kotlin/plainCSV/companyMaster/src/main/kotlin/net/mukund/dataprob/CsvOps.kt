package net.mukund.dataprob

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import java.io.File

data class Company(val authorizedCapital: Long, val registrationYear: Int, val pba: String)

object CsvOps {

    fun loadFromCsv(csvFileName: String): List<Company> {
        val file: File = File(csvFileName)
        return csvReader()
            .readAllWithHeader(file)
            .mapNotNull(::rowToCsv)

    }

    fun rowToCsv(row: Map<String, String>): Company? {

        // Required data
        val authCapString = row["AUTHORIZED_CAP"]
        val regDateString = row["DATE_OF_REGISTRATION"]
        val pba = row["PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN"]

        // Validations
        val regDate = try {
            regDateString?.takeLast(4)?.toInt() ?: 0
        } catch (e: Exception) {
            return null
        }


        val authCap = try {
            authCapString?.toDouble()?.toLong() ?: 0
        } catch (e: Exception) {
            return null
        }

        // Build the row
        return Company(authCap, regDate, pba ?: "")
    }
}