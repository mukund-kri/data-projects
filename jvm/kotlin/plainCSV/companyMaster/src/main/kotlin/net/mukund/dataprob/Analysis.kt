package net.mukund.dataprob

import java.io.File

object Analysis {

    private fun categorize(authCap: Long): String {
        return when {
            authCap < 100000 -> "< 1L"
            authCap < 1000000 -> "1L - 10L"
            authCap < 10000000 -> "10L - 1Cr"
            authCap < 100000000 -> "1Cr - 10Cr"
            else -> "> 10Cr"
        }
    }

    /**
     * Solve problem 1. Plot number of companies by authorized capital
     */
    fun problem1(companies: List<Company>): Map<String, Int> {
        return companies
            .map { categorize(it.authorizedCapital) }
            .groupBy { it }
            .map { it.key to it.value.size }
            .toMap()
    }

    /**
     * Solve problem 2: Plot companies by date of registration, over a range of years
     */
    fun problem2(companies: List<Company>, startYear: Int, endYear: Int): Map<Int, Int> {
        return companies
            .filter { it.registrationYear in startYear..endYear }
            .groupBy { it.registrationYear }
            .map { it.key to it.value.size }
            .toMap()

    }

    /**
     * Solve problem 3: Plot of number of pba's for selected year
     */
    fun problem3(companies: List<Company>, selectedYear: Int, selectTop: Int): Map<String, Int> {
        return companies
            .filter { it.registrationYear == selectedYear }
            .map { it.pba }
            .groupBy { it }
            .map { it.key to it.value.size }
            .sortedBy { it.second }
            .reversed()
            .take(selectTop)
            .toMap()
    }

    /**
     * Solve problem 4: count of pbas over list of years
     */
    fun problem4(companies: List<Company>, startYear: Int, endYear: Int): Map<Int, Map<String, Int>> {
        // First load pbas to render
        val pbas = listOf("Trading", "Business Services", "Real Estate and Renting", "Construction",)

        // Initialize all values to zero
        val counts = mutableMapOf<Int, MutableMap<String, Int>>()
        (startYear .. endYear).forEach {
            val pbaCounts = mutableMapOf<String, Int>()
            pbas.forEach { pbaCounts[it] = 0 }
            counts[it] = pbaCounts
        }

        companies
            .filter { it.registrationYear in startYear..endYear }
            .filter { it.pba in pbas }
            .forEach {
                // There has got to be a better way to do this
                val c = counts[it.registrationYear]?.get(it.pba) ?: 0
                counts[it.registrationYear]?.put(it.pba, c + 1)
            }

        return counts
    }
}