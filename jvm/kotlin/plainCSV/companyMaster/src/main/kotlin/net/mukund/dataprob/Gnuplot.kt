package net.mukund.dataprob

import java.io.File

object Gnuplot {

    private fun plot1(companies: List<Company>) {

        // Analyze the data
        val plotData = Analysis.problem1(companies)

        // List of all categories
        val categories = listOf("< 1L", "1L - 10L", "10L - 1Cr", "1Cr - 10Cr", "> 10Cr")

        File("plots/problem1.dat").printWriter().use { out ->
            categories.forEach { out.println("\"${it}\" ${plotData.get(it)}") }
        }
    }

    /**
     * Take in analyzed data for problem 2 and print it .dat file
     */
    private fun plot2(companies: List<Company>, startYear: Int, endYear: Int) {
        val plotData = Analysis.problem2(companies, startYear, endYear)

        File("plots/problem2.dat").printWriter().use { out ->
            (startYear..endYear).forEach { out.println("${it} ${plotData.get(it)}") }
        }
    }

    /**
     * Take in analyzed data for problem 3 and print it .dat file
     */
    private fun plot3(companies: List<Company>, selectedYear: Int, selectTop: Int) {
        val plotData = Analysis.problem3(companies, selectedYear, selectTop)
        File("plots/problem3.dat").printWriter().use { out ->
            plotData
                .forEach { out.println("\"${it.key.take(20)}\" ${it.value}") }
        }
    }

    /**
     * Take in analyzed data for problem 4 and print it .dat file
     */
    private fun plot4(companies: List<Company>, startYear: Int, endYear: Int) {
        val plotData = Analysis.problem4(companies, startYear, endYear)

        val pbas = listOf("Trading", "Business Services", "Real Estate and Renting", "Construction",)

        File("plots/problem4.dat").printWriter().use { out ->
            out.println(pbas.joinToString(separator = "\"\t\"", prefix = "\"", postfix = "\"" ))

            (startYear .. endYear).forEach {
                out.print("\"$it\"")
                out.println(plotData[it]?.values?.joinToString(separator = "\t", prefix = "\t" ))
            }
        }
    }

    /**
     * Plot all the problems in one shot
     */
    fun plotAll(companies: List<Company>, startYear: Int, endYear: Int, selectedYear: Int) {
        plot1(companies)
        plot2(companies, startYear, endYear)
        plot3(companies, selectedYear, 4)
        plot4(companies, startYear, endYear)
    }
}