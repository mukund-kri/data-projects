
val ktorVersion = "2.2.2"

plugins {
    // Kotlin support
    kotlin("jvm") version "1.6.0"

    // This is a Kotlin cli application
    application
}

repositories {
    jcenter()
}

dependencies {
    // Kotlin BOM
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))

    // JDK 8 standard library
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    // Ktor
    implementation("io.ktor:ktor-server-netty:$ktorVersion")
    implementation("io.ktor:ktor-client-core:$ktorVersion")
    implementation("io.ktor:ktor-server-content-negotiation-jvm:$ktorVersion")
    implementation("io.ktor:ktor-serialization-kotlinx-json-jvm:$ktorVersion")
}

application {
    mainClass.set("dataproblem.unpopulation.MainKt")
}
