package dataproblem.unpopulation

import io.ktor.server.netty.*
import io.ktor.server.engine.*
import io.ktor.server.application.*
import dataproblem.unpopulation.api.configurePlotsAPI


fun main() {
    embeddedServer(Netty, port = 8080, module = Application::module)
        .start(wait = true)
}

fun Application.module() {
    configurePlotsAPI()
}