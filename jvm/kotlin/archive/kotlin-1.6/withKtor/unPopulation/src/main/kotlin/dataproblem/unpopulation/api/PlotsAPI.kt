package dataproblem.unpopulation.api

import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.routing.*
import io.ktor.server.response.*

fun Application.configurePlotsAPI() {

    install(ContentNegotiation) {
        json()
    }

    routing {
        get("/problem1") {
            call.respond(mapOf("Hello" to "World"))
        }
    }
}