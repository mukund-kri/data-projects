package learn.dataproblem

import java.io.File
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader


class Problem4 {

    val pbas = listOf(
        "Real Estate and Renting", 
        "Manufacturing (Wood Products)",
        "Construction",
        "Manufacturing (Textiles)",
        // "Transport",
        // "storage and Communications",
    )

    

    fun makeYearMap(): MutableMap<String, Int> {
        val yearMap = mutableMapOf<String, Int>()
        (2000..2018).forEach { yearMap.put(it.toString(), 0) }
        return yearMap
    }


    fun problem4() {
        // parse csv and load data
        val csvFile: File = File("mca.csv")
        val data = csvReader().readAllWithHeader(csvFile)

        // initialize the solution map
        val counts = pbas
            .map { pba -> pba to makeYearMap() }


        /*        
        // count by principal business activity
        for (row in data) {
            val pba = row["PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN"]
            val year = row["DATE_OF_REGISTRATION"]?.takeLast(4)

            if (pba in pbas && year != "NA") {
                counts.find { it.first == pba }?.second?.put(year, counts.find { it.first == pba }?.second?.get(year)?.plus(1) ?: 0)
            }
        }
         */
             
        println(counts)
    }
}