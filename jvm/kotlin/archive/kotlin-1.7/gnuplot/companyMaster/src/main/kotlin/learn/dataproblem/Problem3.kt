package learn.dataproblem

import java.io.File
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader


class Problem3 {

    val TRESHOLD = 100


    fun problem3() {
        // parse csv and load data
        val csvFile: File = File("mca.csv")
        val data = csvReader().readAllWithHeader(csvFile)

        // count by principal business activity
        val counts = data
            .filter {it.get("DATE_OF_REGISTRATION")?.takeLast(4) == "2015" }
            .map { it["PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN"] }
            .groupBy { it }
            .map { it.key to it.value.size }
            .toMap()

        // Write out the output to file
        File("plots/problem3.dat").printWriter().use { out ->
            counts.filter { it.value > TRESHOLD }
                .forEach { out.println("\"${it.key?.take(20)}\" ${it.value}") }
        }
    }
}