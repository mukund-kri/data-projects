package learn.dataproblem

import java.io.File
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader


class Problem1 {

    fun categorize(authCap: Long): String {
        return when {
            authCap < 100000 -> "< 1L"
            authCap < 1000000 -> "1L - 10L"
            authCap < 10000000 -> "10L - 1Cr"
            authCap < 100000000 -> "1Cr - 10Cr"
            else -> "> 10Cr"
        }
    }


    // read csv and get intersting data
    fun problem1() {
        val csvFile: File = File("mca.csv")
        val data = csvReader().readAllWithHeader(csvFile)
        
        // print line by line
        val counts = data
            .map { it["AUTHORIZED_CAP"] }
            .map { it?.toDouble() ?: 0.0 }
            .map { it.toLong() }
            .map { categorize(it) }
            .groupBy { it }
            .map { it.key to it.value.size }
            .toMap()
        
        // List of all categories
        val categories = listOf("< 1L", "1L - 10L", "10L - 1Cr", "1Cr - 10Cr", "> 10Cr")

        File("plots/problem1.dat").printWriter().use { out ->
            categories.forEach { out.println("\"${it}\" ${counts.get(it)}") }
        }        
        
    }
}
