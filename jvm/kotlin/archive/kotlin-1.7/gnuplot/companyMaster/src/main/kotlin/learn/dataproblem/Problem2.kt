package learn.dataproblem

import java.io.File
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader


class Problem2 {

    fun problem2() {
        // parse csv and load data
        val csvFile: File = File("mca.csv")
        val data = csvReader().readAllWithHeader(csvFile)

        // count by date of registration
        val counts = data
            .map { it["DATE_OF_REGISTRATION"] }
            .map { it?.takeLast(4) }
            .filter { it != "NA" }
            .map { it?.toInt() }
            .filter { it?.let { it >= 2000 } ?: false }
            .groupBy { it }
            .map { it.key to it.value.size }
            .toMap()

        // Write out the output to file
        File("plots/problem2.dat").printWriter().use { out ->
            (2000..2018).forEach { out.println("${it} ${counts.get(it)}") }
        }
    }
}