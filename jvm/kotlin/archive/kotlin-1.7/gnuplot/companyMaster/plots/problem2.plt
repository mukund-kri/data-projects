# Set up title and labels
set title "Problem 2 :: Registration by year of incorporation"
set title font ",20"

# Set up xtics
set xtics ()
set xtics rotate by 0

# Plot
plot "problem2.dat" using 2: xtic(1) with histogram title "Registration by year of incorporation"
