package exercise.unpopulation

import java.io.File
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader


class Problem4 {


    val asean = listOf(
        "Brunei Darussalam",
        "Cambodia",
        "Indonesia",
        "Lao People's Democratic Republic",
        "Malaysia",
        "Myanmar",
        "Philippines",
        "Singapore",
        "Thailand",
        "Viet Nam")

    fun inYear(year: String?, startYear: Int, endYear: Int): Boolean {
        val yearInt = year?.toInt() ?: 0
        return ((yearInt >= startYear) && (yearInt <= endYear))
    }

    fun plot(startYear: Int = 2000, endYear: Int = 2015) {
        val csvFile: File = File("un-population.csv")
        val data = csvReader().readAllWithHeader(csvFile)

        // print line by line
        val filtered = data
            .filter { asean.contains(it["Region"]) }
            .filter  { inYear(it["Year"], startYear, endYear) }

        // Initialize counts map to zero
        val counts: MutableMap<String, MutableMap<String, Double>> = 
            asean.map { it to HashMap<String, Double>() }
            .toMap()
            .toMutableMap()    

        // Add up the population for each country
        filtered.forEach { 
            val country = it["Region"]!!
            val population = it["Population"]?.toDouble() ?: 0.0
            val year = it["Year"]!!
            counts[country]?.set(year, population)
        }
    
         
        File("plots/problem4.dat").printWriter().use { out ->
            (startYear..endYear).forEach { year -> out.print("$year\t") }
            out.println()
            counts.forEach { country ->
                out.print("\"${country.key}\"\t")
                (startYear..endYear).forEach { year ->
                    val population = country.value[year.toString()] ?: 0.0
                    out.print("$population\t")
                }
                out.println()
            }
        }
          
     }          
        
}
