package exercise.unpopulation

import java.io.File
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader


class Problem3 {

    val saarc = listOf(
        "Afghanistan",
        "Bangladesh",
        "Bhutan",
        "India",
        "Maldives",
        "Nepal",
        "Pakistan", 
        "Sri Lanka")

    fun plot() {
        val csvFile: File = File("un-population.csv")
        val data = csvReader().readAllWithHeader(csvFile)

        // print line by line
        val counts = data
            .filter { saarc.contains(it["Region"]) }
            .map { it["Year"] to (it["Population"]?.toDouble() ?: 0.0) }
            .groupBy { it.first }
            .map { it.key to (it.value.map { it.second }).sum() }
               
        File("plots/problem2.dat").printWriter().use { out ->
            counts.forEach { out.println("\"${it.first}\" ${it.second}") }
        }
        
    }
}