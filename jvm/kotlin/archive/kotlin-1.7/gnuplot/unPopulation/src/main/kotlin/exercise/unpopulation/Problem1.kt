package exercise.unpopulation


import java.io.File
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader

class Problem1 {

    fun plot() {
        val csvFile: File = File("un-population.csv")
        val data = csvReader().readAllWithHeader(csvFile)

        // print line by line
        val counts = data.filter { it["Region"] == "India" }
           .map { it["Year"] to it["Population"] }
           .toMap()
        
        File("plots/problem1.dat").printWriter().use { out ->
            counts.forEach { out.println("${it.key} ${it.value}") }
        }
    }
}