package exercise.unpopulation

import java.io.File
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader


class Problem2 {

    val asean = listOf(
        "Brunei Darussalam",
        "Cambodia",
        "Indonesia",
        "Lao People's Democratic Republic",
        "Malaysia",
        "Myanmar",
        "Philippines",
        "Singapore",
        "Thailand",
        "Viet Nam")

    fun plot() {
        val csvFile: File = File("un-population.csv")
        val data = csvReader().readAllWithHeader(csvFile)

        // print line by line
        val counts = data
            .filter { asean.contains(it["Region"]) }
            .filter { (it["Year"]?.toInt() ?: 0) == 2015 }
            .map { it["Region"] to it["Population"] }
            .toMap()
        
        File("plots/problem2.dat").printWriter().use { out ->
            counts.forEach { out.println("\"${it.key}\" ${it.value}") }
        }
    }
}