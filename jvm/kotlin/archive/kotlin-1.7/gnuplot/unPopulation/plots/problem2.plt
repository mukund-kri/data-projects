# Setup the title
set title "Plot of population of ASEAN countries in the year 2015" font ", 18"

# Setup the x-axis label
set xlabel "Country" font ", 14"
set xtics rotate by -45 scale 0

# Plot the data
plot "problem2.dat" using 2: xtic(1) with histogram title "ASEAN population"
