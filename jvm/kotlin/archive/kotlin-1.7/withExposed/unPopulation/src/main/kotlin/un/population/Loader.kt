/**
 All the method related to INITIAL loading of data from CSV files are here 
 */
package un.population 

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File

class Loader {

    /** create tables it they don't exist */
    fun initTable() {
        transaction {
            SchemaUtils.create(Population)
            SchemaUtils.create(CountryGrouping)
        }
    }

    /** populate the country grouping table */
    fun loadGroupings() {
        // Load SAARC countries into db
        transaction {
            Constants.saarc.forEach { countryName ->
                CountryGrouping.insert {
                    it[grouping] = "saarc"
                    it[country] = countryName
                }
            }
        }

        // Load asean countries into db
        transaction {
            Constants.asean.forEach { countryName ->
                CountryGrouping.insert {
                    it[grouping] = "asean"
                    it[country] = countryName
                }
            }
        }
    }

    /* Load data from csv */
    fun loadData() {
        val csvFile = File("un-population.csv")
        val data = csvReader().readAllWithHeader(csvFile)

        transaction {
            data.forEach { row ->
                Population.insert {
                    it[country] = row["Region"]!!
                    it[year] = row["Year"]?.toInt() ?: 0
                    it[population] = row["Population"]?.toFloat() ?: 0.0f
                }
            }
        }
    }
}

fun main() {
    val loader = Loader()

    loader.initTable()
    loader.loadGroupings()
    loader.loadData()
}