/**
 * All the tables for the un data project is here
 */
package un.population

import org.jetbrains.exposed.sql.*

object Population : Table() {
    val country = varchar("country", 500)
    val year = integer("year")
    val population = float("population")
}

object CountryGrouping : Table() {
    val grouping = varchar("grouping", 10)
    val country = varchar("country", 100)
}