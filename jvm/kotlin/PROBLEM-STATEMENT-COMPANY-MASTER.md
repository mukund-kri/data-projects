#  Company Master :: Maharashtra


## Aim
To convert raw open data into charts, that tell a story on the state of company
registration in Maharashtra.

## Preparation

### raw data 

| Name                               | source                                          |
|------------------------------------|-------------------------------------------------|
| Company master data of Maharashtra | https://data.gov.in/catalog/company-master-data |


## Instructions

1. Download all the data needed. Consult your mentor if you have any problems accessing the raw data.
1. Initialize python project with a separate virtualenv. All your code should be in Python.
1. Enable pylint for this project.
2. This project should have separate repo on Gitlab.com.
3. All projects should have README.md with instructions on how to run this project.

### What your program should do

From the CSV and other source files specified above, write python code to ...
1. Read in the data.
2. Write logic to slice / dice / accumulate / transform the data.
3. Using matplotlib plot the plots specified in the following section.

## Problems

### 1. Histogram of Authorized Cap

Plot a histogram on AUTHORIZE_CAP with the following intervals
  
  1. < 1L
  2. 1L to 10L
  3. 10L to 1Cr
  4. 1Cr to 10Cr
  5. 10Cr to 100Cr
  6. More than 100Cr
  
Note: You will have to adjust the intervals if you have a un-balanced plot.

### 2. Histogram of company registration by year

From the field DATE_OF_REGISTRATION parse out the registration year. Using
this data, plot a histogram of the number of company registration by year.

### 3. Top registrations by "Principal Business Activity" for the year 2015

In this exercise ... 

  1. only consider registrations for the year 2015
  2. classify and count registrations by PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN
  3. Pick on the top 10 of these values
  4. Plot
    
### 4. Stacked Bar Chart.

Plot a stacked bar chart by aggregating registrations count over ...
  1. Year of registration
  2. Principal Business Activity
  