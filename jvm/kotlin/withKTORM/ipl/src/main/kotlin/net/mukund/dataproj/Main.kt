package net.mukund.dataproj

import org.ktorm.database.Database

import net.mukund.dataproj.loading.Loader
import net.mukund.dataproj.plotting.*


const val BASE_DATA_PATH = "/home/mukund/workarea/teaching/data-projects/raw_data/ipl"
const val BASE_PLOTS_PATH = "../plots/ipl"

fun main(args: Array<String>) {

    // Connect to the database
    val database = Database.connect(
        "jdbc:postgresql://127.0.0.1:5432/ipl",
        user = "devuser",
        password = "devpass"
    )

    if (args.isEmpty())
        println("Argument not given")
    else
        when (args[0]) {
            // Load data from csv to db
            "load" -> Loader.loadAll(database, BASE_DATA_PATH)
            "gnuplot" ->  Gnuplot(database, BASE_PLOTS_PATH).allPlots()
            "highcharts" -> Highcharts(database, BASE_PLOTS_PATH).allPlots()
            else -> println("\nInvalid Argument")
        }
}
