package net.mukund.dataproj.models

import org.ktorm.schema.Table
import org.ktorm.schema.int
import org.ktorm.schema.varchar


object Deliveries : Table<Nothing>("deliveries") {
    val id = int("id").primaryKey()
    val battingTeam = varchar("batting_team")
    val totalRuns = int("total_runs")
    val batter = varchar("batter")
    val batsmanRuns = int("batsman_runs")
}

object Matches : Table<Nothing>("matches") {
    val id = int("id").primaryKey()
    val winner = varchar("winner")
    val season = varchar("season")
}

object Umpires : Table<Nothing>("umpires") {
    val id = int("id").primaryKey()
    val name = varchar("name")
    val country = varchar("country")
}