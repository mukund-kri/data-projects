package net.mukund.dataproj.analysis

import net.mukund.dataproj.models.Deliveries
import net.mukund.dataproj.models.Matches
import net.mukund.dataproj.models.Umpires
import org.ktorm.database.Database
import org.ktorm.dsl.*

typealias TeamSeasonRuns = Map<String, Map<String, Int>>

class Analysis(private val database: Database) {

    /**
     * Problem 1: Total runs scored by team in all of IPL
     *
     * @return returns a map of team name to runs scored
     */
    fun problem1(): Map<String, Int> {

        return database.from(Deliveries)
            .select(Deliveries.battingTeam, sum(Deliveries.totalRuns))
            .groupBy(Deliveries.battingTeam)
            .associate { it.getString(1)!! to it.getInt(2) }
    }

    /**
     * Problem 2: Top batsman for RCB
     *
     * @returns A map of top batsmen and the total runs they have scored.
     */
    fun problem2(): Map<String, Int> {

        val totalBatsmanRun = sum(Deliveries.batsmanRuns)
        return database.from(Deliveries)
            .select(Deliveries.batter, totalBatsmanRun)
            .groupBy(Deliveries.batter)
            .orderBy(totalBatsmanRun.desc())
            .limit(10)
            .associate { it.getString(1)!! to it.getInt(2) }
    }

    /**
     * Problem 3: Number of foreign umpires by country of origin
     *
     * @returns A map of umpires to the country of origin
     */
    fun problem3(): Map<String, Int> {

        val countryCount = count(Umpires.country)
        return database.from(Umpires)
            .select(Umpires.country, countryCount)
            .where(Umpires.country neq "IND")
            .groupBy(Umpires.country)
            .associate { it.getString(1)!! to it.getInt(2) }
    }

    /**
     * Problem 4: Matches won by ipl team by season
     *
     * @return map of map, for team by season
     */
    fun problem4(): Triple<TeamSeasonRuns, Set<String>, Set<String>> {
        val matchesCount = count(Matches.winner)
        val result = database.from(Matches)
            .select(Matches.winner, Matches.season, matchesCount)
            .groupBy(Matches.winner, Matches.season)

        val counts = mutableMapOf<String, MutableMap<String, Int>>()
        val teams = mutableSetOf<String>()
        val seasons = mutableSetOf<String>()

        result.forEach {
            val team = it.getString(1)!!
            val season = it.getString(2)!!
            val runs = it.getInt(3)

            if (counts[team] == null) counts[team] = mutableMapOf<String, Int>()
            counts[team]?.set(season, runs)

            teams.add(team)
            seasons.add(season)
        }
        return Triple(counts, teams, seasons)
    }
}