package net.mukund.dataproj.plotting

import net.mukund.dataproj.analysis.Analysis
import org.ktorm.database.Database
import java.io.File


class Gnuplot(private val database: Database, private val plotBasePath: String) {

    private val gnuplotsBasePath = "$plotBasePath/gnuplot"
    private val analysis = Analysis(database)

    // Plot 1: Total runs scored by teams in all of IPL
    private fun plot1() {
        val plotData = analysis.problem1()
        File("$gnuplotsBasePath/problem1.dat").printWriter().use { out ->
            plotData.forEach { out.println("\"${it.key}\"\t${it.value}") }
        }
    }

    // Plot 2: Top run scorer for RCB
    private fun plot2() {
        val plotData = analysis.problem2()
        File("$gnuplotsBasePath/problem2.dat").printWriter().use { out ->
            plotData.forEach { out.println("\"${it.key}\"\t${it.value}") }
        }
    }

    // Plot 3: Foreign umpire by country
    private fun plot3() {
        val plotData = analysis.problem3()
        File("$gnuplotsBasePath/problem3.dat").printWriter().use { out ->
            plotData.forEach { out.println("\"${it.key}\"\t${it.value}") }
        }
    }

    // Plot 4: Stacked chart of matches won by team by season
    private fun plot4() {
        val (data, teams, seasons) = analysis.problem4()
        File("$gnuplotsBasePath/problem4.dat").printWriter().use { out ->

            // Write the team names as header
            out.println(teams.joinToString(separator = "\t") { "\"$it\"" })
            seasons.forEach { season ->
                out.print("\"${season}\"")
                teams.forEach { team -> out.print("\t${data[team]?.get(season) ?: 0}") }
                out.println()
            }

        }
    }

    /**
     * Generate .dat files for all 4 plots of the IPL data set problem.
     *
     * @param database The ktorm database connection
     */
    fun allPlots() {
        plot1()
        plot2()
        plot3()
        plot4()
    }
}