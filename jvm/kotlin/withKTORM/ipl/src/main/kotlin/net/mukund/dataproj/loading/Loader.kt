package net.mukund.dataproj.loading

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import net.mukund.dataproj.models.Deliveries
import net.mukund.dataproj.models.Matches
import net.mukund.dataproj.models.Umpires
import org.ktorm.database.Database
import org.ktorm.dsl.batchInsert
import java.io.File


object  Loader {

    // load deliveries.csv to `deliveries` table
    private fun loadDeliveries(database: Database, baseDataPath: String) {
        // Read in the CSV
        val csvData = csvReader()
            .readAllWithHeader(File("$baseDataPath/corrected_deliveries.csv"))

        database.batchInsert(Deliveries) {
            csvData
                .forEach { delivery ->
                    item {
                        set(it.batter, delivery["batter"])
                        set(it.battingTeam, delivery["batting_team"])
                        set(it.batsmanRuns, delivery["batsman_runs"]?.toInt())
                        set(it.totalRuns, delivery["total_runs"]?.toInt())
                    }
                }
        }
    }

    // load matches.csv to `matches` table
    private fun loadMatches(database: Database, baseDataPath: String) {
        // Read in the CSV
        val csvData = csvReader()
            .readAllWithHeader(File("$baseDataPath/corrected_matches.csv"))
            .filter { it["winner"] != "NA" }    // Remove records with no winners

        // Batch insert all matches to database
        database.batchInsert(Matches) {
            csvData.forEach { match ->
                item {
                    set(it.winner, match["winner"])
                    set(it.season, match["season"])
                }
            }
        }
    }

    // load umpires.csv to `umpires` table
    private fun loadUmpires(database: Database, baseDataPath: String) {
        // Read in the csv
        val csvData = csvReader()
            .readAllWithHeader(File("$baseDataPath/ipl-umpire.csv"))

        // Batch insert into `umpires` table
        database.batchInsert(Umpires) {
            csvData.forEach { umpire ->
                item {
                    set(it.name, umpire["name"])
                    set(it.country, umpire["country"])
                }
            }
        }

    }

    /**
     * Load data from the 3 CSV files. deliveries.csv, matches.csv and umpires.txt.
     *
     * @param database KtORM connection to postgres database
     * @param baseDataPath the path where the csv files are located
     */
    fun loadAll(database: Database, baseDataPath: String) {

        loadDeliveries(database, baseDataPath)
        loadMatches(database, baseDataPath)
        loadUmpires(database, baseDataPath)
    }
}