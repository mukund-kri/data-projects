package net.mukund.dataproj.loader

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import net.mukund.dataproj.models.PopulationData
import net.mukund.dataproj.models.Groupings
import org.ktorm.database.Database
import org.ktorm.dsl.batchInsert
import org.ktorm.schema.year
import java.io.File



object Loader {


    fun load(csvFileName: String, database: Database) {

        // Read in csv
        val csvFile = File(csvFileName)
        val csvData = csvReader().readAllWithHeader(csvFile)

        database.batchInsert(PopulationData) {
            csvData.forEach { datum ->
                item {
                    set(it.country, datum["Region"])
                    set(it.year, datum["Year"]?.toInt())
                    set(it.population, datum["Population"]?.toDouble())
                }
            }
        }
    }

    private fun loadGrouping(baseDataPath: String, database: Database, groupName: String) {

        val groupingFileName = "$baseDataPath/$groupName-countries.txt"
        val countries = File(groupingFileName).readLines()

        database.batchInsert(Groupings) {
            countries.forEach { country ->
                item {
                    set(it.country, country)
                    set(it.grouping, groupName)
                }
            }
        }
    }

    fun loadGroupings(baseDataPath: String, database: Database, vararg groupings: String) {
        groupings.forEach { loadGrouping(baseDataPath, database, it) }
    }
}