package net.mukund.dataproj.analytics

import net.mukund.dataproj.models.Groupings
import net.mukund.dataproj.models.PopulationData
import org.ktorm.database.Database
import org.ktorm.dsl.*



object Analytics {

    lateinit var aseanCountries: List<String>

    fun initialize(database: Database) {
        aseanCountries = database
            .from(Groupings)
            .select(Groupings.country)
            .where(Groupings.grouping eq "asean")
            .mapNotNull { it.getString(1) }

    }
    fun problem1(database: Database, startYear: Int, endYear: Int): Map<Int, Double> {

        return database
            .from(PopulationData)
            .select(PopulationData.year, PopulationData.population)
            .where { PopulationData.year between startYear..endYear }
            .map { it.getInt(1) to it.getDouble(2)}
            .toMap()
    }

    fun problem2(database: Database, selectedYear: Int): Map<String, Double> {

        return database
            .from(PopulationData)
            .select(PopulationData.country, PopulationData.population)
            .where(PopulationData.year eq selectedYear)
            .where(PopulationData.country inList aseanCountries)
            .map { (it.getString(1) ?: "") to it.getDouble(2) }
            .toMap()
    }

    fun problem3(database: Database, startYear: Int, endYear: Int): Map<Int, Double> {

        val saarcCountries = database
            .from(Groupings)
            .select(Groupings.country)
            .where(Groupings.grouping eq "saarc")
            .mapNotNull { it.getString(1) }

        return database
            .from(PopulationData)
            .select(PopulationData.year, sum(PopulationData.population))
            .where(
                (PopulationData.country inList saarcCountries)
                        and (PopulationData.year between startYear..endYear))
            .groupBy(PopulationData.year)
            .map { it.getInt(1) to it.getDouble(2) }
            .toMap()
    }

    fun problem4(database: Database, startYear: Int, endYear: Int): Map<Int, Map<String, Double>> {

        val counts = mutableMapOf<Int, MutableMap<String, Double>>()

        val result = database
            .from(PopulationData)
            .select(PopulationData.year, PopulationData.country, sum(PopulationData.population))
            .where((PopulationData.year between startYear .. endYear)
                    and (PopulationData.country inList aseanCountries))
            .groupBy(PopulationData.year, PopulationData.country)
            .forEach {
                val year = it.getInt(1)
                val country = it.getString(2).toString()
                val population = it.getDouble((3))

                if (counts[year] == null) counts[year] = mutableMapOf(country to population)
                else counts[year]?.put(country, population)
            }

        return counts
    }
}