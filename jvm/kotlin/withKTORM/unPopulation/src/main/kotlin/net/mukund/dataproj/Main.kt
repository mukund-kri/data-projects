package net.mukund.dataproj

import net.mukund.dataproj.analytics.Analytics
import net.mukund.dataproj.loader.Loader
import net.mukund.dataproj.gnuplot.Gnuplot
import net.mukund.dataproj.highcharts.HighCharts
import org.ktorm.database.Database

const val BASE_DATA_PATH = "../../../../raw_data/un-population"

fun main(args: Array<String>) {

    // connect to database and insert a company
    val database = Database.connect(
        "jdbc:postgresql://127.0.0.1:5432/un_population",
        user = "devuser",
        password = "devpass")

    Analytics.initialize(database)

    if (args.isEmpty())
        println("Argument not given")
    else
        when (args[0]) {
            // Load data from csv to db
            "load" -> Loader.load("$BASE_DATA_PATH/population-estimates_csv.csv", database)
            "load-groupings" -> Loader.loadGroupings(BASE_DATA_PATH, database,"asean", "saarc")
            "gnuplot" -> Gnuplot.allPlots(database, 2000, 2010, 2010)
            "highcharts" -> HighCharts.allPlots(database, 2000, 2010, 2010)
            else -> println("\nInvalid Argument")
        }


}

