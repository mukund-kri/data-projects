package net.mukund.dataproj.models

import org.ktorm.schema.Table
import org.ktorm.schema.int
import org.ktorm.schema.varchar

object Groupings : Table<Nothing>("grouping") {
    val id = int("id").primaryKey()
    val country = varchar("country")
    val grouping = varchar("grouping")
}