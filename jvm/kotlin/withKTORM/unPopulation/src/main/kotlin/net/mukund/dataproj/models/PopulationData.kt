package net.mukund.dataproj.models

import org.ktorm.schema.Table
import org.ktorm.schema.double
import org.ktorm.schema.int
import org.ktorm.schema.varchar

object PopulationData : Table<Nothing>("population_data") {
    val id = int("id").primaryKey()
    val country = varchar("country")
    val year = int("year")
    val population = double("population")
}