package net.mukund.dataproj.highcharts

import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import net.mukund.dataproj.analytics.Analytics
import org.ktorm.database.Database
import java.io.File


@Serializable
class Problem1(
    val years: List<Int>,
    val population: List<Double>,
    val start_year: Int,
    val end_year: Int
)

@Serializable
class Problem2(val countries: List<String>, val population: List<Double>, val selected_year: Int)

@Serializable
class Series(val name: String, val data: List<Double>)

@Serializable
class Problem4(val categories: List<Int>, val series: List<Series>)

object HighCharts {

    private const val BASE_DIR = "../plots/unpopulation/highcharts"

    private fun plot1(database: Database, startYear: Int, endYear: Int) {
        val plotData = Analytics.problem1(database, startYear, endYear)

        val p1 = Problem1(
            plotData.keys.toList(),
            plotData.values.toList(),
            start_year = startYear,
            end_year = endYear
        )

        File("$BASE_DIR/problem1.json").printWriter().use { out ->
            out.println(Json.encodeToString(p1))
        }
    }

    private fun plot2(database: Database, selectedYear: Int) {
        val plotData = Analytics.problem2(database, selectedYear)

        val p1 = Problem2(
            plotData.keys.toList(),
            plotData.values.toList(),
            selected_year = selectedYear
        )

        File("$BASE_DIR/problem2.json").printWriter().use { out ->
            out.println(Json.encodeToString(p1))
        }
    }

    private fun plot3(database: Database, startYear: Int, endYear: Int) {
        val plotData = Analytics.problem3(database, startYear, endYear)

        val p1 = Problem1(
            plotData.keys.toList(),
            plotData.values.toList(),
            start_year = startYear,
            end_year = endYear
        )

        File("$BASE_DIR/problem3.json").printWriter().use { out ->
            out.println(Json.encodeToString(p1))
        }
    }

    /**
     * Generate highcharts json file for problem 4
     */
    fun plot4(database: Database, startYear: Int, endYear: Int) {
        val plotData = Analytics.problem4(database, startYear, endYear)

        val years = (startYear..endYear).toList()
        val series = mutableListOf<Series>()

        Analytics.aseanCountries.forEach { coutry ->
            val pbaData = mutableListOf<Double>()
            years.forEach { year ->
                pbaData.add(plotData[year]?.get(coutry) ?: 0.0)
            }

            series.add(Series(coutry, pbaData))
        }

        val p4 = Problem4(years, series)
        File("$BASE_DIR/problem4.json").printWriter().use { out ->
            out.println(Json.encodeToString(p4))
        }
    }

    fun allPlots(database: Database, startYear: Int, endYear: Int, selectedYear: Int) {
        plot1(database, startYear, endYear)
        plot2(database, selectedYear)
        plot3(database, startYear, endYear)
        plot4(database, startYear, endYear)
    }


}