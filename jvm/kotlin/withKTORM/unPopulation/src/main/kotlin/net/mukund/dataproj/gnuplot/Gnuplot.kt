package net.mukund.dataproj.gnuplot

import net.mukund.dataproj.analytics.Analytics
import org.ktorm.database.Database
import java.io.File

object Gnuplot {

    private const val PLOTS_BASE_PATH = "../plots/unpopulation/gnuplot"

    // Population growth in India over a range of years
    private fun plot1(database: Database, startYear: Int, endYear: Int) {
        val plotData = Analytics.problem1(database, startYear, endYear)
        File("$PLOTS_BASE_PATH/problem1.dat").printWriter().use { out ->
            plotData.forEach { out.println("${it.key}\t${it.value}") }
        }
    }

    // Population distribution of asean countries in the selected year
    private fun plot2(database: Database, selectedYear: Int) {
        val plotData = Analytics.problem2(database, selectedYear)
        File("$PLOTS_BASE_PATH/problem2.dat").printWriter().use { out ->
            plotData.forEach { out.println("\"${it.key}\"\t${it.value}")}
        }
    }

    private fun plot3(database: Database, startYear: Int, endYear: Int) {
        val plotData = Analytics.problem3(database, startYear, endYear)
        File("$PLOTS_BASE_PATH/problem3.dat").printWriter().use { out ->
            plotData.forEach { out.println("\"${it.key}\"\t${it.value}")}
        }
    }

    private fun plot4(database: Database, startYear: Int, endYear: Int) {
        val plotData = Analytics.problem4(database, startYear, endYear)

        val years = (startYear..endYear).toList()
        File("$PLOTS_BASE_PATH/problem4.dat").printWriter().use { out ->
            out.println(years.joinToString(separator = "\t"))

            Analytics.aseanCountries.forEach { country ->
                out.print("\"$country\"")
                years.forEach { year ->
                    out.print("\t${plotData[year]?.get(country)}")
                }
                out.println()
            }

        }
    }
    fun allPlots(database: Database, startYear: Int, endYear: Int, selectedYear: Int) {

        // plot1(database, startYear, endYear)
        // plot2(database, selectedYear)
        // plot3(database, startYear, endYear)
        plot4(database, startYear, endYear)
    }
}