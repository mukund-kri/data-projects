# Company Master analysis with ktORM

## Problem Statement

The problem definition is [Here](../../PROBLEM-STATEMENT-COMPANY-MASTER.md)

## What is ktORM?

Ktorm is a lightweight and efficient Object Relational Mapper (ORM) framework for
Kotlin. It focuses on providing a strong-typed and flexible SQL DSL (Domain Specific 
Language) to reduce boilerplate code and improve database interaction. Ktorm allows 
developers to write SQL queries in a Kotlin-friendly way, leveraging the language's 
features for type safety and code completion.

## How to run the project?

To generate gnuplot data, run the following command:

```shell
gradle run --args=gnuplot
```

To generate highcharts josn data, run the following command:

```shell
gradle run --args=highcharts
```


## How to plot with gnuplot?

TODO::Add link to plots repo

## How to plot with highcharts?

TODO::Add link to plots repo