package net.mukund.dataproj.loader

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import net.mukund.dataproj.models.Companies
import org.ktorm.database.Database
import org.ktorm.dsl.batchInsert
import java.io.File

object Loader {

    fun load(csvFileName: String, database: Database) {
        // Read in CSV
        val csvFile = File(csvFileName)
        val csvData = csvReader().readAllWithHeader(csvFile)

        database.batchInsert(Companies) {
            csvData
                .map { Triple(it["AUTHORIZED_CAP"], it["DATE_OF_REGISTRATION"], it["PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN"]) }
                .mapNotNull {
                    val (authCap, dateOfRegistration, pba) = it
                    val authCapInt = authCap?.toFloat()?.toLong()
                    if (authCapInt != null)
                        Triple(authCapInt, dateOfRegistration, pba)
                    else
                        null
                }
                .mapNotNull {
                    val (authCap, dateOfRegistration, pba) = it
                    when (val registrationYear = dateOfRegistration?.takeLast(4)) {
                        "NA" -> null
                        null -> null
                        else -> Triple(authCap, registrationYear.toInt(), pba)
                    }
                }
                .forEach { company ->
                    item {
                        set(it.authorizedCapital, company.first)
                        set(it.registrationYear, company.second)
                        set(it.pba, company.third)
                    }
                }
        }
    }
}