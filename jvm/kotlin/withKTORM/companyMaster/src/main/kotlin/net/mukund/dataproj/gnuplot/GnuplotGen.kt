package net.mukund.dataproj.gnuplot

import net.mukund.dataproj.analysis.Analyzer
import org.ktorm.database.Database
import java.io.File


object GnuplotGen {

    /**
     * Generate gnuplot data file from database for problem 1.
     */
    fun plot1(database: Database) {
        val plotData = Analyzer.problem1(database)

        // List of all categories
        val categories = listOf("< 1L", "1L - 10L", "10L - 1Cr", "1Cr - 10Cr", "> 10Cr")

        File("../plots/company-master/gnuplot/problem1.dat").printWriter().use { out ->
            categories.forEach { out.println("\"${it}\" ${plotData.get(it)}") }
        }
    }

    /**
     * Generate gnuplot data file from database data for problem 2.
     */
    fun plot2(database: Database, startYear: Int, endYear: Int) {
        val plotData = Analyzer.problem2(database, startYear, endYear)
        File("../plots/company-master/gnuplot/problem2.dat").printWriter().use { out ->
            (startYear..endYear).forEach { out.println("${it} ${plotData.get(it)}") }
        }
    }

    /**
     * Generate gnuplot data file from database data for problem 3.
     */
    fun plot3(database: Database, selectedYear: Int, selectedTop: Int) {

        val plotData = Analyzer.problem3(database, selectedYear, selectedTop)
        File("../plots/company-master/gnuplot/problem3.dat").printWriter().use { out ->
            plotData
                .forEach { out.println("\"${it.key.take(20)}\" ${it.value}") }
        }
    }

    /**
     * Generate gnuplot data file from database data for problem 4.
     */
    fun plot4(database: Database, startYear: Int, endYear: Int) {

        val pbas = listOf("Trading", "Business Services", "Real Estate and Renting", "Construction",)
        val plotData = Analyzer.problem4(database, startYear, endYear, pbas)

        File("../plots/company-master/gnuplot/problem4.dat").printWriter().use { out ->
            out.println(pbas.joinToString(separator = "\"\t\"", prefix = "\"", postfix = "\"" ))

            (startYear .. endYear).forEach {
                out.print("\"$it\"")
                val yearData = plotData[it]
                pbas.forEach { pba ->
                    val pbaCount = yearData?.get(pba)
                    out.print("\t$pbaCount")
                }
                out.println()
            }
        }
    }

    /**
     * Generate all plots at once
     */
    fun allPlots(database: Database, startYear: Int, endYear: Int, selectedYear: Int, selectedTop: Int = 5) {
        plot1(database)
        plot2(database, startYear, endYear)
        plot3(database, selectedYear, selectedTop)
        plot4(database, startYear, endYear)
    }
}