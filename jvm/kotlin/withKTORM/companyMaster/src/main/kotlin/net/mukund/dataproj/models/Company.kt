package net.mukund.dataproj.models

import org.ktorm.schema.Table
import org.ktorm.schema.int
import org.ktorm.schema.long
import org.ktorm.schema.varchar

object Companies : Table<Nothing>("companies") {
    val id = int("id").primaryKey()
    val authorizedCapital = long("authorized_capital")
    val registrationYear = int("registration_year")
    val pba = varchar("pba")
}