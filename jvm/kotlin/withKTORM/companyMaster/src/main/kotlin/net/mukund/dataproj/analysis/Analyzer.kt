package net.mukund.dataproj.analysis

import net.mukund.dataproj.models.Companies
import org.ktorm.database.Database
import org.ktorm.database.asIterable
import org.ktorm.dsl.*

object Analyzer {

    /**
     * Compute Histogram of registered companies by Authorized Capital
     */
    fun problem1(database: Database): Map<String, Int> {
        val sql = """SELECT
            CASE
                WHEN authorized_capital <= 100000 THEN '< 1L'
                WHEN authorized_capital > 100000 AND authorized_capital <= 1000000 THEN '1L - 10L'
                WHEN authorized_capital > 1000000 AND authorized_capital <= 10000000 THEN '10L - 1Cr'
                WHEN authorized_capital > 10000000 AND authorized_capital <= 100000000 THEN '1Cr - 10Cr'
                ELSE '> 10Cr'
            END
            AS authorized_capital_range,
            COUNT(authorized_capital) AS counts
        FROM companies
        GROUP BY authorized_capital_range
        """.trimIndent()

        val result = database.useConnection { conn ->

            conn.prepareStatement(sql).use { statement ->
                statement.executeQuery().asIterable().map { it.getString(1) to it.getInt(2) }
            }
        }

        return  result.toMap()
    }

    /**
     * Compute counts of companies by Registration Year, between year range
     */
    fun problem2(database: Database, startYear: Int, endYear: Int): Map<Int, Int> {

        // get all companies between startYear And endYear
        return database
            .from(Companies)
            .select(Companies.registrationYear, count(Companies.registrationYear))
            .where { (Companies.registrationYear greaterEq startYear) and (Companies.registrationYear lessEq endYear)}
            .groupBy(Companies.registrationYear)
            .map { it.getInt(1) to it.getInt(2) }
            .toMap()
    }

    /**
     * Compute counts of pba
     */
    fun problem3(database: Database, selectedYear: Int, selectedTop: Int): Map<String, Int> {
        val counts = count(Companies.pba).aliased("counts")

        return  database.from(Companies)
            .select(Companies.pba, counts)
            .where { Companies.registrationYear eq selectedYear }
            .groupBy(Companies.pba)
            .orderBy(counts.desc())
            .map { (it.getString(1) ?: "") to it.getInt(2) }
            .take(selectedTop)
            .toMap()
    }

    fun problem4(database: Database, startYear: Int, endYear: Int, pbas: List<String>): Map<Int, Map<String, Int>> {

        val counts = mutableMapOf<Int, MutableMap<String, Int>>()

        database.from(Companies)
            .select(Companies.pba, Companies.registrationYear, count())
            .where { (Companies.registrationYear greaterEq startYear) and (Companies.registrationYear lessEq endYear) }
            .groupBy(Companies.pba, Companies.registrationYear)
            .forEach {
                val pba = it.getString(1)
                val year = it.getInt(2)
                val count = it.getInt(3)
                if (counts[year] == null) {
                    counts[year] = mutableMapOf<String, Int>()
                }
                counts[year]?.put(pba ?: "", count)
            }

        return counts
    }
}