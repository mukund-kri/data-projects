package net.mukund.dataproj

import net.mukund.dataproj.gnuplot.GnuplotGen
import net.mukund.dataproj.highcharts.Highcharts
import net.mukund.dataproj.loader.Loader
import org.ktorm.database.Database


fun main(args: Array<String>) {
 
    // connect to database
    val database = Database.connect(
        "jdbc:postgresql://127.0.0.1:5432/company_master",
        user = "devuser",
        password = "devpass")


    if (args.isEmpty())
        println("Argument not given")
    else
        when (args[0]) {
            // Load data from csv to db
            "load" -> Loader.load("../data/mca.csv", database)
            "gnuplot" -> GnuplotGen.allPlots(database, 2000, 2010, 2010)
            "highcharts" -> Highcharts.allPlots(database, 2000, 2010, 2010)
            else -> println("\nInvalid Argument")
        }


}
