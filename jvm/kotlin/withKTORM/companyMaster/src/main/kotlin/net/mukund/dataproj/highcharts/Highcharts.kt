package net.mukund.dataproj.highcharts

import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import net.mukund.dataproj.analysis.Analyzer
import org.ktorm.database.Database
import java.io.File

@Serializable
class Problem1(val categories: List<String>, val data: List<Int>)

@Serializable
class Problem2(val categories: List<Int>, val data: List<Int>)

@Serializable
class Series(val name: String, val data: List<Int>)

@Serializable
class Problem4(val categories: List<Int>, val series: List<Series>)


object Highcharts {

    private const val BASE_DIR = "../plots/company-master/highcharts"
    /**
     * Generate highcharts json file for problem 1
     */
    fun plot1(database: Database) {
        val plotData = Analyzer.problem1(database)

        val p1 = Problem1(
            plotData.keys.toList(),
            plotData.values.toList(),
        )

        File("$BASE_DIR/problem1.json").printWriter().use { out ->
            out.println(Json.encodeToString(p1))
        }

    }

    /**
     * Generate highcharts json file for problem 2
     */
    fun plot2(database: Database, startYear: Int, endYear: Int)  {
        val plotData = Analyzer.problem2(database, startYear, endYear)
        val p2 = Problem2(
            plotData.keys.toList(),
            plotData.values.toList()
        )
        File("$BASE_DIR/problem2.json").printWriter().use { out ->
            out.println(Json.encodeToString(p2))
        }
    }

    /**
     * Generate highcharts json file for problem 3
     */
    fun plot3(database: Database, selectedYear: Int, selectedTop: Int) {
        val plotData = Analyzer.problem3(database, selectedYear, selectedTop)
        val p3 = Problem1(
            plotData.keys.toList(),
            plotData.values.toList(),
        )
        File("$BASE_DIR/problem3.json").printWriter().use { out ->
            out.println(Json.encodeToString(p3))
        }
    }

    /**
     * Generate highcharts json file for problem 4
     */
    fun plot4(database: Database, startYear: Int, endYear: Int) {
        val pbas = listOf("Trading", "Business Services", "Real Estate and Renting", "Construction",)
        val plotData = Analyzer.problem4(database, startYear, endYear, pbas)

        val years = (startYear..endYear).toList()
        val series = mutableListOf<Series>()

        pbas.forEach { pba ->
            val pbaData = mutableListOf<Int>()
            years.forEach { year ->
                pbaData.add(plotData[year]?.get(pba) ?: 0)
            }

            series.add(Series(pba, pbaData))
        }

        val p4 = Problem4(years, series)
        File("$BASE_DIR/problem4.json").printWriter().use { out ->
            out.println(Json.encodeToString(p4))
        }
    }

    /**
     * Generate all highcharts plots at one go
     */
    fun allPlots(database: Database, startYear: Int, endYear: Int, selectedYear: Int, selectedTop: Int = 4) {
        plot1(database)
        plot2(database, startYear, endYear)
        plot3(database, selectedYear, selectedTop)
        plot4(database, startYear, endYear)
    }
}