(ns ref-impl-clojure.gnuplot.core
  (:require [clojure.java.io :as io]
            [clojure.data.csv :as csv])
  (:gen-class))


(def DATA-FILE-NAME "test-large.csv")
(def bin-order ["<1L" "1L to 10L" "10L to 1Cr" "1Cr to 10Cr" "10Cr and above"])


;;;; PROBLEM 1 ----------------------------------------------------------------
(defn classify-by-auth-cap [auth-cap]
  (cond
    (< auth-cap 100000) "<1L"
    (< auth-cap 1000000) "1L to 10L"
    (< auth-cap 10000000) "10L to 1Cr"
    (< auth-cap 100000000) "1Cr to 10Cr"
    :else "10Cr and above"))

(defn add-to-bin [acc name]
  (let [curr-value (get acc name 0)
        value (+ curr-value 1)]
    (assoc acc name value)))


(defn dump-dat-file1 [result-map]
  "Write out dat file for problem 1"
  (with-open [writer (io/writer "plots/problem1.dat")]
    (doseq [bin bin-order]
      (.write writer (str "\"" bin "\" " (get result-map bin 0) "\n")))))

(defn do-problem-1 
  "Do the problem 1 calcuations"
  [data]
  (->> data
       (drop 1)
       (mapv #(nth % 8))
       (mapv #(read-string %))
       (mapv classify-by-auth-cap)
       (reduce add-to-bin {})
       (dump-dat-file1)))

;;;; PROBLEM 2 ----------------------------------------------------------------
(defn dump-dat-file2 [result-map]
  "Write out dat file for problem 2"
  (with-open [writer (io/writer "plots/problem2.dat")]
    (doseq [keyval (seq result-map)]
      (.write writer (format "%d %d\n" (first keyval) (last keyval))))))

(defn do-problem-2
  "Do the calcuations for problem 2"
  [data]
  (->> data
       (drop 1)
       (map #(nth % 6))
       (filter #(not= % "NA"))
       (map #(subs % 6))
       (map read-string)
       (filter #(>= % 2000))
       (frequencies)
       (into (sorted-map))
       (dump-dat-file2)))


;;;; PROBLEM 3 ---------------------------------------------------------------
(defn is-2015-data 
  "Check if company is registered in 2015"
  [row]
  (let [registration-date (nth row 6)]
    (if (not= registration-date "NA")
      (let [registration-year (subs registration-date 6)]
        (= registration-year "2015"))
      false)))

(defn dump-dat-file3 [result-map]
  "Write out dat file for problem 3"
  (with-open [writer (io/writer "plots/problem3.dat")]
    (doseq [keyval (seq result-map)]
      (.write writer (format "\"%s\" %d\n" (first keyval) (last keyval))))))
      

(defn do-problem-3
  "Do calculations for problem 3"
  [data]
  (->> data
       (drop 1)
       (filter is-2015-data)
       (map #(nth % 11))
       (frequencies)
       (dump-dat-file3)))

;;;; PROBLEM 4 ----------------------------------------------------------------
(def valid-pbas #{"Real Estate and Renting"
                  "Manufacturing"
                  "Construction"
                  "Manufacturing (Textiles)"})

(def valid-years (apply sorted-set (map str (range 2000 2019))))

(defn add-row [acc row]
  (let [year-name (get row 0)
        year-map (get acc year-name (sorted-map))
        pba-name (get row 1)
        pba-count (get year-map pba-name 0)]
    (assoc acc year-name (assoc year-map pba-name (inc pba-count)))))
 

(defn dump-dat-file4 [data]
  "Dump the map of maps into dat file as a table"
  (with-open [writer (io/writer "plots/problem4.dat")]
    (.write writer "Year")
    (doseq [pba valid-pbas]
      (.write writer (format "\t\"%s\"" pba)))
    (doseq [[k v] data]
      (.write writer (format "\n%s" k))
      (doseq [pba valid-pbas]
        (.write writer (format "\t%d" (get v pba 0)))))))


(defn do-problem-4
  "Problem 4"
  [data]
  (->> data
       (drop 1)
       (map #(vector (nth % 6) (nth % 11)))
       (filter #(not= (first %) "NA"))
       (filter #(contains? valid-pbas (get % 1)))
       (map #(vector (subs (get % 0) 6) (get % 1)))
       (filter #(contains? valid-years (get % 0)))
       ; (frequencies)
       (reduce add-row (sorted-map))
       (dump-dat-file4)))

(defn gen-all []
  "Generate dat files for all problems"

  (with-open [reader (io/reader DATA-FILE-NAME)]
    (let [csv-data (csv/read-csv reader)]
      ; (do-problem-1 csv-data)
      (do-problem-4 csv-data)
      )))
