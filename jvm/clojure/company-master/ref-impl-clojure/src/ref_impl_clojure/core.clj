(ns ref-impl-clojure.core
  (:require [ref-impl-clojure.gnuplot.core :as gnuplot]
                                        ; [ref-impl-clojure.highcharts.core :as highcharts]
            )
  (:gen-class))

(defn -main
  "Entry point to all data analysis code"
  [& args]
  (gnuplot/gen-all))
