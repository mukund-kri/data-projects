(ns ref-impl-clojure.try1
  (:require [clojure.java.io :as io]
            [clojure.data.csv :as csv]
            [clojure.data.json :as json])
  (:gen-class))


(defn get-auth-cap-data [filename]
  (with-open [reader (io/reader filename)]
    (->> (csv/read-csv reader)
         (mapv #(nth % 8))
         (drop 1))))


(defn classify-by-auth-cap [auth-cap-str]
  (let [auth-cap (read-string auth-cap-str)]
    (cond
      (< auth-cap 100000) "<1L"
      (< auth-cap 1000000) "1L to 10L"
      (< auth-cap 10000000) "10L to 1Cr"
      (< auth-cap 100000000) "1Cr to 10Cr"
      :else "10Cr and above")))

(defn add-to-bin [acc name]
  (let [curr-value (get acc name 0)
        value (+ curr-value 1)]
    (assoc acc name value)))

(defn accuml-auth-cap [acc auth-cap]
  (try
    (add-to-bin acc (classify-by-auth-cap auth-cap))
    (catch Exception ex
      (prn "Error: "))))

(defn problem1 []
  (with-open [writer (io/writer "plot1data.json")]
    (-> (reduce
          accuml-auth-cap
          {}
          (get-auth-cap-data "test-large.csv"))
         (json/write writer))))

