CREATE TABLE population_data (
    id SERIAL PRIMARY KEY,
    country VARCHAR(256) NOT NULL,
    year INT NOT NULL,
    population FLOAT NOT NULL
)