package in.net.mukund

import upickle.default.*
import in.net.mukund.Analysis
import java.io.FileWriter
import java.io.File

case class Problem1(years: Seq[Int], population: Seq[Float]) derives ReadWriter
case class Problem2(countries: Seq[String], population: Seq[Float])
    derives ReadWriter
case class Problem3(years: Seq[Int], population: Seq[Float]) derives ReadWriter

case class Series(name: String, data: Seq[Float]) derives ReadWriter
case class Problem4(categories: Seq[Int], series: Seq[Series])
    derives ReadWriter

class Highcharts(
    val plotRoot: String,
    startYear: Int,
    endYear: Int,
    selectedYear: Int
):

  val analysis = Analysis()

  private def plot1: Unit =
    val (years, population) = analysis.problem1(startYear, endYear).unzip
    val p1 = Problem1(years, population)
    Utils.writeJsonFile(p1, s"$plotRoot/problem1.json")

  private def plot2: Unit =
    val (country, population) = analysis.problem2(selectedYear).unzip
    val p2 = Problem2(country, population)
    Utils.writeJsonFile(p2, s"$plotRoot/problem2.json")

  private def plot3: Unit =
    val (years, population) = analysis.problem3(startYear, endYear).unzip
    val p3 = Problem3(years, population)
    Utils.writeJsonFile(p3, s"$plotRoot/problem3.json")

  private def plot4: Unit =
    val plotData = analysis.problem4
    val years = startYear to endYear

    // First construct the series
    val series = loader.asean.map { country =>
      val data = years.map { year =>
        plotData.getOrElse(country, Map.empty).getOrElse(year, 0.0f)
      }
      Series(country, data)
    }

    val p4 = Problem4(years, series)
    val fileWriter = new FileWriter(File(s"$plotRoot/problem4.json"))
    fileWriter.write(write(p4))
    fileWriter.close()

  def plotAll: Unit =
    plot1
    plot2
    plot3
    plot4

    println("\nData for hichcharts generated successfully\n")
