package in.net.mukund

import com.github.tototoshi.csv.CSVReader
import java.io.File

given loader: Loader = Loader(
  "/home/mukund/workarea/teaching/data-projects/raw_data/un-population"
)

case class Country (
  country: String,
  population: Float,
  year: Int,
)

class Loader(val dataPath: String):

  lazy val countries: List[Country] = loadCountries()
  lazy val saarc: List[String] = loadGroup("saarc")
  lazy val asean: List[String] = loadGroup("asean")

  // Load all the deliveries from the corrected_deliveries.csv file into a list for further
  // processing
  private def loadCountries(): List[Country] =
    val deliveriesPath = s"$dataPath/population-estimates_csv.csv"

    val reader = CSVReader.open(new File(deliveriesPath))

    val deliveries = reader.allWithHeaders().map { row =>
      Country(
        row("Region"),
        row("Population").toFloat,
        row("Year").toInt,
      )
    }
    reader.close()
    deliveries

  // Load all the matches from the corrected_matches.csv file into a list for further processing
  private def loadGroup(group: String): List[String] =
    val groupPath = s"$dataPath/$group-countries.txt"
    val source = scala.io.Source.fromFile(groupPath)

    source.getLines().toList