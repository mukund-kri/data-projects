package in.net.mukund

type Counts = Map[String, Map[Int, Float]]

class Analysis(using loader: Loader):

  /** Problem 1: Plot the growth of population in India in given range of years
    *
    * @param startYear
    *   The start year of the range
    * @param endYear
    *   The end year of the range
    *
    * @return
    *   A list of tuples with the year and the population in that year
    */
  def problem1(startYear: Int, endYear: Int): List[(Int, Float)] =
    loader.countries
      .filter(_.country == "India")
      .filter(data => data.year >= startYear && data.year <= endYear)
      .map(data => (data.year, data.population))

  /** Problem 2: Population distribution in ASEAN countries for the selected
    * year
    *
    * @param selectedYear
    *   The year for which the population distribution is to be calculated
    *
    * @return
    *   A list of tuples with the country and the population in that year
    */
  def problem2(selectedYear: Int): List[(String, Float)] =
    loader.countries
      .filter(data => loader.asean.contains(data.country))
      .filter(_.year == selectedYear)
      .map(data => (data.country, data.population))

  /** Problem 3: Total population of SAARC countries over the selected range of
    * years
    *
    * @param startYear
    *   The start year of the range
    * @param endYear
    *   The end year of the range
    *
    * @return
    *   A list of tuples with the country and the total population over the
    *   range
    */
  def problem3(startYear: Int, endYear: Int): List[(Int, Float)] =
    loader.countries
      .filter(data => loader.saarc.contains(data.country))
      .filter(data => data.year >= startYear && data.year <= endYear)
      .groupBy(_.year)
      .map { (year, data) =>
        (year, data.map(_.population).sum)
      }
      .toList
      .sortBy(_._1)

    /** Problem 4: ASEAN countries population by year by country
      */
  def problem4: Counts =
    loader.countries
      .filter(data => loader.asean.contains(data.country))
      .groupBy(data => (data.country, data.year))
      .map { (key, data) =>
        (key._1, key._2, data.map(_.population).sum)
      }
      .groupBy(_._1)
      .map { (country, data) =>
        (
          country,
          data.map { case (_, year, population) =>
            (year, population)
          }.toMap
        )
      }
