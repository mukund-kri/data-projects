package in.net.mukund

import java.io.File
import java.io.FileWriter

class Gnuplot(
    val plotRoot: String,
    startYear: Int,
    endYear: Int,
    selectedYear: Int
):

  val analysis = Analysis()

  // Plot 1: Plot the growth of population in India in given range of years
  private def plot1: Unit =
    val plotData = analysis.problem1(startYear, endYear)

    Utils.writeDatFile(s"$plotRoot/problem1.dat") { out =>
      plotData.foreach { (year, population) =>
        out.write(s"$year\t$population\n")
      }
    }

  // Plot 2: Plot bar chart of population in ASEAN for the selected year
  private def plot2: Unit =
    val plotData = analysis.problem2(selectedYear)
    Utils.writeDatFile(s"$plotRoot/problem2.dat") { out =>
      plotData.foreach { (country, population) =>
        out.write(s"\"$country\"\t$population\n")
      }
    }

  // Plot 3: Plot the growth of population in SAARC countries in given range of years
  private def plot3: Unit =
    val plotData = analysis.problem3(startYear, endYear)
    Utils.writeDatFile(s"$plotRoot/problem3.dat") { out =>
      plotData.foreach { (country, population) =>
        out.write(s"\"$country\"\t$population\n")
      }
    }

  // Plot 4: ASEAN countries population by year by country
  private def plot4: Unit =
    val plotData = analysis.problem4

    Utils.writeDatFile(s"$plotRoot/problem4.dat") { out =>
      val countries = loader.asean
      val years = startYear to endYear

      out.write(years.mkString("\"", "\"\t\"", "\""))
      out.write("\n")
      countries.foreach { country =>
        out.write(s"\"$country\"\t")
        years.foreach { year =>
          out.write(
            s"${plotData.getOrElse(country, Map.empty).getOrElse(year, 0)}\t"
          )
        }
        out.write("\n")
      }
    }

  def plotAll: Unit =
    plot1
    plot2
    plot3
    plot4

    println("\nData for gnuplot generated successfully\n")
