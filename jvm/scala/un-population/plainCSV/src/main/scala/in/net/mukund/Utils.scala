package in.net.mukund

import java.io.FileWriter
import java.io.File

import upickle.default.*

object Utils {

  /** Write the data to a file. Similar to what kotlin does.
    *
    * @param outPutFile
    *   the name file to write to
    * @param writeFn
    *   the function to write to the file
    */
  def writeDatFile(
      outPutFile: String
  )(writeFn: (out: FileWriter) => Unit): Unit = {
    val fileWriter = new FileWriter(outPutFile)
    writeFn(fileWriter)
    fileWriter.close()
  }

  def writeJsonFile[T: Writer](p1: T, plotFile: String): Unit =
    val fileWriter = new FileWriter(File(plotFile))
    fileWriter.write(write(p1))
    fileWriter.close()

}
