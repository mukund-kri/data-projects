package in.net.mukund

import com.typesafe.config.Config
import io.getquill.*
import os.*
import com.github.tototoshi.csv.*

import in.net.mukund.models.*

object Loader:

  // Setup DB context
  val ctx = new PostgresJdbcContext(SnakeCase, "app.db")
  import ctx.*

  /** Load the population data from the CSV file to postgres database
    *
    * @return
    *   Unit
    */
  def loadPopulationFromCsv(using config: Config): Unit =

    println(
      "Loading population data from CSV file to population_data table ..."
    )

    val csvSourcePath =
      Path(config.getString("app.paths.data")) / "population-estimates_csv.csv"

    val reader = CSVReader.open(csvSourcePath.toIO)
    val data = reader
      .allWithHeaders()
      .map { row =>
        PopulationData(
          row("Region"),
          row("Year").toInt,
          row("Population").toFloat
        )
      }

    // Quill query to bulk insert population data
    inline def insertPopulationData =
      liftQuery(data).foreach(e => query[PopulationData].insertValue(e))

    run(insertPopulationData)

  /** Load the country grouping data from text file to postgres database
    *
    * @param groupName
    *   \- Name of the country grouping. Example: "ASEAN"
    *
    * @return
    *   Unit
    */

  def loadGroupingFromText(groupName: String)(using config: Config): Unit =

    println(
      s"Loading country grouping data from text file to grouping table for $groupName ..."
    )

    val textSourcePath =
      Path(config.getString("app.paths.data")) / s"$groupName-countries.txt"

    val data = os.read.lines(textSourcePath).map { case (country) =>
      Grouping(country, groupName)
    }

    // Quill query to bulk insert grouping data
    inline def insertGroupingData =
      liftQuery(data).foreach(e => query[Grouping].insertValue(e))

    run(insertGroupingData)

  /** Clean up data by truncating the tables
    *
    * @return
    *   Unit
    */
  def cleanUpData: Unit =
    println("Cleaning up data ...")

    run(query[PopulationData].delete)
    run(query[Grouping].delete)

    /** Load all data for running this example
      *
      * @return
      *   Unit
      */
  def loadAllData: Unit =
    println("\n Loading all data ...")
    loadPopulationFromCsv
    loadGroupingFromText("asean")
    loadGroupingFromText("saarc")
    println("Data loaded successfully\n")
end Loader
