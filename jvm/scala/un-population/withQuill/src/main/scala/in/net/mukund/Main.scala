// Author: Mukund Krishnamurthy
// Date: 2024-12-02
//
// The UN Population analysis
package in.net.mukund

import scala.util.CommandLineParser

import com.typesafe.config.{Config, ConfigFactory}

// CliParam type to choose which part of the program to run
enum CliParam:
  case Loader // Load the data from the CSV file to db
  case Gnuplot // Generate the gnuplot dat files
  case Highchart // Generate the highcharts json files
  case CleanDB // Dealete all the data from the database

// implicit for converting Cli to String
given CommandLineParser.FromString[CliParam] with
  def fromString(str: String): CliParam = CliParam.valueOf(str)

// load config into an implicit val
given config: Config = ConfigFactory.load()

@main def main(param: CliParam) =
  param match
    case CliParam.Loader =>
      Loader.loadAllData
    case CliParam.Gnuplot =>
      // Generate the gnuplot dat files
      Gnuplot.plotAll
    case CliParam.Highchart =>
      println("Highchart")
    case CliParam.CleanDB =>
      Loader.cleanUpData
