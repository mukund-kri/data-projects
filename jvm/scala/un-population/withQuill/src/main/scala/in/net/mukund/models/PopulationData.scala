package in.net.mukund.models

final case class PopulationData(country: String, year: Int, population: Float)
