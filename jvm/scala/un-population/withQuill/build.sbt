import Dependencies._

ThisBuild / scalaVersion := "3.5.0"
ThisBuild / organization := "in.net.mukund"
ThisBuild / version := "0.0.1-SNAPSHOT"

lazy val root = (project in file(".")).settings(
  name := "withQuill",

  // Quill
  libraryDependencies ++= quill,

  // Postgres
  libraryDependencies += postgres,

  // Lightbend Config
  libraryDependencies += lightbendConfig,

  // os-lib for file operations
  libraryDependencies += osLib,

  // Reading CSV files
  libraryDependencies += scalaCsv,

  // Testing
  libraryDependencies ++= Seq(
    scalaTest % Test
  )
)
