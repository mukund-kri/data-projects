# UN Population analysis with Scala and Quill (Postgres)


## Aim

Implement the un population analysis with Scala and Quill (Postgres). In this project ...

- I will not be using ZIO explicitly. 
- I will be using Quill for database access.

## Runnning the project

1. Enter into the project directory

```bash
cd un-population/withQuill
```

2. Fire up the sbt console

```bash
sbt
```

3. Run Postgres in a docker container

```bash
docker-compose up
```

4. Create the database

```bash


3. Load the data form the csv file into the database
```bash
run 
```

