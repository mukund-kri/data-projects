import sbt._

object Dependencies {

  // Versions --------------------------------------------------------------------------------------
  val quillVersion = "4.7.3"
  val osLibVersion = "0.11.3"
  val lightbendConfigVersion = "1.4.2"
  val postgresVersion = "42.2.23"
  val scalaCSVVersion = "2.0.0"

  // Dependencies ----------------------------------------------------------------------------------
  // Quill
  lazy val quill = Seq(
    "io.getquill" %% "quill-jdbc" % quillVersion,
    "io.getquill" %% "quill-jdbc-zio" % quillVersion
  )

  // Postgres
  lazy val postgres = "org.postgresql" % "postgresql" % "42.2.23"

  // Lightbend Config
  lazy val lightbendConfig = "com.typesafe" % "config" % "1.4.2"

  // os-lib for file operations
  lazy val osLib = "com.lihaoyi" %% "os-lib" % osLibVersion

  // Reading CSV files
  lazy val scalaCsv = "com.github.tototoshi" %% "scala-csv" % scalaCSVVersion

  // Testing -------------------------------------------------------------------------------------

  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.2.9"
}
