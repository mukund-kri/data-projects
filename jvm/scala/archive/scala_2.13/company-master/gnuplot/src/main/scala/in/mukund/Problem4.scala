package in.mukund

import com.github.tototoshi.csv.CSVReader
import java.io._


object Problem4 {
  
  val pbas = List(
    "Real Estate and Renting", 
    "Manufacturing (Wood Products)",
    "Construction",
    "Manufacturing (Textiles)",
    // "Transport",
    // "storage and Communications",
  )

  def increment(row: Map[String, String],
               accumul: Map[String, Map[String, Int]] 
               ): 
    Map[String, Map[String, Int] ] = {

    val year: String = row("DATE_OF_REGISTRATION").takeRight(4)
    val pba = row("PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN")


    accumul.updatedWith(year){
      case Some(yearVals) => Some(yearVals.updatedWith(pba) {
        case Some(count)  => Some(count + 1)
        case None         => Some(1)
      })
      case None           => Some(Map[String, Int]())
    }
  }

  def problem4() {

    val reader = CSVReader.open(new File("data/mca.csv"))

    val result = reader
      .allWithHeaders()
      .foldRight(Map[String, Map[String, Int]]())(increment)

    val pw = new PrintWriter(new File("plots/problem4.dat"))
    pbas.foreach(year => pw.print(s""""${year}"\t"""))
    pw.println()
    (2000 to 2018).foreach(year => {
      pw.print(s"${year}\t")
      pbas.foreach(pba => pw.print(s"${result(year.toString())(pba)}\t"))
      pw.println()
    })
    pw.close()
  }
}