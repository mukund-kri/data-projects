package in.mukund

import com.github.tototoshi.csv.CSVReader
import java.io._


object Problem3 {

  def problem3() {

    val reader = CSVReader.open(new File("data/mca.csv"))

    val result = reader
      .allWithHeaders()
      .filter(_("DATE_OF_REGISTRATION").takeRight(4) == "2015")
      .map(_("PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN"))
      .groupBy(identity)
      .mapValues(_.size)

    val pw = new PrintWriter(new File("plots/problem3.dat"))
    result.foreach(k => pw.write(s""""${k._1}"  ${k._2}\n"""))
    pw.close()

  }
}