package in.mukund

import com.github.tototoshi.csv.CSVReader
import java.io._


object Problem1 {

  def categorize(cap: Int): String = cap match {
    case x if x < 1_00_000 => "< 1L"
    case x if x < 10_00_000 => "1L - 10L"
    case x if x < 1_00_00_000 => "10L - 1Cr"
    case x if x < 10_00_00_000 => "1Cr - 10Cr"
    case x if x < 100_00_00_000 => "10Cr - 100Cr"
    case _ => "> 100Cr"
  }


  def problem1() {

    val initial = Map(
      "< 1L" -> 0,
      "1L - 10L" -> 0,
      "10L - 1Cr" -> 0,
      "1Cr - 10Cr" -> 0,
      "10Cr - 100Cr" -> 0,
      "> 100Cr" -> 0
    )
    val reader = CSVReader.open(new File("data/mca.csv"))

    val result = reader
      .allWithHeaders()
      .map(line => line("AUTHORIZED_CAP").toDouble.toInt)
      .map(categorize)
      .foldRight(initial)((cat, map) => map.updatedWith(cat) {
        case Some(count)  => Some(count + 1)
        case None         => None
      })


    val labels = Array(
      "< 1L",
      "1L - 10L",
      "10L - 1Cr",
      "1Cr - 10Cr",
      "10Cr - 100Cr",
      "> 100Cr",
    )

    val pw = new PrintWriter(new File("plots/problem1.dat"))
    labels.foreach(k => pw.write(s""""${k}" ${result(k)}\n"""))
    pw.close()
 
  }
 }