ThisBuild / scalaVersion := "2.13.6"
ThisBuild / organization := "in.mukund"


lazy val companyMaster = (project in file("."))
  .settings(
    name := "CompanyMaster",
    // DEPENDENCIES
    libraryDependencies += "com.github.tototoshi" %% "scala-csv" % "1.3.8",
    
    // NSPL does not seem to be well documented so using evilplot instead
    // libraryDependencies += "io.github.pityka" %% "nspl-awt" % "0.0.24",
    libraryDependencies += "io.github.cibotech" %% "evilplot" % "0.8.1",

    libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.7" % Test,
  )