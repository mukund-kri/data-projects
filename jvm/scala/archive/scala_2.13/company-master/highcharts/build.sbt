ThisBuild / scalaVersion := "2.13.6"
ThisBuild / organization := "in.mukund"


lazy val companyMaster = (project in file("."))
  .settings(
    name := "CompanyMasterHighCharts",
    // DEPENDENCIES
    libraryDependencies += "com.github.tototoshi" %% "scala-csv" % "1.3.8",
    libraryDependencies += "com.lihaoyi" %% "upickle" % "1.4.0",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.7" % Test,
  )