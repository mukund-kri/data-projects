let plot;

$.getJSON('problem1.json', (data) => {
  plot(data.categories, data.data);  
});

plot = (categories, series) => {
  return Highcharts.chart('problem1', {
    chart: {
      type: 'bar',
    },
    title: {
      text: 'Companies registered in Maharastra by Authorized Cap',
    },
    xAxis: {
      categories,
      title: {
        text: null,
      },
    },
    yAxis: {
      min: 0,
      title: {
        text: 'No of Companies',
        align: 'high',
      },
    },
    labels: {
      overflow: 'justify',
    },
    tooltip: {
      valueSuffix: 'companies',
    },
    plotOptions: {
      bar: {
        dataLabels: {
          enabled: true,
        },
      },
    },
    credits: {
      enabled: false,
    },
    series: [
      {
        name: 'Number of Companies',
        data: series,
      },
    ],
  });
};
