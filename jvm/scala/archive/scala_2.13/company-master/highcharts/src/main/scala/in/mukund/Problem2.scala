package in.mukund

import  com.github.tototoshi.csv.CSVReader
import java.io._


object Problem2 {
 
  def problem2() {

    val reader = CSVReader.open(new File("data/mca.csv"))

    val result = reader
      .allWithHeaders()
      .map(_("DATE_OF_REGISTRATION"))
      .map(_.takeRight(4))
      .filter(_ != "NA")
      .map(_.toInt)
      .filter(_ >= 2000)
      .groupBy(identity)
      .mapValues(_.size)
      .toSeq
      .sortBy(_._1)

    val pw = new PrintWriter(new File("plots/problem2.dat"))
    result.foreach(k => pw.write(s"""${k._1}  ${k._2}\n"""))
    pw.close()

  }
}