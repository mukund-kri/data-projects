package in.mukund

import com.github.tototoshi.csv.CSVReader
import java.io._
import scala.collection.immutable.ListMap
import upickle.default._
import upickle.default.{ReadWriter => RW, macroRW}


case class Data(categories: Seq[String], data: Seq[Int])
object Data {
  implicit val rw: RW[Data] = macroRW
}

object Problem1 {

  def categorize(cap: Int): String = cap match {
    case x if x < 1_00_000 => "< 1L"
    case x if x < 10_00_000 => "1L - 10L"
    case x if x < 1_00_00_000 => "10L - 1Cr"
    case x if x < 10_00_00_000 => "1Cr - 10Cr"
    case x if x < 100_00_00_000 => "10Cr - 100Cr"
    case _ => "> 100Cr"
  }


  def problem1() {

    val initial = ListMap(
      "< 1L" -> 0,
      "1L - 10L" -> 0,
      "10L - 1Cr" -> 0,
      "1Cr - 10Cr" -> 0,
      "10Cr - 100Cr" -> 0,
      "> 100Cr" -> 0
    )
    val reader = CSVReader.open(new File("data/mca.csv"))

    val result = reader
      .allWithHeaders()
      .map(line => line("AUTHORIZED_CAP").toDouble.toInt)
      .map(categorize)
      .foldRight(initial)((cat, map) => map.updatedWith(cat) {
        case Some(count)  => Some(count + 1)
        case None         => None
      })
    
    val pw = new PrintWriter(new File("plots/problem1.json"))
    pw.write(write(
      Data(result.keys.toSeq, result.values.toSeq)
    ))
    pw.close()
    
  }
 }