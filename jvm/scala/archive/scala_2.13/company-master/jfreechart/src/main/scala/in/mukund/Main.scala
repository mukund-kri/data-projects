package in.mukund

import javafx.application.Application
import javafx.scene.Scene
import javafx.scene.layout.StackPane
import javafx.stage.Stage
import javafx.scene.control.Label

import org.jfree.chart.{ChartFactory, JFreeChart}
import org.jfree.chart.fx.ChartViewer
import org.jfree.chart.fx.interaction.ChartMouseListenerFX
import org.jfree.data.category.{CategoryDataset, DefaultCategoryDataset}
import org.jfree.chart.fx.interaction.ChartMouseEventFX

class HelloFx extends Application with ChartMouseListenerFX:

  def createDataset(): CategoryDataset =
    val dataset = new DefaultCategoryDataset()
    dataset.addValue(1.0, "S1", "C1")
    dataset.addValue(4.0, "S2", "C1")
    dataset.addValue(3.0, "S3", "C1")
    dataset

  def createChart(dataset: CategoryDataset): JFreeChart =

    val chart = ChartFactory.createBarChart(
      "Bar Chart Demo",
      "Category",
      "Value",
      dataset,
    )

    chart
  
  override def start(primaryStage: Stage): Unit = 
    
    val dataset = Problem1.problem1Data()
    val chart = createChart(dataset)
    val viewer = new ChartViewer(chart)
    viewer.addChartMouseListener(this)
    primaryStage.setScene(new Scene(viewer, 800, 600))
    primaryStage.setTitle("Company Master")
    primaryStage.show()
  
  override def chartMouseClicked(event: ChartMouseEventFX): Unit =
    println(event)

  override def chartMouseMoved(event: ChartMouseEventFX): Unit =
    println(event)

@main def main(args: Array[String]) =

  Application.launch(classOf[HelloFx], args: _*)
