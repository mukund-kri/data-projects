import Dependencies._

ThisBuild / scalaVersion := "3.1.1"
ThisBuild / organization := "in.mukund"
ThisBuild / version := "0.0.1-SNAPSHOT"

lazy val root = (project in file(".")).
  settings(
    name := "jfreechart",

    libraryDependencies += "com.github.tototoshi" %% "scala-csv" % "1.3.10",
    libraryDependencies += "org.jfree" % "jfreechart" % "1.5.3",
    
    // JavaFX for UI
    libraryDependencies += "org.openjfx" % "javafx-controls" % "17.0.2" classifier "linux", 
    libraryDependencies += "org.jfree" % "org.jfree.chart.fx" % "2.0.1",


    libraryDependencies ++= Seq(
      scalaTest % Test
      // TODO: Add your test dependencies here
    )
  )
