# scala / ipl / plainCSV

Solving the IPL data set problems using scala. The data set is in CSV format.

## Problem Statement

Please refer to the [IPL-DataSet-Problem-Statement](../../../../xx.problem.statements/PROBLEM-STATEMENT-IPL.md)


## Run the code

```bash
sbt run gnuplot
```

To generate the gnuplot .dat files.

And ...

```bash
sbt run highcharts
```

To generate the highcharts .json files.