// Author: Mukund Krishnamurthy
// Date: 2024-11-15
//
// The IPL data plot example in scala using plain CSV
package in.net.mukund


import scala.util.CommandLineParser


// Cli to choose what to plot; gnuplot or highcharts
enum CliParam:
  case gnuplot, highcharts

// implicit for converting Cli to String
given CommandLineParser.FromString[CliParam] with
  def fromString(str: String): CliParam = CliParam.valueOf(str)

@main def main(param: CliParam) = param match
  case CliParam.gnuplot =>
    Gnuplot(
      "Royal Challengers Bangalore",
      "../../plots/ipl/gnuplot"
    ).plotAll
  case CliParam.highcharts =>
    Highcharts(
      "Royal Challengers Bangalore",
      "../../plots/ipl/highcharts"
    ).plotAll
