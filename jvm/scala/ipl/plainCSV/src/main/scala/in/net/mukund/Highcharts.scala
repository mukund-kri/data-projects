package in.net.mukund

import upickle.default.*
import in.net.mukund.Analysis
import java.io.FileWriter
import java.io.File

case class Problem1(teams: Seq[String], runs: Seq[Double]) derives ReadWriter
case class Problem2(batsmen: Seq[String], runs: Seq[Double]) derives ReadWriter
case class Problem3(countries: Seq[String], umpires: Seq[Double])
    derives ReadWriter

case class Series(name: String, data: Seq[Double]) derives ReadWriter
case class Problem4(teams: Seq[String], series: Seq[Series]) derives ReadWriter

class Highcharts(val team: String, val plotRoot: String):

  val analysis = Analysis()

  private def plot1: Unit =
    val plotData = analysis.problem1
    val p1 = Problem1(plotData.map(_._1), plotData.map(_._2.toDouble))
    val fileWriter = new FileWriter(File(s"$plotRoot/problem1.json"))
    fileWriter.write(write(p1))
    fileWriter.close()

  private def plot2: Unit =
    val plotData = analysis.problem2(team).take(10)
    val p2 = Problem2(plotData.map(_._1), plotData.map(_._2.toDouble))
    val fileWriter = new FileWriter(File(s"$plotRoot/problem2.json"))
    fileWriter.write(write(p2))
    fileWriter.close()

  private def plot3: Unit =
    val plotData = analysis.problem3
    val p3 = Problem3(plotData.map(_._1), plotData.map(_._2.toDouble))
    val fileWriter = new FileWriter(File(s"$plotRoot/problem3.json"))
    fileWriter.write(write(p3))
    fileWriter.close()

  private def plot4: Unit =
    val (plotData, teams, seasons) = analysis.problem4

    // First construct the series
    val series = teams.map { team =>
      val data = seasons.map { season =>
        plotData.getOrElse(team, Map.empty).getOrElse(season, 0).toDouble
      }
      Series(team, data)
    }

    val p4 = Problem4(teams, series)
    val fileWriter = new FileWriter(File(s"$plotRoot/problem4.json"))
    fileWriter.write(write(p4))
    fileWriter.close()

  def plotAll: Unit =
    plot1
    plot2
    plot3
    plot4

    println("\nData for hichcharts generated successfully\n")
