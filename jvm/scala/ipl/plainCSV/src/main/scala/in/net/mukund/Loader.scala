package in.net.mukund

import com.github.tototoshi.csv.CSVReader
import java.io.File

given loader: Loader = Loader(
  "/home/mukund/workarea/teaching/data-projects/raw_data/ipl"
)

case class Delivery(
    batting_team: String,
    batter: String,
    total_runs: Int,
    batsman_runs: Int
)

case class Match(winner: String, season: String)

case class Umpire(name: String, country: String)

class Loader(val dataPath: String):

  lazy val deliveries = loadDeliveries()
  lazy val matches = loadMatches()
  lazy val umpires = loadUmpires()

  // Load all the deliveries from the corrected_deliveries.csv file into a list for further
  // processing
  private def loadDeliveries(): List[Delivery] =
    val deliveriesPath = s"$dataPath/corrected_deliveries.csv"

    val reader = CSVReader.open(new File(deliveriesPath))

    val deliveries = reader.allWithHeaders().map { row =>
      Delivery(
        row("batting_team"),
        row("batter"),
        row("total_runs").toInt,
        row("batsman_runs").toInt
      )
    }
    reader.close()
    deliveries

  // Load all the matches from the corrected_matches.csv file into a list for further processing
  private def loadMatches(): List[Match] =
    val matchesPath = s"$dataPath/corrected_matches.csv"

    val reader = CSVReader.open(new File(matchesPath))

    val matches = reader.allWithHeaders().map { row =>
      Match(
        row("winner"),
        row("season")
      )
    }
    reader.close()
    matches

  // Load all umpires from the ipl-umpire.csv file into a list for further processing
  private def loadUmpires(): List[Umpire] =
    val umpiresPath = s"$dataPath/ipl-umpire.csv"

    val reader = CSVReader.open(new File(umpiresPath))

    val umpires = reader.allWithHeaders().map { row =>
      Umpire(
        row("name"),
        row("country")
      )
    }
    reader.close()
    umpires
