package in.net.mukund

type Counts = Map[String, Map[String, Int]]

class Analysis(using loader: Loader):

  println("Total deliveries: " + loader.deliveries.size)

  /** Problem 1: Plot the total runs scored by each team in the IPL
    */
  def problem1: List[(String, Int)] =
    loader.deliveries
      .groupBy(_.batting_team)
      .map { case (team, deliveries) =>
        (team, deliveries.map(_.total_runs).sum)
      }
      .toList
      .sortBy(-_._2)

  /** Problem 2: Top run scorers in the IPL for a given team
    *
    * @param team
    *   The team for which the top run scorers are to be found
    */
  def problem2(team: String): List[(String, Int)] =
    loader.deliveries
      .filter(_.batting_team == team)
      .groupBy(_.batter)
      .map { (batter, deliveries) =>
        (batter, deliveries.map(_.batsman_runs).sum)
      }
      .toList
      .sortBy(-_._2)

  /** Problem 3: Umpires by country of origin excluding India
    */
  def problem3: List[(String, Int)] =
    loader.umpires
      .filterNot(_.country == "IND")
      .groupBy(_.country)
      .map { case (country, umpires) =>
        (country, umpires.size)
      }
      .toList

    /** Problem 4: Matches won by each team in each season
      */
  def problem4: (Counts, List[String], List[String]) =
    val result = loader.matches
      .filter(
        _.winner != "NA"
      ) // NA is a placeholder for matches with no winner
      .groupBy(_match => (_match.winner, _match.season))
      .map { case ((winner, season), matches) =>
        ((winner, season), matches.size)
      }
      .groupBy(_._1._1)
      .map { case (team, wins) =>
        (
          team,
          wins.map { case ((_, season), count) =>
            (season, count)
          }.toMap
        )
      }

    val teams = result.keys.toList
    val season = result.get(teams.head).map(_.keys.toList).getOrElse(List.empty)

    (result, teams, season)
