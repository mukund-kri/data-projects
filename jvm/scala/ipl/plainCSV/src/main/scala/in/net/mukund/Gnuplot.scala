package in.net.mukund

import java.io.File
import java.io.FileWriter

class Gnuplot(val team: String, val plotRoot: String):

  val analysis = Analysis()

  // Plot 1: Total runs scored by each team in the IPL
  private def plot1: Unit =
    val plotData = analysis.problem1
    val fileWriter = new FileWriter(File(s"$plotRoot/problem1.dat"))
    plotData.foreach { case (team, runs) =>
      fileWriter.write(s"\"$team\"\t$runs\n")
    }
    fileWriter.close()

  // Plot 2: Top run scorers for a given team
  private def plot2: Unit =
    val plotData = analysis.problem2(team)
    val fileWriter = new FileWriter(File(s"$plotRoot/problem2.dat"))
    plotData.foreach { case (batter, runs) =>
      fileWriter.write(s"\"$batter\"\t$runs\n")
    }
    fileWriter.close()

  // Plot 3: Umpires by country of origin, excluding India
  private def plot3: Unit =
    val plotData = analysis.problem3
    val fileWriter = new FileWriter(File(s"$plotRoot/problem3.dat"))
    plotData.foreach { case (country, count) =>
      fileWriter.write(s"\"$country\"\t$count\n")
    }

  // Plot 4: Matches won by each team in each season
  private def plot4: Unit =
    val (plotData, teams, seasons) = analysis.problem4
    val fileWriter = new FileWriter(File(s"$plotRoot/problem4.dat"))
    fileWriter.write(teams.map(team => s"\"$team\"").mkString("\t"))
    fileWriter.write("\n")
    seasons.foreach { season =>
      fileWriter.write(s"\"$season\"\t")
      teams.foreach { team =>
        fileWriter.write(
          s"${plotData.getOrElse(team, Map.empty).getOrElse(season, 0)}\t"
        )
      }
      fileWriter.write("\n")
    }
    fileWriter.close()

  def plotAll: Unit =
    plot1
    plot2
    plot3
    plot4

    println("\nData for gnuplot generated successfully\n")