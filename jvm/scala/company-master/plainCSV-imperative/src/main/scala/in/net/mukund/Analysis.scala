package in.net.mukund

import scala.collection.mutable.{Map => MutableMap}

type Counts = Map[Int, Map[String, Int]]

class Analysis(using loader: Loader):

  private def categorize(company: Company): String =
    if company.authorizedCapital <= 1_00_000 then "< 1L"
    else if company.authorizedCapital <= 10_00_000 then "1L - 10L"
    else if company.authorizedCapital <= 1_00_00_000 then "10L - 1Cr"
    else if company.authorizedCapital <= 10_00_00_000 then "1Cr - 10Cr"
    else "> 10Cr"

  /** Problem 1: Plot the total runs scored by each team in the IPL
    */
  def problem1: Map[String, Int] =
    val counts = MutableMap.empty[String, Int].withDefaultValue(0)

    for company <- loader.companies do
      val category = categorize(company)
      counts(category) += 1

    counts.toMap

  /** Problem 2: Registration by year between a range of years
    *
    * @param startYear
    *   The start year of the range
    * @param endYear
    *   The end year of the range
    */
  def problem2(startYear: Int, endYear: Int): Map[Int, Int] =
    val counts = MutableMap.empty[Int, Int].withDefaultValue(0)

    for company <- loader.companies do
      if company.registrationYear >= startYear && company.registrationYear <= endYear
      then counts(company.registrationYear) += 1

    counts.toMap

  /** Problem 3: counts of registered companies by principal business activity
    * for selected year
    *
    * @param selectedYear
    *   The year for which the count is to be calculated
    */
  def problem3(selectedYear: Int): List[(String, Int)] =
    val counts = MutableMap.empty[String, Int].withDefaultValue(0)
    for company <- loader.companies do
      if company.registrationYear == selectedYear then counts(company.pba) += 1
    counts.toList.sortBy(-_._2)

  /** Problem 4: Registrations by year and principal business activity
    */
  def problem4: Counts =
    val counts =
      MutableMap
        .empty[Int, Map[String, Int]]
        .withDefaultValue(
          Map.empty[String, Int].withDefaultValue(0)
        )

    for company <- loader.companies do
      counts(company.registrationYear) += company.pba -> (counts(
        company.registrationYear
      )(company.pba) + 1)

    counts.toMap
