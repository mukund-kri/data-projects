package in.net.mukund

import com.github.tototoshi.csv.CSVReader
import java.io.File
import scala.collection.mutable.ListBuffer

given loader: Loader = Loader(
  "/home/mukund/workarea/teaching/data-projects/raw_data/company-master"
)

case class Company(
    authorizedCapital: Long,
    registrationYear: Int,
    pba: String
)

class Loader(val dataPath: String):

  lazy val companies = loadCompanies()
  lazy val topPBA = loadPBAs()

  // Load all the companies from the maharashtra_company_master.csv file into a list for further
  // processing

  private def loadCompanies(): List[Company] =
    val sourceCSVPath = s"$dataPath/maharashtra_company_master.csv"

    val reader = CSVReader.open(new File(sourceCSVPath))
    val companies = ListBuffer[Company]()

    for (line <- reader.iteratorWithHeaders) do
      val authorizedCapital = line("AUTHORIZED_CAP")
      val registrationYear = line("DATE_OF_REGISTRATION")
      val pba = line("PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN")

      if authorizedCapital != "NA" && registrationYear != "NA" then
        // println(s"Skipping company with NA values: $line")
        companies += Company(
          authorizedCapital.toFloat.toLong,
          registrationYear.takeRight(4).toInt,
          pba
        )

    reader.close()
    companies.toList

  // Load the top principal business activities from the top_pba.txt file into a list
  private def loadPBAs(): List[String] =
    val sourcePath = s"$dataPath/top_pbas.txt"
    val sourceFile = new File(sourcePath)
    val source = scala.io.Source.fromFile(sourceFile)
    val pbas = source.getLines().toList
    source.close()
    pbas
