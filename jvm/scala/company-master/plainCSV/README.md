# scala / company master / plainCSV

Solving the company master data set problems using scala. The data set is in CSV format.

## Problem Statement

Please refer to the [Company Master DataSet Problem Statement](../../../../xx.problem.statements/PROBLEM-STATEMENT-COMPANY-MASTER.md)


## Run the code

```bash
sbt run gnuplot
```

To generate the gnuplot .dat files.

And ...

```bash
sbt run highcharts
```

To generate the highcharts .json files.