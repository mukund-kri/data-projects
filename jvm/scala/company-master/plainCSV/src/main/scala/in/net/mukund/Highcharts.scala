package in.net.mukund

import upickle.default.*
import in.net.mukund.Analysis
import java.io.FileWriter
import java.io.File

case class Problem1(categories: Seq[String], series: Seq[Int])
    derives ReadWriter
case class Problem2(categories: Seq[Int], series: Seq[Int]) derives ReadWriter
case class Problem3(categories: Seq[String], series: Seq[Int])
    derives ReadWriter

case class Series(name: Int, data: Seq[Int]) derives ReadWriter
case class Problem4(categories: Seq[String], series: Seq[Series])
    derives ReadWriter

class Highcharts(
    val plotRoot: String,
    startYear: Int,
    endYear: Int,
    selectedYear: Int
):

  val analysis = Analysis()

  private def plot1: Unit =
    val plotData = analysis.problem1

    val p1 = Problem1(
      plotData.keys.toList,
      plotData.values.toList
    )
    val fileWriter = new FileWriter(File(s"$plotRoot/problem1.json"))
    fileWriter.write(write(p1))
    fileWriter.close()

  private def plot2: Unit =
    val plotData = analysis.problem2(startYear, endYear)
    val p2 = Problem2(
      plotData.keys.toList,
      plotData.values.toList
    )
    val fileWriter = new FileWriter(File(s"$plotRoot/problem2.json"))
    fileWriter.write(write(p2))
    fileWriter.close()

  private def plot3: Unit =
    val plotData = analysis.problem3(selectedYear)
    val (categories, series) = plotData.unzip
    val p3 = Problem3(categories, series)
    val fileWriter = new FileWriter(File(s"$plotRoot/problem3.json"))
    fileWriter.write(write(p3))
    fileWriter.close()

  private def plot4: Unit =
    val plotData = analysis.problem4
    val pbas = loader.topPBA.take(5)
    val years = startYear to endYear

    // First construct the series
    val series = years.map { year =>
      val data = pbas.map { pba =>
        plotData.getOrElse(year, Map.empty).getOrElse(pba, 0)
      }
      Series(year, data)
    }

    val p4 = Problem4(pbas, series)
    val fileWriter = new FileWriter(File(s"$plotRoot/problem4.json"))
    fileWriter.write(write(p4))
    fileWriter.close()

  def plotAll: Unit =
    plot1
    plot2
    plot3
    plot4

    println("\nData for hichcharts generated successfully\n")
