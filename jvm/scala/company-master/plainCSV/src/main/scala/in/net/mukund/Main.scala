// Author: Mukund Krishnamurthy
// Date: 2024-11-15
//
// The IPL data plot example in scala using plain CSV
package in.net.mukund

import scala.util.CommandLineParser

// Cli to choose what to plot; gnuplot or highcharts
enum CliParam:
  case gnuplot, highcharts

// implicit for converting Cli to String
given CommandLineParser.FromString[CliParam] with
  def fromString(str: String): CliParam = CliParam.valueOf(str)

val PlotRoot = "../../plots/company-master"

@main def main(param: CliParam) = param match
  case CliParam.gnuplot =>
    println("Plotting using gnuplot")
    Gnuplot(s"$PlotRoot/gnuplot", 2000, 2010, 2000).plotAll
  case CliParam.highcharts =>
    println("Plotting using highcharts")
    Highcharts(s"$PlotRoot/highcharts", 2000, 2010, 2000).plotAll
