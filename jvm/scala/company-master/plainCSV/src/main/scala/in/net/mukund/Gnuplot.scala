package in.net.mukund

import java.io.File
import java.io.FileWriter

class Gnuplot(
    val plotRoot: String,
    startYear: Int,
    endYear: Int,
    selectedYear: Int
):

  val analysis = Analysis()

  // Truncate the label to 20 characters
  private def truncate(label: String): String =
    if label.length > 20 then label.take(17) + "..." else label

  // Plot 1: Registrations by authorized capital
  private def plot1: Unit =
    val plotData = analysis.problem1

    // The bins for authorized capital
    val bins = List("< 1L", "1L - 10L", "10L - 1Cr", "1Cr - 10Cr", "> 10Cr")

    val fileWriter = new FileWriter(File(s"$plotRoot/problem1.dat"))
    bins.foreach { bin =>
      fileWriter.write(s"\"$bin\"\t${plotData(bin)}\n")
    }
    fileWriter.close()

  // Plot 2: Registration by year
  private def plot2: Unit =
    val plotData = analysis.problem2(startYear, endYear)
    val years = startYear to endYear

    val fileWriter = new FileWriter(File(s"$plotRoot/problem2.dat"))
    years.foreach { year =>
      fileWriter.write(s"\"$year\"\t${plotData(year)}\n")
    }
    fileWriter.close()

  // Plot 3: Plot by registration by pba for the selected year
  private def plot3: Unit =
    val plotData = analysis.problem3(selectedYear)

    val fileWriter = new FileWriter(File(s"$plotRoot/problem3.dat"))
    plotData.take(5).foreach { case (pba, count) =>
      fileWriter.write(s"\"${truncate(pba)}\"\t$count\n")
    }
    fileWriter.close()

  // Plot 4: Matches won by each year in each pba
  private def plot4: Unit =
    val plotData = analysis.problem4

    val years = startYear to endYear
    val pbas = loader.topPBA.take(5)

    val fileWriter = new FileWriter(File(s"$plotRoot/problem4.dat"))
    fileWriter.write(years.map(year => s"\"$year\"").mkString("\t"))
    fileWriter.write("\n")
    pbas.foreach { pba =>
      fileWriter.write(s"\"${truncate(pba)}\"\t")
      years.foreach { year =>
        fileWriter.write(
          s"${plotData.getOrElse(year, Map.empty).getOrElse(pba, 0)}\t"
        )
      }
      fileWriter.write("\n")
    }
    fileWriter.close()

  def plotAll: Unit =
    // plot1
    // plot2
    // plot3
    plot4

    println("\nData for gnuplot generated successfully\n")
