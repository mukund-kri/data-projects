package in.net.mukund

@main def topPbas =
  val analysis = Analysis()
  val topPbas = analysis.problem3(2010)

  // Dump the top PBAs to a file
  val fileWriter = java.io.FileWriter(java.io.File("top_pbas.txt"))
  topPbas.foreach { case (pba, count) =>
    fileWriter.write(s"$pba\n")
  }
  fileWriter.close()
