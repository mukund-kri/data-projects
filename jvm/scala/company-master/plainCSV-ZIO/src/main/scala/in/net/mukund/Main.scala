// Author: Mukund Krishnamurthy
// Date: 2024-11-15
//
// The IPL data plot example in scala using plain CSV
package in.net.mukund

import zio.*
import zio.Console.*

val PlotRoot = "../../plots/company-master"

// ZIO entry point
object Plotter extends ZIOAppDefault:

  // generate gunplot data files
  def gnuplot: Task[Unit] =
    for
      _ <- printLine("Writing gnuplot data files .... \n")
      _ <- Gnuplot(s"$PlotRoot/gnuplot", 2000, 2010, 2000).plotAll
    yield ()

  def highcharts: Task[Unit] =
    for
      _ <- printLine("Writing highcharts data files .... \n")
      _ <- Highcharts(s"$PlotRoot/highcharts", 2000, 2010, 2000).plotAll
    yield ()

  def run = for
    args <- getArgs
    _ <- args match
      case Chunk("gnuplots")   => gnuplot
      case Chunk("highcharts") => highcharts
      case _ => printLine("Usage: sbt run gnuplots | highcharts")
  yield ()
