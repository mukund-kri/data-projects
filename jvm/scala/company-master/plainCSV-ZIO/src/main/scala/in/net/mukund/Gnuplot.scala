package in.net.mukund

import java.io.{File, FileWriter}

import zio.*

class Gnuplot(
    val plotRoot: String,
    startYear: Int,
    endYear: Int,
    selectedYear: Int
):

  val analysis = Analysis()

  // Truncate the label to 20 characters
  private def truncate(label: String): String =
    if label.length > 20 then label.take(17) + "..." else label

  // Plot 1: Registrations by authorized capital
  private def plot1(companies: List[Company]): Task[Unit] =

    // The bins for authorized capital
    val bins = List("< 1L", "1L - 10L", "10L - 1Cr", "1Cr - 10Cr", "> 10Cr")

    // Compute plot data
    val plotData = analysis.problem1(companies)

    for _ <- Utils.dumpDatFile(s"$plotRoot/problem1.dat") { fileWriter =>
        bins.foreach { bin =>
          fileWriter.write(s"\"$bin\"\t${plotData(bin)}\n")
        }
      }
    yield ()

  // Plot 2: Registration by year
  private def plot2(companies: List[Company]): Task[Unit] =
    val plotData = analysis.problem2(companies, startYear, endYear)
    val years = startYear to endYear

    for _ <- Utils.dumpDatFile(s"$plotRoot/problem2.dat") { fileWriter =>
        years.foreach { year =>
          fileWriter.write(s"\"$year\"\t${plotData(year)}\n")
        }
      }
    yield ()

  // Plot 3: Plot by registration by pba for the selected year
  private def plot3(companies: List[Company]): Task[Unit] =
    val plotData = analysis.problem3(companies, selectedYear)

    for _ <- Utils.dumpDatFile(s"$plotRoot/problem3.dat") { fileWriter =>
        plotData.take(5).foreach { case (pba, count) =>
          fileWriter.write(s"\"${truncate(pba)}\"\t$count\n")
        }
      }
    yield ()

  // Plot 4: Matches won by each year in each pba
  private def plot4(
      companies: List[Company],
      topPbas: List[String]
  ): Task[Unit] =
    val plotData = analysis.problem4(companies)

    val years = startYear to endYear
    val pbas = topPbas.take(5)

    for _ <- Utils.dumpDatFile(s"$plotRoot/problem4.dat") { fileWriter =>
        fileWriter.write(years.map(year => s"\"$year\"").mkString("\t"))
        fileWriter.write("\n")
        pbas.foreach { pba =>
          fileWriter.write(s"\"${truncate(pba)}\"\t")
          years.foreach { year =>
            fileWriter.write(
              s"${plotData.getOrElse(year, Map.empty).getOrElse(pba, 0)}\t"
            )
          }
          fileWriter.write("\n")
        }
      }
    yield ()

  def plotAll: Task[Unit] =
    val loader = Loader(
      "/home/mukund/workarea/teaching/data-projects/raw_data/company-master"
    )

    for
      companies <- loader.loadCompanies()
      topPbas <- loader.loadPBAs()
      _ <- plot1(companies)
      _ <- plot2(companies)
      _ <- plot3(companies)
      _ <- plot4(companies, topPbas)
    yield ()
