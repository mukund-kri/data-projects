/*
 *  Dump the top PBAs in 2010 to a file. This will serve as the input where we need to
 * filter the results based on PBAs.
 */
package in.net.mukund

import zio.*

// ZIO entry point
object TopPBAs extends ZIOAppDefault:

  val loader = Loader(
    "/home/mukund/workarea/teaching/data-projects/raw_data/company-master"
  )
  val analysis = Analysis()

  def run = for
    companies <- loader.loadCompanies()
    topPbas = analysis.problem3(companies, 2010)
    _ <- Utils.dumpDatFile("top_pbas.txt") { fileWriter =>
      topPbas.foreach { case (pba, count) =>
        fileWriter.write(s"$pba\n")
      }
    }
  yield ()
