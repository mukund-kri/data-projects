// Common function used all over the project
package in.net.mukund

import java.io.{FileWriter, File}
import scala.util.Try

import upickle.default.*
import zio.*

object Utils:

  /** My attempt to make a more scala idiomatic way of dumping data into a dat
    * file, that is intended to be used in the gnuplot script.
    *
    * @param fileName
    *   The name of the file to be created
    * @param writeBlock
    *   The block of code that writes to the file
    * @return
    *   Try[Unit] to make it compatible with ZIO
    */
  def dumpDatFile(
      fileName: String
  )(writeBlock: FileWriter => Unit): Task[Unit] = ZIO.attempt {
    val fileWriter = new FileWriter(File(fileName))
    writeBlock(fileWriter)
    fileWriter.close()
  }

  /** Dump json data to a file
    *
    * @param fileName
    *   The name of the file to be created
    * @param data
    *   The data to be written to the file
    * @return
    *   Try[Unit] to make it compatible with ZIO
    */
  def dumpJsonFile[T: ReadWriter](fileName: String, data: T): Task[Unit] =
    ZIO.attempt {
      val fileWriter = new FileWriter(File(fileName))
      fileWriter.write(write(data))
      fileWriter.close()
    }
