package in.net.mukund

type Counts = Map[Int, Map[String, Int]]

class Analysis():

  private def categorize(company: Company): String =
    if company.authorizedCapital <= 1_00_000 then "< 1L"
    else if company.authorizedCapital <= 10_00_000 then "1L - 10L"
    else if company.authorizedCapital <= 1_00_00_000 then "10L - 1Cr"
    else if company.authorizedCapital <= 10_00_00_000 then "1Cr - 10Cr"
    else "> 10Cr"

  /** Problem 1: Plot the total runs scored by each team in the IPL
    */
  def problem1(companies: List[Company]): Map[String, Int] =
    companies
      .map(categorize)
      .groupBy(identity)
      .map { case (category, companies) =>
        (category, companies.size)
      }

  /** Problem 2: Registration by year between a range of years
    *
    * @param startYear
    *   The start year of the range
    * @param endYear
    *   The end year of the range
    */
  def problem2(
      companies: List[Company],
      startYear: Int,
      endYear: Int
  ): Map[Int, Int] =
    companies
      .filter(company =>
        company.registrationYear >= startYear && company.registrationYear <= endYear
      )
      .groupBy(_.registrationYear)
      .map { case (year, companies) =>
        (year, companies.size)
      }

  /** Problem 3: counts of registered companies by principal business activity
    * for selected year
    *
    * @param selectedYear
    *   The year for which the count is to be calculated
    */
  def problem3(
      companies: List[Company],
      selectedYear: Int
  ): List[(String, Int)] =
    companies
      .filter(_.registrationYear == selectedYear)
      .groupBy(_.pba)
      .map { case (pba, companies) =>
        (pba, companies.size)
      }
      .toList
      .sortBy(_._2)(Ordering[Int].reverse)

  /** Problem 4: Registrations by year and principal business activity
    */
  def problem4(companies: List[Company]): Counts =
    companies
      .groupBy(company => (company.registrationYear, company.pba))
      .map { case ((year, pba), counts) =>
        ((year, pba), counts.size)
      }
      .groupBy(_._1._1)
      .map { case (team, wins) =>
        (
          team,
          wins.map { case ((_, season), count) =>
            (season, count)
          }.toMap
        )
      }
