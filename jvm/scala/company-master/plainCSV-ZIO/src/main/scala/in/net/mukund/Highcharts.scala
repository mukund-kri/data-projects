package in.net.mukund

import in.net.mukund.Analysis

import java.io.{FileWriter, File}

import zio.*
import upickle.default.*

case class Problem1(categories: Seq[String], series: Seq[Int])
    derives ReadWriter
case class Problem2(categories: Seq[Int], series: Seq[Int]) derives ReadWriter
case class Problem3(categories: Seq[String], series: Seq[Int])
    derives ReadWriter

case class Series(name: Int, data: Seq[Int]) derives ReadWriter
case class Problem4(categories: Seq[String], series: Seq[Series])
    derives ReadWriter

class Highcharts(
    val plotRoot: String,
    val startYear: Int,
    val endYear: Int,
    val selectedYear: Int
):

  val analysis = Analysis()

  // Plot 1: Registrations by authorized capital
  private def plot1(companies: List[Company]): Task[Unit] =
    val plotData = analysis.problem1(companies)
    val p1 = Problem1(
      plotData.keys.toList,
      plotData.values.toList
    )
    Utils.dumpJsonFile(s"$plotRoot/problem1.json", p1)

    // Plot 2: Registration by year
  private def plot2(companies: List[Company]): Task[Unit] =
    val plotData = analysis.problem2(companies, startYear, endYear)
    val p2 = Problem2(
      plotData.keys.toList,
      plotData.values.toList
    )
    Utils.dumpJsonFile(s"$plotRoot/problem2.json", p2)

    // Plot 3: Plot by registration by pba for the selected year
  private def plot3(companies: List[Company]): Task[Unit] =
    val plotData = analysis.problem3(companies, selectedYear)
    val (categories, series) = plotData.unzip
    val p3 = Problem3(categories, series)
    Utils.dumpJsonFile(s"$plotRoot/problem3.json", p3)

  // Plot 4: Matches won by each year in each pba
  private def plot4(
      companies: List[Company],
      topPBAs: List[String]
  ): Task[Unit] =
    val plotData = analysis.problem4(companies)
    val pbas = topPBAs.take(5)
    val years = startYear to endYear

    // First construct the series
    val series = years.map { year =>
      val data = pbas.map { pba =>
        plotData.getOrElse(year, Map.empty).getOrElse(pba, 0)
      }
      Series(year, data)
    }

    val p4 = Problem4(pbas, series)
    Utils.dumpJsonFile(s"$plotRoot/problem4.json", p4)

    /** Generate all the json files for the plots
      */
  def plotAll: Task[Unit] =
    val loader = Loader(
      "/home/mukund/workarea/teaching/data-projects/raw_data/company-master"
    )

    for
      companies <- loader.loadCompanies()
      topPbas <- loader.loadPBAs()
      _ <- plot1(companies)
      _ <- plot2(companies)
      _ <- plot3(companies)
      _ <- plot4(companies, topPbas)
    yield ()
