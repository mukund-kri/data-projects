import Dependencies._

ThisBuild / scalaVersion := "3.5.2"
ThisBuild / organization := "in.net.mukund"
ThisBuild / version := "0.0.1-SNAPSHOT"

lazy val root = (project in file(".")).settings(
  name := "plainCSV_ZIO",
  libraryDependencies += "com.github.tototoshi" %% "scala-csv" % "2.0.0",
  libraryDependencies += "com.lihaoyi" %% "upickle" % "3.1.0",

  // ZIO
  libraryDependencies += zio,

  // Csv Reading
  libraryDependencies += scalaCsv,

  // Dumping JSON
  libraryDependencies += upickle
)
