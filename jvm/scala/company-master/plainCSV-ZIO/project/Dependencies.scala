import sbt._

object Dependencies {

  val zioVersion = "2.1.13"

  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.2.9"

  // ZIO group of dependencies
  lazy val zio = "dev.zio" %% "zio" % zioVersion

  // For reading CSV files
  lazy val scalaCsv = "com.github.tototoshi" %% "scala-csv" % "2.0.0"

  // For dumping JSON
  lazy val upickle = "com.lihaoyi" %% "upickle" % "3.1.0",
}
