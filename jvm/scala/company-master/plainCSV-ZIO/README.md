# scala / company master / plainCSV :: ZIO Version

Solving the company master data set problems using scala. The data set is in CSV format.

This version uses ZIO and it's methodology for solving the company master data set 
problems.

## Problem Statement

Please refer to the [Company Master DataSet Problem Statement](../../../../xx.problem.statements/PROBLEM-STATEMENT-COMPANY-MASTER.md)


## Before running the code

### Prep plot directory

I have a separate repository with plotting code, which includes gnuplot code and
JavaScript code for highcharts. The repo is [here](https://gitlab.com/mukund-kri/plots)

The plots repo is already added as a submodule in this repo. So at the root of 
the repo, run the following command to get the plots repo.

**Note:** you need to run this only once for all languages / data sets.

```bash
git submodule update --init --recursive
```


### Getting and preparing the data

TODO: Link to the dataset

## Run the code

### Configuration

### Start SBT
Since it is scala we will run everything from the sbt console. So, first start the sbt 
console.

```bash
sbt
```

### Generate `top PBAs`


### Generate gnuplot dat files
Then run the following command to run the code.
```bash
run gnuplots
```

### Viewing the gnuplot plots

### Generate highcharts json files
Then run the following command to run the code.
```bash
run highcharts
```

To generate the highcharts .json files.

### Viewing the highcharts plots
