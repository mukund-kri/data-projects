''' funtions related plotting for our problems '''

import matplotlib.pyplot as plt


def plot_dict(data, title, formatter=None, x_tick_rotation=0):
    '''
    Plot a dict, with keys representing the X axis and the values representing
    the Y axis.
    '''

    x_values = data.keys()
    x_range = range(len(x_values))
    
    _, ax = plt.subplots()
    if formatter:
        ax.yaxis.set_major_formatter(formatter)
    plt.title(title)
    plt.bar(
        x_range,
        data.values(),
    )
    plt.xticks(x_range, x_values, rotation=x_tick_rotation)
    plt.show()


