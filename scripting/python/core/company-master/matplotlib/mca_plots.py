import csv
from collections import defaultdict

import numpy as np
import matplotlib.pyplot as plt

from plotters import plot_dict


def problem1(csv_data):
    ''' Histogram of 'Authorized Capital' '''

    # start of with an empty accumulator
    auth_cap_hist = {
        '<= 1L': 0,
        '1L to 10L': 0,
        '10L to 1Cr': 0,
        '1Cr to 10Cr': 0,
        '> 10Cr': 0,
    }

    # Accumulate values in bins
    for row in csv_data:
        auth_cap = float(row['AUTHORIZED_CAP'])
        if auth_cap <= 100_000:
            auth_cap_hist['<= 1L'] += 1
        elif auth_cap <= 1_000_000:
            auth_cap_hist['1L to 10L'] += 1
        elif auth_cap <= 10_000_000:
            auth_cap_hist['10L to 1Cr'] += 1
        elif auth_cap <= 100_000_000:
            auth_cap_hist['1Cr to 10Cr'] += 1
        else:
            auth_cap_hist['> 10Cr'] += 1

    plot_dict(auth_cap_hist, 'Histogram of Authorized Capital')


def problem2(csv_data):
    ''' Bar plot of company registration by year '''

    # Start of with empty accumulator
    registrations_year = {str(year): 0 for year in range(2000, 2019)}

    for row in csv_data:
        # Extract out the year
        year = row['DATE_OF_REGISTRATION'][-4:]
        if '2000' <= year <= '2018':
            registrations_year[year] += 1

    plot_dict(
        registrations_year,
        'Company registrations between 2000 and 2018'
    )


def problem3(csv_data):
    '''
    Plot the registration of companies by "Principal Business Activitvity"
    for the year 2015
    '''

    # empty accumulator
    pba_counts = defaultdict(int)
    for row in csv_data:
        if row['DATE_OF_REGISTRATION'][-4:] == '2015':
            pba = row['PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN']
            pba_counts[pba] += 1

    sorted_counts = dict(sorted(pba_counts.items(), key=lambda x: x[1])[-10:])
    plot_dict(
        sorted_counts,
        'Company registration by "Principal Buisness Activitvity" in 2015',
        x_tick_rotation=45,
    )

    # Need this data for problem 4
    return list(sorted_counts.keys())[-4:]


def problem4(csv_data, pbas):
    '''
    Plot group bar plot of year of registration and "principal business 
    activity"
    '''

    # empty accumulator, two levels of defaultdict in this case
    accumulator = defaultdict(lambda: defaultdict(int))

    for row in csv_data:
        pba = row['PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN']
        year = row['DATE_OF_REGISTRATION'][-4:]
        if (pba in pbas) and '2000' <= year <= '2018':
            accumulator[pba][year] += 1

    width = 0.2

    xlength = 19
    xticks = np.arange(xlength)
    x = xticks  -  width * 2 

    _, ax = plt.subplots()
    for pba, year in accumulator.items():
        x = x + width
        plt.bar(
            x,
            year.values(),
            width,
            label=pba
        )

    ax.set_title('Company Registration by PRINCIPAL BUSINESS ACTIVITY between 2000 and 2018')
    ax.set_xticks(xticks)
    ax.set_xticklabels(range(2000, 2019))
    ax.legend()
    plt.show()


if __name__ == '__main__':

    with open(
            './data/mca_westbengal_21042018.csv',
            encoding='ISO-8859-1',
    ) as csv_file:
        csv_data = list(csv.DictReader(csv_file))

        problem1(csv_data)
        problem2(csv_data)
        pbas = problem3(csv_data)
        problem4(csv_data, pbas)
