with import <nixpkgs> {};

let
  pythonEnv = python37.withPackages (ps: [
    ps.matplotlib
  ]);

in mkShell {
  buildInputs = [
    pythonEnv
  ];
}
