import csv

import matplotlib.pyplot as plt
import numpy as np

from formatters import (
    billion_formatter,
    million_formatter,
    asean_name_converter
)
from unions import SAARC_COUNTRIES, ASEAN_COUNTRIES
from plotters import plot_dict


def problem1(csv_data):
    '''
    Plot a bar chart of the population of india over the year range
    2000 - 2015
    '''

    india_population = {}

    for row in csv_data:
        if row['Region'] == 'India':
            if '2000' < row['Year'] < '2016':
                india_population[row['Year']] = float(row['Population'])

    plot_dict(
        india_population,
        "Population of India, from 2000 to 2015",
        billion_formatter,
    )


def problem2(csv_data):
    '''
    Bar chart of population of ASEAN countries, for year 2014
    '''

    asean_2014_population = {}

    for row in csv_data:
        country = asean_name_converter(row['Region'])
        year = row['Year']
        population = float(row['Population'])

        if country in ASEAN_COUNTRIES:
            if year == '2014':
                asean_2014_population[country] = population

    plot_dict(
        asean_2014_population,
        'Poulation of ASEAN countries, in 2014',
        million_formatter,
    )


def problem3(csv_data):
    '''
    Total population of SAARC countries, from 2000 to 2015.
    '''

    saarc_population = {}

    for row in csv_data:
        country = row['Region']
        year = row['Year']
        population = float(row['Population'])

        if country in SAARC_COUNTRIES:
            if '2000' < year < '2016':
                saarc_population[year] = saarc_population.get(year, 0) \
                    + population

    plot_dict(
        saarc_population,
        'Poulation of SAARC countries, from 2000 to 2015',
        billion_formatter,
    )


def problem4(csv_data):
    '''
    Group plot of population of ASEAN countries from 2010 to 2015
    '''

    asean_population = {}

    for row in csv_data:
        country = asean_name_converter(row['Region'])
        year = row['Year']
        population = float(row['Population'])

        if country in ASEAN_COUNTRIES:
            if '2010' <= year <= '2015':
                asean_population[country] = asean_population.get(country, {})

                asean_population[country][year] = asean_population[country] \
                    .get(year, 0.0) + population

    width = 0.1
    years = [str(year) for year in range(2010, 2016)]
    xlength = len(years)
    xticks = np.arange(xlength)
    x = xticks - (xlength + 2) * width / 2

    _, ax = plt.subplots()
    for country, year in asean_population.items():
        x = x + width
        plt.bar(
            x,
            year.values(),
            width,
            label=country
        )

    ax.set_ylabel('Population')
    ax.set_title('Population of ASEAN countries from 2010 to 2015')
    ax.set_xticks(xticks)
    ax.set_xticklabels(years)
    ax.legend()
    ax.yaxis.set_major_formatter(million_formatter)
    plt.show()


if __name__ == '__main__':
    with open('data/population-estimates_csv.csv', 'r') as csv_file:
        csv_data = list(csv.DictReader(csv_file))

        problem1(csv_data)
        problem2(csv_data)
        problem3(csv_data)
        problem4(csv_data)
