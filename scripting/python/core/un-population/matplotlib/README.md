In this assignment, we use raw data from the UN to plot population growth in
India, South Asia, and ASEAN countries.

## Data Preparation

Download the raw data file from the URL given below, into a folder called `data`
in the root of the project.

https://datahub.io/core/population-growth-estimates-and-projections/r/population-estimates.csv

## Running the project

1.  Create a new python virtual environment, and activate it.
2.  Install all dependencies into the virtual env with ...
	```bash
	$ pip install -r requirements.txt
	```
3.  Run the python program `un_population_plots.py`
	```sh
	$ python un_population_plots.py
	```
