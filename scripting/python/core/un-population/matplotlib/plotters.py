''' funtions related plotting for our problems '''

import matplotlib.pyplot as plt


def plot_dict(data, title, formatter):
    '''
    Plot a dict, with keys representing the X axis and the values representing
    the Y axis.
    '''
    _, ax = plt.subplots()
    ax.yaxis.set_major_formatter(formatter)
    plt.title(title)
    plt.bar(
        data.keys(),
        data.values(),
    )
    plt.show()


def plot_dict_of_dict(data):

    _, ax = plt.subplots()
