from matplotlib.ticker import FuncFormatter


def billions(x, pos):
    ''' The two args are the value and tick position '''
    return '%1.1f Billion' % (x * 1e-6)


def millions(x, pos):
    return '%1.1f Million' % (x * 1e-3)


billion_formatter = FuncFormatter(billions)
million_formatter = FuncFormatter(millions)


def asean_name_converter(name):
    if name == 'Brunei Darussalam':
        return 'Brunei'
    if name == "Lao People's Democratic Republic":
        return 'Laos'
    if name == 'Viet Nam':
        return 'Vietnam'
    return name
