require 'csv'
require 'gruff'

obj = CSV.parse(
  File.read('deliveries.csv'),
  headers: true
)

runs_acc = Hash.new(0)

obj.each do |row|
  runs = row[17].to_i
  team = row[2]

  runs_acc[team] += runs
end

def abbrivator(full_text)
  return full_text.split(' ')
end

g = Gruff::Bar.new
g.title = 'Runs in IPL'
g.data :Runs, runs_acc.values

lst = runs_acc.each_with_index.map do |counts, i| 
  [i, counts[0]] 
end
pp lst

g.x_axis_label = 'sdfjdkfj'
g.write('problem1.png')
